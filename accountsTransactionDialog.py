import wx
from EnterTextCtrl import *
from EnterDatePicker import *
from Journals import *
from SpecialCombobox import *

class TextObjectValidator(wx.PyValidator):
    """ This validator is used to ensure that the user has entered something
        into the text object editor dialog's text field.
    """
    def __init__(self):
        """ Standard constructor.
        """
        wx.PyValidator.__init__(self)



    def Clone(self):
        """ Standard cloner.

            Note that every validator must implement the Clone() method.
        """
        return TextObjectValidator()


    def Validate(self, win):
        """ Validate the contents of the given text control.
        """
        textCtrl = self.GetWindow()
        text = textCtrl.GetValue()

        if len(text) == 0:
            wx.MessageBox("An Account must be selected!", "Error")
            textCtrl.SetBackgroundColour("pink")
            textCtrl.SetFocus()
            textCtrl.Refresh()
            return False
        else:
            textCtrl.SetBackgroundColour(
                wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW))
            textCtrl.Refresh()
            return True


    def TransferToWindow(self):
        """ Transfer data from validator to window.

            The default implementation returns False, indicating that an error
            occurred.  We simply return True, as we don't do any data transfer.
        """
        return True # Prevent wxDialog from complaining.




class accountsTransactionDialog ( wx.Dialog ):

    def __init__( self, parent,name='Testing Name' ):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 300,200 ), style = wx.DEFAULT_DIALOG_STYLE )

        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        self.journals = Journals()
        self.chart_of_accounts = self.journals.get_chart_of_accounts()
        self.account_selected = None



        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "Accounts Transactions Listing Dialog")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "GL Account:")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        choices_ = [x.description for x in self.chart_of_accounts]



        self.cbx = SpecialCombobox(self,choices_,self.selectCallback,"")
        self.cbx.SetValidator(TextObjectValidator())
        box.Add(self.cbx, 1, wx.ALIGN_CENTRE|wx.ALL, 5)


        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Starting Date :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        description = name
        self.dpc1 = EnterDatePicker(self, size=(100,-1),
                        style = wx.DP_DROPDOWN
                              | wx.DP_SHOWCENTURY)

        box.Add(self.dpc1, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Cut-off Date :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        description = name
        self.dpc2 = EnterDatePicker(self, size=(100,-1),
                        style = wx.DP_DROPDOWN
                              | wx.DP_SHOWCENTURY)

        box.Add(self.dpc2, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        btnsizer = wx.StdDialogButtonSizer()
        self.Okbtn = wx.Button(self, wx.ID_OK)
        self.Okbtn.SetHelpText("The OK button completes the dialog")
        #self.Okbtn.SetDefault()
        btnsizer.AddButton(self.Okbtn)

        self.Okbtn.Bind( wx.EVT_BUTTON, self.CloseWin )


        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")

        btnsizer.AddButton(btn)

        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.CENTER|wx.ALL, 5)

        self.SetSizer( sizer )
        self.Layout()

        self.Centre( wx.BOTH )

    def selectCallback(self,values):
        data = self.journals.get_one_account(values[0])
        self.account_selected  = data.id


    def __del__( self ):
        pass


    def GetName(self):
        return self.descript.GetValue()

    def getInfo(self):
        return {'date1':self.dpc1.GetValue().Format("%Y-%m-%d"),'date2':self.dpc2.GetValue().Format("%Y-%m-%d"),'account_id':self.account_selected}

    def theclose(self,evt):
        pass

    def CloseWin(self,evt):
        if self.Validate():
            evt.Skip()
            self.EndModal(wx.ID_OK)



class MyFrame(wx.Frame):
    #----------------------------------------------------------------------
    def __init__(self):
        dlg = accountsTransactionDialog(None)
        if dlg.ShowModal() == wx.ID_OK:
            date = dlg.getInfo()
            print date['date1']

class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
