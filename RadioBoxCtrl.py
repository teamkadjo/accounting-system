import wx
 #wx.DefaultPosition, wx.DefaultSize
class RadioBoxCtrl(wx.RadioBox) :
    def __init__(self, parent,run_id=-1, title="",position = wx.DefaultPosition,size=wx.DefaultSize,value = None, two=None,specifycol=None,  **par):
        wx.RadioBox.__init__(self, parent, run_id,title,position,size, value,two,specifycol,**par) 
        self.WindowStyle |= wx.WANTS_CHARS | wx.TE_PROCESS_ENTER
        self.Bind(wx.EVT_KEY_DOWN, self.EvtChar)    
        self.Bind(wx.EVT_CHAR, self.EvtChar)   
        self.ignoreEvtText = False
        self.Bind(wx.EVT_RADIOBOX ,self.onCheckBox)
        self.Bind(wx.EVT_SET_FOCUS, self.onFocus)         

    def onFocus(self,evt):
        print "The focuss.."

    def onCheckBox(self,evt):
        print 'check box',evt.IsChecked()
        
    def EvtChar(self, event):
        keycode = event.GetKeyCode()
        if (keycode == wx.WXK_SHIFT):
            self.is_shift_pressed = True
            return
        elif keycode == wx.WXK_TAB:            
            if event.ShiftDown():
                event.EventObject.Navigate(wx.NavigationKeyEvent.IsBackward)
            else:
                event.EventObject.Navigate()
            return
        elif keycode == wx.WXK_RETURN or keycode == wx.WXK_NUMPAD_ENTER:
            self.process_text(event=None)
            event.EventObject.Navigate()
            return
        elif keycode == 8:
            self.ignoreEvtText = True        
        event.Skip()
    
    def ProcessEvent(self,evt):
        print "The event"
        pass 
 
    def process_text(self,event):
        print "processing txt...!!!"
