'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: passWord.py
'''


import wx
import sys
from EnterTextCtrl import *

class passWord ( wx.Dialog ):
    
    def __init__( self, parent,parentOwner ):
        no_close = (wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION  | wx.CLIP_CHILDREN | wx.FRAME_NO_TASKBAR) & ~ (wx.RESIZE_BORDER |   wx.RESIZE_BOX |  wx.MAXIMIZE_BOX) 
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 242,122 ), style = no_close )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        gSizer1 = wx.GridSizer( 3, 2, 0, 0 )
        
        self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        gSizer1.Add( self.m_staticText1, 1, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        self.m_textCtrl1 = EnterTextCtrl( self, wx.ID_ANY, wx.EmptyString ,style=wx.TE_PROCESS_ENTER)
        gSizer1.Add( self.m_textCtrl1, 0, wx.TOP|wx.BOTTOM, 5 )
        
        self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        gSizer1.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        self.m_textCtrl2 = EnterTextCtrl( self, wx.ID_ANY, wx.EmptyString,  style=wx.TE_PASSWORD|wx.TE_PROCESS_ENTER )
        gSizer1.Add( self.m_textCtrl2, 0, wx.TOP, 5 )
        
        self.m_button1 = wx.Button( self, wx.ID_ANY, u"OK", wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_button1, 0, wx.ALIGN_RIGHT|wx.BOTTOM|wx.LEFT, 5 )
        
        self.m_button2 = wx.Button( self, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_button2, 0, wx.BOTTOM|wx.LEFT, 5 )
        
        
        self.m_button2.Bind( wx.EVT_BUTTON, self.CloseWin )
        self.m_button1.Bind( wx.EVT_BUTTON, self.CheckPassword )
        self.m_button1.SetDefault()
        self.parentOwner = parentOwner
        
        
        self.SetSizer( gSizer1 )
        self.Layout()
        
        self.Centre( wx.BOTH )
    
    def CloseWin(self,evt):
        self.Close()
        sys.exit(0)
        
    def CheckPassword(self,evt):
        
        username = self.m_textCtrl1.GetValue()
        password = self.m_textCtrl2.GetValue()
        theResult = False 
        if self.parentOwner != None:
            try:
                theResult = self.parentOwner.CheckPassword(username,password)
            except:
                #print "Unexpected error:", sys.exc_info()
                dlg = wx.MessageDialog(self.parentOwner,"Unexpected error:" + str(sys.exc_info()[1]), "Login error",style=wx.OK|wx.CENTRE|wx.ICON_ERROR)
                #dlg.SetYesNoLabels("&Quit", "&Don't quit")
                dlg.ShowModal()                 
                theResult = None
            
        if theResult:
            self.Close()

        
    
def main():
    app = wx.App()
    frame = passWord(None,None)
    frame.Show(True)
    app.MainLoop()


if __name__ == '__main__':
    main()
