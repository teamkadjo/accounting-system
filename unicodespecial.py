

#_u = lambda t: t.decode('UTF-8', 'replace') if isinstance(t, str) else t
#_uu = lambda *tt: tuple(_u(t) for t in tt) 
## guarantee byte string in UTF8 encoding
#_ux8 = lambda t: t.encode('UTF-8', 'replace') if isinstance(t, unicode) else t
#_uu8 = lambda *tt: tuple(_u8(t) for t in tt)


def _u8(t):
    return t.encode('UTF-8','replace') if isinstance(t,unicode) else t
