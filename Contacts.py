'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: Contacts.py
'''
import sys
from db import *
import sqlalchemy
import traceback

class Contacts:
    def __init__(self):
        self.Contacts = DatabaseMySQL.contacts
        self.db = DatabaseMySQL
        self.counter = 0
        return None

    def insert_account(self,**data):
        try:
            self.UserAccount.insert(**data)
            self.db.commit()
            return 'Ok'
        except:
            return None

    def delete_account(self,data):
         try:
            user_account = self.UserAccount.filter_by(user_id=data).one()
            Database.delete(user_account)
            Database.commit()
            return 'Ok'
         except:
             if sqlalchemy.orm.exc.NoResultFound:
                 return 'No Result Found'
             else:
                 return None

    def update_account(self,id,data):
        try:
            self.UserAccount.filter_by(user_id=id).update(data)
            self.db.commit()
            return 'Ok'
        except:
            return None





    def count_members(self,where=None):
        try:
            if where :
                where_value = " and ab.primary_name like '%%%%" + where + "%%%%' "
            else:
                where_value = " "

            sql = "select count(*) as counter from contacts c left join address_book ab on c.id = ab.ref_id where c.type = 'c' " + where_value.encode('utf-8').decode('utf-8').encode('utf-8')
            #print sql
            rp = self.db.bind.execute(sql)
            value = rp.fetchone()
            return value.counter
        except:
            print(traceback.format_exc())
            return 0

    def get_members(self,_offset=0,_limit=3,where = None):
        try :
            if where:
                where_value = " and ab.primary_name like '%%%%" + where + "%%%%' "
            else:
                where_value = " "
            rp = self.db.bind.execute("select * from contacts c left join address_book ab on c.id = ab.ref_id where c.type='c'  " + where_value.encode('utf-8').decode('utf-8').encode('utf-8') + "  order by ab.primary_name limit " + str(_offset* _limit) + ", " + str(_limit) )
            data = rp.fetchall()
            listing = []
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return 0

    def get_all_members(self):
        #select c.id,ab.primary_name from contacts c left join address_book ab on c.id = ab.ref_id where c.type = 'c'
        try :
            rp = self.db.bind.execute("select c.id,ab.primary_name as name from contacts c left join address_book ab on c.id = ab.ref_id where c.type = 'c' and not (ab.primary_name is null) order by ab.primary_name")
            data = rp.fetchall()
            listing = []
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return None

    def get_member(self,id):
        #select c.id,ab.primary_name from contacts c left join address_book ab on c.id = ab.ref_id where c.type = 'c'
        try :
            rp = self.db.bind.execute("select c.id,ab.primary_name as name from contacts c left join address_book ab on c.id = ab.ref_id where c.id = '{0}' order by ab.primary_name".format(id))
            data = rp.fetchone()
            listing = []
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return None

    def get_suppliers(self,_offset=0,_limit=3):
        try :
            limit = ""
            if _limit > 0:
                limit =  " limit "  + str(_offset) + ", " + str(_limit)

            rp = self.db.bind.execute("select * from contacts c left join address_book ab on c.id = ab.ref_id where c.type='v' order by c.id " + limit)

            data = rp.fetchall()
            listing = []
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return None

    def get_costcenters(self,_offset=0,_limit=10):
        try :
            # sql = "select * from contacts c left join address_book ab on c.id = ab.ref_id where c.type='j' order by c.id limit " + str(_offset) + ", " + str(_limit)
            sql = "select distinct c.id,ab.primary_name,c.short_name, ab.address1,ab.city_town,ab.state_province,c.contact_first,c.contact_middle,c.contact_last from contacts c left join address_book ab on c.id = ab.ref_id where c.type='j' order by c.id limit " + str(_offset) + ", " + str(_limit)
            #print sql
            rp = self.db.bind.execute(sql)

            data = rp.fetchall()
            listing = []
            return data
            # for a in data:
                # listing.append(a.__dict__)
            # return listing
        except:
            print(traceback.format_exc())
            return None

    def find_account(self,data):
        try:
            data = self.UserAccount.filter(self.UserAccount.user_id==data).one()
            return data
        except:
            return None

    def get_list(self):
        datas = self.UserAccount.all()
        listing = []
        for a in datas:
            listing.append(a.__dict__)
        return  listing


    def check_credential(self,uname,passw) :
        try:
            data = self.UserAccount.filter(self.UserAccount.username == uname,self.UserAccount.password==passw).one()
            return data
        except:
            return None

    def increment_count(self):
        self.counter = self.counter + 1

    def getCount(self):
        return self.counter

    def updateMemberInfo(self,data):
        self.db.contacts.filter_by(id=data['id']).update({'contact_first':data['firstname'],'contact_last':data['lastname']})
        self.db.commit()
        self.db.address_book.filter_by(ref_id=data['id']).update({'primary_name':data['lastname'] + ", " + data['firstname'],'address1':data['address1'],'city_town':data['city_town'],'state_province':data['state_province'],'postal_code':data['zipcode']})
        self.db.commit()

    def updateCCInfo(self,data):
        self.db.contacts.filter_by(id=data['id']).update({'short_name':data['short_name']})
        self.db.commit()
        self.db.address_book.filter_by(ref_id=data['id']).update({'primary_name':data['short_name'],'address1':data['address1'],'city_town':data['city_town'],'state_province':data['state_province']})
        self.db.commit()


    def addMemberRecord(self,data):
        new_record = self.db.contacts.insert(type='c',contact_first=data['firstname'],contact_last=data['lastname'])
        self.db.commit()
        inserted_id = new_record.id
        new_addressbook = self.db.address_book.insert(ref_id=inserted_id,primary_name=data['lastname'] + ", " + data['firstname'],address1=data['address1'],city_town=data['city_town'],state_province=data['state_province'],postal_code=data['zipcode'])
        self.db.commit()
        return inserted_id

    def addCCRecord(self,data):
        new_record = self.db.contacts.insert(type='j',short_name=data['short_name'])
        self.db.commit()
        inserted_id = new_record.id
        new_addressbook = self.db.address_book.insert(ref_id=inserted_id,primary_name=data['short_name'],address1=data['address1'],city_town=data['city_town'],state_province=data['state_province'])
        self.db.commit()
        print "Inserted id is {}".format(inserted_id)
        return inserted_id

    def deleteMemberRecord(self,member_id):
        member = self.db.contacts.filter_by(id=member_id).one()
        if member:
            self.db.delete(member)
            self.db.commit()

    def deleteCCRecord(self,member_id):
        member = self.db.contacts.filter_by(id=member_id).one()
        if member:
            self.db.delete(member)
            self.db.commit()
            ab = self.db.address_book.filter_by(ref_id = member_id).one()
            self.db.delete(ab)
            self.db.commit()




if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyReportingMain.py file ***************"

