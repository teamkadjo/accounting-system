'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: EditTransaction.py
'''

import wx
import PromptingComboBox
from Journals import * 
from Contacts import *
from EnterTextCtrl import *
from RadioBoxCtrl import * 
from pprint import pprint

class EntryWindow(object):
    #----------------------------------------------------------------------
    def __init__(self, gl_account, sl_id, cost_center, description, debit,credit,rec_id):
        self.gl_account = gl_account
        self.sl_id = sl_id
        self.cost_center = cost_center
        self.description = description
        self.debit = debit
        self.credit = credit
        self.id = rec_id
    
    def SetCostCenter(self, value):
        if value is None or value == "":
            self.cost_center = None
        else:
            self.cost_center = value
    
    def SetGLAccount(self,value):
        if value is None or value == "":
            self.gl_account = None
        else:
            self.gl_account = value   


class EditTransactionDialog(wx.Dialog):
    def __init__(
            self, parent, ID, title,transact_data=None, size=wx.DefaultSize, pos=wx.DefaultPosition, 
            style=wx.DEFAULT_DIALOG_STYLE,
            ):

        self.journals = Journals()
        transaction_data = None
        self.transactionData = None

        s_ledger = ""
        c_center = "ADMIN"
        debit_string = ""
        credit_string = ""
        description = ""
        self.current_id = None
         
        
        if (transact_data ):
            if transact_data.sl_description:
                s_ledger = transact_data.sl_description
            c_center = transact_data.cost_center.upper()
            transaction_data = self.journals.get_je_transaction(transact_data.id)
            
            if (transact_data.debit):
                debit_string = str(transact_data.debit).replace(",","")
            else:
                debit_string = ""
            
            if transact_data.credit :
                credit_string = str(transact_data.credit).replace(",","")
            else:
                credit_string = ""
                
            description = transact_data.description
            descript = transact_data.gl_account
            self.current_id = transact_data.id
        else:
            s_ledger = ""
            c_center = "ADMIN"
            transaction_data = None
            debit_string = "0.00"
            credit_scring = "0.00"
            description = ""
            descript = ""

                
        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        self.PostCreate(pre)
        self.SetSize((300, 300))

        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "Transaction Editor")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "GL Account:")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        #text = wx.TextCtrl(self, -1, "", size=(80,-1))
        #text.SetHelpText("Here's some help text for field #1")
        self.chart_of_accounts = self.journals.db.chart_of_accounts.all()
        choices = [(str(x.id) + " - " + x.description) for x in self.chart_of_accounts]


            
        self.cbx = PromptingComboBox.PromptingComboBox(self,descript,choices)
        self.cbx.Bind(wx.EVT_COMBOBOX,self.ChangeAccountHandler)
        self.cbx.Bind(wx.EVT_TEXT_ENTER,self.ChangeAccountHandler)
        self.cbx.Bind(wx.EVT_TEXT,self.ChangeAccountHandler)


        box.Add(self.cbx, 1, wx.ALIGN_CENTRE|wx.ALL, 5)

        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Ledger Type :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        choices = ["Member","Supplier"]
        #choices = {{u'value':u'1',u'name':u"Member"},{u"value":u"2",u"name":u"Supplier"}}
        self.subledger_type = PromptingComboBox.PromptingComboBox(self,u"Member",choices,style= wx.TE_PROCESS_ENTER | wx.CB_DROPDOWN | wx.WANTS_CHARS)
        box.Add(self.subledger_type, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        self.subledger_type.Bind(wx.EVT_COMBOBOX,self.ChangeLedgerSelection)
        self.subledger_type.Bind(wx.EVT_TEXT_ENTER,self.ChangeLedgerSelection)

        
        #self.subledger_type.SetBackgroundColour((255,23,23))


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "SubLedger :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        self._contacts = Contacts()
        self.contacts = self._contacts.get_all_members()
        self.memberchoices = [( x.name) for x in self.contacts]
        
        the_suppliers = self._contacts.get_suppliers(0,0)
        
        self.supplierchoices = [( x.primary_name) for x in the_suppliers]

        self.member = PromptingComboBox.PromptingComboBox(self,s_ledger.encode('utf-8'),self.memberchoices)
        box.Add(self.member, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Cost Center :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        self.cost_center_choices = self.journals.db.contacts.filter(self.journals.db.contacts.type == 'j').all()

        choices = [( x.short_name) for x in self.cost_center_choices ]

        self.costcenter = PromptingComboBox.PromptingComboBox(self,c_center,choices,style=wx.CB_READONLY |  wx.WANTS_CHARS)
        box.Add(self.costcenter, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Description :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        self.descript = EnterTextCtrl(self, -1, description, size=(100,50),style=wx.TE_MULTILINE)
        box.Add(self.descript, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        

        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "Debit :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        self.debit = EnterTextCtrl(self, -1, debit_string, size=(100,-1),style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
        box.Add(self.debit, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)

        label1 = wx.StaticText(self, -1, "Credit :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label1, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)
        

            
        self.credit = EnterTextCtrl(self, -1, credit_string , size=(100,-1),style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
        box.Add(self.credit, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        
        
        
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        btnsizer = wx.StdDialogButtonSizer()
        
        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.AddButton(btn)
        
        btn = wx.Button(self, wx.ID_OK)
        btn.SetHelpText("The OK button completes the dialog")
        #btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")
        btnsizer.AddButton(btn)
        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.CENTER|wx.ALL, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        
        self.CheckAccounts()

        self.Center()

    def ToggleSubLedger(self,value):
        self.subledger_type.Enable(value)
        self.member.Enable(value)

    def CheckAccounts(self):
        split_acct =  self.cbx.GetValue().split(" - ")
        account =  split_acct[0]
        #print account
        if account in ['11703','11704','11705','21001','21002','30101','21007']:
            self.ToggleSubLedger(True)
        else:
            self.ToggleSubLedger(False)


    def ChangeAccountHandler(self, event):
        self.CheckAccounts()


    def ChangeLedgerSelection(self,event):
        ledgertype =  self.subledger_type.GetValue()
        if ledgertype == "Member":
            choices = self.memberchoices
        else:
            choices = self.supplierchoices
        self.member.Clear()
        self.member.AppendItems(choices)
        #print "Changing Ledger"
        #pass
        
        
        
    def EvtChar(self, event):
        '''
        handler for the radio button pressing of enter/tab key
        '''
        keycode = event.GetKeyCode()
        if (keycode == wx.WXK_SHIFT):
            self.is_shift_pressed = True
            return
        elif keycode == wx.WXK_TAB:            
            if event.ShiftDown():
                event.EventObject.Navigate(wx.NavigationKeyEvent.IsBackward)
            else:
                event.EventObject.Navigate()
            return
        elif keycode == wx.WXK_RETURN or keycode == wx.WXK_NUMPAD_ENTER:
            #self.process_text(event=None)
            event.EventObject.Navigate()
            event.Skip()
            return
        elif keycode == 8:
            self.ignoreEvtText = True        
        event.Skip()
    

    def getChanges(self):
        value = u'%s' % self.member.GetValue()
        #print type(value)
        account = next((x for x in self.chart_of_accounts if (x.id.strip() + " - " + x.description.strip())== self.cbx.GetValue()), None)
        #subledger = next((x for x in self.contacts if (  x.name.encode('utf-8') == value.encode('utf-8'))), None)
        if (self.subledger_type.Enabled) and (value != ""):
            subledger = self._contacts.db.address_book.filter(self._contacts .db.address_book.primary_name == value.encode('utf-8')).one()
        else:
            subledger = None
        cost_center = self._contacts.db.address_book.filter(self._contacts .db.address_book.primary_name == self.costcenter.GetValue().upper()).one()
        #subledger = next((x for x in self.contacts if (  x.name.encode('utf-8') == value.encode('utf-8'))), None)

        
        if account:            
            #print account.id + " - " + account.description
            glID = account.id
            account_description = account.description
        else:
            glID = None
            account_description = None
        if subledger :
            subledger_id = subledger.ref_id
            subledger_text = subledger.primary_name
        else:
            subledger_id = None
            subledger_text = None
            
        if cost_center:
            costcenter_id = cost_center.ref_id
            costcenter_name = cost_center.primary_name 
        else:
            costcenter_id = None
            costcenter_name = None
        
        description =  self.descript.GetValue()
        
        debit = self.debit.GetValue()
        credit = self.credit.GetValue()
        
        
        
        self.transactionData = {'account_id':glID, 'account_description':account_description,'subledger_id':subledger_id,'subledger_description':subledger_text, 'costcenter_id':costcenter_id, 'costcenter_name':costcenter_name,'description':description, 'debit':debit,'credit':credit,'refid':self.current_id}
        return self.transactionData        
        
class MyFrame(wx.Frame):
    #----------------------------------------------------------------------
    def __init__(self):
        #wx.Frame.__init__(self, parent=None, id=wx.ID_ANY, 
                          #title="ObjectListView Demo", size=(800,600))

        #trans_id = 14
        
        journals = Journals()
        transaction_data = journals.get_je_transaction(14)
        dlg = EditTransactionDialog(None, -1, "Edit Transaction Dialog (Yey Test)" ,transaction_data, size=(300, 300),
                         style=wx.DEFAULT_DIALOG_STYLE)
        #dlg.ShowWindowModal()       
        if dlg.ShowModal() == wx.ID_OK:
            result = dlg.getChanges()
            pprint (result)

class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        #frame.Show(True)
        #self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()



