'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: ChartOfAccounts.py
'''

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Journals import *
from Pagination import *
from PromptingComboBox import *
from specialolv import *

class ChartOfAccounts(object):
    #----------------------------------------------------------------------
    def __init__(self, id,name):
        self.id = id
        self.name = name


class chartofAccountsPanel(wx.Panel):
    def __init__(self,parent_window):
        self.parent_window = parent_window
        self.book = parent_window.book



        super(chartofAccountsPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("ChartOfAccounts")
        self.chart_of_accounts = Journals()


        self.accounts = []
        for u in self.chart_of_accounts.get_chart_of_accounts_by_page(0,20):
            self.accounts.append(ChartOfAccounts(u.id,u.description))

        self.Paging  = Pagination(1,20,self.chart_of_accounts.count_accounts())


        self.dataOlv = SpecialOLV(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setChartOfAccounts()

        self.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE

        btnbox = wx.BoxSizer(wx.HORIZONTAL)

        id_new_Account = wx.NewId()
        btn_new_Account = wx.Button(self, label = "New Accounts (F2)",name="new_Account")
        btnbox.Add(btn_new_Account, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON,self.addAccount,btn_new_Account)

        id_EditMember = wx.NewId()
        btn_EditMember = wx.Button(self, label = "Edit Accounts (F3)",name="editMember")
        btnbox.Add(btn_EditMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON,self.editAccount,btn_EditMember)


        id_delMember = wx.NewId()
        btn_delMember = wx.Button(self, label = "Delete Accounts (Del)",name="delMember")
        btnbox.Add(btn_delMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.OnDeleteRows, btn_delMember)

        id_Search = wx.NewId()
        btn_Search = wx.Button(self, label = "Search Accounts (F5)",name="searchMember")
        btnbox.Add(btn_Search, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.OnDoSearch, btn_Search)





        btMap = wx.Bitmap("image/media-skip-backward.png", wx.BITMAP_TYPE_ANY)
        mask = wx.Mask(btMap, wx.BLUE)
        btMap.SetMask(mask)
        btnTop = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnTop.Bind(wx.EVT_BUTTON,self.TopPage)

        btMap = wx.Bitmap("image/media-seek-backward.png", wx.BITMAP_TYPE_ANY)
        btnPrev = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnPrev.Bind(wx.EVT_BUTTON,self.PrevPage)

        btMap = wx.Bitmap("image/media-seek-forward.png", wx.BITMAP_TYPE_ANY)
        btnNext = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnNext.Bind(wx.EVT_BUTTON,self.NextPage)


        btMap = wx.Bitmap("image/media-skip-forward.png", wx.BITMAP_TYPE_ANY)
        btnBottom = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnBottom.Bind(wx.EVT_BUTTON,self.BottomPage)
        # Create some sizers
        sbSizerTop = wx.BoxSizer(wx.HORIZONTAL)

        choices = []
        for i in xrange(1,self.Paging.pages+1):
            choices.append(str(i))

        self.pages = PromptingComboBox(self,"1",choices,style= wx.WANTS_CHARS)


        sbSizer1 = wx.BoxSizer(wx.HORIZONTAL)
        sbSizer1.Add(btnTop, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(btnPrev, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(self.pages,0,wx.ALL|wx.CENTER,5)
        sbSizer1.Add(btnNext, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(btnBottom, 0, wx.ALL|wx.CENTER, 5)


        self.search = wx.SearchCtrl(self, size=(200,-1), style=wx.TE_PROCESS_ENTER)

        sizerTop = wx.BoxSizer(wx.HORIZONTAL)
        sizerTop.AddSpacer( ( 900, 0), 1, wx.EXPAND|wx.ALIGN_RIGHT, 5 )
        sizerTop.Add(self.search, 3,  wx.ALL|wx.ALIGN_RIGHT, 5)



        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(sizerTop, 0, wx.TOP, 5)
        mainSizer.Add(self.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(sbSizer1, 0, wx.TOP|wx.ALIGN_RIGHT|wx.BOTTOM, 5)
        mainSizer.Add(btnbox, 0, wx.ALL|wx.CENTER, 5)
        self.SetSizer(mainSizer)

        self.dataOlv.Bind(wx.EVT_LEFT_DCLICK, self.onOLVItemSelected)


        id_F2 = wx.NewId()
        randomId = wx.NewId()
        self.pages.Bind(wx.EVT_COMBOBOX,self.ChangePage)
        self.pages.Bind(wx.EVT_TEXT_ENTER,self.ChangePage)
        self.Bind(wx.EVT_MENU, self.addAccount, id=id_F2)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnDoSearch, self.search)



        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('N'), randomId ),(wx.ACCEL_NORMAL, wx.WXK_F2, id_F2 )])
        self.SetAcceleratorTable(accel_tbl)



    def refreshGrid(self,updateValue=True):
        #print self.Paging.current_page
        if updateValue:
            self.pages.SetValue(str(self.Paging.current_page))
        if self.search.GetValue():
            search_value = self.search.GetValue()
        else:
            search_value = ""

        self.accounts = []
        for u in self.chart_of_accounts.get_chart_of_accounts_by_page(self.Paging.current_page-1,20,search_value):
            self.accounts.append(ChartOfAccounts(u.id,u.description))
        self.setChartOfAccounts()


    def OnDoSearch(self,data):
        print "doing some searching...." + self.search.GetValue()
        self.Paging.set_total_count(self.chart_of_accounts.count_accounts(self.search.GetValue()))

        self.pages.Clear()
        for i in xrange(1,self.Paging.pages+1):
            self.pages.Append(str(i))
        self.refreshGrid()
        self.dataOlv.SetFocus()

    def ChangePage(self,evt):
        self.Paging.page = int(self.pages.GetValue())
        self.refreshGrid(True)


    def PrevPage(self,evt):
        if self.Paging.has_prev:
            self.Paging.prev()
            self.refreshGrid()

    def TopPage(self,evt):
        if not self.Paging.is_ontop:
            self.Paging.top()
            self.refreshGrid()

    def BottomPage(self,evt):
        if not self.Paging.is_onbottom:
            self.Paging.bottom()
            self.refreshGrid()

    def NextPage(self,evt):
        if self.Paging.has_next:
            self.Paging.next()
            self.refreshGrid()


    def onOLVItemSelected(self,evt):
        the_obj = self.dataOlv.GetSelectedObjects()
        print (the_obj)
        if the_obj:
            name = the_obj[0].name
            id = the_obj[0].id
            self.parent_window.OpenAccountInfo(id,name)


    def setChartOfAccounts(self, data=None):
        self.dataOlv.SetColumns([
            ColumnDefn("ID", "left", 100, "id"),
            ColumnDefn("Name", "left", 400, "name"),
            ColumnDefn("Account Type(required)", "left", 400, "name1"),
            ColumnDefn("SubAccount", "left", 400, "name2"),
        ])

        self.dataOlv.SetObjects(self.accounts)

    def OnNewView(self,event):
        pass

    def addAccount(self,event):
        self.parent_window.OpenAccountInfo(None,"")


    def editAccount(self,event):
        the_obj = self.dataOlv.GetSelectedObjects()
        if the_obj:
            name = the_obj[0].name
            id = the_obj[0].id
            self.parent_window.OpenAccountInfo(id,name)

    def OnAddRow(self,event):
        print "Add Row"
        pass

    def OnDeleteRows(self,event):

        the_obj = self.dataOlv.GetSelectedObjects()
        if the_obj:
            message = "Are you sure you want to delete %s record? " % the_obj[0].name
            caption = "Delete CC Record Window"
            dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
            if dlg.ShowModal() == wx.ID_YES:
                message = "Are you really really sure you want to delete %s record? " % the_obj[0].name
                caption = "Delete CC Record Window"
                dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
                if dlg.ShowModal() == wx.ID_YES:
                    user_account = self.chart_of_accounts.db.chart_of_accounts.filter_by(id=the_obj[0].id).one()
                    self.chart_of_accounts.db.delete(user_account)
                    self.chart_of_accounts.db.commit()
                    self.refreshGrid()
                    wx.MessageBox("Finished deleting the row...", "Error")




    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()

    def return_panel(self):
        return self

def CreateChartOfAccountsWindow(the_parent_window):
   panel = chartofAccountsPanel(the_parent_window)
   return panel

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
