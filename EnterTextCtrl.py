import wx
 
class EnterTextCtrl(wx.TextCtrl) :
    def __init__(self, parent,run_id=-1, value=None,   pos_=wx.DefaultPosition,size_=wx.DefaultSize,**par):
        wx.TextCtrl.__init__(self, parent, run_id, value,pos=pos_,size=size_,**par) 
        #self.Bind(wx.EVT_KEY_DOWN, self.EvtChar)       
        self.Bind(wx.EVT_CHAR_HOOK, self.EvtChar)       
        self.ignoreEvtText = False

        
    def EvtChar(self, event):
        keycode = event.GetKeyCode()
        if (keycode == wx.WXK_SHIFT):
            self.is_shift_pressed = True
            return
        elif keycode == wx.WXK_TAB:            
            if event.ShiftDown():
                event.EventObject.Navigate(wx.NavigationKeyEvent.IsBackward)
            else:
                event.EventObject.Navigate()
            event.StopPropagation()    
            return
        elif keycode == wx.WXK_RETURN or keycode == wx.WXK_NUMPAD_ENTER:
            self.process_text(event=None)
            event.EventObject.Navigate()
            return   
        elif keycode == 8:
            self.ignoreEvtText = True 
    
        event.Skip()
        
 
    def process_text(self,event):
        pass
