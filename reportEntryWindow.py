
import  wx
have_package = True
try:
    import fitz
except ImportError:    
    try:
        import PyPDF2
    except ImportError:
        try:
            import pyPdf
        except ImportError:
            have_package = False

if have_package:            
    from wx.lib.pdfviewer import pdfViewer, pdfButtonPanel

#----------------------------------------------------------------------

class reportEntryWindow:
    def __init__(self, parent, pdf_name='spreadsheet.pdf'):
        self.parent_window = parent             
        self.book = parent.book
        self.the_panel =  wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.the_panel.SetLabel("ReportEntryWindow")        

       
        hsizer = wx.BoxSizer( wx.HORIZONTAL )
        vsizer = wx.BoxSizer( wx.VERTICAL )
        self.the_panel.buttonpanel = pdfButtonPanel(self.the_panel, wx.NewId(),
                                wx.DefaultPosition, wx.DefaultSize, 0)  
        vsizer.Add(self.the_panel.buttonpanel, 0,
                                wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.LEFT|wx.RIGHT|wx.TOP, 5)
        self.the_panel.viewer = pdfViewer( self.the_panel, wx.NewId(), wx.DefaultPosition,
                                wx.DefaultSize, wx.HSCROLL|wx.VSCROLL|wx.SUNKEN_BORDER)
        vsizer.Add(self.the_panel.viewer, 1, wx.GROW|wx.LEFT|wx.RIGHT|wx.BOTTOM, 5)

        hsizer.Add(vsizer, 1, wx.GROW|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5) 
        self.the_panel.SetSizer(hsizer)
        self.the_panel.SetAutoLayout(True)

        id_CtrlQ = wx.NewId()
        printId = wx.NewId()
        # introduce buttonpanel and viewer to each other
        self.the_panel.buttonpanel.viewer = self.the_panel.viewer
        self.the_panel.viewer.buttonpanel = self.the_panel.buttonpanel

        self.the_panel.Bind(wx.EVT_MENU,self.CloseReportWindow,id=id_CtrlQ)
        self.the_panel.Bind(wx.EVT_MENU,self.printNow,id=printId)
        self.the_panel.Bind(wx.EVT_CHAR_HOOK, self.OnKeyUP) # closes this page when escape key is press
  
        wx.BeginBusyCursor()
        self.the_panel.viewer.LoadFile(pdf_name)
        wx.EndBusyCursor()



        
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('p'), printId ),(wx.ACCEL_CTRL,  wx.WXK_F4, id_CtrlQ )])
        self.the_panel.SetAcceleratorTable(accel_tbl) 
        self.the_panel.SetFocus()

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("ReportEntryWindow",True)
            
    def OnKeyUP(self, event):
        keyCode = event.GetKeyCode()
        if keyCode == wx.WXK_ESCAPE:
            self.CloseThisPanel()
            return
        event.Skip()
        
    def CloseReportWindow(self,evt):
        self.CloseThisPanel()  

    def printNow(self,evt):
        self.the_panel.viewer.Print() 
        
    def OnLoadButton(self, event):
        pass

    def return_panel(self):        
        return self.the_panel

def CreateReportWindow(the_parent_window,pdf_name):
   panel = reportEntryWindow(the_parent_window,pdf_name)
   return panel.return_panel()
   
