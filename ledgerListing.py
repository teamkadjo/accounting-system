from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import *

from datetime import datetime
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
from numberword import *
import re
from Users import *
from Contacts import *

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None,date1='',date2='',member_id = '',account_id = ''):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 35
        date_object1 = datetime.strptime(date1, '%Y-%m-%d')
        date_object2 = datetime.strptime(date2, '%Y-%m-%d')
        self.date1 = date_object1
        self.date2 = date_object2
        self.member_id = member_id
        self.account_id = account_id
            
    def _draw_header(self):
        config = Configuration()
        contacts = Contacts()
        info_member = contacts.get_member(self.member_id)
        
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,770,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        
        self.canvas.setFont('Helvetica', 10)        
        self.canvas.drawString(230,760,complete_address) 
        self.canvas.setFont('Helvetica', 8)
        self.canvas.drawString(236,750,"From : {0:%B %d, %Y} to : {1:%B %d, %Y}  ".format(self.date1,self.date2))  
        
        #self.canvas.setFont('Helvetica-Bold', 10)
        #self.canvas.drawString(250,720,"ACCOUNTS RECEIVABLE LEDGER")
        
        j = Journals()
        
        account_info = j.get_one_account_by_id(self.account_id)
        self.canvas.setFont('Helvetica-Bold', 9)
        self.canvas.drawString(270,710,account_info.description.upper())
        
        
        
        
        self.canvas.setFont('Helvetica', 8)
        self.canvas.drawString(44,690,"ACCOUNT NAME : " + info_member.name)
  

    def setExplanation(self,explanation=None):
        self.explanation = explanation
    def getExplanation(self):
        return self.explanation               

class SpecialTable(Table):   
    
    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
        
    def setSLName(self,sl_name):
        self.sl_name = sl_name
    
    def setCheckValue(self,value):
        self.check_value = value
    
    def setBankName(self,value):
        self.bank_name = value
    
    def setCheckNumber(self,value):
        self.check_number = value
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)

        config = Configuration()        
        
        #preparedByText =  config.getConfig("PREPARED_BY")
        preparedByText =  "_______________________________________"

        #approvedByText =  config.getConfig("APPROVED_BY")        
        approvedByText =  '_______________________________________'        
        
        box_displacement = 90 
    
        canvas.drawString(44,yoff-(box_displacement - 20),"Prepared By:")
        canvas.drawString(305,yoff-(box_displacement - 20),"Noted By:")


        canvas.setFont('Helvetica-Bold', 8)
        canvas.drawString(44,yoff-(box_displacement - 2),preparedByText)
        canvas.drawString(322,yoff-(box_displacement - 2),approvedByText)
        canvas.setFont('Helvetica', 8)      

def ledgerListingReport(pdf_name='ledgerlisting.pdf',title="LedgerListing",username='admin',date1='', date2='',member_acctid='',gl_accountid=''):
    table = SpecialTable()

    u = Users()
    uinfo = u.get_userinfo(username)
            

    j = Journals()
    col = table.add_column('Date',60)
    col.row.format = format_dmy
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.horizontal_alignment = alignment.CENTER

    col = table.add_column('Reference',60)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.horizontal_alignment = alignment.CENTER

    col = table.add_column('Particulars',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    


    journals_listing = Journals()
    
    explanation = ""
   

    col = table.add_column('debit',50)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('credit',50)  
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')


    col = table.add_column('Balance',50)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    sl_name = ""
    check_value = 0
    table.setCheckValue(0)
    running_balance = 0
    
    
    r = j.get_beginning_balance_member(date1,member_acctid,gl_accountid)
    
    if r[0].id:
        for x in r:
            
            date_c = datetime.strptime(x.post_date, '%Y-%m-%d')
            if (x.gl_account != '30101' and x.gl_account != '21007'):
                running_balance = running_balance + (x.debit_amount - x.credit_amount)
            else:
                running_balance = running_balance + (x.credit_amount - x.debit_amount)
            table.add_row( [date_c,x.purchase_invoice_id, x.description,x.debit_amount,x.credit_amount,running_balance ])
    
    for x in j.get_ledger_listing_member(date1, date2,member_acctid,gl_accountid):
        if (x.gl_account != '30101' and x.gl_account != '21007'):
            running_balance = running_balance + (x.debit_amount - x.credit_amount)
        else:
            running_balance = running_balance + (x.credit_amount - x.debit_amount)
        table.add_row( [x.post_date,x.purchase_invoice_id, x.description,x.debit_amount,x.credit_amount,running_balance ])



    table.sum_column('credit')
    table.sum_column('debit')
    col = table.get_footer_field('debit')    
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True

    col = table.get_footer_field('credit')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True

    col = table.get_footer_field('Reference')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9
    col.value = ""

    col = table.get_footer_field('Particulars')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.value=""

    col = table.get_footer_field('Balance')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.value=""

    col = table.get_footer_field('Date')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9
    col.value = "T O T A L "


    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name,date1,date2,member_acctid,gl_accountid)
    report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {}'.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    ledgerListingReport('ledgerlisting.pdf','Ledger Listing','admin','2015-10-3','2016-10-16',170,'11705')
    os.system('start ledgerlisting.pdf')
