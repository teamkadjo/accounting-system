import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from  PromptingComboBox import *
from Pagination import *
from specialolv import *
from Journals import * 
from wxSpecialPanel import *
from onefourth import *


class Member(object):
    #----------------------------------------------------------------------
    def __init__(self, fullname, street, city, province,member_id):
        self.fullname = fullname
        self.street = street
        self.city = city
        self.province = province
        self.id = member_id
        
class membersPanel(wxSpecialPanel):
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book
        self.journals = Journals() 
    
        super(membersPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        
        self.SetLabel("Members")
        self.contact_members = Contacts()
        
        self.Paging  = Pagination(1,50,self.contact_members.count_members())
 
        self.members = []

        self.dataOlv = SpecialOLV(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.dataOlv.SetEmptyListMsg("There is no member information found.")
        self.dataOlv.SetEmptyListMsgFont(wx.FFont(24, wx.DEFAULT, face="Tekton"))      
 
        self.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE
        self.dataOlv.setEditCallback(self.editMemberInfoNow)

        
        btMap = wx.Bitmap("image/media-skip-backward.png", wx.BITMAP_TYPE_ANY)
        mask = wx.Mask(btMap, wx.BLUE)
        btMap.SetMask(mask)
        btnTop = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnTop.Bind(wx.EVT_BUTTON,self.TopPage)

        btMap = wx.Bitmap("image/media-seek-backward.png", wx.BITMAP_TYPE_ANY)
        btnPrev = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnPrev.Bind(wx.EVT_BUTTON,self.PrevPage)
        
        btMap = wx.Bitmap("image/media-seek-forward.png", wx.BITMAP_TYPE_ANY)
        btnNext = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnNext.Bind(wx.EVT_BUTTON,self.NextPage)

        
        btMap = wx.Bitmap("image/media-skip-forward.png", wx.BITMAP_TYPE_ANY)
        btnBottom = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnBottom.Bind(wx.EVT_BUTTON,self.BottomPage)
        # Create some sizers
        sbSizerTop = wx.BoxSizer(wx.HORIZONTAL)  
        
        choices = []
        for i in xrange(1,self.Paging.pages+1):
            choices.append(str(i))

        self.pages = PromptingComboBox(self,"1",choices,style= wx.WANTS_CHARS)

        self.search = wx.SearchCtrl(self, size=(200,-1), style=wx.TE_PROCESS_ENTER)

        sizerTop = wx.BoxSizer(wx.HORIZONTAL)
        sizerTop.AddSpacer( ( 900, 0), 1, wx.EXPAND|wx.ALIGN_RIGHT, 5 )
        sizerTop.Add(self.search, 3,  wx.ALL|wx.ALIGN_RIGHT, 5)        
           
        sbSizer1 = wx.BoxSizer(wx.HORIZONTAL)     
        mainSizer = wx.BoxSizer(wx.VERTICAL)  
        mainSizer.Add(sizerTop, 0, wx.TOP, 5)
        mainSizer.Add(self.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        sbSizer1.Add(btnTop, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(btnPrev, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(self.pages,0,wx.ALL|wx.CENTER,5)
        sbSizer1.Add(btnNext, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(btnBottom, 0, wx.ALL|wx.CENTER, 5)
       

        btnbox = wx.BoxSizer(wx.HORIZONTAL)

        id_NewMember = wx.NewId()
        btn_newMember = wx.Button(self, label = "New Contact (F2)",name="newMember")
        btnbox.Add(btn_newMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON,self.addMemberInfo,btn_newMember)

        id_EditMember = wx.NewId()
        btn_EditMember = wx.Button(self, label = "Edit Contact (F3)",name="editMember")
        btnbox.Add(btn_EditMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON,self.editMemberInfo,btn_EditMember)


        id_delMember = wx.NewId()
        btn_delMember = wx.Button(self, label = "Delete Contact (Del)",name="delMember")
        btnbox.Add(btn_delMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.deleteMember, btn_delMember)
                
        id_Search = wx.NewId()
        btn_Search = wx.Button(self, label = "Search Contact (F5)",name="searchMember")
        btnbox.Add(btn_Search, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.gotoSearch, btn_Search)

        id_viewSL = wx.NewId()
        btn_viewSL = wx.Button(self, label = "View SL (F4)",name="viewSL")
        btnbox.Add(btn_viewSL, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.viewSL, btn_viewSL)

        id_AccInfo = wx.NewId()
        btn_viewAccInfo = wx.Button(self, label = "View Acct. Info (F6)",name="viewInfo")
        btnbox.Add(btn_viewAccInfo, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.viewAccInfo, btn_viewAccInfo)
        
        
        mainSizer.Add(sbSizer1, 0, wx.TOP|wx.ALIGN_RIGHT|wx.BOTTOM, 5)
        mainSizer.Add(btnbox, 0, wx.TOP|wx.CENTER|wx.BOTTOM, 5)
                
        self.search.SetFocus()
        self.search.ShowCancelButton(1)

        id_CtrlQ = wx.NewId()

        ################### search and paging event #######################
        self.Bind(wx.EVT_TEXT_ENTER, self.OnDoSearch, self.search)
        self.pages.Bind(wx.EVT_COMBOBOX,self.ChangePage)
        self.pages.Bind(wx.EVT_TEXT_ENTER,self.ChangePage)
        self.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.OnCancel, self.search)        
        self.Bind(wx.EVT_MENU,self.CloseMemberWindow,id=id_CtrlQ)   
        self.Bind(wx.EVT_MENU,self.editMemberInfo,id=id_EditMember)             
        self.Bind(wx.EVT_MENU,self.addMemberInfo,id=id_NewMember)             
        self.Bind(wx.EVT_MENU,self.gotoSearch,id=id_Search)             
        self.Bind(wx.EVT_MENU,self.viewAccInfo,id=id_AccInfo)             
        ####################################################################
        self.setEscapeHandler(self.CloseThisPanel)        
        
        self.SetSizer(mainSizer)
        self.refreshGrid()

        self.Bind(wx.EVT_MENU,self.viewSL,id=id_viewSL)

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_NORMAL, wx.WXK_F2, id_NewMember ),(wx.ACCEL_NORMAL, wx.WXK_F5, id_Search ),(wx.ACCEL_NORMAL, wx.WXK_F4, id_viewSL ),(wx.ACCEL_NORMAL, wx.WXK_F3, id_EditMember ),(wx.ACCEL_CTRL,  wx.WXK_F4, id_CtrlQ ),(wx.ACCEL_NORMAL,  wx.WXK_F6, id_AccInfo )])
        self.SetAcceleratorTable(accel_tbl)          

    def viewAccInfo(self,evt):
        the_obj = self.dataOlv.GetSelectedObjects()[0]
        if the_obj:
            OneFourthReport(the_obj.id,'onefourth.pdf','Member Account Information ')
            self.parent_window.SpecialReportWindow("Member's Account Information","onefourth.pdf")

    def deleteMember(self,evt):
        the_obj = self.dataOlv.GetSelectedObjects()[0]      
        message = "Are you sure you want to delete %s record? " % the_obj.fullname
        caption = "Delete Member Record Window"
        dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
        if dlg.ShowModal() == wx.ID_YES:
            the_obj = self.dataOlv.GetSelectedObjects()[0]
            self.DeleteRecord(the_obj)
            self.refreshGrid()
    
    def DeleteRecord(self,obj):
        self.contact_members.deleteMemberRecord(obj.id)
            
    def gotoSearch(self,evt):
        self.search.SetFocus()

    def addMemberInfo(self,evt):
        self.parent_window.OpenMemberInfo(None,'')    

    def editMemberInfo(self,evt):
        self.editMemberInfoNow()

    def editMemberInfoNow(self):
        the_obj = self.dataOlv.GetSelectedObjects()
        if the_obj:
            fullname = the_obj[0].fullname
            emp_id = the_obj[0].id          
            self.parent_window.OpenMemberInfo(emp_id,fullname)      


    def viewSL(self,evt):
        with_sl =  self.journals.getAccountsWithSL()
        sl_list = []
        sl_list.append("All Accounts")
        for d in with_sl:
            sl_list.append(d[0])
        the_obj = self.dataOlv.GetSelectedObjects()
        if the_obj:
            fullname = the_obj[0].fullname     
            dlg = wx.SingleChoiceDialog(
                    self, 'Please chooce Account Code ', 'Viewing SL of ' + fullname,
                    sl_list, 
                    wx.CHOICEDLG_STYLE
                    )
            dlg.SetSize((400,300))

            if dlg.ShowModal() == wx.ID_OK:
                #print ("ID %s : %s " ) % (the_obj[0].id,the_obj[0].fullname)
                #print dlg.GetStringSelection()
                account = dlg.GetStringSelection()                
                self.parent_window.OpenMemberLedger(the_obj[0].id,the_obj[0].fullname,account)

    def CloseMemberWindow(self,evt):
        self.CloseThisPanel()                

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("Members",True)  
        
    def refreshGrid(self,updateValue=True):
        #print self.Paging.current_page
        if updateValue:
            self.pages.SetValue(str(self.Paging.current_page))
        if self.search.GetValue():
            search_value = self.search.GetValue()
        else:
            search_value = ""
            
        self.members = []
        for u in self.contact_members.get_members(self.Paging.current_page-1,50,search_value):            
            self.members.append(Member(u.primary_name,u.address1,u.city_town,u.state_province,u.id))
        self.setMembers()
    
    def ChangePage(self,evt):
        self.Paging.page = int(self.pages.GetValue()) 
        self.refreshGrid(False)

    
    def PrevPage(self,evt):
        if self.Paging.has_prev:
            self.Paging.prev()
            self.refreshGrid()
    
    def TopPage(self,evt):
        if not self.Paging.is_ontop:
            self.Paging.top()
            self.refreshGrid()
    
    def BottomPage(self,evt):
        if not self.Paging.is_onbottom:
            self.Paging.bottom()
            self.refreshGrid()
  
    def NextPage(self,evt):
        if self.Paging.has_next:
            self.Paging.next()
            self.refreshGrid()
        
        
        
    def OnDoSearch(self,data):
        self.Paging.set_total_count(self.contact_members.count_members(self.search.GetValue()))
        
        self.pages.Clear()
        for i in xrange(1,self.Paging.pages+1):
            self.pages.Append(str(i))       
        self.refreshGrid()
        self.dataOlv.SetFocus()

        
        
    def OnCancel(self,evt):
        self.Paging.set_total_count(self.contact_members.count_members())        
        self.pages.Clear()
        for i in xrange(1,self.Paging.pages+1):
            self.pages.Append(str(i))       
        self.refreshGrid()      
        
    def setMembers(self, data=None):
        self.dataOlv.SetColumns([
            ColumnDefn("Name", "left", 220, "fullname"),
            ColumnDefn("Street", "left", 200, "street"),
            ColumnDefn("City", "right", 100, "city"),            
            ColumnDefn("Province", "left", 180, "province")
        ])
 
        self.dataOlv.SetObjects(self.members)

    def OnNewView(self,event):
        pass
    
    def OnAddRow(self,event):
        print "Add Row"
        pass
    
    def OnDeleteRows(self,event):
        pass
    
    
    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()        
    
    def return_panel(self):        
        return self

def CreateMemberWindow(the_parent_window):
   panel = membersPanel(the_parent_window)
   return panel.return_panel()
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
