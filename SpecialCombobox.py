
import wx.lib.colourdb as wb


from TextCtrlAUC import *


class SpecialCombobox(TextCtrlAUC):
    def __init__(self,parent,choices_=None,selectCallback_=None,value='',**args):
        super(SpecialCombobox,self).__init__(parent,colNames=None, choices = choices_, multiChoices=None, showHead=True, dropDownClick=False,colFetch=-1, colSearch=None, hideOnNoMatch=False,selectCallback=selectCallback_,style=wx.TE_PROCESS_ENTER,**args)

        wx.lib.colourdb.updateColourDB()

        self.SetValue(value)
        self.dropdownlistbox.SetBackgroundColour(wx.Colour(83,92,170))
        self.dropdownlistbox.SetForegroundColour(wx.Colour(255,255,255))


