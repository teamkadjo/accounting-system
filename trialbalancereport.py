#!/usr/bin/python
'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: trialbalancereport.py
'''

from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from podunk.prefab.fonts import *
from reportlab.lib.colors import *
from podunk.prefab import paper 
import os
from Configuration import *
from Users import *
import datetime
from reportlab.lib.pagesizes import letter, landscape




class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None,cost_center=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        #self.canvas.setPageSize(landscape(letter))
        self._working_height = self._working_height - 230
        self.cost_center = cost_center

            
    def _draw_header(self):
        #super(ReportSpecialHeader,self)._draw_header()
        config = Configuration()
        #self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,550,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(230,540,complete_address)   
        self.canvas.drawString(265,530,self.cutoff_date)   

        self.canvas.setFont('Helvetica-Bold', 10)
        #if (self.cost_center):
            #self.canvas.drawString(250,710,"T R I A L    B A L A N C E (" + self.cost_center + ")")
        #else:
            #self.canvas.drawString(250,710,"T R I A L    B A L A N C E ")
            
        #self.canvas.setFont('Helvetica', 8)
        pass

    def setExplanation(self,explanation=None):
        self.explanation = explanation

    def setCutOffDate(self,date1):
#        print "displaying cutoffdate " + date1      
        self.cutoff_date = datetime.datetime.strptime(date1,"%Y-%m-%d").strftime("As of %B %d, %Y")
         

class SpecialTable(Table):

    
    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):    
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        config = Configuration()
        canvas.line(24,yoff-20,580,yoff-20)
        canvas.line(24,yoff-60,580,yoff-60)
        canvas.line(24,yoff-20,24,yoff-60)
        canvas.line(580,yoff-20,580,yoff-60)
        canvas.line(163,yoff-20,163,yoff-60)
        canvas.line(302,yoff-20,302,yoff-60)
        canvas.line(441,yoff-20,441,yoff-60)
       
        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")
       
        canvas.drawString(27,yoff-30,"Prepared By:")
        canvas.drawString(166,yoff-30,"Checked By:")
        canvas.drawString(305,yoff-30,"Approved By:")
#        canvas.drawString(444,yoff-30,"JV NO: " + self.getInvoiceNumber())
#        canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.drawString(44,yoff-50,preparedByText)
        canvas.drawString(183,yoff-50,checkedByText)
        canvas.drawString(322,yoff-50,approvedByText)
        self.auto_width(canvas)
        

def trialBalance_report(pdf_name='test.pdf',title="Voucher Number",cutoff_date='2016-11-30',username='admin',cost_center=None):
    table = SpecialTable()

    u = Users()
    uinfo = u.get_userinfo(username)
        
    j = Journals()
    #col = table.add_column('Acct. ID',50)
    #col.header.box.background_color = toColor('rgb(173,216,230)')
    #col.header.style.color = toColor('rgb(0,0,0)')
    #col.row.style.horizontal_alignment = alignment.CENTER
    
    col = table.add_column('Account Title',120)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.style.size = 8 
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Beg. Debit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8 
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Beg. Credit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False   
    col.row.style.size = 8   
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('This Month Debit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8   
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('This Month Credit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8   
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Ending Debit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8      
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Ending Credit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8      
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    

    col = table.add_column('Total Debit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8      
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Total Credit',70)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.font = HELVETICA
    col.row.style.bold = False
    col.row.style.size = 8      
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    

    
    journals_listing = Journals()
    #explanation = ""

    data = j.get_beginning_balance(cutoff_date,cost_center)
    data2 = j.get_monthly_sum(cutoff_date,cost_center)
    #print data
    collated_data = {}
    for r in data:
        collated_data[r.account_id] = {'account_id':r.account_id,'account_name':r.account_name,'debit1':r.debit,'credit1':r.credit,'debit2':0,'credit2': 0,'is_asset':r.is_asset,'weight':r.weight}

    for r in data2:
        try:
            collated_data[r.account_id]['debit2'] = r.debit
            collated_data[r.account_id]['credit2'] = r.credit
            collated_data[r.account_id]['weight'] = r.weight
        except:
            collated_data[r.account_id] =  {'account_id':r.account_id,'account_name':r.account_name,'debit1':0,'credit1':0,'debit2':0,'credit2': 0,'is_asset':r.is_asset,'weight':r.weight}
            collated_data[r.account_id]['debit2'] = r.debit
            collated_data[r.account_id]['credit2'] = r.credit             

    the_sorted = sorted(collated_data.items(), key=lambda x:x[1])
    for r in the_sorted:
        if int(r[1]['weight']) >= 0:
            dr_total_wt = (((r[1]['debit1']+r[1]['debit2']) - (r[1]['credit1']+r[1]['credit2'])) * r[1]['weight'])
            cr_total_wt = 0
        else:
            dr_total_wt = 0
            cr_total_wt = (((r[1]['debit1']+r[1]['debit2']) - (r[1]['credit1']+r[1]['credit2'])) * r[1]['weight'])
        table.add_row([str(r[1]['account_id']) + ' ' + r[1]['account_name'],r[1]['debit1'],r[1]['credit1'],r[1]['debit2'],r[1]['credit2'],(r[1]['debit1']+r[1]['debit2']),(r[1]['credit1']+r[1]['credit2']),dr_total_wt,cr_total_wt])

         
    #col = table.get_footer_field('Acct. ID')
    #col.style.horizontal_alignment = alignment.LEFT
    #col.style.bold = True
    #col.style.size = 10
    #col.value = ""

    col = table.get_footer_field('Account Title')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    #col.style.size = 10
    #col.value = ""

    col = table.get_footer_field('Beg. Debit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Beg. Credit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True

    col = table.get_footer_field('This Month Debit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10
    #col.value = ""

    col = table.get_footer_field('This Month Credit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10

    col = table.get_footer_field('Ending Debit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10
    #col.value = ""

    col = table.get_footer_field('Ending Credit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10


    col = table.get_footer_field('Total Debit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10
    #col.value = ""

    col = table.get_footer_field('Total Credit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    #col.style.size = 10


    table.sum_column('Beg. Debit')
    table.sum_column('Beg. Credit')

    table.sum_column('This Month Debit')
    table.sum_column('This Month Credit')

    table.sum_column('Ending Debit')
    table.sum_column('Ending Credit')
    table.sum_column('Total Debit')
    table.sum_column('Total Credit')

    veryNiceHeader = Heading(None)

    if cost_center:
        cost_center_data_name = journals_listing.get_contact_info_byid(cost_center).short_name
    else:
        cost_center_data_name = None
    
    report = ReportSpecialHeader(pdf_name,cost_center_data_name)
    report.setCutOffDate(cutoff_date)

    report.top_margin = 120
    report.page_width, report.page_height = paper.LETTER_LANDSCAPE
    report.canvas.setPageSize(landscape(letter))
    #report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {} '.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()
        
        
if __name__ == '__main__':
    trialBalance_report('trialbalance.pdf','Journal Voucher','2017-1-31','admin')
    os.system('start trialbalance.pdf')
