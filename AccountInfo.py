

from db import *
import sqlalchemy

class AccountInfo:
    def __init__(self):
        self.UserAccount = Database.user
        self.db = Database
        self.counter = 0
        return None
    
    def insert_account(self,**data):
        try:
            self.UserAccount.insert(**data)
            self.db.commit()
            return 'Ok'
        except:
            return None
    
    def delete_account(self,data):
         try:
            user_account = self.UserAccount.filter_by(user_id=data).one()
            Database.delete(user_account)
            Database.commit()
            return 'Ok'    
         except:
             if sqlalchemy.orm.exc.NoResultFound:
                 return 'No Result Found'
             else:
                 return None
    
    def update_account(self,id,data):
        try:
            self.UserAccount.filter_by(user_id=id).update(data)
            self.db.commit()
            return 'Ok'
        except:
            return None
    
    def find_account(self,data):
        try:
            data = self.UserAccount.filter(self.UserAccount.user_id==data).one()
            return data
        except:
            return None
    def get_list(self):
        datas = self.UserAccount.all()
        listing = []
        for a in datas:
            listing.append(a.__dict__)
            
        return  listing
    def check_credential(self,uname,passw) :
        try:
            data = self.UserAccount.filter(self.UserAccount.username == uname,self.UserAccount.password==passw).one()
            return data
        except:
            return None
            
    def increment_count(self):
        self.counter = self.counter + 1
    
    def getCount(self):
        return self.counter
    
    
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyReportingMain.py file ***************"    
    
