from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
import os,sys
 
canvas = canvas.Canvas("test.pdf", pagesize=letter)
canvas.translate(7*cm,6*cm)
canvas.rotate(90)
canvas.setLineWidth(.3)
canvas.setFont('Courier-Bold', 10)

canvas.drawString(2.5 * cm ,4.5 * cm,"**** JOHN JAKE TALLEDO GAMHANAN ****")
canvas.drawString(5.5 * cm ,3.5 * cm,"ONE MILLION  AND 00/100 ONLY")
canvas.drawString(14.5 * cm ,5.5 * cm,"September 10, 2015")
canvas.drawString(16 * cm ,4.5 * cm,"1,000,000.00")

#canvas.drawString(0 * cm,5.5 * cm,'012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789')
#canvas.drawString(0 * cm,.1 * cm,'012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789')
 
#canvas.drawString(30,750,'OFFICIAL COMMUNIQUE')
#canvas.drawString(30,735,'OF ACME INDUSTRIES')
#canvas.drawString(500,750,"12/12/2010")
#canvas.line(480,747,580,747)
 
#canvas.drawString(275,725,'AMOUNT OWED:')
#canvas.drawString(500,725,"$1,000.00")
#canvas.line(378,723,580,723)
 
#canvas.drawString(30,703,'RECEIVED BY:')
#canvas.line(120,700,580,700)
#canvas.drawString(120,703,"JOHN DOE")
 
canvas.save()
os.system("start test.pdf")
sys.exit(1)

