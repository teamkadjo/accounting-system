import wx
from wx import *

class MyFrame(wx.Frame):

    def __init__(self, parent=None, id=-1, title='Process Tab'):
        wx.Frame.__init__(self, parent, id, title)
        panel = wx.Panel(self, -1)
        t1 = wx.TextCtrl(panel, 10, '',
                        wx.DefaultPosition,
                        wx.DefaultSize)
        t2 = wx.TextCtrl(panel, 20, '',
                        wx.DefaultPosition,
                        wx.DefaultSize)
        t3 = wx.TextCtrl(panel, 30, '',
                        wx.DefaultPosition,
                        wx.DefaultSize)
        EVT_KILL_FOCUS(t1, self.OnLeave)
        EVT_KILL_FOCUS(t2, self.OnLeave)
        EVT_KILL_FOCUS(t3, self.OnLeave)
        # the rest of this is to control the frame layout
        box0 = wx.BoxSizer(wx.VERTICAL)
        box0.Add(t1, 0, wx.EXPAND)
        box0.Add(t2, 0, wx.EXPAND)
        box0.Add(t3, 0, wxEXPAND)
        panel.SetSizer(box0)
        panel.Layout()
        panel.SetAutoLayout(true)
        box0.Fit(self)
        box0.SetSizeHints(self)

    def OnLeave(self, event):
        t = event.GetEventObject()
        print ('Text: %s\n' % t.GetValue())

class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        frame.Show(true)
        self.SetTopWindow(frame)
        return true

app = MyApp(0)
app.MainLoop()
