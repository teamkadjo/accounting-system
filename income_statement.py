from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
from Users import *
import datetime
import sys
import subprocess

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 35        
            
    def _draw_header(self):
        #super(ReportSpecialHeader,self)._draw_header()
        config = Configuration()
        #self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,750,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(230,740,complete_address)   
        self.canvas.drawString(265,730,self.cutoff_date)   

        self.canvas.setFont('Helvetica-Bold', 10)
        self.canvas.drawString(250,710,"I N C O M E    S T A T E M E N T")
        self.canvas.setFont('Helvetica', 8)


    def setExplanation(self,explanation=None):
        self.explanation = explanation

    def setCutOffDate(self,date1): 
        self.cutoff_date = datetime.datetime.strptime(date1,"%Y-%m-%d").strftime("As of %B %d, %Y")
         

class SpecialTable(Table):

    
    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):    
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        config = Configuration()
        canvas.line(24,yoff-20,580,yoff-20)
        canvas.line(24,yoff-60,580,yoff-60)
        canvas.line(24,yoff-20,24,yoff-60)
        canvas.line(580,yoff-20,580,yoff-60)
        canvas.line(163,yoff-20,163,yoff-60)
        canvas.line(302,yoff-20,302,yoff-60)
        canvas.line(441,yoff-20,441,yoff-60)
       
        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")
       
        canvas.drawString(27,yoff-30,"Prepared By:")
        canvas.drawString(166,yoff-30,"Checked By:")
        canvas.drawString(305,yoff-30,"Approved By:")
#        canvas.drawString(444,yoff-30,"JV NO: " + self.getInvoiceNumber())
#        canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.drawString(44,yoff-50,preparedByText)
        canvas.drawString(183,yoff-50,checkedByText)
        canvas.drawString(322,yoff-50,approvedByText)
        self.auto_width(canvas)
        

def incomeStatement_report(pdf_name='test.pdf',title="Voucher Number",startdate='2016-06-01',cutoff_date1='2016-06-30',username='admin',cost_center=None):
    table = SpecialTable()
    u = Users()
    uinfo = u.get_userinfo(username)
        
    j = Journals()
    col = table.add_column('Description',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')
    col.header.style.size = 8
    
    col.row.style.size = 8
    col.row.style.horizontal_alignment = alignment.LEFT
    
    col = table.add_column('Value',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8  
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    



 
    
    journals_listing = Journals()

    config = Configuration()
    acct_period = config.getConfig("CURRENT_ACCOUNTING_PERIOD")

    print "The cat off date is ***************" + cutoff_date1

    list_data = journals_listing.add_income_statement3(startdate,cutoff_date1,30)
    table.add_row(["REVENUE ",0])


    spaces = "      "

    printed = 0
    income_total = 0
    total_sales = 0
    total_otherincome = 0
    for r in list_data:
        if r.descript.startswith("Sale"):
            if printed == 0:
                table.add_row([spaces + "SALES",0])
                printed = 1
            total_sales = total_sales + abs(r.balance)
        else:
            if r.descript.startswith("Other Income") :
                if printed == 1:
                    table.add_row(["Total Income ",income_total])
                    printed = 0
                    table.add_row([spaces + "",0])
                    table.add_row([spaces + "OTHER INCOME",0])
                total_otherincome = total_otherincome + abs(r.balance)
        table.add_row( [spaces + spaces + " " + r.descript,abs(r.balance)] )
        income_total = income_total + abs(r.balance)


    table.add_row(["Total Other Income ",total_otherincome])

    
    table.add_row([" ",0])

    list_data = journals_listing.add_income_statement3(startdate,cutoff_date1,32)
    table.add_row(["PURCHASES ",0])


    spaces = "      "

    printed = 0
    purchases_total = 0
    for r in list_data:
        table.add_row( [spaces + spaces + " " + r.descript,abs(r.balance)] )
        purchases_total = purchases_total + abs(r.balance)

    

    table.add_row(["Total Purchases ",purchases_total ])
    table.add_row([" ",0])
    table.add_row(["TOTAL GROSS INCOME ",purchases_total + income_total])


    table.add_row([" ",0])

    list_data = journals_listing.add_income_statement3(startdate,cutoff_date1,34)
    table.add_row(["GENERAL & ADMINISTRATIVE EXPENSES ",0])


    spaces = "      "

    printed = 0
    expenses_total = 0
    for r in list_data:
        table.add_row( [spaces + spaces + " " + r.descript,abs(r.balance)] )
        expenses_total = expenses_total + abs(r.balance)
    #explanation = ""

    table.add_row(["TOTAL EXPENSES ",expenses_total])
    table.add_row([" ",0])
    
    table.add_row(["NET INCOME ",(purchases_total + income_total) - expenses_total])
 
 
    col = table.get_footer_field('Description')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Value')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""



    

    #table.sum_column('Sub Total')
    #table.sum_column('Total')


    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    print cutoff_date1
    report.setCutOffDate(cutoff_date1)
    #report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {} '.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()
    return 
    #print "End of creating..."

        
        
if __name__ == '__main__':
    incomeStatement_report('incomestatement.pdf','Income Statement','2016-06-01','2016-06-30','admin')
    if sys.platform == "linux2":
        subprocess.Popen("evince incomestatement.pdf",shell=True)
    else:
        os.system('start incomestatement.pdf')

