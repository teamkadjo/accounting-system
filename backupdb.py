import wx

try:
    from agw import pybusyinfo as PBI
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.pybusyinfo as PBI

import images2
import traceback
import ConfigParser
import datetime
import gzip
import shutil
import psutil
import time

def kill(proc_pid):
    process = psutil.Process(proc_pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()


def doBackup():
    message = "Please wait, working..."

    try:
        config = ConfigParser.RawConfigParser()
        config.read("acc_system.cfg")
        system_path = config.get("SYSTEM","system_path")
        host = config.get("SYSTEM","host")

        busy = PBI.PyBusyInfo(message, parent=None, title="Doing a backup for the systems database ..",icon=images2.book.GetBitmap())
        wx.Yield()

        d = datetime.datetime.now()
        filename = d.strftime("%Y-%m-%d-%H%M%S.sql")
        import os
        from subprocess import Popen

        complete_file_path = system_path + "/backup/" + filename
        f = open(complete_file_path ,"w")
        x = Popen(["c:/xampp/mysql/bin/mysqldump","-u","root","-pjojo","-h{0}".format(host),"pb"],stdout = f)
        x.wait()
        f.close()


        with open(complete_file_path, 'rb') as f_in, gzip.open(complete_file_path + ".gz", 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        f_in.close()
        f_out.close()
        #kill(x.pid)
        os.remove(complete_file_path)
        del busy

        wx.MessageBox("Finished Backing up...", "Success")
    except:
        wx.MessageBox("Error in backing up...", "Error")
        traceback.print_exc()
