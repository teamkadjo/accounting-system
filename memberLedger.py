

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from Journals import *
from specialolv import *
from wxSpecialPanel import *
import re

class memberLedger(object):
    #----------------------------------------------------------------------
    def __init__(self, post_date, invoice_id, account_id, account_description,title,debit,credit,balance):
        self.post_date = post_date
        self.invoice_id = invoice_id
        self.account_id = account_id
        self.account_description = account_description
        self.title = title
        self.debit = debit
        self.credit = credit
        self.balance = balance
        
class memberLedgerPanel(wxSpecialPanel):
    def __init__(self,parent_window,id,account_id=None):  
        self.parent_window = parent_window             
        self.book = parent_window.book
        self.records = None
      
        super(memberLedgerPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("MemberLedger")
        self.journals = Journals()
        if account_id:
            self.account_id = account_id
        else:
            self.account_id = None
            
        self.ledgers = []
        self.id = id

        self.setEscapeHandler(self.CloseThisPanel)

        self.dataOlv = SpecialOLV(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)

 
        self.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE
 
        # create an update button
        self.printLedger = wx.Button(self, wx.ID_ANY, "Print (F4)")
        self.closePanel = wx.Button(self, wx.ID_ANY, "Close (Esc)")

        btnbox = wx.BoxSizer(wx.HORIZONTAL)
        btnbox.Add(self.printLedger, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(self.closePanel, 0, wx.ALL|wx.LEFT, 5)




        self.Bind(wx.EVT_BUTTON, self.printLedgerAction, self.printLedger)

        id_F4 = wx.NewId()
        self.Bind(wx.EVT_MENU,self.printLedgerAction,id=id_F4)
        mainSizer = wx.BoxSizer(wx.VERTICAL)        
 
        mainSizer.Add(self.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(btnbox, 0, wx.ALL|wx.CENTER, 5)
        self.SetSizer(mainSizer)

        randomId = wx.NewId()
        self.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('N'), randomId ),(wx.ACCEL_NORMAL, wx.WXK_F4, id_F4 )])
        self.SetAcceleratorTable(accel_tbl)    
        
        self.refreshGrid()    

    def onKeyCombo(self,evt):
        print "Pinindot"
        pass
        

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("MemberLedger",True)    
                
    def refreshGrid(self):
        
        self.records = self.journals.subledger_Entries(self.id,self.account_id)
        balance = 0
        
        if self.account_id :
            self.printLedger.Enable(True) 
        else:
            self.printLedger.Enable(False)
            
            
        if self.records:
            for u in self.records:
                
                
                if (u.accounts.find("Receivable")) > 0 or (u.accounts.find("Liquidation")) > 0:
                    balance = balance + (u.debit_amount - u.credit_amount)
                else:
                    balance = balance + (u.credit_amount - u.debit_amount)
                
                debit_amount = "{:,.2f}".format(u.debit_amount)
                credit_amount = "{:,.2f}".format(u.credit_amount)
                if self.account_id:
                    balance_amount = "{:,.2f}".format(balance)
                else:
                    balance_amount = ""
                self.ledgers.append(memberLedger(u.post_date,u.purchase_invoice_id,u.account_id,u.accounts,u.description,debit_amount,credit_amount,balance_amount))
                
        else:
            self.printLedger.Enable(False)
        
        self.setMemberLedgers() 
    
    
    def printLedgerAction(self,evt):
        self.parent_window.SLReport(self.id,self.account_id)
        
    def setMemberLedgers(self, data=None):
                
        self.dataOlv.SetColumns([
            ColumnDefn("Date Posted", "left", 100, "post_date"),
            ColumnDefn("Number", "left", 120, "invoice_id"),
            ColumnDefn("Account ID", "right", 100, "account_id"),            
            ColumnDefn("Account Description", "left", 180, "account_description"),
            ColumnDefn("Title", "left", 220, "title"),
            ColumnDefn("Debit", "right", 180, "debit"),
            ColumnDefn("Credit", "right", 180, "credit"),
            ColumnDefn("Running Balance", "right", 180, "balance")
        ])
 
        self.dataOlv.SetObjects(self.ledgers)

    def OnNewView(self,event):
        pass
    
    def OnAddRow(self,event):
        print "Add Row"
        pass
    
    def OnDeleteRows(self,event):
        pass
    
    
    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()        
    
    def return_panel(self):
        return self

def CreateMemberLedger(the_parent_window,id,account):
    account_number = None
    if account:
        account_num = re.findall(r'\b\d+\b', account)
        if account_num:
            account_number = account_num[0]
           
    panel = memberLedgerPanel(the_parent_window,id,account_number)
    return panel
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
