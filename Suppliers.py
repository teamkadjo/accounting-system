

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *

class Supplier(object):
    #----------------------------------------------------------------------
    def __init__(self, fullname, street, city, province):
        self.fullname = fullname
        self.street = street
        self.city = city
        self.province = province
        
class suppliersPanel:
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book
        
        
        #self.members = [Supplier("Jojo Maquiling","Aradaza St","General Santos City","South Cotabato")]        
        self.the_panel = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.the_panel.SetLabel("Suppliers")
        self.suppliers = Contacts()

        self.members = []
        for u in self.suppliers.get_suppliers(0,50):
            self.members.append(Supplier(u[26],u[28],u[30],u[31]))

        self.the_panel.dataOlv = ObjectListView(self.the_panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setSuppliers()
 
        self.the_panel.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE
 
        # create an update button
        updateBtn = wx.Button(self.the_panel, wx.ID_ANY, "Update OLV")
        #updateBtn.Bind(wx.EVT_BUTTON, self.updateControl)
 
        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)        
 
        mainSizer.Add(self.the_panel.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(updateBtn, 0, wx.ALL|wx.CENTER, 5)
        self.the_panel.SetSizer(mainSizer)

        randomId = wx.NewId()
        self.the_panel.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('N'), randomId )])
        self.the_panel.SetAcceleratorTable(accel_tbl)        

    def onKeyCombo(self,evt):
        print "Pinindot"
        pass
        

  
    def setSuppliers(self, data=None):
        self.the_panel.dataOlv.SetColumns([
            ColumnDefn("Name", "left", 220, "fullname"),
            ColumnDefn("Street", "left", 200, "street"),
            ColumnDefn("City", "right", 100, "city"),            
            ColumnDefn("Province", "left", 180, "province")
        ])
 
        self.the_panel.dataOlv.SetObjects(self.members)

    def OnNewView(self,event):
        pass
    
    def OnAddRow(self,event):
        print "Add Row"
        pass
    
    def OnDeleteRows(self,event):
        pass
    
    
    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()        
    
    def return_panel(self):        
        return self.the_panel

def CreateSupplierWindow(the_parent_window):
   panel = suppliersPanel(the_parent_window)
   return panel.return_panel()
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
