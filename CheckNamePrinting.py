import wx
from EnterTextCtrl import *
from EnterDatePicker import *

class CheckNamePrintingDialog ( wx.Dialog ):
    
    def __init__( self, parent,name='Testing Name' ):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 385,230 ), style = wx.DEFAULT_DIALOG_STYLE )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "Check Printing Editor")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        
        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Check Owner :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        description = name
        self.descript = EnterTextCtrl(self, -1, description,wx.DefaultPosition, (255,50),style=wx.TE_MULTILINE)
        box.Add(self.descript, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Checque Date :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        description = name
        self.dpc = EnterDatePicker(self, size=(120,-1),
                        style = wx.DP_DROPDOWN
                              | wx.DP_SHOWCENTURY)
       
        box.Add(self.dpc, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
    
    
        btnsizer = wx.StdDialogButtonSizer()        
        btn = wx.Button(self, wx.ID_OK)
        btn.SetHelpText("The OK button completes the dialog")
        #btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")
        btnsizer.AddButton(btn)
        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.CENTER|wx.ALL, 5)        
        
        self.SetSizer( sizer )
        self.Layout()
        
        self.Centre( wx.BOTH )
    
    def __del__( self ):
        pass
    
    
    def GetName(self):
        return self.descript.GetValue()
    
    def getInfo(self):
        return {'name':self.descript.GetValue(),'date':self.dpc.GetValue().Format("%B %d, %Y")}

class MyFrame(wx.Frame):
    #----------------------------------------------------------------------
    def __init__(self):
        dlg = CheckNamePrintingDialog(None)    
        if dlg.ShowModal() == wx.ID_OK:
            pass
        
class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
