'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: db.py
'''
import sqlsoup
########################## config reading #####################
import os
import sys
import ConfigParser
import traceback
import crypt
import mysql

Database = None
DatabaseMySQL = None

if (os.path.isfile("acc_system.cfg")):     
    try:
        config = ConfigParser.RawConfigParser()
        config.read("acc_system.cfg")
        system_path = config.get("SYSTEM","system_path")

        if (sys.platform == "linux2"):
            system_path = config.get("SYSTEM","lin_sys_path")
        password = crypt.decrypt('jojomaquiling',config.get('SYSTEM','dbpass'))
        host = config.get('SYSTEM','host')
        #dbname=crypt.decrypt('jojomaquiling',config.get('SYSTEM','dbname'))
	dbname="pb"
        #print dbname
        os.chdir(system_path)    
        sys.path.append(system_path)
        Database = sqlsoup.SQLSoup("sqlite:///" + system_path + "/system.db")
        # DatabaseMySQL = sqlsoup.SQLSoup('mysql+mysqlconnector://%s:%s@%s/%s?charset=utf8&use_unicode=1' %('root',password,host,dbname)) 
        DatabaseMySQL = sqlsoup.SQLSoup('mysql+pymysql://%s:%s@%s/%s?charset=utf8&use_unicode=1' %('root',password,host,dbname)) 
    except:
        traceback.print_exc()
        print "Program is closing.."
        exit(0)
########################## config reading end #####################
else:
    print "Error in loading the config file"
    sys.exit(1)




class userinfo():
    def __init__(self, username, firstname,lastname):
        self.Username = username
        self.Firsname = firstname
        self.Lastname = lastname

