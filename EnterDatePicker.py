                                      
import wx

class EnterDatePicker(wx.DatePickerCtrl):

    def __init__(self, parent, id=wx.ID_ANY, dt=wx.DefaultDateTime, pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.DP_DEFAULT|wx.DP_SHOWCENTURY, validator=wx.DefaultValidator, name="datectrl"):
        super(EnterDatePicker,self).__init__(parent,id,dt,pos,size,style,validator,name)
        self.Bind(wx.EVT_CHAR_HOOK, self.EvtChar)       
        self.ignoreEvtText = False

        
    def EvtChar(self, event):
        keycode = event.GetKeyCode()
        if (keycode == wx.WXK_SHIFT):
            self.is_shift_pressed = True
            return
        elif keycode == wx.WXK_TAB:            
            if event.ShiftDown():
                event.EventObject.Navigate(wx.NavigationKeyEvent.IsBackward)
            else:
                event.EventObject.Navigate()
            event.StopPropagation()    
            return
        elif keycode == wx.WXK_RETURN or keycode == wx.WXK_NUMPAD_ENTER:
            self.process_text(event=None)
            event.EventObject.Navigate()
            return   
        elif keycode == 8:
            self.ignoreEvtText = True 
    
        event.Skip()
        
 
    def process_text(self,event):
        pass                                      
