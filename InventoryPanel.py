

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from AccountInfo import *
from Contacts import *
from specialolv import *
import Journals
import Contacts
import datetime
sa = Contacts.Contacts()
j = Journals.Journals()
from sqlalchemy import extract,and_

class Book(object):
    #----------------------------------------------------------------------
    def __init__(self, title, author, isbn, mfg):
        self.isbn = isbn
        self.author = author
        self.mfg = mfg
        self.title = title

class dict2obj(object):
    def __init__(self, d):
        self.__dict__['d'] = d
    def __getattr__(self, key):
        value = self.__dict__['d'][key]
        if type(value) == type({}):
            return dict2obj(value)
        return value

class objInventoryPanel:
    def __init__(self,parent_window):
        self.parent_window = parent_window
        self.book = parent_window.book

        self.inventory_entries = [dict2obj({'date':'2018-11-9','bakery':200,'admin':300,'trading':400,'bakery1':500,'canteen':600,'entry_type':'Beginning','id':100}),
                         dict2obj({'date':'2018-11-9','bakery':200,'admin':300,'trading':400,'bakery1':500,'canteen':600,'entry_type':'Ending','id':102})]
        self.the_panel = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.account_info = AccountInfo()

        account_list = {}
        for u in self.account_info.get_list():
            account_list[u['user_id']] = (u['username'],u['firstname'],u['lastname'])
        account_list = account_list.items()
        account_list.sort()
        account_list = [[str(k)] + list(v) for k,v in account_list]

        self.the_panel.dataOlv = SpecialOLV(self.the_panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setBooks()

        self.the_panel.dataOlv.setEditCallback(self.editInventoryEntry)

        # Allow the cell values to be edited when double-clicked
        self.the_panel.dataOlv.cellEditMode = ObjectListView.CELLEDIT_SINGLECLICK
        # self.the_panel.dataOlv.cellEditMode = ObjectListView.CELLEDIT_F2ONLY



        # mainSizer.Add(sbSizer1, 0, wx.TOP|wx.ALIGN_RIGHT|wx.BOTTOM, 5)

        id_NewEntry = wx.NewId()
        newEntry = wx.Button(self.the_panel, label = "New Entry (F2)",name="newEntry")
        # self.the_panel.Bind(wx.EVT_BUTTON, self.newTransactionEntry, newEntry)


        editEntry = wx.Button(self.the_panel, label = "Edit Entry (F3)",name="editEntry")
        self.the_panel.Bind(wx.EVT_BUTTON, self.editInventoryEntry, editEntry)


        # create an update button
        printBtn = wx.Button(self.the_panel, wx.ID_ANY, "Print Income Statement from Operation")
        #updateBtn.Bind(wx.EVT_BUTTON, self.updateControl)


        btnbox = wx.BoxSizer(wx.HORIZONTAL)
        btnbox.Add(newEntry, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(editEntry, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(printBtn,0,wx.ALL|wx.LEFT,5)



        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        mainSizer.Add(self.the_panel.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(btnbox, 0, wx.ALL|wx.CENTER, 5)
        self.the_panel.SetSizer(mainSizer)

    def setBooks(self, data=None):
        c = Contacts.Contacts()
        cc = c.get_costcenters()
        cols = [ ColumnDefn("Date", "left", 220, "date")]
        cols.append(ColumnDefn("Entry Type", "left", 110, "entry_type"))
        for ccn in cc:
            titled_case_primary_name = ccn.primary_name.title()
            primary_name_noblank = str(ccn.primary_name).lower().replace(" ","")
            if (primary_name_noblank == "date"):
                cols.append( ColumnDefn(titled_case_primary_name, "left", 100,primary_name_noblank ))
            else:
                cols.append( ColumnDefn(titled_case_primary_name, "right", 100,primary_name_noblank ))
        self.the_panel.dataOlv.SetColumns(cols)

        self.the_panel.dataOlv.SetObjects(self.inventory_entries)

    def getInventoryEntries(self):
        pass



    def editInventoryEntryNow(self):
        the_obj = self.the_panel.dataOlv.GetSelectedObjects()
        if the_obj:
            #print the_obj[0].date
            #print the_obj[0].__dict__
            date = the_obj[0].date
            entry_type = the_obj[0].entry_type
            #print the_obj[0].__dict__
            print the_obj[0].__dict__['d']
            self.parent_window.OpenInventoryEntryEditor(date,the_obj[0].__dict__['d'])



    def editInventoryEntry(self):
        self.editInventoryEntryNow()



    def return_panel(self):
        return self.the_panel

def CreateInventoryPanel(the_parent_window):
   panel = objInventoryPanel(the_parent_window)
   return panel.return_panel()

if __name__ == '__main__':
    class MyApp(wx.App):
        def OnInit(self):
            app = wx.App()
            frame = wx.Frame (None, -1, 'Demo PromptingComboBox Control', size=(400, 150))
            objInventoryPanel(frame)
            frame.Show()
            app.MainLoop()


# if __name__ == '__main__':
    # print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
