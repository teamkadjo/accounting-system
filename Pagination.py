from math import ceil


class Pagination(object):

    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    def set_total_count(self,total):
        self.total_count = total
    
        
    @property
    def pages(self):
        if (self.per_page):
            return int(ceil(self.total_count / float(self.per_page)))
        else:
            return 1
    @property
    def has_prev(self):
        return self.page > 1

    @property
    def current_page(self):
        return self.page
        
    @property
    def is_ontop(self):
        return self.page == 1

    @property 
    def is_onbottom(self):
        return self.page == self.pages

           
    @property
    def has_next(self):
        return self.page < self.pages


    def next(self):
        if self.has_next:
            self.page += 1 
        else:
            self.page = self.pages
        return
    
    def prev(self):
        if self.has_prev:
            self.page -= 1
        else:
            self.page = 1
        return 
        
    def top(self):
        self.page = 1
        return

    def bottom(self):
        self.page = self.pages
        
    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in xrange(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num
