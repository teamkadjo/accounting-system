from distutils.core import setup
import py2exe
import os
import shutil

# cleanup dist and build directory first (for new py2exe version) 
if os.path.exists("dist/prog"): 
    shutil.rmtree("dist/prog") 

if os.path.exists("dist/lib"): 
    shutil.rmtree("dist/lib") 

if os.path.exists("build"): 
    shutil.rmtree("build") 


packages= [
    'reportlab',
    'reportlab.lib',
    'reportlab.pdfbase',
    'reportlab.pdfgen',
    'reportlab.platypus',
    'mysql',
    'psutil',
    'inflect',
    'sqlalchemy',
    'pymysql'
]

setup(windows=[{"script" : "AccountingSystemMain.py"}], options={"py2exe" : {"packages":packages,"dll_excludes": ["MSVCP90.dll","api-ms-win-core-string-obsolete-l1-1-0.dll","api-ms-win-core-delayload-l1-1-1.dll","api-ms-win-core-processthreads-l1-1-2.dll","api-ms-win-core-sysinfo-l1-2-1.dll","api-ms-win-core-errorhandling-l1-1-1.dll","api-ms-win-core-heap-l2-1-0.dll","api-ms-win-core-profile-l1-1-0.dll","api-ms-win-core-libraryloader-l1-2-0.dll","api-ms-win-security-activedirectoryclient-l1-1-0.dll","crypt32.dll","mswsock.dll", "powrprof.dll", "user32.dll", "shell32.dll", "wsock32.dll", "advapi32.dll", "kernel32.dll", "ntwdblib.dll", "ws2_32.dll", "oleaut32.dll", "ole32.dll","numpy-atlas.dll","libopenblas.UWVN3XTD2LSS7SFIFK6TIQ5GONFDBJKU.gfortran-win32.dll","api-ms-win-core-delayload-l1-1-0.dll","api-ms-win-core-processthreads-l1-1-0.dll","api-ms-win-core-errorhandling-l1-1-0.dll",'api-ms-win-core-sysinfo-l1-1-0.dll']}})
#setup(windows=[{"script" : "AccountingSystemMain.py"}])

