from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from podunk.prefab.formats import *
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
import time
import traceback
from datetime import datetime
from Users import *
from numberword import *
import re



class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 30
        self.bottom_margin = 36        
            
    def _draw_header(self):
        pass

    def setExplanation(self,explanation=None):
        self.explanation = explanation
        
    def getExplanation(self):
        return self.explanation 
           

class SpecialTable(Table):

    def __init__(self,ending_date,report=None):
        super(SpecialTable,self).__init__()
        self.yoff = 800
        self.xoff = 30
        
        self.left_margin = 54
        self.top_margin = 0
        self.right_margin = 54
        self.bottom_margin = 72
        self.ending_date = ending_date
        self.page_counter = 1
        self.report = report


    def setAccountTitle(self,account_title):
        self.account_title = account_title
        
    def setReport(self,report):
        self.report = report
        
    def create_page_header(self,canvas):
        
        config = Configuration()
        canvas.setLineWidth(.3)
        canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        canvas.drawString(180,770,companyName)
        canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        
        canvas.setFont('Helvetica', 10)        
        canvas.drawString(230,760,complete_address) 

        canvas.setFont('Helvetica-Bold', 10)
        canvas.drawString(236,740,"ACCOUNTS SUMMARY  LISTING PER TYPE ")
        
        canvas.setFont('Helvetica', 8)

        ending_dateobject = datetime.strptime(self.ending_date,"%Y-%m-%d")
        canvas.drawString(290,720,"As of " + ending_dateobject.strftime("%B %d, %Y") )

         #self.account_title
        canvas.setFont('Helvetica-Bold', 10)
        canvas.drawString(245,730,self.account_title.upper())
        
                    
    def draw_amount(self,canvas,r):
        if (r.debit-r.credit) != 0:
            canvas.drawString(self.xoff + 30, self.yoff, r.gl_account_desc)
            canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(r.debit-r.credit), pivotChar='.')
            self.lineFeed()

    def draw_grandtotal(self,canvas,amount):
        canvas.setFont('Helvetica-Bold', 8) 
        canvas.drawString(self.xoff + 30, self.yoff, 'GRAND TOTAL: ')
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.setFont('Helvetica', 8) 
        self.lineFeed()

    def draw_total(self,canvas,amount):
        canvas.line(self.xoff + 150,self.yoff + 9,self.xoff + 200,self.yoff + 9)
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.line(self.xoff + 150,self.yoff-2 ,self.xoff + 200,self.yoff-2 )
        self.lineFeed()
    
    
        
        
    def lineFeed(self):
        self.yoff = self.yoff - 10      
    
    def draw_name(self,canvas,name):
        canvas.drawString(self.xoff,self.yoff,name )
        self.lineFeed()
    
    def line_number(self):
        return self.yoff
    
    def reset_linenumber(self,canvas):
        self.yoff = 800
        if self.xoff == 350 : 
            self.report._page_count = self.report._page_count + 1 
            canvas.showPage()
            self.create_page_header(canvas)   
            self.report._draw_footer()                       
            canvas.setFont('Helvetica', 8)
            self.xoff = 30
            canvas.doForm('last_page')
        else:
            self.xoff = 350


    #def _draw_footer(self,canvas):
        #super(SpecialTable,self)._draw_footer(canvas)
        
    def get_page_count(self):
        return self.page_counter
        

    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)
        self.create_page_header(canvas)



def arListing_Per_Type(pdf_name='test.pdf',title="Voucher Number",cutoff_date='2016-04-01',account_id='11704',username='admin'):
    table = SpecialTable(cutoff_date)


    col = table.add_column('Rec. No.',60)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.horizontal_alignment = alignment.CENTER
    col.row.style.size = 9

    col = table.add_column('Account Name',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.horizontal_alignment = alignment.LEFT
    col.row.style.size = 9

    col = table.add_column('Amount',100)
    col.row.format = format_two_decimals
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.row.style.size = 9 

    j = Journals()

    u = Users()
    uinfo = u.get_userinfo(username)

    r = j.ar_listing_per_acct(cutoff_date,account_id)
    running_balance = 0

    acc = j.get_one_account_by_id(account_id)

    table.setAccountTitle(acc.description)
    counter = 1
    for x in r:
        #print x.post_date
        #table.add_row( [x.mem_id,x.primary_name,x.total ])
        table.add_row( [counter,x.primary_name,x.total ])
        counter = counter + 1

    table.sum_column('Amount')
   

    col = table.get_footer_field('Rec. No.')
    col.style.horizontal_alignment = alignment.CENTER
    col.style.bold = True
    col.value=""

    col = table.get_footer_field('Account Name')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.value = "T O T A L "

    col = table.get_footer_field('Amount')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9



    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    table.setReport(report)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {}'.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    arListing_Per_Type('aclistingreport.pdf','Journal Voucher ','2016-06-7')
    os.system('start aclistingreport.pdf')
