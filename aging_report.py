from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from podunk.prefab.formats import *
from podunk.prefab import paper
from Journals import Journals
from reportlab.lib.colors import *
from reportlab.lib.pagesizes import legal, landscape
import os
from Configuration import *
import time
import traceback
from datetime import datetime
from Users import *
from numberword import *
import re
import sys
import subprocess

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 170
        self.bottom_margin = 36


    def _draw_header(self):
        pass

    def setExplanation(self,explanation=None):
        self.explanation = explanation

    def getExplanation(self):
        return self.explanation


class SpecialTable(Table):

    def __init__(self,ending_date,report=None):
        super(SpecialTable,self).__init__()

        self.yoff = 100
        self.xoff = 30

        self.left_margin = 54
        #self.top_margin = 72
        self.top_margin = 300
        self.right_margin = 54
        self.bottom_margin = 35
        self.ending_date = ending_date
        self.page_counter = 1
        self.report = report

    def setAccountTitle(self,account_title):
        self.account_title = account_title

    def setReport(self,report):
        self.report = report

    def create_page_header(self,canvas):

        config = Configuration()
        canvas.setLineWidth(.3)
        canvas.setPageSize(landscape(legal))
        canvas.setFont('Helvetica', 9)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        # canvas.drawString(430,570, companyName.title())
        # #canvas.setFont('Helvetica', 8)

        canvas.drawString(430,560,companyName)


        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")

        canvas.drawString(450,550,complete_address)


        #canvas.setFont('Helvetica-Bold', 8)

        #canvas.setFont('Helvetica-Bold', 10)

        canvas.drawString(480,540,"AGING OF ACCOUNTS ")


        canvas.setFont('Helvetica', 8)
        ending_dateobject = datetime.strptime(self.ending_date,"%Y-%m-%d")

        canvas.drawString(485,530,"As of : {} " .format(
        ending_dateobject.strftime("%B %d, %Y")) )



    def draw_amount(self,canvas,r):
        if (r.debit-r.credit) != 0:
            canvas.drawString(self.xoff + 30, self.yoff, r.gl_account_desc)
            canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(r.debit-r.credit), pivotChar='.')
            self.lineFeed()

    def draw_grandtotal(self,canvas,amount):
        canvas.setFont('Helvetica-Bold', 8)
        canvas.drawString(self.xoff + 30, self.yoff, 'GRAND TOTAL: ')
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.setFont('Helvetica', 8)
        self.lineFeed()

    def draw_total(self,canvas,amount):
        canvas.line(self.xoff + 150,self.yoff + 9,self.xoff + 200,self.yoff + 9)
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.line(self.xoff + 150,self.yoff-2 ,self.xoff + 200,self.yoff-2 )
        self.lineFeed()




    def lineFeed(self):
        self.yoff = self.yoff - 10

    def draw_name(self,canvas,name):
        canvas.drawString(self.xoff,self.yoff,name )
        self.lineFeed()

    def line_number(self):
        return self.yoff

    def reset_linenumber(self,canvas):

        self.yoff = 100

        if self.xoff == 350 :
            self.report._page_count = self.report._page_count + 1
            canvas.showPage()
            self.create_page_header(canvas)
            self.report._draw_footer()
            canvas.setFont('Helvetica', 8)
            self.xoff = 30
            canvas.doForm('last_page')
        else:
            self.xoff = 350


    #def _draw_footer(self,canvas):
        #super(SpecialTable,self)._draw_footer(canvas)

    def get_page_count(self):
        return self.page_counter


    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)
        self.create_page_header(canvas)



def aging_Report(pdf_name='agingreport.pdf',title="Voucher Number",ending_date='2016-06-01',username='admin'):
    table = SpecialTable(ending_date)

    col = table.add_column('MEMBERS NAME',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.LEFT


    col = table.add_column('TOTALS RECEIVABLE',100)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    # col = table.add_column('REGULAR PAYMENT(MONTHLY)',100)
    # col.row.format = format_two_decimals
    # col.row.style.horizontal_alignment = alignment.ALIGNED
    # col.header.box.background_color = toColor('rgb(173,216,230)')
    # col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('CURRENT',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.format = format_two_decimals
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.ALIGNED

    col = table.add_column('PAST DUE (1-30 days)',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.format = format_two_decimals
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.ALIGNED

    col = table.add_column('PAST DUE (31-60 days)',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.format = format_two_decimals
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.ALIGNED

    col = table.add_column('PAST DUE (61-90 days)',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.format = format_two_decimals
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.ALIGNED

    col = table.add_column('PAST DUE (over 90days)',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.format = format_two_decimals
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.ALIGNED



    # col = table.get_footer_field('TOTALS RECEIVABLE')
    # col.style.horizontal_alignment = alignment.ALIGNED
    # col.style.bold = True
    # #col.style.size = 10

    # table.sum_column('TOTALS RECEIVABLE')

    j = Journals()

    u = Users()
    uinfo = u.get_userinfo(username)

    r = j.get_account_aginglist(ending_date)
    running_balance = 0



    #table.setAccountTitle(acc.description)
    account_number  = None



    subtotal_current = 0
    subtotal_130 = 0
    subtotal_3160 = 0
    subtotal_6190 = 0
    subtotal_91over= 0
    subtotal = 0
    total = 0
    total_current = 0
    total_130 = 0
    total_3160 = 0
    total_6190 = 0
    total_91over = 0


    for x in r:
        if account_number != x.gl_account :
            if not(account_number is None):
                table.add_row( ["SUB TOTAL ({})".format(account_number) ,subtotal,subtotal_current,subtotal_130,subtotal_3160 ,subtotal_6190,subtotal_91over])
            account_number = x.gl_account
            descript = j.get_one_account_by_id(account_number).description
            if account_number:
                descript = j.get_one_account_by_id(account_number).description
                table.add_row( [x.gl_account + " " +
                descript,0.0,0.00 ,0.0,0.00,0.00,0.00])
                subtotal = 0
                subtotal_current = 0
                subtotal_130 = 0
                subtotal_3160 = 0
                subtotal_6190 = 0
                subtotal_91over= 0

        # #print x.post_date
        # sl_name = ""
        # if  x.sl_name :
            # sl_name = x.sl_name
        current = 0.00
        s1_30 = 0
        s31_60 = 0
        s61_90 = 0
        s91_over = 0

        if (x.age in range(0,31)):
            current = x.debit- x.credit
        elif (x.age in range(31,61)):
            s1_30 = x.debit- x.credit
        elif (x.age in range(61,91)):
            s31_60 = x.debit- x.credit
        elif (x.age in range(91,121)):
            s61_90 = x.debit - x.credit
        elif (x.age > 120):
            s91_over = x.debit - x.credit
        subtotal_current = subtotal_current + current
        subtotal_130 = subtotal_130 + s1_30
        subtotal_3160 = subtotal_3160 + s31_60
        subtotal_6190 = subtotal_6190 + s61_90
        subtotal_91over= subtotal_91over + s91_over
        subtotal = subtotal + (x.debit - x.credit)

        total_current = total_current + current
        total_130 = total_130+ s1_30
        total_3160 = total_3160 + s31_60
        total_6190 = total_6190 + s61_90
        total_91over = total_91over + s91_over
        total = total + (x.debit - x.credit)
        table.add_row( ["         "  + x.name,x.debit-x.credit,current,s1_30,s31_60,s61_90,s91_over ])

    if not(account_number is None):
        table.add_row( ["SUB TOTAL ({})".format(account_number),subtotal,subtotal_current,subtotal_130,subtotal_3160 ,subtotal_6190,subtotal_91over])

    table.add_row( ["TOTAL",total,total_current,total_130,total_3160 ,total_6190,total_91over])


    veryNiceHeader = Heading(None)

    # table.sum_column('TOTALS RECEIVABLE')
    report = ReportSpecialHeader(pdf_name)
    report.page_width, report.page_height = paper.LEGAL_LANDSCAPE
    table.setReport(report)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {}'.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()



if __name__ == '__main__':
    aging_Report('aging_report.pdf','Aging Account','2018-10-25')
    if sys.platform == "linux2":
        subprocess.Popen("evince aging_report.pdf",shell=True)
    else:
        os.system('start aging_report.pdf')
