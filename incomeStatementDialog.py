import wx
from EnterTextCtrl import *
from EnterDatePicker import *
from Journals import * 
from SpecialCombobox import *
import datetimeutil
import datetime


class TextObjectValidator(wx.PyValidator):
    """ This validator is used to ensure that the user has entered something
        into the text object editor dialog's text field.
    """
    def __init__(self):
        """ Standard constructor.
        """
        wx.PyValidator.__init__(self)



    def Clone(self):
        """ Standard cloner.

            Note that every validator must implement the Clone() method.
        """
        return TextObjectValidator()


    def Validate(self, win):
        """ Validate the contents of the given text control.
        """
        textCtrl = self.GetWindow()
        text = textCtrl.GetValue()

        if len(text) == 0:
            wx.MessageBox("An Account must be selected!", "Error")
            textCtrl.SetBackgroundColour("pink")
            textCtrl.SetFocus()
            textCtrl.Refresh()
            return False
        else:
            textCtrl.SetBackgroundColour(
                wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW))
            textCtrl.Refresh()
            return True


    def TransferToWindow(self):
        """ Transfer data from validator to window.

            The default implementation returns False, indicating that an error
            occurred.  We simply return True, as we don't do any data transfer.
        """
        return True # Prevent wxDialog from complaining.




class incomeStatementDialog ( wx.Dialog ):
    
    def __init__( self, parent,name='Testing Name' ):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 300,200 ), style = wx.DEFAULT_DIALOG_STYLE )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        self.journals = Journals()
        #self.chart_of_accounts = self.journals.get_chart_of_accounts()
        self.cost_center_selected = None
        
        
        
        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "Income Statement Info")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        
        #box = wx.BoxSizer(wx.HORIZONTAL)

        #label = wx.StaticText(self, -1, "Cost Center:")
        #label.SetHelpText("This is the help text for the label")
        #box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        

        #self.cost_center_choices = self.journals.db.contacts.filter(self.journals.db.contacts.type == 'j').all()

        #choices = [( x.short_name) for x in self.cost_center_choices ]
            
        #self.cbx = SpecialCombobox(self,choices,self.selectCallback,"")
        ##self.cbx.SetValidator(TextObjectValidator())
        #self.cbx.SetValue("All Cost Centers")
        #box.Add(self.cbx, 1, wx.ALIGN_CENTRE|wx.ALL, 5)
        
        #sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Starting Date :")
        label.SetHelpText("Starting date")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        description = name
        self.dpStartdate = EnterDatePicker(self, size=(100,-1),
                        style = wx.DP_DROPDOWN
                              | wx.DP_SHOWCENTURY)
       
        box.Add(self.dpStartdate, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        startdate = datetimeutil.pydate2wxdate(datetimeutil.get_month_day_range(datetime.datetime.now())[0])
        self.dpStartdate.SetValue(startdate)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Cut-off Date :")
        label.SetHelpText("Ending date")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        description = name
        self.dpCutOff = EnterDatePicker(self, size=(100,-1),
                        style = wx.DP_DROPDOWN
                              | wx.DP_SHOWCENTURY)
       
        box.Add(self.dpCutOff, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
    
    
        btnsizer = wx.StdDialogButtonSizer()        
        self.Okbtn = wx.Button(self, wx.ID_OK)
        self.Okbtn.SetHelpText("The OK button completes the dialog")
        #self.Okbtn.SetDefault()
        btnsizer.AddButton(self.Okbtn)

        self.Okbtn.Bind( wx.EVT_BUTTON, self.CloseWin )


        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")
        
        btnsizer.AddButton(btn)
        
        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.CENTER|wx.ALL, 5)        
        
        self.SetSizer( sizer )
        self.Layout()
        
        self.Centre( wx.BOTH )
        
    def selectCallback(self,values):
        #data = self.journals.get_one_account(values[0]) 
        print values[0]     

    
    def __del__( self ):
        pass
    
    
    def GetName(self):
        return self.descript.GetValue()
    
    def getInfo(self):
        return {'startdate':self.dpStartdate.GetValue().Format("%Y-%m-%d"),'cutoff_date':self.dpCutOff.GetValue().Format("%Y-%m-%d"),'cost_center_id':self.cost_center_selected}

    def theclose(self,evt):
        pass
        
    def CloseWin(self,evt):
        if self.Validate():
            evt.Skip()
            self.EndModal(wx.ID_OK)



class MyFrame(wx.Frame):
    #----------------------------------------------------------------------
    def __init__(self):
        dlg = incomeStatementDialog(None)    
        if dlg.ShowModal() == wx.ID_OK:
            date = dlg.getInfo()

        
class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
