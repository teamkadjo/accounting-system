

import wx
import wx.dataview as dv
from AccountInfo import *


class AccountManager:
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book
        self.the_panel = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.account_info = AccountInfo()

        account_list = {}
        for u in self.account_info.get_list():
            account_list[u['user_id']] = (u['username'],u['firstname'],u['lastname'])
        account_list = account_list.items()
        account_list.sort()
        account_list = [[str(k)] + list(v) for k,v in account_list]

        # create the listctrl
        self.dvlc = dvlc = dv.DataViewListCtrl(self.the_panel)

        # Give it some columns.
        # The ID col we'll customize a bit:
        dvlc.AppendTextColumn('id', width=40)
        dvlc.AppendTextColumn('username', width=170)
        dvlc.AppendTextColumn('firstname', width=260)
        dvlc.AppendTextColumn('lastname', width=80)
        dvlc.Bind(wx.dataview.EVT_DATAVIEW_ITEM_ACTIVATED, self.OnDoubleClick)

        # Load the data. Each item (row) is added as a sequence of values
        # whose order matches the columns
        for itemvalues in account_list:
            dvlc.AppendItem(itemvalues)

        # Set the layout so the listctrl fills the panel
        self.the_panel.Sizer = wx.BoxSizer(wx.VERTICAL)
        self.the_panel.Sizer.Add(self.dvlc, 1, wx.EXPAND)
        
        b1 = wx.Button(self.the_panel, label="New View", name="newView")
        self.the_panel.Bind(wx.EVT_BUTTON, self.OnNewView, b1)
        b2 = wx.Button(self.the_panel, label="Add Row")
        self.the_panel.Bind(wx.EVT_BUTTON, self.OnAddRow, b2)
        b3 = wx.Button(self.the_panel, label="Delete Row(s)")
        self.the_panel.Bind(wx.EVT_BUTTON, self.OnDeleteRows, b3)        
        

        btnbox = wx.BoxSizer(wx.HORIZONTAL)
        btnbox.Add(b1, 0, wx.LEFT|wx.RIGHT, 5)
        btnbox.Add(b2, 0, wx.LEFT|wx.RIGHT, 5)
        btnbox.Add(b3, 0, wx.LEFT|wx.RIGHT, 5)
        self.the_panel.Sizer.Add(btnbox, 0, wx.TOP|wx.BOTTOM, 5)


        #self.the_panel.Sizer.Add(dvlc, 1, wx.ALL|wx.EXPAND)

  
    def OnNewView(self,event):
        pass
    
    def OnAddRow(self,event):
        print "Add Row"
        pass
    
    def OnDeleteRows(self,event):
        pass
    
    
    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()        
    
    def return_panel(self):        
        return self.the_panel

def CreateAccountManager(the_parent_window):

   panel = AccountManager(the_parent_window)
   return panel.return_panel()
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
