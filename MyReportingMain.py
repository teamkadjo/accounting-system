'''
Created on Dec 15, 2013

@author: jojo
'''
#!/usr/bin/python

# -*- coding: utf-8 -*- 

import wx
from LogoPanel import * 
from PanelSample import *
from employee_panel import *
from employee_details import * 
from AccountManager import *
from AccountDetails import * 
from Password import * 
from AccountInfo import *
from objListPanel import *

try:
    import agw.flatnotebook as FNB
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatnotebook as FNB


class MyReportingMain ( wx.Frame ):
    
    def __init__( self,parent):
        
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = "RDEMC Accounting System", pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
        self.userInfo = AccountInfo()
        #self.Maximize(True)
        self._newPageCounter = 0        
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

        self.LayoutItems()

        # statusbar fields
        statusbar_fields = [("TopSoftDev Solutions Team @ 2014"),
                            ("RDEMC Accounting System")]
                            
        
        self._ImageList = wx.ImageList(16, 16)

        img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)   
        
        self._ImageList.Add(new_img)

        img = wx.Image("icon/gnome-icon/Gnome-Audio-Card.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)   
        
        self._ImageList.Add(new_img)
        
        img = wx.Image("icon/gnome-icon/Gnome-Applications-Science.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)   
                
        self._ImageList.Add(new_img)        
       
        self.book.SetImageList(self._ImageList) 
        
        custom = LogoPanel(self.book, -1)
        self.book.SetCustomPage(custom)   
        
        style = self.book.GetAGWWindowStyleFlag()
        style |= FNB.FNB_X_ON_TAB
        self.book.SetAGWWindowStyleFlag(style)                 
        
        self.m_statusBar1 = self.CreateStatusBar( 2, wx.ST_SIZEGRIP, wx.ID_ANY )
        self.m_statusBar1.SetStatusWidths([-2, -1])
        for i in range(len(statusbar_fields)):
            self.m_statusBar1.SetStatusText(statusbar_fields[i], i)

        self.m_menubar1 = wx.MenuBar( 0 )
        self.m_menu1 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Members", "Members Information Editing/Updating", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem1 )
        self.m_menu1.AppendSeparator()
        self.Bind(wx.EVT_MENU,self.ShowObjDetails,id=self.m_menuItem1.GetId())
        
 
        self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Suppliers", "Suppliers Information Editing/Updating", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem2 )
        #self.Bind(wx.EVT_MENU,self.OpenAccount,id=self.m_menuItem2.GetId())

        self.m_menu1.AppendSeparator()   
        self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Cost Centers", "Cost Center Information Editing/Updating", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem3 )
        #self.Bind(wx.EVT_MENU,self.OpenAccount,id=self.m_menuItem2.GetId())
        
        
        self.m_menu1.AppendSeparator()        
        self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem3 )
        self.Bind( wx.EVT_MENU, self.OnExit, id = self.m_menuItem3.GetId() )
        
        self.m_menubar1.Append( self.m_menu1, u"File" ) 
        
        self.m_menu4 = wx.Menu()
        self.m_menuItem4 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Journal Entry Manager", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem4 )
        
        self.m_menuItem5 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Check Voucher Manager", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem5 )
        
        self.m_menuItem6 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Reports", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem6 )
        
        self.m_menubar1.Append( self.m_menu4, u"General Ledger" ) 
        
        self.m_menu2 = wx.Menu()
        self.m_menuItem7 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Options", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem7 )
        
        self.m_menuItem8 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Check for Updates", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem8 )
        
        self.m_menuItem9 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem9 )
        
        self.m_menubar1.Append( self.m_menu2, u"Help" ) 
        
        self.SetMenuBar( self.m_menubar1 )
        

        
        self.m_toolBar1 = self.CreateToolBar( wx.TB_HORIZONTAL, wx.ID_ANY ) 

        
        
        
        _icon = wx.EmptyIcon()
        _icon.CopyFromBitmap(wx.Bitmap("icon/ACDC-Tux.ico", wx.BITMAP_TYPE_ANY))
        
        img = wx.Image("icon/gnome-icon/Gnome-Applications-Science.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(64,64)
        new_img = wx.BitmapFromImage(img)
      
        
        #img = wx.Image('icon/ACDC-Tux.ico', wx.BITMAP_TYPE_ANY)
        cbID = wx.NewId()
        tsize = (128,128)
        #new_bmp =  wx.ArtProvider.GetBitmap("icon/ACDC-Tux.ico", wx.ART_TOOLBAR, tsize)
        #self.m_toolBar1.AddLabelTool(10, "New", new_bmp, shortHelp="New", longHelp="Long help for 'New'")
        self.m_toolBar1.SetToolBitmapSize((32,32))
        self.m_toolBar1.AddLabelTool(3, '', new_img,shortHelp="New",longHelp="New Employee")

        # Final thing to do for a toolbar is call the Realize() method. This
        # causes it to render (more or less, that is).
        self.m_toolBar1.Realize()
        
        self.Centre( wx.BOTH )

    def ToggleMenu(self,menu_status):
        self.m_menubar1.EnableTop(0, menu_status) # disable the first menu
        self.m_menubar1.EnableTop(1, menu_status)  # second
        self.m_menubar1.EnableTop(2, menu_status) # third menu

    def AccountDetails(self,employee_id):
        self.Freeze()
        image = -1
        img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)        
#         image = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
#         image.Resize(16,16)
        self.book.AddPage(CreateAccountDetails(self), "Account Information", True, 1)
        self.Thaw()
        self._newPageCounter = self._newPageCounter + 1

    def ShowObjDetails(self,employee_id):
        self.Freeze()
        image = -1
        img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)        
#         image = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
#         image.Resize(16,16)
        self.book.AddPage(CreateObjListWindow(self), "ObjListWindow", True, 1)
        self.Thaw()
        self._newPageCounter = self._newPageCounter + 1
    
    def OpenAccount(self,evt):
        self.Freeze()
        image = -1
        self.book.AddPage(CreateAccountManager(self), "Account List", True, 0)
        self.Thaw()
        self._newPageCounter = self._newPageCounter + 1 
    
        

    def EmployeeDetails(self,event):
        caption = "Record Payment Sample #" + str(self._newPageCounter)

        self.Freeze()

        image = -1
        #if self._bShowImages:
        #image = random.randint(0, self._ImageList.GetImageCount()-1)

        self.book.AddPage(self.CreateSample("caption"), "Employee Information", True, image)
        self.Thaw()
        self._newPageCounter = self._newPageCounter + 1
        
        
    def OnAddPage(self, event):

        caption = "New Page Added #" + str(self._newPageCounter)

        self.Freeze()

        image = -1
        #if self._bShowImages:
            #image = random.randint(0, self._ImageList.GetImageCount()-1)

        self.book.AddPage(self.CreatePage(caption), caption, True, image)
        self.Thaw()
        self._newPageCounter = self._newPageCounter + 1


    def CreatePage(self, caption):
        p = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        wx.StaticText(p, -1, caption, (20,20))
        wx.TextCtrl(p, -1, "", (20,40), (150,-1))
        return p

    def CreateSample(self,caption):

       panel = employee_panel(self.book,self)
       return panel.return_panel()

    def CheckPassword(self,username,password):
        check_passwd = self.userInfo.check_credential(username,password)
        if (self.userInfo.getCount() < 3) :
            
            if (check_passwd):
                print "Password ok"
                self.ToggleMenu(True)
                return True
            else:
                print "Password not ok"
                self.userInfo.increment_count()
                return False
        else:
            self.Close()


    def LayoutItems(self):

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)

        bookStyle = FNB.FNB_NODRAG

        self.book = FNB.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        bookStyle &= ~(FNB.FNB_NODRAG)
        bookStyle |= FNB.FNB_ALLOW_FOREIGN_DND 
        self.secondBook = FNB.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        # Set right click menu to the notebook
        #self.book.SetRightClickMenu(self._rmenu)

        # Set the image list 
        #self.book.SetImageList(self._ImageList)
        mainSizer.Add(self.book, 6, wx.EXPAND)

        # Add spacer between the books
        spacer = wx.Panel(self, -1)
        spacer.SetBackgroundColour(wx.SystemSettings_GetColour(wx.SYS_COLOUR_3DFACE))
        mainSizer.Add(spacer, 0, wx.ALL | wx.EXPAND)

        #mainSizer.Add(self.secondBook, 2, wx.EXPAND)

        ## Add some pages to the second notebook
        self.Freeze()

        #text = wx.TextCtrl(self.secondBook, -1, "Second Book Page 1\n", style=wx.TE_MULTILINE|wx.TE_READONLY)  
        #self.secondBook.AddPage(text, "Second Book Page 1")
 
        #text = wx.TextCtrl(self.secondBook, -1, "Second Book Page 2\n", style=wx.TE_MULTILINE|wx.TE_READONLY)
        #self.secondBook.AddPage(text,  "Second Book Page 2")

        self.Thaw() 

        mainSizer.Layout()
        self.SendSizeEvent()
        
            
    def __del__( self ):
        pass

    def OnExit(self, evt):
        self.Close(True)

#from singleinstance import singleinstance
#from sys import exit
 
 # do this at beginnig of your application
#myapp = singleinstance()

# check is another instance of same program running
#if myapp.aleradyrunning():
#    print "Another instance of this program is already running"
#    exit(0)
   
class MyApp(wx.App):
    def OnInit(self):
        frame = MyReportingMain(None)
        #frame.ToggleMenu(False)
        frame.Show(True)
        #f = passWord(None,frame)
        #f.ShowModal()            
        return True

app = MyApp(None)
app.MainLoop()
