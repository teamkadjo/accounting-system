import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *
from specialolv import *

        
class optionPanel(wxSpecialPanel):
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book
      
        super(optionPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("OptionInfo")
        self.Contacts = Contacts()
        self.contacts_ = self.Contacts.db.contacts

        self.setEscapeHandler(self.CloseThisPanel)   

        self.dataOlv = SpecialOLV(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.dataOlv.SetEmptyListMsg("There are no transactions posted yet.")
        self.dataOlv.SetEmptyListMsgFont(wx.FFont(24, wx.DEFAULT, face="Tekton"))
        
        mainSizer = wx.BoxSizer(wx.HORIZONTAL)  
        mainSizer.Add(self.dataOlv, 1, wx.ALL|wx.EXPAND, 5)

        self.SetSizer(mainSizer) 

    
               
    def updateOptionInfo(self,evt):  
        type_window = "Member information of " + self.firstname.GetValue() + " " + self.lastname.GetValue()
        if self.member_id:    
            data = {'id':self.member_id,'firstname':self.firstname.GetValue(),'lastname':self.lastname.GetValue(),'city_town':self.city.GetValue(),'state_province':self.province.GetValue(),'zipcode':self.zipcode.GetValue(),'address1':self.address1.GetValue()}
            self.Contacts.updateOptionInfo(data)
            self.parent_window.refreshPage('Members')
            dlg = wx.MessageDialog(self.parent_window, "Finished updating...", "Updating " + type_window,style=wx.OK|wx.CENTRE)
            dlg.ShowModal()             
        else:
            data = {'id':None,'firstname':self.firstname.GetValue(),'lastname':self.lastname.GetValue(),'city_town':self.city.GetValue(),'state_province':self.province.GetValue(),'zipcode':self.zipcode.GetValue(),'address1':self.address1.GetValue()}
            self.member_id = self.Contacts.addMemberRecord(data)
            self.parent_window.refreshPage('Members')           
            dlg = wx.MessageDialog(self.parent_window, "Finished saving...", "Saving " + type_window,style=wx.OK|wx.CENTRE)
            self.btn_Update.SetLabel("Update (Ctr-S)")
            dlg.ShowModal()


    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("OptionInfo",True)  
        

    def CloseNow(self,evt):
        self.CloseThisPanel()

    
    def return_panel(self):        
        return self

def CreateOptionPanel(the_parent_window):
   panel = optionPanel(the_parent_window)
   return panel
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
