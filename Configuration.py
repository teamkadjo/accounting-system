'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: Configuration.py
'''
import sys
from db import *
import sqlalchemy
import traceback

class Configuration:
    def __init__(self):
        self.db = None
        try :
            self.configuration = DatabaseMySQL.configuration
            self.db = DatabaseMySQL
        except:
            traceback.print_exc()
            print "Serious connection problem occured"
            return None
        
        self.counter = 0
        return None
        
    def getConfig(self,name):
        try:            
            result = self.configuration.filter(self.configuration.configuration_key==name).one()
            return result.configuration_value
        except:
            print repr(traceback.extract_stack())
            return ''


    def updateConfig(self,name,value):
        try:            
            result = self.configuration.filter(self.configuration.configuration_key==name).update({self.configuration.configuration_value:value})
        except:
            return None
