from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
import os,sys
from numberword import *


class CheckPrinting:
    def __init__(self,filename='test.pdf',info=None):       
        self.canvas = canvas.Canvas(filename, pagesize=letter)
        self.filename = filename
        self.info = info

    def create_pdf(self):
        self.canvas.translate(14 * cm,7.5 * cm)
        self.canvas.rotate(90)
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Courier-Bold', 10)
        #canvas.setPageSize((7.5*cm,20*cm))
        name = self.info["name"].upper()
        amount_in_words = get_number_as_words(self.info["amount"]).upper()
        amount_comma = "{:,.2f}".format(self.info['amount'])
        
        space = 180
        
        self.canvas.drawString(2.5 * cm ,4.5 * cm + space, "**** " + name + " ****")
        self.canvas.drawString(5.5 * cm ,3.5 * cm + space, amount_in_words + " ONLY")
        self.canvas.drawString(14.5 * cm ,5.5 * cm + space , self.info["date"])
        self.canvas.drawString(16 * cm ,4.5 * cm + space,amount_comma)
        self.canvas.drawString(0* cm,0.5* cm + space,self.info['cvnumber'])
    
    def open_pdf(self):
        os.system("start " + self.filename)


if __name__ == "__main__":
    info = {"name":'Justin Bieber','amount':1000.00,'date':'September 11, 2015','cvnumber':'CV-4722'}
    chk = CheckPrinting('test.pdf',info)
    chk.create_pdf()
    chk.open_pdf()


