import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *
from specialolv import *
from pprint import pprint
from Users import *
from Journals import *
from Contacts import *

class InventoryEntryPanel(wxSpecialPanel):
    def __init__(self,parent_window):
        self.parent_window = parent_window
        self.book = parent_window.book
        self.journals = Journals()
        self.contacts = Contacts()
        self.cost_centers = self.contacts.get_costcenters()

        super(InventoryEntryPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("InventoryEntryPanel")
        self.users = Users()


        self.setEscapeHandler(self.CloseThisPanel)

        sizerTop = wx.FlexGridSizer( 0, 2, 0, 0 )


        administrator_entry = wx.StaticText(self, -1, "Administrator Entry :" )
        self.admin_entry = EnterTextCtrl(self, -1, "",(600,20), (150,-1))

        sizerTop.Add(administrator_entry, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.admin_entry, -1, wx.ALL|wx.CENTER, 5)

        bakery_entry = wx.StaticText(self, -1, "Bakery Entry :" )
        self.bakery_entry = EnterTextCtrl(self, -1, "",(600,20), (150,-1))

        sizerTop.Add(bakery_entry, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.bakery_entry, -1, wx.ALL|wx.CENTER, 5)

        canteen_entry = wx.StaticText(self, -1, "Canteen Entry :" )
        self.canteen_entry = EnterTextCtrl(self, -1, "",(600,20), (150,-1))

        sizerTop.Add(canteen_entry, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.canteen_entry, -1, wx.ALL|wx.CENTER, 5)

        trading_entry = wx.StaticText(self, -1, "Trading Entry :" )
        self.trading_entry = EnterTextCtrl(self, -1, "",(600,20), (150,-1))

        sizerTop.Add(trading_entry, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.trading_entry, -1, wx.ALL|wx.CENTER, 5)


        updateLabel = "Update (Ctr-S)"

        self.btn_Update = wx.Button(self, -1, updateLabel)
        self.btn_Cancel = wx.Button(self, -1, "Cancel (Esc)")
        self.btnIncomeStatement = wx.Button(self, -1, "Open Income Statement")
        self.Bind(wx.EVT_BUTTON, self.updateInventory, self.btn_Update)
        self.Bind(wx.EVT_BUTTON, self.CloseNow, self.btn_Cancel)
        self.Bind(wx.EVT_BUTTON, self.OpenIncomeStatement, self.btnIncomeStatement)


        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer.Add(self.btn_Update, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btn_Cancel, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btnIncomeStatement, 0, wx.ALL|wx.LEFT, 5)


        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(sizerTop, 0, wx.TOP|wx.LEFT|wx.BOTTOM, 5)
        mainSizer.Add(btnsizer, 0, wx.LEFT|wx.BOTTOM, 5)

        self.SetSizer(mainSizer)

        self.userInfo = self.parent_window.userInfo

        print self.userInfo.admin_name

        self.loadInfo()


    def OpenIncomeStatement(self,evt):
        self.parent_window.openIncomeStatement(evt)



    def loadInfo(self):
        inventory_info = self.journals.getInventoryEntry()
        #print inventory_info
        self.admin_entry.SetValue("{:.2f} ".format(inventory_info['admin_value']))
        self.bakery_entry.SetValue("{:.2f} ".format(inventory_info['bakery_value']))
        self.canteen_entry.SetValue("{:.2f} ".format(inventory_info['canteen_value']))
        self.trading_entry.SetValue("{:.2f} ".format(inventory_info['trading_value']))

    def isBlank(self):
        return (self.bakery_entry.GetValue().strip() == "" or  self.canteen_entry.GetValue().strip() == "" or  self.admin_entry.GetValue().strip() == "" or  self.trading_entry.GetValue().strip() == "")


    def isPasswordMatched(self):
        return self.password1.GetValue() == self.password2.GetValue()


    def updateInventory(self,evt):
        if (not self.isBlank()):
            data = {}


            admin_entry_value = self.admin_entry.GetValue().strip()
            bakery_entry_value = self.bakery_entry.GetValue().strip()
            canteen_entry_value = self.canteen_entry.GetValue().strip()
            trading_entry_value = self.trading_entry.GetValue().strip()

            is_ok = True
            try:
                self.journals.update_inventory_month(admin_entry_value,bakery_entry_value,canteen_entry_value,trading_entry_value)
                is_ok = True
            except:
                is_ok = False

            if is_ok:
                dlg = wx.MessageDialog(self.parent_window, "Successfully Updated the Inventory Entries" , "Inventory Updated!",style=wx.ICON_WARNING|wx.CENTRE)
                dlg.ShowModal()

        else:
            dlg = wx.MessageDialog(self.parent_window, "Your password should matched or the password you provide is empty.", "Password Mismatched!",style=wx.ICON_WARNING|wx.CENTRE)
            dlg.ShowModal()


    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("InventoryEntryPanel",True)


    def CloseNow(self,evt):
        self.CloseThisPanel()


    def return_panel(self):
        return self

def CreateInventoryEntryPanel(the_parent_window):
   panel = InventoryEntryPanel(the_parent_window)
   return panel

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
