#!/usr/bin/python
'''
// +-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2018 PyBooks (demo.online-acctg.com/pybooks)       |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: AccountingSystemMain.py
'''
#*- coding: utf-8 -*-

import wx
from LogoPanel import *
from PanelSample import *
from employee_panel import *
from employee_details import *
from AccountManager import *
from AccountDetails import *
from Password import *
from AccountInfo import *
from objListPanel import *
from Members import *
from Suppliers import *
from CostCenters import *
from JournalEntry import *
from CheckVouchers import *
from EntryWindow import *
from InventoryPanel import *
from Users import *
from reportPanel import *
from reportEntryWindow import *
from reportEntryWindow2 import *
from Configuration import *
from memberLedger import *
from MemberInfo import *
from arlisting import *
from arDateDialog import *
import images2
from optionPanel import *
from pprint import *
from  slDateDialog import *
from wx.lib.wordwrap import wordwrap
from ledgerListing import *
from generalSettings import *
from profilePanel import *
from accountsTransactionDialog import *
from datetimeutil import *
from accountlistingsreport import *
from trialbalanceDialog import *
from trialbalancereport import *
from balanceSheetDialog import *
from balance_sheet import *
from income_statement_detail import *
import os
from pdfPanel import *
from roleSetting import *
from InventoryGrid import *
from InventoryEntryPanel import *
from incomeStatementDialog import *
from backupdb import *
from arListingTypeDialog import *
import pprint
from arlisting_per_type import *
import datetime
from JournalsTransactionDialog import *
from jvlisting_rangereport import *
from cvlisting_rangereport import *
from ChartOfAccounts import *
from accountsPanel import *
import traceback
from accountAgingDialog import *
from aging_report import *
from CostCenterInfoPanel import *
from InventoryEntryEditorPanel import *
import datetime


try:
    import agw.flatnotebook as FNB
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatnotebook as FNB


class AccountingSystemMain ( wx.Frame ):

    def __init__( self,parent):

        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = "RDEMPC Accounting System", pos = wx.DefaultPosition,  style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL |  wx.MAXIMIZE )
        self.userInfo = AccountInfo()
        #self.Maximize(True)
        self._newPageCounter = 0
        self.loginOk = False

        config = Configuration()

        self.companyName =  config.getConfig("COMPANY_NAME")

        self.SetTitle(self.companyName)

        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

        self.LayoutItems()


        # statusbar fields
        statusbar_fields = [(self.companyName),
                            ("TopSoftDev team")]


        self.ico = wx.Icon('icon/desktop-icon/ico/card file.ico', wx.BITMAP_TYPE_ICO)
        self.SetIcon(self.ico)
        #self.set_icon

        self._ImageList = wx.ImageList(16, 16)

        img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)

        self._ImageList.Add(new_img)

        img = wx.Image("icon/gnome-icon/Gnome-Audio-Card.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)

        self._ImageList.Add(new_img)

        img = wx.Image("icon/gnome-icon/Gnome-Applications-Science.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(16,16)
        new_img = wx.BitmapFromImage(img)

        self._ImageList.Add(new_img)

        self.book.SetImageList(self._ImageList)

        custom = LogoPanel(self.book, -1)
        self.book.SetCustomPage(custom)

        style = self.book.GetAGWWindowStyleFlag()
        style |= FNB.FNB_X_ON_TAB
        self.book.SetAGWWindowStyleFlag(style)

        self.m_statusBar1 = self.CreateStatusBar( 2, wx.ST_SIZEGRIP, wx.ID_ANY )
        self.m_statusBar1.SetStatusWidths([-2, -1])
        for i in range(len(statusbar_fields)):
            self.m_statusBar1.SetStatusText(statusbar_fields[i], i)

        self.m_menubar1 = wx.MenuBar( 0 )
        self.m_menu1 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Contacts Manager", "Contact  Information Editing/Updating", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem1 )
        id_memberPage = self.m_menuItem1.GetId()
        self.Bind(wx.EVT_MENU,self.OpenMemberPage,id=id_memberPage)


        self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Cost Centers", "Cost Center Information Editing/Updating", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem3 )
        self.Bind(wx.EVT_MENU,self.OpenCostCenters,id=self.m_menuItem3.GetId())


        self.m_menuItem33 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Chart of Accounts", "Chart of Accounts", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem33 )
        self.Bind(wx.EVT_MENU,self.ChartOfAccounts,id=self.m_menuItem33.GetId())

        self.m_menu1.AppendSeparator()
        self.m_menuItem4 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Log Out", "Logging out", wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem4 )
        self.Bind(wx.EVT_MENU,self.LogOut,id=self.m_menuItem4.GetId())


        self.m_menu1.AppendSeparator()
        self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem3 )
        self.Bind( wx.EVT_MENU, self.OnExit, id = self.m_menuItem3.GetId() )
        self.Bind(wx.EVT_CLOSE,self.OnExit)
        self.m_menubar1.Append( self.m_menu1, u"File" )

        self.m_menu4 = wx.Menu()
        self.m_menuItem4 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Journal Entry Manager", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem4 )
        je_manager_id = self.m_menuItem4.GetId()
        self.Bind( wx.EVT_MENU, self.JournalManager, id = je_manager_id )


        self.m_menuItem5 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Check Voucher Manager", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem5 )
        cv_manager_id = self.m_menuItem5.GetId()
        self.Bind( wx.EVT_MENU, self.CheckVoucherManager, id = cv_manager_id )

        self.m_menuItem6 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Inventory Entry", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem6 )
        inventoryentry_page = self.m_menuItem6.GetId()
        self.Bind( wx.EVT_MENU, self.InventoryEntryPageOrig, id = inventoryentry_page )

        self.m_menubar1.Append( self.m_menu4, u"General Ledger" )

        self.m_menu6 = wx.Menu()
        self.m_menuCreditReport = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Accounts Receivable Listing", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuCreditReport )
        credit_report_menu = self.m_menuCreditReport.GetId()
        self.Bind( wx.EVT_MENU, self.CreditReport, id = credit_report_menu )

        #self.m_menu66 = wx.Menu()
        self.m_menuCreditReport2 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Accounts Receivable Listing per Type", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuCreditReport2 )
        ar_list_type = self.m_menuCreditReport2.GetId()
        self.Bind( wx.EVT_MENU, self.arList_Type, id = ar_list_type )

        self.m_menu6.AppendSeparator()


        self.m_menuAccountsListing = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Accounts Transaction Listing", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuAccountsListing )
        accounts_listing_menu = self.m_menuAccountsListing.GetId()
        self.Bind( wx.EVT_MENU, self.OpenActListing, id = self.m_menuAccountsListing.GetId() )


        self.m_menuItem5 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Trial Balance", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuItem5 )
        trial_balance_id = self.m_menuItem5.GetId()
        self.Bind( wx.EVT_MENU, self.openTrialBalance, id = trial_balance_id )

        self.m_menuItem9 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Income from Operation Statement", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuItem9 )
        income_statement_id = self.m_menuItem9.GetId()
        self.Bind( wx.EVT_MENU, self.openIncomeStatement, id = income_statement_id )

        self.m_menuItem5 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Financial Condition Statement", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuItem5 )
        cv_manager_id = self.m_menuItem5.GetId()
        self.Bind( wx.EVT_MENU, self.openBalanceSheet, id = cv_manager_id )

        self.m_menu6.AppendSeparator()

        self.m_menuItem5 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Journal Book Listing", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuItem5 )
        self.Bind(wx.EVT_MENU,self.OpenJournalTransactionsListing,id = self.m_menuItem5.GetId())

        self.m_menuItem5 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Disbursement Book Listing", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuItem5 )
        #cv_manager_id = self.m_menuItem5.GetId()
        self.Bind( wx.EVT_MENU, self.OpenCVTransactionsListing, id = self.m_menuItem5.GetId() )

        self.m_menu6.AppendSeparator()

        self.m_menuItem5 = wx.MenuItem( self.m_menu6, wx.ID_ANY, u"Aging of Accounts", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu6.AppendItem( self.m_menuItem5 )
        #cv_manager_id = self.m_menuItem5.GetId()
        self.Bind( wx.EVT_MENU, self.OpenAccountAgingDialog, id = self.m_menuItem5.GetId() )


        self.m_menubar1.Append( self.m_menu6, u"Reports" )


        self.m_menu2 = wx.Menu()
        self.m_company = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"General Company Settings", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_company )
        self.Bind( wx.EVT_MENU, self.generalSettings, id =  self.m_company.GetId() )


        self.m_profile = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"My Profile", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_profile )
        self.Bind( wx.EVT_MENU, self.profilePanelWindow, id =  self.m_profile.GetId() )


        self.m_role = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Roles", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_role )
        self.Bind( wx.EVT_MENU, self.settingRoles, id = self.m_role.GetId() )

        self.m_users = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"User Management", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_users )
        self.Bind( wx.EVT_MENU, self.AboutBoxShow, id = self.m_users.GetId() )

        self.m_menubar1.Append( self.m_menu2, u"Company" )



        self.m_menu2 = wx.Menu()

        self.m_menuItem9 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Backup Database", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem9 )
        self.Bind( wx.EVT_MENU, self.BackupDatabase, id = self.m_menuItem9.GetId() )

        self.m_menubar1.Append( self.m_menu2, u"Tools" )




        self.m_menu2 = wx.Menu()

        self.menuOptionPanel = wx.Menu()
        self.m_menuItem7 = wx.MenuItem( self.menuOptionPanel, wx.ID_ANY, u"Options", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem7 )
        openOptionswindow = self.m_menuItem7.GetId()
        self.Bind( wx.EVT_MENU, self.openOptions, id = openOptionswindow )


        self.m_menuItem8 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Check for Updates", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem8 )

        self.m_menuItem9 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem9 )
        self.Bind( wx.EVT_MENU, self.AboutBoxShow, id = self.m_menuItem9.GetId() )

        self.m_menubar1.Append( self.m_menu2, u"Help" )

        self.SetMenuBar( self.m_menubar1 )



        self.m_toolBar1 = self.CreateToolBar( wx.TB_HORIZONTAL, wx.ID_ANY )

        _icon = wx.EmptyIcon()
        _icon.CopyFromBitmap(wx.Bitmap("icon/ACDC-Tux.ico", wx.BITMAP_TYPE_ANY))


        cbID = wx.NewId()
#******************** toolbar **************************
        img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(64,64)
        new_img = wx.BitmapFromImage(img)
        self.m_toolBar1.SetToolBitmapSize((32,32))
        self.m_toolBar1.AddLabelTool(id_memberPage, 'Contact List Manager (J)', new_img,shortHelp="Contact List Manager (Alt-M)",longHelp="Contact List Manager")
        self.Bind(wx.EVT_TOOL, self.OpenMemberPage, id=id_memberPage)

        img = wx.Image("icon/gnome-icon/Emblem-Money.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(64,64)
        new_img = wx.BitmapFromImage(img)
        idJournalEntry = wx.NewId()
        self.m_toolBar1.SetToolBitmapSize((32,32))
        self.m_toolBar1.AddLabelTool(idJournalEntry, 'Journal Entry (Alt-J)', new_img,shortHelp="Journal Entry (Alt-J)",longHelp="Journal Voucher Manager")
        self.Bind(wx.EVT_TOOL, self.JournalManager, id=idJournalEntry)

        img = wx.Image("icon/desktop-icon/ico/money.ico", wx.BITMAP_TYPE_ANY)
        img = img.Scale(64,64)
        new_img = wx.BitmapFromImage(img)
        idCheckVoucher = wx.NewId()
        self.m_toolBar1.SetToolBitmapSize((32,32))
        self.m_toolBar1.AddLabelTool(idCheckVoucher, 'Check Voucher (Alt-C)', new_img,shortHelp="Check Voucher (Alt-C)",longHelp="Check Voucher Manager")
        self.Bind(wx.EVT_TOOL, self.CheckVoucherManager, id=idCheckVoucher)

        # Final thing to do for a toolbar is call the Realize() method. This
        # causes it to render (more or less, that is).
        self.m_toolBar1.Realize()

#*******************************************************
        self.Centre( wx.BOTH )

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_ALT,  ord('M'), id_memberPage ),(wx.ACCEL_ALT,  ord('C'), idCheckVoucher ),(wx.ACCEL_ALT, ord('J'), idJournalEntry )])
        self.SetAcceleratorTable(accel_tbl)


    def arList_Type(self,evt):
        if self.checkIfAlreadyInTab("arlistingtype.pdf"):
            pass
        else:
            dlg = arListingTypeDialog(None)
            if dlg.ShowModal() == wx.ID_OK:
                arInfo = dlg.getInfo()
                #pprint.pprint(arInfo)

                #{'account_id': u'21007', 'cutoff_date': u'2016-08-02'}
                arListing_Per_Type(pdf_name='arlistingpertype.pdf',title="AR Listing Per Type",cutoff_date=arInfo['cutoff_date'],account_id=arInfo['account_id'])
                self.SpecialReportWindow("AR Listing Per Type","arlistingpertype.pdf")

    #arListing_Per_Type('aclistingreport.pdf','Journal Voucher ','2016-06-7')
    #os.system('start aclistingreport.pdf')


    def openBalanceSheet(self,evt):
        if self.checkIfAlreadyInTab("balancesheetreport.pdf"):
            pass
        else:
            dlg = balanceSheetDialog(None)
            if dlg.ShowModal() == wx.ID_OK:
                date = dlg.getInfo()
                cutoff_date = date['cutoff_date']
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Balance Sheet Report ..",icon=images2.book.GetBitmap())
                wx.Yield()
                balanceSheet_report('balancesheetreport.pdf','Balance Sheet',cutoff_date,self.userInfo.admin_name)
                #self.SpecialReportWindow("Balance Sheet Report","balancesheetreport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'balancesheetreport.pdf'), "Balance Sheet Report" , True, 1)
                del busy


    def openTrialBalance(self,evt):
        if self.checkIfAlreadyInTab("trialbalancereport.pdf",True) :
            pass
        else:
            dlg = trialbalanceDialog(None)
            if dlg.ShowModal() == wx.ID_OK:
                date = dlg.getInfo()
                cutoff_date = date['cutoff_date']
                cost_center = date['cost_center_id']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Trial Balance Report ..",icon=images2.book.GetBitmap())
                wx.Yield()
                trialBalance_report('trialbalancereport.pdf','Trial Balance Report',cutoff_date,self.userInfo.admin_name,cost_center)
                #self.ReportWindow("Trial Balance Report","trialbalancereport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'trialbalancereport.pdf'), "Trial Balance Report" , True, 1)
                del busy
                #os.system('start trialbalancereport.pdf')


    def settingRoles(self,evt):
        #for item in self.m_menubar1.GetMenus():
                #for menuitem in item[0].GetMenuItems():
                    #print menuitem.GetId(),menuitem.GetLabel().lower().replace(' ','_')
        #pass

        if self.checkIfAlreadyInTab("roleSetting"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateRoleSettingWindow(self), "Role Settings" , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

#CreateRoleSettingWindow


    def profilePanelWindow(self,evt):
        if self.checkIfAlreadyInTab("profilePanel"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateProfilePanel(self), "My Profile Settings" , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def generalSettings(self,evt):
        if self.checkIfAlreadyInTab("generalSettings"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreategeneralSettingsPanel(self), "General Company Settings" , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def CreditReport(self,evt):
        if self.checkIfAlreadyInTab("arlisting.pdf",True) :
            pass
        else:
            dlg = arDateDialog(None)
            if dlg.ShowModal() == wx.ID_OK:
                date = dlg.getInfo()
                cutoff_date = date['date']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Accounts Receivable Listing..",icon=images2.book.GetBitmap())
                wx.Yield()
                ar_Listing('arlisting.pdf','Accounts Receivable Listing',cutoff_date,self.userInfo.admin_name)
                del busy
                self.SpecialReportWindow("Accounts Receivable Listing","arlisting.pdf")

    def ChartOfAccounts(self,evt):
        if self.checkIfAlreadyInTab("ChartOfAccounts"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateChartOfAccountsWindow(self), "Charts Of Accounts List", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1


    def openIncomeStatement(self,evt):
        if self.checkIfAlreadyInTab("incomestatement.pdf"):
            pass
        else:
            dlg = incomeStatementDialog(None)
            if dlg.ShowModal() == wx.ID_OK:
                date = dlg.getInfo()
                startdate = date['startdate']
                cutoff_date = date['cutoff_date']
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Income Statement Report ..",icon=images2.book.GetBitmap())
                wx.Yield()
                incomeStatement_report('incomestatement.pdf','Balance Sheet',startdate,cutoff_date,self.userInfo.admin_name)
                self.book.AddPage(CreatePDFLoaderWindow(self,'incomestatement.pdf'), "Income Statement  Report" , True, 1)
                del busy

    def BackupDatabase(self,evt):
        doBackup()

    def InventoryEntryPage(self,evt):
        if self.checkIfAlreadyInTab("InventoryEntry"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            title = "Inventory Entry Page"
            #self.book.AddPage(CreateInventoryEntryPanel(self), title , True, 1)
            self.book.AddPage(CreateObjListWindow(self), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1
        pass

    def InventoryEntryPageOrig(self,evt):
        if self.checkIfAlreadyInTab("InventoryEntryGrid"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            title = "Inventory Entry Page"
            # self.book.AddPage(CreateInventoryEntry(self), title , True, 1)
            self.book.AddPage(CreateInventoryPanel(self), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1
        pass




    def OpenActListing(self,evt):
        if self.checkIfAlreadyInTab("aclistingreport.pdf",True) :
            pass
        else:
            (first,last ) = get_month_day_range(datetime.datetime.now())
            current_datetime = datetime.datetime.now()

            the_first = pydate2wxdate(first)
            the_last = pydate2wxdate(current_datetime )


            dlg = accountsTransactionDialog(None)
            dlg.dpc1.SetValue(the_first)
            dlg.dpc2.SetValue(the_last)
            result = dlg.ShowModal()

            if result == wx.ID_OK:
                data = dlg.getInfo()
                starting_date = data['date1']
                ending_date = data['date2']
                account_id = data['account_id']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Accounts Transaction Listing..",icon=images2.book.GetBitmap())
                wx.Yield()


                ac_Listing_Report('aclistingreport.pdf','Accounts Listing',starting_date,ending_date,account_id,self.userInfo.admin_name)
                #self.ReportWindow2("Accounts Transaction Listing","aclistingreport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'aclistingreport.pdf'), "Accounts Listing" , True, 1)
                del busy
                #os.system("start aclistingreport.pdf")

    def OpenJournalTransactionsListing(self,evt):
        if self.checkIfAlreadyInTab("aclistingreport.pdf",True) :
            pass
        else:
            (first,last ) = get_month_day_range(datetime.datetime.now())
            current_datetime = datetime.datetime.now()
            #the_first = pydate2wxdate(first)
            #the_last = pydate2wxdate(last)

            #first = datetime.datetime.strptime("10/1/2015", "%m/%d/%Y")
            #last = datetime.datetime.strptime("10/5/2015", "%m/%d/%Y")

            the_first = pydate2wxdate(first)
            the_last = pydate2wxdate(current_datetime )


            dlg = JournalsTransactionDialog(None)
            dlg.dpc1.SetValue(the_first)
            dlg.dpc2.SetValue(the_last)
            result = dlg.ShowModal()

            if result == wx.ID_OK:
                data = dlg.getInfo()
                starting_date = data['date1']
                ending_date = data['date2']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Journal  Listing Report..",icon=images2.book.GetBitmap())
                wx.Yield()


                jv_Listing_Report('jv_listing_report.pdf','Journal Listing',starting_date,ending_date,self.userInfo.admin_name)
                #self.ReportWindow2("Accounts Transaction Listing","aclistingreport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'jv_listing_report.pdf'), "Journal Listing Report" , True, 1)
                del busy

    def OpenCVTransactionsListing(self,evt):
        if self.checkIfAlreadyInTab("aclistingreport.pdf",True) :
            pass
        else:
            (first,last ) = get_month_day_range(datetime.datetime.now())
            current_datetime = datetime.datetime.now()
            #the_first = pydate2wxdate(first)
            #the_last = pydate2wxdate(last)

            #first = datetime.datetime.strptime("10/1/2015", "%m/%d/%Y")
            #last = datetime.datetime.strptime("10/5/2015", "%m/%d/%Y")

            the_first = pydate2wxdate(first)
            the_last = pydate2wxdate(current_datetime )


            dlg = JournalsTransactionDialog(None)
            dlg.dpc1.SetValue(the_first)
            dlg.dpc2.SetValue(the_last)
            result = dlg.ShowModal()

            if result == wx.ID_OK:
                data = dlg.getInfo()
                starting_date = data['date1']
                ending_date = data['date2']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating CV  Listing Report..",icon=images2.book.GetBitmap())
                wx.Yield()


                cv_Listing_Report('cv_listing_report.pdf','CV Listing',starting_date,ending_date,self.userInfo.admin_name)
                #self.ReportWindow2("Accounts Transaction Listing","aclistingreport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'cv_listing_report.pdf'), "CV Listing Report" , True, 1)
                del busy


    def OpenCVTransactionsListing(self,evt):
        if self.checkIfAlreadyInTab("aclistingreport.pdf",True) :
            pass
        else:
            (first,last ) = get_month_day_range(datetime.datetime.now())
            current_datetime = datetime.datetime.now()
            #the_first = pydate2wxdate(first)
            #the_last = pydate2wxdate(last)

            #first = datetime.datetime.strptime("10/1/2015", "%m/%d/%Y")
            #last = datetime.datetime.strptime("10/5/2015", "%m/%d/%Y")

            the_first = pydate2wxdate(first)
            the_last = pydate2wxdate(current_datetime )


            dlg = JournalsTransactionDialog(None,"Disbursement Listing Dialog Date Range")
            dlg.dpc1.SetValue(the_first)
            dlg.dpc2.SetValue(the_last)
            result = dlg.ShowModal()

            if result == wx.ID_OK:
                data = dlg.getInfo()
                starting_date = data['date1']
                ending_date = data['date2']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Check Disbursement Book Listing ..",icon=images2.book.GetBitmap())
                wx.Yield()


                cv_Listing_Report('cv_listing_report.pdf','Check Disbursement Book Listing',starting_date,ending_date,self.userInfo.admin_name)
                #self.ReportWindow2("Accounts Transaction Listing","aclistingreport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'cv_listing_report.pdf'), "Check Disbursement Book Listing" , True, 1)
                del busy

    def OpenAccountAgingDialog(self,evt):
        if self.checkIfAlreadyInTab("aging_report.pdf",True) :
            pass
        else:
            (first,last ) = get_month_day_range(datetime.datetime.now())
            current_datetime = datetime.datetime.now()
            #the_first = pydate2wxdate(first)
            #the_last = pydate2wxdate(last)

            #first = datetime.datetime.strptime("10/1/2015", "%m/%d/%Y")
            #last = datetime.datetime.strptime("10/5/2015", "%m/%d/%Y")

            the_first = pydate2wxdate(first)
            the_last = pydate2wxdate(current_datetime )


            dlg = agingAccountDialog(None)
            result = dlg.ShowModal()

            if result == wx.ID_OK:
                data = dlg.getInfo()
                cutoff_date = data['cutoff_date']
                evt.Skip()
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Aging of Accounts Listing ..",icon=images2.book.GetBitmap())
                wx.Yield()


                aging_Report('aging_report.pdf','Aging Report Listing',cutoff_date,self.userInfo.admin_name)
                #self.ReportWindow2("Accounts Transaction Listing","aclistingreport.pdf")
                self.book.AddPage(CreatePDFLoaderWindow(self,'aging_report.pdf'), "Aging of Accounts Listing" , True, 1)
                del busy


    def SLReport(self,member_id,gl_account):
        if self.checkIfAlreadyInTab("ledgerlisting.pdf",True) :
            pass
        else:
            dlg = slDateDialog(None)
            if dlg.ShowModal() == wx.ID_OK:
                date = dlg.getInfo()
                date1 = date['date1']
                date2 = date['date2']
                message = "Please wait, working..."
                busy = PBI.PyBusyInfo(message, parent=None, title="Generating Accounts Receivable Listing..",icon=images2.book.GetBitmap())
                wx.Yield()
                ledgerListingReport('ledgerlisting.pdf','Ledger Listing',self.userInfo.admin_name,date1,date2,member_id,gl_account)

                del busy
                self.SpecialReportWindow("Ledger Listing","ledgerlisting.pdf")

    def updateStatusBarText(self,text):
        self.m_statusBar1.SetStatusText(text,1)

    def openOptions(self,text):
        if self.checkIfAlreadyInTab("OptionInfo"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            title = "Option Page Setup"
            self.book.AddPage(CreateOptionPanel(self), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def ToggleMenu(self,menu_status):
        self.m_menubar1.EnableTop(0, menu_status) # disable the first menu
        self.m_menubar1.EnableTop(1, menu_status)  # second
        self.m_menubar1.EnableTop(2, menu_status) # third menu
        self.m_toolBar1.Enable(menu_status)

    def LogOut(self,evt):
        self.book.DeleteAllPages()
        self.ToggleMenu(False)
        f = passWord(None,self)
        self.updateStatusBarText("")
        f.ShowModal()

    def AboutBoxShow(self,evt):
        # First we create and fill the info object
        info = wx.AboutDialogInfo()
        info.Name = "RD EMPC Accounting System"
        info.Version = "1.0.5"
        info.Copyright = "(C) 2016 TopSoftDev Shop"
        info.Description = wordwrap(
            "An Accounting System for RD Employee Multi-Purpose Cooperative",
            400, wx.ClientDC(self))
        info.WebSite = ("http://topsoftdev.kapamilya.info/pybooks", "Accounting System Page")
        info.Developers = [ "jojomaq",
                            "dantem",
                            ]

        licenseText = '''
         RDEMPC Accounting System
Copyright (c) 2016, TopSoftDev Shop
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

        info.License = licenseText
        #wordwrap(licenseText, 500, wx.ClientDC(self))

        # Then we call wx.AboutBox giving it that info object
        wx.AboutBox(info)


    def OpenMemberPage(self,employee_id):
        if self.checkIfAlreadyInTab("Members"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateMemberWindow(self), "Contact List Manager", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1


    def OpenSupplierPage(self,employee_id):
        if self.checkIfAlreadyInTab("Suppliers"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateSupplierWindow(self), "Supplier List", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def OpenCostCenters(self,employee_id):
        if self.checkIfAlreadyInTab("CostCenter"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateCostCenterWindow(self), "CostCenter List", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def OpenMemberLedger(self,employee_id,name,account=None):
        if self.checkIfAlreadyInTab("MemberLedger"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            if account :
                title = " Ledger (%s) : %s " % (account,name)
            else:
                title = " Ledger  : %s " %  name

            self.book.AddPage(CreateMemberLedger(self,employee_id,account), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1


    def OpenMemberInfo(self,employee_id,employee_name):
        if self.checkIfAlreadyInTab("MemberInfo"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            if employee_name :
                title = " Contact Information (%s)  " % (employee_name)
            else:
                title = " Contact Information"

            self.book.AddPage(CreateMemberInfo(self,employee_id), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def OpenCCInfo(self,employee_id,cc_name):
        if self.checkIfAlreadyInTab("CCInfo"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            if cc_name :
                title = " Cost Center  (%s)  " % (cc_name)
            else:
                title = " Cost Center "

            self.book.AddPage(CreateCCInfo(self,employee_id), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def OpenInventoryEntryEditor(self,date_posted,cc_inv_info):
        if self.checkIfAlreadyInTab("InventoryEntryEditor"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            if date_posted :
                title = " Inventory Entry  (%s)  " % (date_posted)
            else:
                title = " Inventory Entry "

            self.book.AddPage(CreateInventoryEntryEditor(self,date_posted,cc_inv_info), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def OpenAccountInfo(self,id_,name):
        if self.checkIfAlreadyInTab("AccountInfo"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            if name :
                title = " Account Name (%s)  " % (name)
            else:
                title = " Account Name"

            self.book.AddPage(CreateAccountInfo(self,id_), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def refreshPage(self,tabname):
        count = -1
        for i in self.book.GetChildren():
            if tabname ==  i.GetLabel():
                print type(i)
                print i.refreshGrid()
        return False



    def checkIfAlreadyInTab(self,tabname,close=False):
        count = -1
        for i in self.book.GetChildren():
            if tabname ==  i.GetLabel():
                if (close):
                    self.book.DeletePage(count-1)
                else:
                    self.book.SetSelection(count-1)
                    return True
            else:
                if i.GetLabel() != "Panel":
                    count = count + 1
        return False

    def OpenEntryWindow(self,ref_id,evt='Journal Entry',acc_type='journal'):
        if self.checkIfAlreadyInTab("EntryWindow",True):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            if ref_id:
                ref_num = GetReferenceNumber(ref_id)
            else:
                ref_num = "New"
            self.book.AddPage(CreateEntryWindowWindow(self,ref_id,acc_type), evt + (" Entry Page (#%s)" % ref_num), True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1


    def JournalManager(self,evt):
        if self.checkIfAlreadyInTab("JournalEntry"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateJournalEntryWindow(self), "Journal Entry Manager", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def ReportManager(self,evt):
        if self.checkIfAlreadyInTab("ReportWindow"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateReportWindow(self), "Report Window Manager", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def ReportWindow(self,title="Report Entry Window",pdf_name="spreadsheet.pdf"):
        if self.checkIfAlreadyInTab("ReportEntryWindow",True):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateReportWindow(self,pdf_name), title, True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def SpecialReportWindow(self,title="Report Entry Window",pdf_name="spreadsheet.pdf"):
        if self.checkIfAlreadyInTab(pdf_name,True):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            #self.book.AddPage(CreateReportWindow(self,pdf_name), title, True, 1)
            self.book.AddPage(CreatePDFLoaderWindow(self,pdf_name), title , True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1


    def ReportWindow2(self,title="Report Entry Window",pdf_name="spreadsheet.pdf"):
        if self.checkIfAlreadyInTab("ReportEntryWindow2",True):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateReportWindow2(self,pdf_name), title, True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1

    def CheckVoucherManager(self,evt):
        if self.checkIfAlreadyInTab("CheckVoucher"):
            pass
        else:
            self.Freeze()
            image = -1
            img = wx.Image("icon/gnome-icon/User-Info.ico", wx.BITMAP_TYPE_ANY)
            img = img.Scale(16,16)
            new_img = wx.BitmapFromImage(img)
            self.book.AddPage(CreateCheckVoucherWindow(self), "Check Voucher Manager", True, 1)
            self.Thaw()
            self._newPageCounter = self._newPageCounter + 1


    def ProduceReport(self,evt):
        for i in self.book.GetChildren():
            print i.GetLabel()
        pass


    def CheckPassword(self,username,password):
        users = Users()
        validate_password = users.validate_user(username,password)
        if (self.userInfo.getCount() < 2) :

            if (validate_password):
                self.ToggleMenu(True)
                self.userInfo = users.get_userinfo(username)
                self.loginOk = True
                self.updateStatusBarText("  Logged in : " + self.userInfo.display_name)
                return True
            else:
                self.userInfo.increment_count()
                return False
        else:
            self.loginOk = False
            self.Close()


    def LayoutItems(self):

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)

        bookStyle = FNB.FNB_NODRAG

        self.book = FNB.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        bookStyle &= ~(FNB.FNB_NODRAG)
        bookStyle |= FNB.FNB_ALLOW_FOREIGN_DND
        self.secondBook = FNB.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        # Set right click menu to the notebook
        #self.book.SetRightClickMenu(self._rmenu)

        # Set the image list
        #self.book.SetImageList(self._ImageList)
        mainSizer.Add(self.book, 6, wx.EXPAND)

        # Add spacer between the books
        spacer = wx.Panel(self, -1)
        spacer.SetBackgroundColour(wx.SystemSettings_GetColour(wx.SYS_COLOUR_3DFACE))
        mainSizer.Add(spacer, 0, wx.ALL | wx.EXPAND)

        self.Freeze()

        self.Thaw()

        mainSizer.Layout()
        self.SendSizeEvent()


    def __del__( self ):
        pass

    def OnExit(self, evt):
        if self.loginOk:
            message = "Do you really want to quit?"
            caption = "Quitting RDEMPC AC"
            dlg = wx.MessageDialog(self, message, caption)
            dlg.SetYesNoLabels("&Quit", "&Don't quit")
            #dlg.SetMessage("What do you want to do?")
            if dlg.ShowModal() == wx.ID_OK:
                self.Destroy()
                sys.exit(1)
        else:
            sys.exit(0)

#from singleinstance import singleinstance
#from sys import exit

  ##do this at beginnig of your application
#myapp = singleinstance()

 ##check is another instance of same program running
#if myapp.aleradyrunning():
    #print "Another instance of this program is already running"
    #exit(0)

class MyApp(wx.App):
    def OnInit(self):
        self.locale = wx.Locale(wx.LANGUAGE_ENGLISH)
        frame = AccountingSystemMain(None)
        frame.ToggleMenu(False)
        frame.Show(True)
        frame.Maximize(True)
        f = passWord(None,frame)
        f.ShowModal()
        return True

app = MyApp(None)
app.MainLoop()
sys.exit(1)
