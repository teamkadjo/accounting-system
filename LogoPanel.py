import wx
#import images

import rdempclogo

class LogoPanel(wx.Panel):

    def __init__(self, parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.CLIP_CHILDREN):

        wx.Panel.__init__(self, parent, id, pos, size, style)
        
        #wx.Panel.__init__(self, parent)

        img = wx.Image("rdempc-logo.png", wx.BITMAP_TYPE_PNG)
        #img = wx.Image(image, wx.BITMAP_TYPE_ANY)
        self.sBmp = wx.StaticBitmap(self, wx.ID_ANY, wx.BitmapFromImage(img))
        #self.sBmp = img.GetBitmap()
    
        

        self.SetBackgroundColour(wx.LIGHT_GREY)
        #self.bmp = rdempclogo.RDEMPCLogo.GetBitmap()
        self.bmp = wx.BitmapFromImage(img)
#        self.bmp = images.Vippi.GetBitmap()
#        self.bmp = self.sBmp

        self.bigfont = wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD, False)
        self.normalfont = wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD, True)

        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)

      
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_SIZE, self.OnSize)

        #self.Bind(wx.EVT_PAINT, self.make_canvas)
        #self.Bind(wx.EVT_SIZE, self.make_canvas)

        self.sizer = wx.BoxSizer()
        self.sizer.Add(item=self.sBmp, proportion=0, flag=wx.ALL, border=10)
        #self.SetBackgroundColour('gray')
        #self.SetSizerAndFit(self.sizer)    

        wx.FutureCall(50, self.make_canvas)
        
        

    def OnSize(self, event):
        self.SetSizerAndFit(self.sizer) 
        event.Skip()
        self.Refresh()


    def make_canvas(self, event=None):
        # create the paint canvas
        dc = wx.ClientDC(self)
        # forms a wall-papered background
        # formed from repeating image tiles
        brush_bmp = wx.BrushFromBitmap(self.bmp)
        dc.SetBrush(brush_bmp)
        # draw a rectangle to fill the canvas area
        w, h = self.GetClientSize()
        dc.DrawRectangle(0, 0, w, h)
        self.SetSizerAndFit(self.sizer) 
        

    def OnPaint(self, event):
        
        dc = wx.AutoBufferedPaintDC(self)
        self.DoDrawing(dc)    
        self.SetSizerAndFit(self.sizer)         

    def DoDrawing(self, dc):

        dc.SetBackground(wx.WHITE_BRUSH)
        dc.Clear()
        
        w, h = self.GetClientSize()
        bmpW, bmpH = self.bmp.GetWidth(), self.bmp.GetHeight()

        xpos, ypos = int((w - bmpW)/2), int((h - bmpH)/2)
        
        dc.DrawBitmap(self.bmp, xpos, ypos, True)
        self.SetSizerAndFit(self.sizer) 

        

#---------------------------------------------------------------------------
