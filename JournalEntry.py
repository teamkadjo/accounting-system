#!/usr/bin/python
'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: JournalEntry.py
'''

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn, EVT_CELL_EDIT_FINISHED, EVT_CELL_EDIT_STARTING
from Journals import *
from specialolv import *
from wxSpecialPanel import *
from Pagination import *


class JournalEntry(object):
    #----------------------------------------------------------------------
    def __init__(self, postdate, post_id, description, currency, amount,ref_id):
        self.postdate = postdate
        self.post_id = post_id
        if not description:
            self.description = " "
        else:
            self.description = description
        self.currency = currency
        self.amount = amount
        self.ref_id = ref_id
        
class journalentryPanel(wxSpecialPanel):
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book  
        super(journalentryPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)




        
        self.SetLabel("JournalEntry")
        self.journals_listing = Journals()

        self.Paging  = Pagination(1,50,self.journals_listing.count_journals())

        self.journals = []        
        self.dataOlv = SpecialOLV(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.dataOlv.setEditCallback(self.editRow)
        self.dataOlv.setDeleteCallback(self.DeleteOLVNow)


        self.dataOlv.SetEmptyListMsg("There is no JE available.")
        self.dataOlv.SetEmptyListMsgFont(wx.FFont(24, wx.DEFAULT, face="Tekton"))           
        
        self.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE
        self.dataOlv.handleStandardKeys = True


        btMap = wx.Bitmap("image/media-skip-backward.png", wx.BITMAP_TYPE_ANY)
        mask = wx.Mask(btMap, wx.BLUE)
        btMap.SetMask(mask)
        btnTop = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnTop.Bind(wx.EVT_BUTTON,self.TopPage)

        btMap = wx.Bitmap("image/media-seek-backward.png", wx.BITMAP_TYPE_ANY)
        btnPrev = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnPrev.Bind(wx.EVT_BUTTON,self.PrevPage)
        
        btMap = wx.Bitmap("image/media-seek-forward.png", wx.BITMAP_TYPE_ANY)
        btnNext = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnNext.Bind(wx.EVT_BUTTON,self.NextPage)


                
        btMap = wx.Bitmap("image/media-skip-forward.png", wx.BITMAP_TYPE_ANY)
        btnBottom = wx.BitmapButton(self, -1, btMap, (10, 20),(50,25))
        btnBottom.Bind(wx.EVT_BUTTON,self.BottomPage)
        # Create some sizers
        sbSizerTop = wx.BoxSizer(wx.HORIZONTAL)  

        #search box
        sbSizerTop = wx.BoxSizer(wx.HORIZONTAL)         
        choices = []
        for i in xrange(1,self.Paging.pages+1):
            choices.append(str(i))
        self.pages = PromptingComboBox(self,"1",choices,style= wx.WANTS_CHARS)

        self.search = wx.SearchCtrl(self, size=(200,-1), style=wx.TE_PROCESS_ENTER)
        sizerTop = wx.BoxSizer(wx.HORIZONTAL)
        sizerTop.AddSpacer( ( 900, 0), 1, wx.EXPAND|wx.ALIGN_RIGHT, 5 )
        sizerTop.Add(self.search, 3,  wx.ALL|wx.ALIGN_RIGHT, 5)
        #end of search box


        self.Bind(wx.EVT_TEXT_ENTER, self.OnDoSearch, self.search)
        self.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.OnCancel, self.search)  

        self.pages.Bind(wx.EVT_COMBOBOX,self.ChangePage)
        self.pages.Bind(wx.EVT_TEXT_ENTER,self.ChangePage)



        sbSizer1 = wx.BoxSizer(wx.HORIZONTAL)   
        sbSizer1.Add(btnTop, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(btnPrev, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(self.pages,0,wx.ALL|wx.CENTER,5)
        sbSizer1.Add(btnNext, 0, wx.ALL|wx.CENTER, 5)
        sbSizer1.Add(btnBottom, 0, wx.ALL|wx.CENTER, 5)
 
        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)        
 
        
        mainSizer.Add(sizerTop, 0, wx.TOP, 5)        
        mainSizer.Add(self.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        
        b1 = wx.Button(self, label="Add Entry (F2)", name="newView")
        self.Bind(wx.EVT_BUTTON, self.OnNewView, b1)

        b2 = wx.Button(self, label="Edit Entry (F3) ")
        self.Bind(wx.EVT_BUTTON, self.pressingF3, b2)


        b3 = wx.Button(self, label="Delete Entry (Del)")
        self.Bind(wx.EVT_BUTTON, self.OnDeleteRows, b3)        
        

        btnbox = wx.BoxSizer(wx.HORIZONTAL)
        btnbox.Add(b1, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(b2, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(b3, 0, wx.ALL|wx.LEFT, 5)
        b1.SetToolTip(wx.ToolTip('Press F2 to enter new Journal Entry'))
        b2.SetToolTip(wx.ToolTip('Press F3 to Edit the selected Journal Entry'))

        self.search.SetFocus()
        self.search.ShowCancelButton(1)

        mainSizer.Add(sbSizer1, 0, wx.TOP|wx.ALIGN_RIGHT|wx.BOTTOM, 5)       
        mainSizer.Add(btnbox, 0, wx.TOP|wx.CENTER|wx.BOTTOM, 5)
        
        self.SetSizer(mainSizer)       
        self.refreshGrid()
        
        self.dataOlv.Bind(wx.EVT_LEFT_DCLICK, self.onOLVItemSelected) 

        self.setEscapeHandler(self.CloseThisPanel)           
        
        id_F2 = wx.NewId()
        id_F3 = wx.NewId()
        id_ENTER = wx.NewId()
        randomId = wx.NewId()
        id_CtrlQ = wx.NewId()
        self.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)
        self.Bind(wx.EVT_MENU, self.OnNewView, id=id_F2)
        self.Bind(wx.EVT_MENU, self.pressingF3, id=id_F3)
        self.Bind(wx.EVT_MENU, self.pressingF3, id=id_ENTER)
        self.Bind(wx.EVT_MENU,self.CloseEntryWindow,id=id_CtrlQ)
                
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('N'), randomId ),(wx.ACCEL_NORMAL, wx.WXK_F2, id_F2 ),(wx.ACCEL_CTRL,  wx.WXK_F4, id_CtrlQ ),(wx.ACCEL_NORMAL, wx.WXK_F3, id_ENTER ),(wx.ACCEL_NORMAL,  wx.WXK_ESCAPE, id_CtrlQ )])
        self.SetAcceleratorTable(accel_tbl)  
        
        randomId = wx.NewId()
        self.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)

        self.refreshGrid()
        
    def DeleteOLVNow(self):
        message = "Are you sure you want to delete this journal entry? "
        caption = "Delete Journal Entry Window"
        dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
        if dlg.ShowModal() == wx.ID_YES:
            the_obj = self.dataOlv.GetSelectedObjects()[0]
            self.DeleteRecord(the_obj)
            self.dataOlv.RemoveObject(the_obj)
            self.dataOlv.RepopulateList()
    
    def DeleteRecord(self,obj):
        self.journals_listing.delete_journalmain(obj.ref_id)

    def CloseEntryWindow(self,evt):
        self.CloseThisPanel()   
    
    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("JournalEntry",True)
        
    def FocusEvent(self,evt):
        print "focus event.."
                      
    def OnOlvSelect(self, evt):
        pass
    
    def onKeyCombo(self,evt):
        pass

    def editRow(self):
        obj = self.dataOlv.GetSelectedObject()
        if obj:
            self.parent_window.OpenEntryWindow(obj.ref_id,'Journal')

    def pressingF3(self,evt):
        self.editRow()
        
        
                
    def onOLVItemSelected(self,evt):
        obj = self.dataOlv.GetSelectedObject()
        if obj:
            self.parent_window.OpenEntryWindow(obj.ref_id,'Journal')


    def refreshGrid(self,updateValue=True):
        #print self.Paging.current_page
        if updateValue:
            self.pages.SetValue(str(self.Paging.current_page))
        if self.search.GetValue():
            search_value = self.search.GetValue()
        else:
            search_value = ""
            
        self.journals = []
        for u in self.journals_listing.get_journal_main2(self.Paging.current_page-1,50,search_value):
            amount = "{:,.2f}".format(u[15])            
            self.journals.append(JournalEntry(u[3],u[19],u[5],u[16],amount,u[0]))
        self.setJournalEntrys() 



    def refreshGridOld(self):
        self.journals = [] 
        for u in self.journals_listing.get_journal_main(0,0):
            amount = "{:,.2f}".format(u[15])
            self.journals.append(JournalEntry(u[3],u[19],u[5],u[16],amount,u[0]))

        self.setJournalEntrys()        


   
    def ChangePage(self,evt):
        self.Paging.page = int(self.pages.GetValue()) 
        self.refreshGrid(False)


    def TopPage(self,evt):
        if not self.Paging.is_ontop:
            self.Paging.top()
            self.refreshGrid()

    def OnCancel(self,evt):
        self.Paging.set_total_count(self.journals_listing.count_journals())        
        self.pages.Clear()
        for i in xrange(1,self.Paging.pages+1):
            self.pages.Append(str(i))       
        self.refreshGrid()    
        

    def PrevPage(self,evt):
        if self.Paging.has_prev:
            self.Paging.prev()
            self.refreshGrid()
            
    def NextPage(self,evt):
        if self.Paging.has_next:
            self.Paging.next()
            self.refreshGrid()

    def BottomPage(self,evt):
        if not self.Paging.is_onbottom:
            self.Paging.bottom()
            self.refreshGrid()
  
    def setJournalEntrys(self, data=None):
        self.dataOlv.SetColumns([
            ColumnDefn("PostDate", "left", 220, "postdate"),
            ColumnDefn("Reference ID", "left", 200, "post_id"),
            ColumnDefn("Description", "left", 200, "description"),
            ColumnDefn("Currency", "right", 70, "currency"),            
            ColumnDefn("Amount", "right", 180, "amount")
        ])
        self.dataOlv.SetObjects(self.journals)

    def OnDoSearch(self,data):
        self.Paging.set_total_count(self.journals_listing.count_journals(self.search.GetValue()))
        
        self.pages.Clear()
        for i in xrange(1,self.Paging.pages+1):
            self.pages.Append(str(i))       
        self.refreshGrid()
        self.dataOlv.SetFocus()
 
        

    def OnNewView(self,event):
        self.parent_window.OpenEntryWindow(None,'Journal')
    
    def OnAddRow(self,event):
        print "Add Row"
        pass
    
    def OnDeleteRows(self,event):
        pass
    
    
    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()        
    
    def return_panel(self):        
        return self

def CreateJournalEntryWindow(the_parent_window):
   panel = journalentryPanel(the_parent_window)
   return panel.return_panel()
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
