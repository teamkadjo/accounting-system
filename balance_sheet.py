from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import *
from podunk.prefab import paper
import os
from Configuration import *
from Users import *
import datetime
import sys

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 35

    def _draw_header(self):
        #super(ReportSpecialHeader,self)._draw_header()
        config = Configuration()
        #self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,750,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(230,740,complete_address)
        self.canvas.drawString(265,730,self.cutoff_date)

        self.canvas.setFont('Helvetica-Bold', 10)
        self.canvas.drawString(230,710,"STATEMENT OF FINANCIAL CONDITION")
        self.canvas.setFont('Helvetica', 8)


    def setExplanation(self,explanation=None):
        self.explanation = explanation

    def setCutOffDate(self,date1):
        self.cutoff_date = datetime.datetime.strptime(date1,"%Y-%m-%d").strftime("As of %B %d, %Y")


class SpecialTable(Table):


    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date

    def getInvoiceNumber(self):
        return self.number

    def getPostDate(self):
        return self.post_date

    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)


    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        config = Configuration()
        canvas.line(24,yoff-20,580,yoff-20)
        canvas.line(24,yoff-60,580,yoff-60)
        canvas.line(24,yoff-20,24,yoff-60)
        canvas.line(580,yoff-20,580,yoff-60)
        canvas.line(163,yoff-20,163,yoff-60)
        canvas.line(302,yoff-20,302,yoff-60)
        canvas.line(441,yoff-20,441,yoff-60)

        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")

        canvas.drawString(27,yoff-30,"Prepared By:")
        canvas.drawString(166,yoff-30,"Checked By:")
        canvas.drawString(305,yoff-30,"Approved By:")
#        canvas.drawString(444,yoff-30,"JV NO: " + self.getInvoiceNumber())
#        canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.drawString(44,yoff-50,preparedByText)
        canvas.drawString(183,yoff-50,checkedByText)
        canvas.drawString(322,yoff-50,approvedByText)
        self.auto_width(canvas)


def balanceSheet_report(pdf_name='test.pdf',title="Voucher Number",cutoff_date='2015-10-31',username='admin',cost_center=None):
    table = SpecialTable()

    u = Users()
    uinfo = u.get_userinfo(username)

    j = Journals()
    col = table.add_column('Description',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')
    col.header.style.size = 8

    col.row.style.size = 8
    col.row.style.horizontal_alignment = alignment.LEFT

    col = table.add_column('Value',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('Sub Total',60)
    col.row.format = format_two_decimals
    col.row.style.size = 8
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('Total',60)
    col.row.format = format_two_decimals
    col.row.style.size = 8
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')



    journals_listing = Journals()
    spaces = "      "
    #print "get_summation goes ehre..."
    list_data = journals_listing.get_summation("0,2,4,6",cutoff_date)
    negate_arr = {"0":False,"2":False,"4":False,"6":False}
    ac_type = 0
    ac_descript = "Cash"
    table.add_row(["CURRENT ASSETS",0.00,0.00,0.00])
    total_cat = 0
    asset_total = 0
    for r in list_data:
        if ac_type != r.account_type:
            asset_total = asset_total + total_cat
            table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
            total_cat = 0
            ac_type = r.account_type
            ac_descript = r.descript
        if (negate_arr[str(r.account_type)]):
            total_cat = total_cat - (r.debit - r.credit)
        else:
            total_cat = total_cat + (r.debit - r.credit)
        table.add_row([ spaces + spaces + r.acc_code + " " + r.description,abs(r.debit - r.credit),0.00,0.00])
    asset_total = asset_total + total_cat
    table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
    table.add_row(["TOTAL CURRENT ASSETS",0.00,0.00,asset_total])

    table.add_row(["",0.00,0.00,0.00]) # spaces in between major accounts

    list_data = journals_listing.get_summation("8,12",cutoff_date)
    negate_arr = {"8":False,"10":True,"12":False}
    ac_type = 8
    ac_descript = "Fixed Asset"
    property_total = 0
    table.add_row(["PROPERTY, PLANT AND EQUIPMENT",0.00,0.00,0.00])
    total_cat = 0
    for r in list_data:
        if ac_type != r.account_type:
            property_total = property_total + total_cat
            if (ac_type != 10):
                table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
            total_cat = 0
            ac_type = r.account_type
            ac_descript = r.descript
        if (negate_arr[str(r.account_type)]):
            total_cat = total_cat - (r.debit - r.credit)
        else:
            total_cat = total_cat + (r.debit - r.credit)
        table.add_row([ spaces + spaces + r.acc_code + "  "  + r.description,abs(r.debit - r.credit),0.00,0.00])
    property_total = property_total + total_cat
    table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])

    list_data = journals_listing.get_summation("10",cutoff_date)
    ac_type = 10
    ac_descript = "Accumulated Depreciation"
    total_cat = 0
    for r in list_data:
        if ac_type != r.account_type:
            if (ac_type != 10):
                table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
            ac_type = r.account_type
            ac_descript = r.descript
        if (negate_arr[str(r.account_type)]):
            total_cat = total_cat - (r.debit - r.credit)
        else:
            total_cat = total_cat + (r.debit - r.credit)
        table.add_row([ spaces + spaces + r.acc_code + "  "  + r.description,abs(r.debit - r.credit),0.00,0.00])
    property_total = property_total - total_cat
    table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])


    table.add_row(["TOTAL PROPERTY, PLANT AND EQUIPMENT",0.00,0.00,property_total])

    table.add_row(["",0.00,0.00,0.00]) # spaces in between major accounts


    list_data = journals_listing.get_summation("20,22",cutoff_date)
    negate_arr = {"20":True,"22":True}
    ac_type = 20
    current_liabilities_total = 0
    ac_descript = "Accounts Payable"

    table.add_row(["=============================================",0.00,0.00,0.00]) # the best line


    table.add_row(["CURRENT LIABILITIES",0.00,0.00,0.00])
    total_cat = 0
    for r in list_data:
        if ac_type != r.account_type:
            current_liabilities_total = current_liabilities_total + total_cat
            table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
            total_cat = 0
            ac_type = r.account_type
            ac_descript = r.descript
        if (negate_arr[str(r.account_type)]):
            total_cat = total_cat - (r.debit - r.credit)
        else:
            total_cat = total_cat + (r.debit - r.credit)
        table.add_row([ spaces + spaces + r.acc_code + " " + r.description,abs(r.debit - r.credit),0.00,0.00])
    current_liabilities_total = current_liabilities_total + total_cat
    table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
    table.add_row(["TOTAL CURRENT LIABILITIES",0.00,0.00,current_liabilities_total])

    table.add_row(["",0.00,0.00,0.00]) # spaces in between major accounts

    list_data = journals_listing.get_summation("24",cutoff_date)
    negate_arr = {"24":True}
    ac_type = 24
    ac_descript = "Long Term Liabilities"
    table.add_row(["LONG TERM LIABILITIES",0.00,0.00,0.00])
    total_cat = 0
    for r in list_data:
        if ac_type != r.account_type:
            table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
            total_cat = 0
            ac_type = r.account_type
            ac_descript = r.descript
        if (negate_arr[str(r.account_type)]):
            total_cat = total_cat - (r.debit - r.credit)
        else:
            total_cat = total_cat + (r.debit - r.credit)
        table.add_row([ spaces + spaces + r.acc_code + " " + r.description,abs(r.debit - r.credit),0.00,0.00])
    table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
    table.add_row(["TOTAL LONG TERM LIABILITIES",0.00,0.00,0.00])

    table.add_row(["",0.00,0.00,0.00]) # spaces in between major accounts

    list_data = journals_listing.get_summation("40,42,44",cutoff_date)
    negate_arr = {"40":True,"42":True,"44":True}
    ac_type = 40
    ac_descript = "Capital"
    table.add_row(["CAPITAL ",0.00,0.00,0.00])
    total_cat = 0
    capital_total = 0
    for r in list_data:
        if ac_type != r.account_type:
            capital_total = capital_total + total_cat
            table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
            total_cat = 0
            ac_type = r.account_type
            ac_descript = r.descript
        if (negate_arr[str(r.account_type)]):
            total_cat = total_cat - (r.debit - r.credit)
        else:
            total_cat = total_cat + (r.debit - r.credit)
        table.add_row([ spaces + spaces + r.acc_code + " " + r.description,abs(r.debit - r.credit),0.00,0.00])
    capital_total = capital_total + total_cat
    table.add_row([ spaces + "Total " + ac_descript ,0.00,total_cat,0.00])
    table.add_row(["TOTAL CAPITAL",0.00,0.00,capital_total])

    #explanation = ""



    col = table.get_footer_field('Description')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Value')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Sub Total')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Total')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True



    #table.sum_column('Sub Total')
    #table.sum_column('Total')


    veryNiceHeader = Heading(None)

    report = ReportSpecialHeader(pdf_name)
    report.setCutOffDate(cutoff_date)
    report.page_width, report.page_height = paper.LETTER_PORTRAIT
    #report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {} '.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()
    return
    #print "End of creating..."



if __name__ == '__main__':
    balanceSheet_report('balansheetreport.pdf','Journal Voucher','2016-06-30','admin')
    os.system('start balansheetreport.pdf')
    sys.exit(0)
