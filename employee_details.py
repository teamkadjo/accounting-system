

import wx

class employee_details:
    def __init__(self,the_book):
        self.book = the_book
        #pass
        self.the_panel = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        wx.StaticText(self.the_panel, -1, "Firstname:", (20,20))
        wx.TextCtrl(self.the_panel, -1, "", (100,20), (150,-1))
        wx.StaticText(self.the_panel, -1, "Lastname:", (20,50))
        wx.TextCtrl(self.the_panel, -1, "", (100,50), (150,-1))        

        wx.StaticText(self.the_panel, -1, "Sex:", (20,80))
        #wx.TextCtrl(self.the_panel, -1, "", (100,80), (30,-1))    
        
        sex_choice = ['Male','Female']
        cb = wx.ComboBox(self.the_panel, pos=(100, 80), choices=sex_choice, 
            style=wx.CB_READONLY)
        cb.SetValue("Male")        
        
        the_button = wx.Button(self.the_panel, -1, "Save", (20,120))    
    
        
    
    def return_panel(self):        
        return self.the_panel
    



def CreateEmployeeDetails(thebook_self):

   panel = employee_details(thebook_self.book)
   return panel.return_panel()
    
    
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"