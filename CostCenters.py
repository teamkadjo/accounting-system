'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: CostCenters.py
'''

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *

class CostCenter(object):
    #----------------------------------------------------------------------
    def __init__(self,id, fullname, street, city, province):
        self.id = id
        self.fullname = fullname
        self.street = street
        self.city = city
        self.province = province

class costcentersPanel(wxSpecialPanel):
    def __init__(self,parent_window):
        self.parent_window = parent_window
        self.book = parent_window.book

        super(costcentersPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)


        self.SetLabel("CostCenter")
        self.contact_members = Contacts()

        self.members = []
        for u in self.contact_members.get_costcenters(0,50):
            print u
            self.members.append(CostCenter(u.id,u.short_name,u.address1,u.city_town,u.state_province))

        self.dataOlv = ObjectListView(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setCostCenters()

        self.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE

        btnbox = wx.BoxSizer(wx.HORIZONTAL)

        id_NewMember = wx.NewId()
        btn_newMember = wx.Button(self, label = "New CostCenter (F2)",name="newMember")
        btnbox.Add(btn_newMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON,self.addCCInfo,btn_newMember)

        id_EditMember = wx.NewId()
        btn_EditMember = wx.Button(self, label = "Edit CostCenter (F3)",name="editMember")
        btnbox.Add(btn_EditMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON,self.editCCInfo,btn_EditMember)


        id_delMember = wx.NewId()
        btn_delMember = wx.Button(self, label = "Delete CostCenter (Del)",name="delMember")
        btnbox.Add(btn_delMember, 0, wx.ALL|wx.LEFT, 5)
        self.Bind(wx.EVT_BUTTON, self.deleteCC, btn_delMember)



        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        mainSizer.Add(self.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(btnbox, 0, wx.ALL|wx.CENTER, 5)
        self.SetSizer(mainSizer)

        self.dataOlv.Bind(wx.EVT_LEFT_DCLICK, self.onOLVItemSelected)


        id_F2 = wx.NewId()
        randomId = wx.NewId()
        self.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)
        self.Bind(wx.EVT_MENU, self.pressingF2, id=id_F2)

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('N'), randomId ),(wx.ACCEL_NORMAL, wx.WXK_F2, id_F2 )])
        self.SetAcceleratorTable(accel_tbl)

        randomId = wx.NewId()
        self.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)

    def addCCInfo(self,evt):
        self.parent_window.OpenCCInfo(None,'')


    def editCCInfo(self,evt):
        self.editCCInfoNow()

    def editCCInfoNow(self):
        the_obj = self.dataOlv.GetSelectedObjects()
        if the_obj:
            #print the_obj[0].__dict__
            fullname = the_obj[0].fullname
            cc_id = the_obj[0].id
            self.parent_window.OpenCCInfo(cc_id,fullname)

    def refreshGrid(self,updateValue=True):
        self.members = []
        for u in self.contact_members.get_costcenters(0,50):
            print u
            self.members.append(CostCenter(u.id,u.short_name,u.address1,u.city_town,u.state_province))
        self.setCostCenters()

    def onKeyCombo(self,evt):

        theobj = self.dataOlv.GetSelectedObject()
        if theobj:
            theobj.fullname = "Pablooooow Pikasowwwwww"
            self.dataOlv.RefreshObject(theobj)

    def pressingF2(self,evt):
        print "Pinindot F2"
        pass

    def DeleteRecord(self,obj):
        self.contact_members.deleteCCRecord(obj.id)

    def onOLVItemSelected(self,evt):
        self.editCCInfo(evt)


    def deleteCC(self,evt):
        the_obj = self.dataOlv.GetSelectedObjects()[0]
        message = "Are you sure you want to delete %s record? " % the_obj.fullname
        caption = "Delete CC Record Window"
        dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
        if dlg.ShowModal() == wx.ID_YES:
            the_obj = self.dataOlv.GetSelectedObjects()[0]
            message = "Are you really really sure you want to delete %s record? " % the_obj.fullname
            caption = "Delete CC Record Window"
            dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
            if dlg.ShowModal() == wx.ID_YES:
                the_obj = self.dataOlv.GetSelectedObjects()[0]
                self.DeleteRecord(the_obj)
                self.refreshGrid()

    def setCostCenters(self, data=None):
        self.dataOlv.SetColumns([
            ColumnDefn("", "left", 0, "id"),
            ColumnDefn("Name", "left", 220, "fullname"),
            ColumnDefn("Street", "left", 200, "street"),
            ColumnDefn("City", "right", 100, "city"),
            ColumnDefn("Province", "left", 180, "province")
        ])

        self.dataOlv.SetObjects(self.members)

    def OnNewView(self,event):
        pass

    def OnAddRow(self,event):
        print "Add Row"
        pass

    def OnDeleteRows(self,event):
        pass


    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()

    def return_panel(self):
        return self

def CreateCostCenterWindow(the_parent_window):
   panel = costcentersPanel(the_parent_window)
   return panel.return_panel()

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
