from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
from numberword2 import *
import re
from Users import *

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 35
        self.check_number = None
                    
    def _draw_header(self):
        config = Configuration()
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,770,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(230,760,complete_address)   
        
        self.canvas.setFont('Helvetica-Bold', 10)
        self.canvas.drawString(250,720,"C H E C K    V O U C H E R")
        #self.canvas.drawString(0,750,"0123456789012345678901234567890123456789012345678901234567890123456789")
        self.canvas.setFont('Helvetica', 8)
        #self.canvas.drawString(30,705,"Pay to : ")
        #self.canvas.line(24,700,580,700)
        self.canvas.drawString(27,685,"EXPLANATION: " )
        self.canvas.setFont('Helvetica-Bold', 8)
        self.canvas.drawString(88,685,self.getExplanation() )
        self.canvas.setFont('Helvetica', 8)
                
        self.canvas.rect(24, 698, 562, 20, stroke=1, fill=0)     
        self.canvas.rect(24, 676, 562, 20, stroke=1, fill=0)     

    def setExplanation(self,explanation=None):
        self.explanation = explanation
    def getExplanation(self):
        return self.explanation               

class SpecialTable(Table):
    
    def __init_(self):
        Table.__init__()
        self.check_number= ""

    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
        
    def setSLName(self,sl_name):
        self.sl_name = sl_name
    
    def setCheckValue(self,value):
        self.check_value = value
    
    def setBankName(self,value):
        self.bank_name = value
    
    def setCheckNumber(self,value):
        self.check_number = value
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        canvas.setFont('Helvetica', 8)
        config = Configuration()
        canvas.line(24,yoff-5,586,yoff-5)
        #canvas.line(24,yoff-60,586,yoff-60)
        #canvas.line(24,yoff-20,24,yoff-60)
        #canvas.line(586,yoff-20,586,yoff-60)
        
        box_displacement = 90
        canvas.rect(24,yoff-box_displacement,562,30,stroke=1, fill=0)        
        canvas.line(163,yoff-5,163,yoff-box_displacement) 
        canvas.line(24,yoff-5,24,yoff-box_displacement) 
        canvas.line(586,yoff-5,586,yoff-box_displacement) 
        canvas.line(302,yoff-(box_displacement - 30),302,yoff-box_displacement)
        canvas.line(441,yoff-(box_displacement - 30),441,yoff-box_displacement)
        #canvas.line(24,yoff-(box_displacement - 30),441,yoff-box_displacement)
        
        
        canvas.line(400,yoff-45,550,yoff-45)
        canvas.drawString(460,yoff-55,"Payee")
       
        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")
       

        if self.bank_name:
            canvas.drawString(27,yoff-15,"DRAWEE :" + self.bank_name)
        if self.check_number:
            canvas.drawString(27,yoff-30,"NUMBER :" + self.check_number)
        
        canvas.drawString(27,yoff-45,"DATE   : " + self.post_date)
        
        
        canvas.drawString(166,yoff-15,"Received from  ADMIN of stated account amounting in PESOS ")
        
        canvas.setFont('Helvetica-Bold', 10)
        if (self.check_value):
            numbervalue = get_number_as_words(self.check_value)
            canvas.drawString(190,yoff-40,numbervalue.upper() + " ONLY")
        canvas.setFont('Helvetica', 10)
        canvas.drawString(30,705,"Pay to : " + self.sl_name)
        
        canvas.drawString(27,yoff-(box_displacement - 20),"Prepared By:")
        canvas.drawString(166,yoff-(box_displacement - 20),"Checked By:")
        canvas.drawString(305,yoff-(box_displacement - 20),"Approved By:")
        canvas.drawString(444,yoff-(box_displacement - 20),"CV NO: " + self.getInvoiceNumber())
        #canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.setFont('Helvetica-Bold', 8)
        canvas.drawString(44,yoff-(box_displacement - 5),preparedByText)
        canvas.drawString(183,yoff-(box_displacement - 5),checkedByText)
        canvas.drawString(322,yoff-(box_displacement - 5),approvedByText)
        canvas.setFont('Helvetica', 8)      


def cvEntryReport(ref_id=50,pdf_name='test.pdf',title="Voucher Number",username='admin'):
    table = SpecialTable()

    u = Users()
    uinfo = u.get_userinfo(username)
    #print uinfo
            

    j = Journals()
    col = table.add_column('CostCtr',30)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Account Title',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Subsidiary',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Description',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    
    journals_listing = Journals()
    explanation = ""
    if ref_id:
        data = journals_listing.db.journal_main.filter(journals_listing.db.journal_main.id == ref_id).one()
        explanation = data.description
        table.setInvoiceNumber(data.purchase_invoice_id)
        table.setPostDate("{:%m/%d/%Y}".format(data.post_date))

    

    col = table.add_column('debit',50)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('credit',50)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    
    sl_name = ""
    check_value = 0
    table.setCheckValue(0)
    for x in j.get_jelist(ref_id,0,0):
        if x.subledger:
            sl_name = str(x.subledger).encode('utf-8')
            sl_name = re.sub("\(|\d+|\)","",sl_name).strip()
        if x.gl_account in ['11131','11132','11133']:
            check_value = x.credit_amount
            table.setCheckValue(check_value)
            s = x.gl_desc
            s_splits = s.split("-")
            table.setBankName(s_splits[2].strip())
            descript = re.findall(r'\b\d+\b', x.description)
            if len(descript) > 0:
                if descript[0]:
                    checkNumber = descript[0]
                    table.setCheckNumber(checkNumber)
            else:
                checkNumber  = ""
        if x.subledger:        
            subledger_name = str(x.subledger).encode('utf-8').decode('utf-8')
            subledger_name = re.sub("\(|\d+|\)","",subledger_name).strip()
        else:
            subledger_name =  " "    
        table.add_row( [x.cost_center,x.gl_desc, subledger_name,x[4], x[5], x[6] ])

    #print "SL Name is " + sl_name
    table.setSLName(sl_name)
    table.sum_column('debit')
    col = table.get_footer_field('debit')    
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.size = 10
    col.style.bold = True
    
    table.sum_column('credit')
    
    col = table.get_footer_field('CostCtr')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('credit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.size = 9    
    col.style.bold = True

    col = table.get_footer_field('debit')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.size = 9    
    col.style.bold = True

    col = table.get_footer_field('Subsidiary')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Account Title')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Description')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 11
    col.value = "T O T A L "

    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {}'.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.setWithFooter(False)
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    cvEntryReport(23,'test3.pdf','Check Voucher ','admin')
    os.system('start test3.pdf')
