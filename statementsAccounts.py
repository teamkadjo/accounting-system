'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: statementsAccounts.py
'''

import sys
from db import *
import sqlalchemy
import traceback
from Configuration import *
from datetime import date
from datetimeutil import *

class statementsAccounts:
    def __init__(self):
        self.db = None
        try :
            self.Journals = DatabaseMySQL.journal_main
            self.db = DatabaseMySQL
            self.cv_series = DatabaseMySQL.cv_series
            self.config = Configuration()
        except:
            traceback.print_exc()
            print "Serious connection problem occured"
            sys.exit(0)
            return None
        self.counter = 0
        return None

    def get_costcenters(self):
        return self.db.contacts.filter_by(type='j').all()
