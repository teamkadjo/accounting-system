# Create the lists of word-equivalents from 1-19, then one for the tens group.
# Finally, a list of the (for lack of a better word) "zero-groups".
import math

ByOne = [
"zero",
"one",
"two",
"three",
"four",
"five",
"six",
"seven",
"eight",
"nine",
"ten",
"eleven",
"twelve",
"thirteen",
"fourteen",
"fifteen",
"sixteen",
"seventeen",
"eighteen",
"nineteen"
]

ByTen = [
"zero",
"ten",
"twenty",
"thirty",
"forty",
"fifty",
"sixty",
"seventy",
"eighty",
"ninety"
]

zGroup = [
"",
"thousand",
"million",
"billion",
"trillion",
"quadrillion",
"quintillion",
"sextillion",
"septillion",
"octillion",
"nonillion",
"decillion",
"undecillion",
"duodecillion",
"tredecillion",
"quattuordecillion",
"sexdecillion",
"septendecillion",
"octodecillion",
"novemdecillion",
"vigintillion"
]

#strNum = raw_input("Please enter an integer:\n>> ")

# A recursive function to get the word equivalent for numbers under 1000.

def subThousand(inputNum):
    num = int(inputNum)
    if 0 <= num <= 19:
        return ByOne[num]
    elif 20 <= num <= 99:
        if inputNum[-1] == "0":
            return ByTen[int(inputNum[0])]
        else:
            return ByTen[int(inputNum[0])] + "-" + ByOne[int(inputNum[1])]
    elif 100 <= num <= 999:
        rem = num % 100
        dig = num / 100
        if rem == 0:
            return ByOne[dig] + " hundred"
        else:
            return ByOne[dig] + " hundred " + subThousand(str(rem))

# A looping function to get the word equivalent for numbers above 1000
# by splitting a number by the thousands, storing them in a list, and 
# calling subThousand on each of them, while appending the correct
# "zero-group".

def thousandUp(inputNum):
    num = int(inputNum)
    arrZero = splitByThousands(num)
    lenArr = len(arrZero) - 1
    resArr = []
    for z in arrZero[::-1]:
        wrd = subThousand(str(z)) + " "
        zap = zGroup[lenArr] + " "
        if wrd == " ":
            break
        elif wrd == "zero ":
            wrd, zap = "", ""
        resArr.append(wrd + zap)
        lenArr -= 1
    res = "".join(resArr).strip()
    if res[-1] == " ": res = res[:-1]
    return res

# Function to return a list created from splitting a number above 1000.

def splitByThousands(inputNum):
    num = int(inputNum)
    arrThousands = []
    while num != 0:
        arrThousands.append(num % 1000)
        num /= 1000
    return arrThousands

### Last part is pretty much just the output.

#intNum = int(strNum)

#if intNum < 0:
    #print "Minus",
    #intNum *= -1
    #strNum = strNum[1:]

#if intNum < 1000:
    #print subThousand(strNum)
#else:
    #print thousandUp(strNum)
    
def get_number_as_words(num):
    (whole1, frac1) = (int(num), int(str(num)[(len(str(int(num)))+1):]))
    (frac,whole) = math.modf(float(num))
    print frac
    print whole
    whole = whole1
    
    strNum = str(whole)

    intNum = int(strNum)
    if intNum < 0:
      print "Minus", # I didn't see this at the beginning but I'll fix it at the end
      intNum *= -1
      strNum = strNum[1:]
      #print "Number is " + strNum

    if intNum < 1000:
        if (frac == 0):
            return subThousand(strNum) 
        else:
            frac = int(frac * 100)
            return subThousand(strNum) + " and " + str(frac) + "/100 "
    else:
        if (frac == 0):
            return thousandUp(strNum) 
        else:
            frac = int(float( str(frac)) * 100)

            print "stNum"
            print strNum
            
            print "fract is "
            
            print frac
            return subThousand(strNum) + " and " + str(frac) + "/100 "
          
def main():
  number = 10005.93
  #raw_input("Please enter an integer:\n>> ")
  expected='ninety-five quadrillion, five hundred and five trillion, eight hundred  ninety-six billion, six hundred and thirty-nine million, six hundred and thirty-one thousand, eight hundred and ninety-three '
  print(get_number_as_words(number))
  #print(expected)
  #assert(get_number_as_words(strNum)==expected)

if __name__ == "__main__":
  main()    
