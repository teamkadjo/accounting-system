from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
from Users import *

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 30
            
    def _draw_header(self):
        #super(ReportSpecialHeader,self)._draw_header()
        config = Configuration()
        #self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,750,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(230,740,complete_address)   
        self.canvas.drawString(27,710,"EXPLANATION: ")   
        self.canvas.setFont('Helvetica-Bold', 8)  
        self.canvas.drawString(88,710, self.getExplanation())     

        self.canvas.setFont('Helvetica-Bold', 10)
        self.canvas.drawString(250,720,"J O U R N A L    V O U C H E R")
        self.canvas.setFont('Helvetica', 8)

    def setExplanation(self,explanation=None):
        self.explanation = explanation
    def getExplanation(self):
        return self.explanation               

class SpecialTable(Table):

    
    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):    
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        config = Configuration()
        canvas.line(24,yoff-20,580,yoff-20)
        canvas.line(24,yoff-60,580,yoff-60)
        canvas.line(24,yoff-20,24,yoff-60)
        canvas.line(580,yoff-20,580,yoff-60)
        canvas.line(163,yoff-20,163,yoff-60)
        canvas.line(302,yoff-20,302,yoff-60)
        canvas.line(441,yoff-20,441,yoff-60)
       
        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")
       
        canvas.drawString(27,yoff-30,"Prepared By:")
        canvas.drawString(166,yoff-30,"Checked By:")
        canvas.drawString(305,yoff-30,"Approved By:")
        canvas.drawString(444,yoff-30,"JV NO: " + self.getInvoiceNumber())
        canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.drawString(44,yoff-50,preparedByText)
        canvas.drawString(183,yoff-50,checkedByText)
        canvas.drawString(322,yoff-50,approvedByText)
        

def report_Entry(ref_id=14,pdf_name='test.pdf',title="Voucher Number",username='admin'):
    table = SpecialTable()

    u = Users()
    uinfo = u.get_userinfo(username)
        
    j = Journals()
    col = table.add_column('CostCtr',20)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.size = 7
    
    col = table.add_column('Subsidiary',60)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.size = 7
    
    col = table.add_column('Account Name',145)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.size = 7    
    
    col = table.add_column('Description',170)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.row.style.size = 8
    col.header.style.color = toColor('rgb(0,0,0)')    
    
    journals_listing = Journals()
    explanation = ""
    if ref_id:
        data = journals_listing.db.journal_main.filter(journals_listing.db.journal_main.id == ref_id).one()
        explanation = data.description
        table.setInvoiceNumber(data.purchase_invoice_id)
        table.setPostDate("{:%m/%d/%Y}".format(data.post_date))

    

    col = table.add_column('debit')
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('credit')
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    for x in j.get_jelist(ref_id,0,0):
        if x.subledger:
            #subledger_name = bytearray(x.subledger, encoding="utf-8")
            subledger_name = str(x.subledger).encode('utf-8')
            subledger_name = re.sub("\(|\d+|\)","",subledger_name).strip()
        else:
            subledger_name = ""
            
        table.add_row( [x.cost_center[:3],subledger_name,x.gl_desc,x.description,  x.debit_amount, x.credit_amount])

    table.sum_column('debit')
    col = table.get_footer_field('debit')    
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    
    table.sum_column('credit')
    
    col = table.get_footer_field('credit')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True

    col = table.get_footer_field('Subsidiary')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 9
    col.value = ""

    col = table.get_footer_field('CostCtr')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 9
    col.value = ""

    col = table.get_footer_field('Description')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 9
    col.value = ""

    col = table.get_footer_field('Account Name')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9
    col.value = "T O T A L "

    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {} '.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.setWithFooter(False)    
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    report_Entry(1006,'test3.pdf','Journal Voucher','admin')
    os.system('start test3.pdf')
