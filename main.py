import wx
import wx.aui

from thetest import * 
# -*- coding: utf-8 -*- 

import images_2
import images
import ColorPanel

colourList = [ "Aquamarine", "Grey", "Blue", "Blue Violet", "Brown", "Cadet Blue",
               "Coral", "Wheat", #"Cyan", "Dark Grey", "Dark Green",
               #"Steel Blue",
               ]



def getNextImageID(count):
    imID = 0
    while True:
        yield imID
        imID += 1
        if imID == count:
            imID = 0

class MyFrame ( wx.Frame ):
    
    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = "Payroll System", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        self.m_statusBar1 = self.CreateStatusBar( 1, wx.ST_SIZEGRIP, wx.ID_ANY )
        self.m_menubar1 = wx.MenuBar( 0 )
        self.m_menu1 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem1 )
        
        self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem2 )
        
        self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem3 )
        
        self.m_menubar1.Append( self.m_menu1, u"File" ) 
        
        self.m_menu4 = wx.Menu()
        self.m_menuItem4 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem4 )
        
        self.m_menuItem5 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem5 )
        
        self.m_menuItem6 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem6 )
        
        self.m_menubar1.Append( self.m_menu4, u"Report" ) 
        
        self.m_menu2 = wx.Menu()
        self.m_menuItem7 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem7 )
        
        self.m_menuItem8 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem8 )
        
        self.m_menuItem9 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem9 )
        
        self.m_menubar1.Append( self.m_menu2, u"Help" ) 
        
        self.SetMenuBar( self.m_menubar1 )
#         
#         self.m_toolBar1 = self.CreateToolBar(wx.TB_HORIZONTAL, wx.ID_ANY) 
#         self.m_tool1 = self.m_toolBar1.AddLabelTool(wx.ID_ANY, u"tool", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None) 
#         
#         self.m_tool2 = self.m_toolBar1.AddLabelTool(wx.ID_ANY, u"tool", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None) 
#         
#         self.m_tool3 = self.m_toolBar1.AddLabelTool(wx.ID_ANY, u"tool", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None) 
#         

        
        bSizer3 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_tb = wx.Toolbook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LB_DEFAULT )
        
        #self.m_listbook2 = TestTB()

        il = wx.ImageList(32, 32)
        for x in range(12):
            obj = getattr(images_2, 'LB%02d' % (x+1))
            bmp = obj.GetBitmap()
            il.Add(bmp)
        self.m_tb.AssignImageList(il)
        imageIdGenerator = getNextImageID(il.GetImageCount())
        

        
        # Now make a bunch of panels for the list book
        first = True
        for colour in colourList:
#             win = self.makeColorPanel(colour)
#             
#             self.m_tb.AddPage(win, colour, imageId=imageIdGenerator.next())
            if first:
                #act = MyPanel2(self)
                win = self.makeColorPanel(colour)
               # self.m_tb.AddPage(act, colour, imageId=imageIdGenerator.next())
                st = wx.StaticText(win.win, -1,
                          "You can put nearly any type of window here,\n"
                          "and the toolbar can be on either side of the Toolbook",
                          wx.Point(10, 10))
                self.m_tb.AddPage(win, colour, imageId=imageIdGenerator.next())
                first = False
            else:
                win = self.makeColorPanel(colour)             
                self.m_tb.AddPage(win, colour, imageId=imageIdGenerator.next())                
            
        bSizer3.Add( self.m_tb, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        self.SetSizer( bSizer3 )
        self.Layout()
        
        self.Centre( wx.BOTH )
    
    def __del__( self ):
        pass
    

    def makeColorPanel(self, color):
        p = wx.Panel(self, -1)
        win = ColorPanel.ColoredPanel(p, color)
        p.win = win
        def OnCPSize(evt, win=win):
            win.SetPosition((0,0))
            win.SetSize(evt.GetSize())
        p.Bind(wx.EVT_SIZE, OnCPSize)
        return p


            
            
app = wx.App()
frame = MyFrame(None)
frame.Show()
app.MainLoop()
