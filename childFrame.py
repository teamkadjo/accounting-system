
import wx
import wx.aui
from samplePanel import * 


class childFrame(wx.aui.AuiMDIChildFrame):
    def __init__(self, parent, count):
        wx.aui.AuiMDIChildFrame.__init__(self, parent, -1,
                                         title="Child: %d" % count)
        mb = parent.MakeMenuBar()
        menu = wx.Menu()
        item = menu.Append(-1, "This is child %d's menu" % count)
        mb.Append(menu, "&Child")
        self.SetMenuBar(mb)
        
        p = wx.Panel(self)
        #wx.StaticText(p, -1, "This is child %d" % count, (10,10))
        #p.SetBackgroundColour('red')
        
        fgSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fgSizer1.SetFlexibleDirection( wx.BOTH )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        p.m_staticText11 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        p.m_staticText11.Wrap( -1 )
        fgSizer1.Add( p.m_staticText11, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        p.m_textCtrl13 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( p.m_textCtrl13, 0, wx.ALL|wx.EXPAND, 5 )
        
        p.m_staticText12 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        p.m_staticText12.Wrap( -1 )
        fgSizer1.Add( p.m_staticText12, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        p.m_textCtrl14 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( p.m_textCtrl14, 1, wx.ALL|wx.EXPAND, 5 )
        
        p.m_staticText13 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        p.m_staticText13.Wrap( -1 )
        fgSizer1.Add( p.m_staticText13, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        p.m_textCtrl15 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( p.m_textCtrl15, 0, wx.ALL, 5 )
        
        p.m_staticText14 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        p.m_staticText14.Wrap( -1 )
        fgSizer1.Add( p.m_staticText14, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        p.m_textCtrl16 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( p.m_textCtrl16, 0, wx.ALL, 5 )
        
        p.m_button7 = wx.Button( self, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( p.m_button7, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        p.m_button8 = wx.Button( self, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( p.m_button8, 0, wx.ALL, 5 )
        
        p.SetSizer( fgSizer1)
        p.Layout()
        




        sizer = wx.BoxSizer()
        sizer.Add(p, 1, wx.EXPAND)
        self.SetSizer(fgSizer1)
        
        wx.CallAfter(self.Layout)
