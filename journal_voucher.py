from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals

def report_JournalEntry(ref_id=50,pdf_name='test.pdf'):
    table = Table()

    j = Journals()
    col = table.add_column('Account Name',300)

    col = table.add_column('debit')
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT

    col = table.add_column('credit')
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT


    for x in j.get_jelist(ref_id,0,0):
        table.add_row( [x[8],  x[5], x[6] ])

    table.sum_column('debit')
    table.sum_column('credit')

    report = Report(pdf_name)
    report.title = 'RD Employees Multipurpose Cooperative'
    report.author = 'Printed by UserName'
    report.add(Heading('Journal Voucher'))
    report.add(table)
    report.create()
        
        
if __name__ == '__main__':
    report_JournalEntry(50,'test3.pdf')
            
