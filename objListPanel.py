

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from AccountInfo import *

class Book(object):
    #----------------------------------------------------------------------
    def __init__(self, title, author, isbn, mfg):
        self.isbn = isbn
        self.author = author
        self.mfg = mfg
        self.title = title

class objListPanel:
    def __init__(self,parent_window):
        self.parent_window = parent_window
        self.book = parent_window.book


        self.products = [Book("wxPython in Action", "Robin Dunn",
                              "1932394621", "Manning"),
                         Book("Hello World", "Warren and Carter Sande",
                              "1933988495", "Manning")]
        self.the_panel = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.account_info = AccountInfo()

        account_list = {}
        for u in self.account_info.get_list():
            account_list[u['user_id']] = (u['username'],u['firstname'],u['lastname'])
        account_list = account_list.items()
        account_list.sort()
        account_list = [[str(k)] + list(v) for k,v in account_list]

        self.the_panel.dataOlv = ObjectListView(self.the_panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setBooks()

        # Allow the cell values to be edited when double-clicked
        self.the_panel.dataOlv.cellEditMode = ObjectListView.CELLEDIT_SINGLECLICK
        #self.the_panel.dataOlv.cellEditMode = ObjectListView.CELLEDIT_F2ONLY

        # create an update button
        updateBtn = wx.Button(self.the_panel, wx.ID_ANY, "Update OLV")
        #updateBtn.Bind(wx.EVT_BUTTON, self.updateControl)

        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        mainSizer.Add(self.the_panel.dataOlv, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(updateBtn, 0, wx.ALL|wx.CENTER, 5)
        self.the_panel.SetSizer(mainSizer)

    def setBooks(self, data=None):
        self.the_panel.dataOlv.SetColumns([
            ColumnDefn("Title", "left", 220, "title"),
            ColumnDefn("Author", "left", 200, "author"),
            ColumnDefn("ISBN", "right", 100, "isbn"),
            ColumnDefn("Mfg", "left", 180, "mfg")
        ])

        self.the_panel.dataOlv.SetObjects(self.products)

    def OnNewView(self,event):
        pass

    def OnAddRow(self,event):
        print "Add Row"
        pass

    def OnDeleteRows(self,event):
        pass


    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()

    def return_panel(self):
        return self.the_panel

def CreateObjListWindow(the_parent_window):
   panel = objListPanel(the_parent_window)
   return panel.return_panel()

if __name__ == '__main__':
    class MyApp(wx.App):
        def OnInit(self):
            app = wx.App()
            frame = wx.Frame (None, -1, 'Demo PromptingComboBox Control', size=(400, 150))
            objListPanel(frame)
            frame.Show()
            app.MainLoop()


# if __name__ == '__main__':
    # print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
