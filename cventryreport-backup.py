from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 100
        
    def _draw_header(self):

        super(ReportSpecialHeader,self)._draw_header()
        config = Configuration()
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(180,770,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(230,760,complete_address)   
        #self.canvas.drawString(27,710,"EXPLANATION: " + self.getExplanation())
        self.canvas.setFont('Helvetica', 12)
        self.canvas.drawString(250,720,"C H E C K    V O U C H E R")
        #self.canvas.drawString(0,750,"0123456789012345678901234567890123456789012345678901234567890123456789")     

    def setExplanation(self,explanation=None):
        self.explanation = explanation
    def getExplanation(self):
        return self.explanation               

class SpecialTable(Table):

    
    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):
        pass
        #self.top_margin = 300
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        canvas.setFont('Helvetica', 8)
        config = Configuration()
        canvas.line(24,yoff-20,580,yoff-20)
        canvas.line(24,yoff-60,580,yoff-60)
        canvas.line(24,yoff-20,24,yoff-60)
        canvas.line(580,yoff-20,580,yoff-60)
        canvas.line(163,yoff-20,163,yoff-60)
        canvas.line(302,yoff-20,302,yoff-60)
        canvas.line(441,yoff-20,441,yoff-60)
       
        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")
       
        canvas.drawString(27,yoff-30,"Prepared By:")
        canvas.drawString(166,yoff-30,"Checked By:")
        canvas.drawString(305,yoff-30,"Approved By:")
        canvas.drawString(444,yoff-30,"JV NO: " + self.getInvoiceNumber())
        canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.drawString(44,yoff-50,preparedByText)
        canvas.drawString(183,yoff-50,checkedByText)
        canvas.drawString(322,yoff-50,approvedByText)



def cvEntryReport(ref_id=50,pdf_name='test.pdf',title="Voucher Number"):
    table = SpecialTable()

    j = Journals()
    col = table.add_column('Account Title',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Subsidiary',100)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('Description',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    
    journals_listing = Journals()
    explanation = ""
    if ref_id:
        data = journals_listing.db.journal_main.filter(journals_listing.db.journal_main.id == ref_id).one()
        explanation = data.description
        table.setInvoiceNumber(data.purchase_invoice_id)
        table.setPostDate("{:%m/%d/%Y}".format(data.post_date))

    

    col = table.add_column('debit')
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('credit')
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.RIGHT
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    for x in j.get_jelist(ref_id,0,0):
        table.add_row( [x[8], x[10],x[4], x[5], x[6] ])

    table.sum_column('debit')
    col = table.get_footer_field('debit')    
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    
    table.sum_column('credit')
    
    col = table.get_footer_field('credit')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True

    col = table.get_footer_field('Subsidiary')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Account Title')
    col.style.horizontal_alignment = alignment.LEFT
    col.style.bold = True
    col.style.size = 10
    col.value = ""

    col = table.get_footer_field('Description')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9
    col.value = "T O T A L "

    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by UserName'
    #report.add(veryNiceHeader)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    cvEntryReport(50,'test3.pdf','Check Voucher ')
    os.system('start test3.pdf')
