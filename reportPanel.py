
import  wx
have_package = True
try:
    import fitz
except ImportError:    
    try:
        import PyPDF2
    except ImportError:
        try:
            import pyPdf
        except ImportError:
            have_package = False

if have_package:            
    from wx.lib.pdfviewer import pdfViewer, pdfButtonPanel

#----------------------------------------------------------------------

class reportPanel:
    def __init__(self, parent):
        self.parent_window = parent             
        self.book = parent.book
        self.the_panel =  wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.the_panel.SetLabel("ReportWindow")        

       
        hsizer = wx.BoxSizer( wx.HORIZONTAL )
        vsizer = wx.BoxSizer( wx.VERTICAL )
        self.the_panel.buttonpanel = pdfButtonPanel(self.the_panel, wx.NewId(),
                                wx.DefaultPosition, wx.DefaultSize, 0)  
        vsizer.Add(self.the_panel.buttonpanel, 0,
                                wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.LEFT|wx.RIGHT|wx.TOP, 5)
        self.the_panel.viewer = pdfViewer( self.the_panel, wx.NewId(), wx.DefaultPosition,
                                wx.DefaultSize, wx.HSCROLL|wx.VSCROLL|wx.SUNKEN_BORDER)
        vsizer.Add(self.the_panel.viewer, 1, wx.GROW|wx.LEFT|wx.RIGHT|wx.BOTTOM, 5)

        hsizer.Add(vsizer, 1, wx.GROW|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5) 
        self.the_panel.SetSizer(hsizer)
        self.the_panel.SetAutoLayout(True)

        # introduce buttonpanel and viewer to each other
        self.the_panel.buttonpanel.viewer = self.the_panel.viewer
        self.the_panel.viewer.buttonpanel = self.the_panel.buttonpanel


        wx.BeginBusyCursor()
        self.the_panel.viewer.LoadFile("test3.pdf")
        wx.EndBusyCursor()
        
        
    def OnLoadButton(self, event):
        pass

    def return_panel(self):        
        return self.the_panel

def CreateReportWindow(the_parent_window,pdf_name):
   panel = reportPanel(the_parent_window,pdf_name)
   return panel.return_panel()
   
