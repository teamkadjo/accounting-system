from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab import paper
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from reportlab.lib.pagesizes import legal, landscape
from Journals import Journals
from reportlab.lib.colors import *
import os
from Configuration import *
from Users import *
import datetime
import sys
import subprocess

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = 450
        # print "Page width is {}".format(self.page_width)
        # print "Page height is {}".format(self.page_height)
        #self._working_height = self._working_height - 35

    def _draw_header(self):
        #super(ReportSpecialHeader,self)._draw_header()
        config = Configuration()
        self.canvas.setLineWidth(.3)
        self.canvas.setPageSize(landscape(legal))
        self.canvas.setFont('Helvetica', 12)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(350,580,companyName)
        self.canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(400,570,complete_address)
        self.canvas.drawString(385,560,self.period_value)

        self.canvas.setFont('Helvetica-Bold', 10)
        self.canvas.drawString(375,540,"STATEMENT OF INCOME FROM OPERATION")
        self.canvas.setFont('Helvetica', 8)


    def setExplanation(self,explanation=None):
        self.explanation = explanation

    def setCutOffDate(self,date1,date2):
        self.period_value = "For the period of {0} to {1} ".format(datetime.datetime.strptime(date1,"%Y-%m-%d").strftime("%B %d, %Y"),datetime.datetime.strptime(date2,"%Y-%m-%d").strftime("%B %d, %Y"))



class SpecialTable(Table):

    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date

    def getInvoiceNumber(self):
        return self.number

    def getPostDate(self):
        return self.post_date

    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)


    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        config = Configuration()
        canvas.line(24,yoff-20,580,yoff-20)
        canvas.line(24,yoff-60,580,yoff-60)
        canvas.line(24,yoff-20,24,yoff-60)
        canvas.line(580,yoff-20,580,yoff-60)
        canvas.line(163,yoff-20,163,yoff-60)
        canvas.line(302,yoff-20,302,yoff-60)
        canvas.line(441,yoff-20,441,yoff-60)

        preparedByText =  config.getConfig("PREPARED_BY")
        checkedByText =  config.getConfig("CHECKED_BY")
        approvedByText =  config.getConfig("APPROVED_BY")

        canvas.drawString(27,yoff-30,"Prepared By:")
        canvas.drawString(166,yoff-30,"Checked By:")
        canvas.drawString(305,yoff-30,"Approved By:")
#        canvas.drawString(444,yoff-30,"JV NO: " + self.getInvoiceNumber())
#        canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        canvas.drawString(44,yoff-50,preparedByText)
        canvas.drawString(183,yoff-50,checkedByText)
        canvas.drawString(322,yoff-50,approvedByText)
        self.auto_width(canvas)


def incomeStatement_report(pdf_name='test.pdf',title="Voucher Number",startdate='2016-06-01',cutoff_date='2015-10-31',username='admin',cost_center=None):
    table = SpecialTable()

    u = Users()
    uinfo = u.get_userinfo(username)

    j = Journals()
    col = table.add_column('Description',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')
    col.header.style.size = 8

    col.row.style.size = 8
    col.row.style.horizontal_alignment = alignment.LEFT

    col = table.add_column('LENDING',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('TRADING',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('CANTEEN',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('BAKERY',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('TOTAL',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.row.style.size = 8
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')


    journals_listing = Journals()

    config = Configuration()
    acct_period = config.getConfig("CURRENT_ACCOUNTING_PERIOD")


    list_data = journals_listing.add_income_statement(startdate,cutoff_date,30)
    table.add_row(["REVENUE ",0,0,0,0,0])


    spaces = "      "

    printed = 0
    income_total = 0
    total_sales = 0
    total_admin = 0
    total_canteen = 0
    total_bakery = 0
    total_otherincome = 0
    total_otherincome_admin = 0
    total_otherincome_trading = 0
    total_otherincome_canteen = 0
    total_otherincome_bakery = 0
    total_trading = 0
    for r in list_data:
        if r.descript.startswith("Sale"):
            if printed == 0:
                table.add_row([spaces + "SALES",0,0,0,0,0])
                printed = 1
            total_sales = total_sales + abs(r.balance)
            total_admin = total_admin + abs(r.admin)
            total_trading = total_trading + abs(r.trading)
            total_canteen = total_canteen + abs(r.canteen)
            total_bakery = total_bakery + abs(r.bakery)
        else:
            if r.descript.startswith("Other Income") :
                if printed == 1:
                    table.add_row(["Total Sales ",total_admin,total_trading,total_canteen,total_bakery,income_total])
                    printed = 0
                    table.add_row([spaces + "",0,0,0,0,0])
                    table.add_row([spaces + "OTHER INCOME",0,0,0,0,0])
                total_otherincome = total_otherincome + abs(r.balance)
                total_otherincome_admin = total_otherincome_admin + abs(r.admin)
                total_otherincome_trading = total_otherincome_trading + abs(r.trading)
                total_otherincome_canteen = total_otherincome_canteen + abs(r.canteen)
                total_otherincome_bakery = total_otherincome_bakery + abs(r.bakery)

        table.add_row( [spaces + spaces + " " + r.descript,abs(r.admin),abs(r.trading),abs(r.canteen),abs(r.bakery),abs(r.balance)] )
        income_total = income_total + abs(r.balance)


    table.add_row(["Total Other Income ",total_otherincome_admin,total_otherincome_trading,total_otherincome_canteen,total_otherincome_bakery,total_otherincome])


    table.add_row([" ",0,0,0,0,0])
    table.add_row(["Cost of Sales ",0,0,0,0,0])


    list_data = journals_listing.get_inventory_previousmonth()
    table.add_row(["    Beginning Inventory ",list_data.admin,list_data.trading,list_data.canteen,list_data.bakery,(list_data.admin + list_data.trading + list_data.canteen + list_data.bakery)])
    inv_beg_admin = list_data.admin
    inv_beg_trading = list_data.trading
    inv_beg_canteen = list_data.canteen
    inv_beg_bakery = list_data.bakery
    inv_beg_total = (list_data.admin + list_data.trading + list_data.canteen + list_data.bakery)



    list_data = journals_listing.add_income_statement(startdate,cutoff_date,32)
    table.add_row(["    Purchases ",0,0,0,0,0])


    spaces = "      "

    printed = 0
    purchases_total = 0
    purchases_admin = 0
    purchases_trading = 0
    purchases_canteen = 0
    purchases_bakery = 0
    for r in list_data:
        table.add_row( [spaces + spaces + " " + r.descript,abs(r.admin),abs(r.trading),abs(r.canteen),abs(r.bakery),abs(r.balance)] )
        purchases_total = purchases_total + abs(r.balance)
        purchases_admin = purchases_admin + abs(r.admin)
        purchases_trading = purchases_trading + abs(r.trading)
        purchases_canteen = purchases_canteen + abs(r.canteen)
        purchases_bakery = purchases_bakery + abs(r.bakery)

    list_data = journals_listing.add_income_statement(startdate,cutoff_date,35)
    table.add_row(["    Add Direct Cost ",0,0,0,0,0])


    spaces = "      "

    printed = 0
    directcost_total = 0
    directcost_admin = 0
    directcost_trading = 0
    directcost_canteen = 0
    directcost_bakery = 0
    for r in list_data:
        table.add_row( [spaces + spaces + " " + r.descript,abs(r.admin),abs(r.trading),abs(r.canteen),abs(r.bakery),abs(r.balance)] )
        directcost_total = directcost_total + abs(r.balance)
        directcost_admin = directcost_admin + abs(r.admin)
        directcost_trading = directcost_trading + abs(r.trading)
        directcost_canteen = directcost_canteen + abs(r.canteen)
        directcost_bakery = directcost_bakery + abs(r.bakery)



    table.add_row(["    Goods Available for Sale ",purchases_admin + inv_beg_admin + directcost_admin,purchases_trading + inv_beg_trading + directcost_trading ,purchases_canteen + inv_beg_canteen + directcost_canteen,purchases_bakery + inv_beg_bakery + directcost_bakery,purchases_total + inv_beg_total + directcost_total ])

    list_data = journals_listing.get_inventory_current()
    table.add_row(["    Less: Ending Inventory ",list_data.admin,list_data.trading,list_data.canteen,list_data.bakery,(list_data.admin + list_data.trading + list_data.canteen + list_data.bakery)])
    inv_end_admin = list_data.admin
    inv_end_trading = list_data.trading
    inv_end_canteen = list_data.canteen
    inv_end_bakery = list_data.bakery

    co_sales_admin = purchases_admin + inv_beg_admin + directcost_admin - inv_end_admin
    co_sales_trading = purchases_trading + inv_beg_trading + directcost_trading - inv_end_trading
    co_sales_canteen = purchases_canteen + inv_beg_canteen + directcost_canteen - inv_end_canteen
    co_sales_bakery = purchases_bakery + inv_beg_bakery + directcost_bakery - inv_end_bakery
    co_sales_total = co_sales_admin + co_sales_trading + co_sales_canteen + co_sales_bakery

    table.add_row(["Cost Of Sales",co_sales_admin,co_sales_trading,co_sales_canteen,co_sales_bakery,co_sales_total])


    tgross_admin = (total_admin + total_otherincome_admin) - co_sales_admin
    tgross_trading = (total_trading + total_otherincome_trading) - co_sales_trading
    tgross_canteen = (total_canteen + total_otherincome_canteen) - co_sales_canteen
    tgross_bakery = (total_bakery + total_otherincome_bakery) - co_sales_bakery
    tgross_total = (tgross_admin + tgross_trading + tgross_canteen + tgross_bakery)

    table.add_row([" ",0,0,0,0,0])
    table.add_row(["TOTAL GROSS INCOME ",tgross_admin,tgross_trading,tgross_canteen,tgross_bakery,tgross_total])


    table.add_row([" ",0,0,0,0,0])

    list_data = journals_listing.add_income_statement(startdate,cutoff_date,34)
    table.add_row(["GENERAL & ADMINISTRATIVE EXPENSES ",0,0,0,0,0])


    spaces = "      "

    printed = 0
    expenses_total = 0
    expenses_admin = 0
    expenses_trading = 0
    expenses_canteen = 0
    expenses_bakery = 0
    for r in list_data:
        table.add_row( [spaces + spaces + " " + r.descript,abs(r.admin),abs(r.trading),abs(r.canteen),abs(r.bakery),abs(r.balance)] )
        expenses_total = expenses_total + abs(r.balance)
        expenses_admin = expenses_admin + abs(r.admin)
        expenses_trading = expenses_trading + abs(r.trading)
        expenses_canteen = expenses_canteen + abs(r.canteen)
        expenses_bakery = expenses_bakery + abs(r.bakery)
    explanation = ""

    table.add_row(["TOTAL EXPENSES ",expenses_admin,expenses_trading,expenses_canteen,expenses_bakery,expenses_total])
    table.add_row([" ",0,0,0,0,0])

    table.add_row(["NET INCOME ",(tgross_admin) - expenses_admin,(tgross_trading) - expenses_trading,(tgross_canteen) - expenses_canteen,(tgross_bakery) - expenses_bakery,(tgross_total) - expenses_total])


    #col = table.get_footer_field('Description')
    #col.style.horizontal_alignment = alignment.LEFT
    #col.style.bold = True
    #col.style.size = 10
    #col.value = ""

    #col = table.get_footer_field('Value')
    #col.style.horizontal_alignment = alignment.LEFT
    #col.style.bold = True
    #col.style.size = 10
    #col.value = ""





    #table.sum_column('Sub Total')
    #table.sum_column('Total')


    veryNiceHeader = Heading(None)

    report = ReportSpecialHeader(pdf_name)
    report.page_width, report.page_height = paper.LEGAL_LANDSCAPE

    print cutoff_date
    report.setCutOffDate(startdate,cutoff_date)
    #report.setExplanation(explanation)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {} '.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()
    return
    #print "End of creating..."



if __name__ == '__main__':
    incomeStatement_report('incomestatement.pdf','Income Statement','2016-02-01','2018-10-18','admin')
    if sys.platform == "linux2":
        subprocess.Popen("evince incomestatement.pdf",shell=True)
    else:
        os.system('start incomestatement.pdf')
