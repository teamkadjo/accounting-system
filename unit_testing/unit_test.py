
import sys
import os
import random 
os.chdir("C:/Users/jojo/Dropbox/codes/Python/WXPaymentRecorder/src/")
sys.path.append(".") 

import unittest

import random
from AccountInfo import * 

class TestAccountManager(unittest.TestCase):
    
    def setUp(self):
        self.ac = AccountInfo()
    
    def test_insert(self):
        r = random.SystemRandom()
        the_username = 'jopix' + str(int(r.random() * 1000000000))        
        self.assertIsNotNone(self.ac.insert_account(username=the_username,password='picasso',firstname='Jojo',lastname='PepeSmith'),"Failure in insertion command")

    def test_update(self):
        r = random.SystemRandom()
        the_pass_random = 'jojo123' 
        self.assertIsNotNone(self.ac.update_account(21,{'firstname':'Jojo','lastname':'Lacson','password':'papasmith'}),"Failure in update command")
    
    def test_delete(self):
        self.assertIsNotNone(self.ac.delete_account(44444),"Failure in deleting command")
        
    def test_find(self):
        self.assertIsNotNone(self.ac.find_account(1),"Unable to find 'jojo' record")
    
    def test_list_all(self):
        pass


if __name__ == '__main__':
    unittest.main()    
