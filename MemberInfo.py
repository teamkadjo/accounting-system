

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *

        
class memberInfoPanel(wxSpecialPanel):
    def __init__(self,parent_window,member_id=None):  
        self.parent_window = parent_window             
        self.book = parent_window.book
        self.member_id = member_id
      
        super(memberInfoPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("MemberInfo")
        self.Contacts = Contacts()
        self.contacts_ = self.Contacts.db.contacts

        self.setEscapeHandler(self.CloseThisPanel)   

        mainSizer = wx.BoxSizer(wx.VERTICAL)  

        sizerTop = wx.FlexGridSizer( 0, 2, 0, 0 )
        
        firstname_label = wx.StaticText(self, -1, "Firstname:", (20,20))
        self.firstname = EnterTextCtrl(self, -1, "", (100,20), (150,-1))
        lastname_label = wx.StaticText(self, -1, "Lastname:", (520,20))
        self.lastname = EnterTextCtrl(self, -1, "", (600,20), (150,-1))     
        
           
        address1 = wx.StaticText(self, -1, "Street: " )
        self.address1 = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     

        city = wx.StaticText(self, -1, "City/Town:" )
        self.city = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     
        province = wx.StaticText(self, -1, "Province:" )
        self.province = EnterTextCtrl(self, -1, "",(600,20),(300,-1))     
        zipcode = wx.StaticText(self, -1, "Zip Code:" )
        self.zipcode = EnterTextCtrl(self, -1, "",(600,20), (100,-1))     

            
        sizerTop.Add(firstname_label, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.firstname, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(lastname_label, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.lastname, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(address1, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.address1, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(city, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.city, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(province, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.province, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(zipcode, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.zipcode, -1, wx.ALL|wx.CENTER, 5)


        if member_id:
            self.contact = self.contacts_.filter_by(id = member_id).one()
            self.firstname.SetValue(self.contact.contact_first)
            self.lastname.SetValue(self.contact.contact_last)
            
            address_book = self.Contacts.db.address_book
            
            self.address_book = address_book.filter_by(ref_id = member_id).one()
            if self.address_book:               
                self.address1.SetValue(self.address_book.address1)
                self.city.SetValue(self.address_book.city_town)
                self.province.SetValue(self.address_book.state_province)
                self.zipcode.SetValue(self.address_book.postal_code)
        
        
        if self.member_id :
            updateLabel = "Update (Ctr-S)"
        else:
            updateLabel = "Save (Ctr-S)"            
        
        self.btn_Update = wx.Button(self, -1, updateLabel)          
        self.btn_Cancel = wx.Button(self, -1, "Cancel (Esc)")          
        self.Bind(wx.EVT_BUTTON, self.updateMemberInfo, self.btn_Update)
        self.Bind(wx.EVT_BUTTON, self.CloseNow, self.btn_Cancel)


        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer.Add(self.btn_Update, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btn_Cancel, 0, wx.ALL|wx.LEFT, 5)
        
        id_updateMemberInfo = wx.ID_ANY
        self.Bind(wx.EVT_MENU,self.updateMemberInfo,id=id_updateMemberInfo)
        
        mainSizer.Add(sizerTop, 0, wx.TOP|wx.LEFT|wx.BOTTOM, 5)
        mainSizer.Add(btnsizer, 0, wx.LEFT|wx.BOTTOM, 5)
        self.SetSizer(mainSizer) 

       
        self.firstname.SetFocus()
        self.firstname.SetSelection(-1,-1)  
        
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('S'), id_updateMemberInfo )])
        self.SetAcceleratorTable(accel_tbl)  
     
               
    def updateMemberInfo(self,evt):  
        type_window = "Member information of " + self.firstname.GetValue() + " " + self.lastname.GetValue()
        if self.member_id:    
            data = {'id':self.member_id,'firstname':self.firstname.GetValue(),'lastname':self.lastname.GetValue(),'city_town':self.city.GetValue(),'state_province':self.province.GetValue(),'zipcode':self.zipcode.GetValue(),'address1':self.address1.GetValue()}
            self.Contacts.updateMemberInfo(data)
            self.parent_window.refreshPage('Members')
            dlg = wx.MessageDialog(self.parent_window, "Finished updating...", "Updating " + type_window,style=wx.OK|wx.CENTRE)
            dlg.ShowModal()             
        else:
            data = {'id':None,'firstname':self.firstname.GetValue(),'lastname':self.lastname.GetValue(),'city_town':self.city.GetValue(),'state_province':self.province.GetValue(),'zipcode':self.zipcode.GetValue(),'address1':self.address1.GetValue()}
            self.member_id = self.Contacts.addMemberRecord(data)
            self.parent_window.refreshPage('Members')           
            dlg = wx.MessageDialog(self.parent_window, "Finished saving...", "Saving " + type_window,style=wx.OK|wx.CENTRE)
            self.btn_Update.SetLabel("Update (Ctr-S)")
            dlg.ShowModal()


    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("MemberInfo",True)  
        

    def CloseNow(self,evt):
        self.CloseThisPanel()

    
    def return_panel(self):        
        return self

def CreateMemberInfo(the_parent_window,member_id):
   panel = memberInfoPanel(the_parent_window,member_id)
   return panel
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
