if __name__ == '__main__':
    import time
    from reportlab.lib.pagesizes import A4
    from reportlab.platypus import BaseDocTemplate, Frame, PageTemplate
    from reportlab.lib.units import mm
    from reportlab.platypus.flowables import PageBreak, Spacer
    from reportlab.platypus.paragraph import Paragraph
    from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
    from reportlabx import *

    styleSheet = getSampleStyleSheet()
    MARGIN_SIZE = 25 * mm
    PAGE_SIZE = A4

    def create_pdfdoc(pdfdoc, story):
        """
        Creates PDF doc from story.
        """
        pdf_doc = BaseDocTemplate(pdfdoc, pagesize = PAGE_SIZE,
            leftMargin = MARGIN_SIZE, rightMargin = MARGIN_SIZE,
            topMargin = MARGIN_SIZE, bottomMargin = MARGIN_SIZE)
        main_frame = Frame(MARGIN_SIZE, MARGIN_SIZE,
            PAGE_SIZE[0] - 2 * MARGIN_SIZE, PAGE_SIZE[1] - 2 * MARGIN_SIZE,
            leftPadding = 0, rightPadding = 0, bottomPadding = 0,
            topPadding = 0, id = 'main_frame')
        main_template = PageTemplate(id = 'main_template', frames = [main_frame])
        pdf_doc.addPageTemplates([main_template])

        pdf_doc.build(story)

    table_style = [
        ('GRID', (0,0), (-1,-1), 1, None),
        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
        ('LEFTPADDING', (0,0), (-1,-1), 3),
        ('RIGHTPADDING', (0,0), (-1,-1), 3),
        ('FONTNAME', (0,0), (-1,-1), 'Times-Bold'),
        ('FONTNAME', (1,1), (-2,-2), 'Times-Roman'),
        ('FONTSIZE', (0,0), (-1,-1), 10)
    ]

    data = [
        ['', 'Col 1', 'Col 2', 'Col3', 'Row sum'],
        ['Row 1', 999991, 2, 3, RowSum()],
        ['Row 2', 999994, 5, 6, RowSum()],
        ['Row 3', 999997, 8, 9, RowSum()],
        ['Row 3', 1, 8, 9, RowSum()],
        ['Row 4', 2, 8, 9, RowSum()],
        ['Row 5', 3, 8, 9, RowSum()],
        ['Row 6', 4, 8, 9, RowSum()],
        ['Row 7', 5, 8, 9, RowSum()],
        ['Row 8', 6, 8, 9, RowSum()],
        ['Row 9', 7, 8, 9, RowSum()],
        ['Row 10', 8, 8, 9, RowSum()],
        ['Row 11', 9, 8, 9, RowSum()],
        ['Row 12', 1, 9, 9, RowSum()],
        ['Row 13', 2, 9, 9, RowSum()],
        ['Page col sum', PageColSum(), PageColSum(), PageColSum(), PageColSum()],
    ]

    spreadsheet_table = SpreadsheetTable(data, repeatRows = 1, repeatRowsB = 1)
    spreadsheet_table.setStyle(table_style)
    print 'Generating Table to spreadsheet-man.pdf in current working directiory'
    story = []
    story.append(Paragraph("This shows how formulas behave without split. Notice that col sum formula for col 1 detected not enough space to draw itself so returned ### instead of value.", styleSheet['BodyText']))
    story.append(Spacer(0, 10 * mm))
    story.append(spreadsheet_table)
    story.append(PageBreak())
    story.append(Paragraph("This shows how formulas behave with split.", styleSheet['BodyText']))
    story.append(Spacer(0, 10 * mm))
    spreadsheet_table = SpreadsheetTable(data, repeatRows = 1, repeatRowsB = 1)
    spreadsheet_table.setStyle(table_style)
    s = spreadsheet_table.split(PAGE_SIZE[0], 90)

    for part in s:
        story.append(part)
        story.append(Spacer(0, 10 * mm))

    create_pdfdoc('spreadsheet-man.pdf', story)
