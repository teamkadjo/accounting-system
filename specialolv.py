from ObjectListView import ObjectListView, ColumnDefn
import wx


class SpecialOLV(ObjectListView):
    def __init__(self, *args, **kwargs):
        super(SpecialOLV,self).__init__(*args,**kwargs)
        self._editnow = self.EditCommand
        self._deletenow = self.DeleteCommand
        self._escapenow = self.EscapeCommand
        
    def _HandleChar(self, evt):
        keycode = evt.GetKeyCode()
        if keycode==wx.WXK_DELETE:
            self._deletenow()
        elif keycode == wx.WXK_RETURN:
            self._editnow()
            evt.StopPropagation()
            return
        elif keycode == wx.WXK_ESCAPE:
            self._escapenow()
        
        evt.Skip()       

    
    def setEditCallback(self,cb=None):
        self._editnow = cb
    
    def setDeleteCallback(self,cb=None):
        self._deletenow = cb

    def setEscapeCallback(self,cb=None):
        self._escapenow = cb
    
    def EditCommand(self):
        print "Edit command.."
        pass
    
    def DeleteCommand(self):
        print "Delete command.."
        pass
    
    def EscapeCommand(self):
        print "Escape command.."
        pass


