
import wx
from PromptingComboBox import *

###########################################################################
## Class wxSpecialPanel
###########################################################################

class wxSpecialPanel(wx.Panel):
    def __init__(self, parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.TAB_TRAVERSAL|wx.NO_BORDER, name="") :
        super(wxSpecialPanel,self).__init__(parent,id,pos,size,style)
        self.Bind(wx.EVT_CHAR_HOOK, self.OnKeyUP)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyUP)
        self.escapehandler = self.escapeHandler

    def setEscapeHandler(self,cb_handler):
        self.escapehandler = cb_handler

    def OnKeyUP(self, event):
        keyCode = event.GetKeyCode()
        if keyCode == wx.WXK_ESCAPE:
            self.escapehandler()
            return
        event.Skip()    

    def escapeHandler(self):
        print "Escape was pressed..."


class TrialPanel ( wxSpecialPanel ):
    
    def __init__( self, parent ):
        super(TrialPanel,self).__init__ (parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
        
        fgSizer2 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fgSizer2.SetFlexibleDirection( wx.BOTH )
        fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        #self.m_auinotebook2 = wx.aui.AuiNotebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_DEFAULT_STYLE )
        
        #fgSizer2.Add( self.m_auinotebook2, 1, wx.EXPAND |wx.ALL, 5 )
        
        self.m_staticText15 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText15.Wrap( -1 )
        fgSizer2.Add( self.m_staticText15, 0, wx.ALL, 5 )
        
        self.m_textCtrl17 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer2.Add( self.m_textCtrl17, 0, wx.ALL, 5 )
        
        self.m_bpButton1 = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW )
        fgSizer2.Add( self.m_bpButton1, 0, wx.ALL, 5 )
        
        self.m_textCtrl18 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer2.Add( self.m_textCtrl18, 0, wx.ALL, 5 )
        
        m_comboBox1Choices = []
        self.m_comboBox1 = wx.ComboBox( self, wx.ID_ANY, u"Combo!", wx.DefaultPosition, wx.DefaultSize, m_comboBox1Choices, 0 )
        fgSizer2.Add( self.m_comboBox1, 0, wx.ALL, 5 )
        
        self.m_listCtrl1 = wx.ListCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_ICON )
        fgSizer2.Add( self.m_listCtrl1, 0, wx.ALL, 5 )
        
        m_radioBox1Choices = [ u"Radio Button" ]
        self.m_radioBox1 = wx.RadioBox( self, wx.ID_ANY, u"wxRadioBox", wx.DefaultPosition, wx.DefaultSize, m_radioBox1Choices, 1, wx.RA_SPECIFY_COLS )
        self.m_radioBox1.SetSelection( 0 )
        fgSizer2.Add( self.m_radioBox1, 0, wx.ALL, 5 )
        
        
        self.SetSizer( fgSizer2 )
        self.Layout()
    
    
if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame (None, -1, 'Demo PromptingComboBox Control', size=(400, 150))
    TrialPanel(frame)
    frame.Show()
    app.MainLoop()





