
import wx.lib.colourdb as wb


from TextCtrlAUC import *



class SpecialCB(TextCtrlAUC):
    def __init__(self,parent,choices_=None,selectCallback_=None,value='',**args):
        super(SpecialCB,self).__init__(parent,colNames=None, choices = None, multiChoices=choices_, showHead=True, dropDownClick=True,colFetch=1, colSearch=1, hideOnNoMatch=True,selectCallback=selectCallback_,style=wx.TE_PROCESS_ENTER,**args)
        
        wx.lib.colourdb.updateColourDB() 
        
        self.SetValue(value)
        self.dropdownlistbox.SetBackgroundColour(wx.Colour(83,92,170))
        
    

