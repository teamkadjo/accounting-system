'''+-----------------------------------------------------------------+
// |                   TopSoftDev POS                                |
// +-----------------------------------------------------------------+
// | Copyright(c) 2016 TopSoftDev POS (topsoftdev.kapamilya.info/pos)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: EntryWindow.py
'''

import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Journals import *
from Password import *
from EditTransaction import *
from ObjectListView import EVT_CELL_EDIT_STARTING, EVT_CELL_EDIT_FINISHING, CellEditorRegistry

import OwnerDrawnEditor
from kawal import *
from locale import *
from pprint import pprint
from journalentryreport import *
from cventryreport import *
import datetime

from specialolv import *
from checqueprinting_normal import *
from CheckNamePrinting import *
import re
from EnterTextCtrl import *
from EnterDatePicker import *
import images2

try:
    from agw import pybusyinfo as PBI
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.pybusyinfo as PBI

#locale = wx.Locale(wx.LANGUAGE_ENGLISH)


class EntryWindow(object):
    #----------------------------------------------------------------------
    def __init__(self, gl_account="", sl_id="",sl_description="", costcenter_id="",cost_center="ADMIN", description="", debit="",credit="",rec_id=""):
        self.gl_account = gl_account
        self.sl_id = sl_id
        self.sl_description = sl_description
        self.cost_center = cost_center
        self.cost_center_id = costcenter_id

        self.description = description
        self.debit = debit
        self.credit = credit
        self.id = rec_id

    def SetCostCenter(self, value):
        if value is None or value == "":
            self.cost_center = None
        else:
            self.cost_center = value

    def SetGLAccount(self,value):
        if value is None or value == "":
            self.gl_account = None
        else:
            self.gl_account = value


    def setNewValue(self,obj):
        self.gl_account = obj["account_id"] + " - " + obj["account_description"]
        self.sl_id = obj["subledger_id"]
        self.cost_center = obj["costcenter_name"].upper()
        self.cost_center_id = obj["costcenter_id"]
        self.description = obj["description"]
        self.sl_description = obj["subledger_description"]



        if obj["debit"]:
            self.debit = "{:,.2f} ".format(float(obj["debit"]))
        else:
            self.debit = "0.00"

        if obj["credit"]:
            self.credit ="{:,.2f} ".format(float(obj["credit"]))
        else:
            self.credit = "0.00"
        if obj["refid"]:
            self.id = obj["refid"]





class entryjournalPanel:
    def __init__(self,parent_window,ref_id,acc_type):
        self.total_value = 0
        self.parent_window = parent_window
        self.book = parent_window.book
        self.acc_type = acc_type
        self.ref_id = ref_id

        self.the_panel = wx.Panel(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.the_panel.SetLabel("EntryWindow")
        self.journals_listing = Journals()

        if ref_id:
            data = self.journals_listing.db.journal_main.filter(self.journals_listing.db.journal_main.id == ref_id).one()
        else:
            data = None
        self.journals = []



        if ref_id :
            for u in self.journals_listing.get_jelist(ref_id,0,0):
                debit = "{:,.2f} ".format(u[5])
                credit = "{:,.2f} ".format(u[6])
                sl_ledger = ' '
                if u[3] == None:
                    sl_ledger = ' '
                else:
                    sl_ledger = u[3]

                self.journals.append(EntryWindow(u.gl_desc,u.sublgr_id,u.subledger,u.project_id,u.cost_center,u.description,debit,credit,u.id))

        self.the_panel.dataOlv = SpecialOLV(self.the_panel, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setEntryWindows()


        self.the_panel.dataOlv.SetEmptyListMsg("There are no transactions posted yet.")
        self.the_panel.dataOlv.SetEmptyListMsgFont(wx.FFont(24, wx.DEFAULT, face="Tekton"))
        id_keyDown = wx.NewId()

        self.the_panel.dataOlv.setEditCallback(self.EditOLVNow)
        self.the_panel.dataOlv.setDeleteCallback(self.DeleteOLVNow)


        self.the_panel.dataOlv.cellEditMode  = ObjectListView.CELLEDIT_NONE


        # Create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)


        #print data.post_date

        self.dpc = EnterDatePicker(self.the_panel, pos=(120,-1),
                                style = wx.DP_DROPDOWN
                                      | wx.DP_SHOWCENTURY)


        if data:
            self.dpc.SetValue(self._pydate2wxdate(data.post_date))

        btnbox1 = wx.BoxSizer(wx.HORIZONTAL)
        s1 = wx.StaticText(self.the_panel, -1, "Date :")
        s2 = wx.StaticText(self.the_panel, -1, "Reference # :", (20,50))
        s3 = wx.StaticText(self.the_panel, -1, "Explanation :", (20,50))


        if data:
            invoice_id = data.purchase_invoice_id
        else:
            invoice_id = "(AUTO-GENERATED)"

        if data:
            explanation = data.description
        else:
            explanation = ""
        self.invoice_id = EnterTextCtrl(self.the_panel, -1, invoice_id, (100,50), (150,-1))
        self.explanation = EnterTextCtrl(self.the_panel, -1, explanation, (100,50), (500,-1))

        self.explanation.SetFocus()

        btnbox1.Add(s1, 0, wx.ALL|wx.LEFT, 5)
        btnbox1.Add(self.dpc, 0, wx.ALL|wx.LEFT, 5)
        btnbox1.Add(s2, 0, wx.ALL|wx.LEFT, 5)
        btnbox1.Add(self.invoice_id, 0, wx.ALL|wx.LEFT, 5)
        btnbox1.Add(s3, 0, wx.ALL|wx.LEFT, 5)
        btnbox1.Add(self.explanation, 0, wx.ALL|wx.LEFT, 5)
        #btnbox1.Add(s3, 0, wx.ALL|wx.LEFT, 5)
        #btnbox1.Add(t3, 0, wx.ALL|wx.LEFT, 5)
        #b1.SetToolTip(wx.ToolTip('Press F2 to enter new Journal Entry'))
        mainSizer.Add(btnbox1, 0, wx.TOP|wx.CENTER|wx.BOTTOM, 5)

        mainSizer.Add(self.the_panel.dataOlv, 1, wx.ALL|wx.EXPAND, 5)


        sbSizer1 = wx.FlexGridSizer( 0, 4, 0, 0 )

        self.m_staticText3 = wx.StaticText( self.the_panel, wx.ID_ANY, u"Total Debit", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE|wx.ALIGN_LEFT|wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE )
        self.m_staticText3.Wrap( -1 )
        sbSizer1.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textCtrl3 = wx.TextCtrl( self.the_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_RIGHT)
        self.m_textCtrl3.SetEditable(False)
        #self.m_textCtrl3.SetDefaultStyle(wx.TE_RIGHT)
        self.m_textCtrl3.SetBackgroundColour((134,227,149))
        sbSizer1.Add( self.m_textCtrl3, 0, wx.ALL, 5 )

        self.m_staticText4 = wx.StaticText( self.the_panel, wx.ID_ANY, u"Total Credit", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        sbSizer1.Add( self.m_staticText4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textCtrl4 = wx.TextCtrl( self.the_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_RIGHT)
        self.m_textCtrl4.SetEditable(False)
        #self.m_textCtrl4.SetDefaultStyle(wx.TE_RIGHT)
        self.m_textCtrl4.SetBackgroundColour((134,227,149))
        sbSizer1.Add( self.m_textCtrl4, 0, wx.ALL, 5 )

        sbSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        sbSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_staticText5 = wx.StaticText( self.the_panel, wx.ID_ANY, u"Unbalanced :", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText5.Wrap( -1 )
        sbSizer1.Add( self.m_staticText5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.totalDebitCredit = wx.TextCtrl( self.the_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_RIGHT)
        self.totalDebitCredit.SetEditable(False)
        #self.m_textCtrl4.SetDefaultStyle(wx.TE_RIGHT)
        self.totalDebitCredit.SetBackgroundColour((255,255,153))
        sbSizer1.Add( self.totalDebitCredit, 0, wx.ALL, 5 )

        mainSizer.Add(sbSizer1, 0, wx.TOP|wx.ALIGN_RIGHT|wx.BOTTOM, 5)

        id_NewEntry = wx.NewId()
        newEntry = wx.Button(self.the_panel, label = "New Entry (F2)",name="newEntry")
        self.the_panel.Bind(wx.EVT_BUTTON, self.newTransactionEntry, newEntry)

        printEntry = wx.Button(self.the_panel, label = "Print Entry (F4)",name="printEntry")
        self.the_panel.Bind(wx.EVT_BUTTON, self.printEntry, printEntry)

        editEntry = wx.Button(self.the_panel, label = "Edit Entry (F3)",name="editEntry")
        self.the_panel.Bind(wx.EVT_BUTTON, self.editTransactionEntry, editEntry)





        b1 = wx.Button(self.the_panel, label="Save (Ctrl-S)", name="newView")
        self.the_panel.Bind(wx.EVT_BUTTON, self.SaveEntry, b1)
        b2 = wx.Button(self.the_panel, label="Close")
        self.the_panel.Bind(wx.EVT_BUTTON, self.CloseEntryWindow, b2)




        btnbox = wx.BoxSizer(wx.HORIZONTAL)
        btnbox.Add(newEntry, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(editEntry, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(printEntry,0,wx.ALL|wx.LEFT,5)

        self.checkPrintButton = None




        btnbox.Add(b1, 0, wx.ALL|wx.LEFT, 5)
        btnbox.Add(b2, 0, wx.ALL|wx.LEFT, 5)

        b1.SetToolTip(wx.ToolTip('Save entry (Ctrl-S)'))
        mainSizer.Add(btnbox, 0, wx.TOP|wx.CENTER|wx.BOTTOM, 5)

        self.the_panel.SetSizer(mainSizer)


        self.the_panel.dataOlv.Bind(wx.EVT_LEFT_DCLICK, self.editTransactionEntry)

        self.the_panel.dataOlv.Bind(EVT_CELL_EDIT_STARTING, self.handleCellEditStarting)
        self.the_panel.dataOlv.Bind(EVT_CELL_EDIT_FINISHING, self.handleCellEditFinishing)

        id_F3 = wx.NewId()
        id_F2 = wx.NewId()
        id_F4 = wx.NewId()
        id_CtrlQ = wx.NewId()
        id_CtrlS = wx.NewId()
        id_CtrlP = wx.NewId()

        randomId = wx.NewId()
        self.the_panel.Bind(wx.EVT_MENU, self.onKeyCombo, id=randomId)
        self.the_panel.Bind(wx.EVT_MENU, self.editTransactionEntry, id=id_F3)
        self.the_panel.Bind(wx.EVT_MENU, self.newTransactionEntry, id=id_F2)
        self.the_panel.Bind(wx.EVT_MENU,self.printEntry,id=id_F4)
        self.the_panel.Bind(wx.EVT_MENU,self.SaveEntry,id=id_CtrlS)
        self.the_panel.Bind(wx.EVT_MENU,self.CloseEntryWindow,id=id_CtrlQ)
        self.the_panel.Bind(wx.EVT_MENU,self.printChecque,id=id_CtrlP)
        self.the_panel.Bind(wx.EVT_CHAR_HOOK, self.OnKeyUP) # closes this page when escape key is press

        #Only show the print check voucher if we are on the check voucher entry window
        if acc_type == "checkvoucher":
            self.checkPrintButton = wx.Button(self.the_panel, label = "Print Check (Ctrl-P)",name="printCheck")
            self.the_panel.Bind(wx.EVT_BUTTON, self.printChecque,self.checkPrintButton)
            btnbox.Add(self.checkPrintButton, 0, wx.ALL|wx.LEFT, 5)

        self.the_panel.dataOlv._SelectAndFocus(0)

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('N'), randomId ),(wx.ACCEL_NORMAL, wx.WXK_F3, id_F3 ),(wx.ACCEL_CTRL,  wx.WXK_F4, id_CtrlQ ),(wx.ACCEL_NORMAL, wx.WXK_F2, id_F2 ),(wx.ACCEL_NORMAL, wx.WXK_F4, id_F4 ),(wx.ACCEL_CTRL,  ord('S'), id_CtrlS ),(wx.ACCEL_CTRL,  ord('P'), id_CtrlP )])
        self.the_panel.SetAcceleratorTable(accel_tbl)

        self.totalData()

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("EntryWindow",True)

    def OnKeyUP(self, event):
        keyCode = event.GetKeyCode()
        if keyCode == wx.WXK_ESCAPE:
            self.CloseThisPanel()
            return
        event.Skip()



    def getFirstSubsidiaryName(self):
        objects = self.the_panel.dataOlv.GetObjects()
        for o in objects:
            if o.sl_description:
                return o.sl_description

    def printChecque(self,evt):

        the_obj = self.the_panel.dataOlv.GetSelectedObjects()
        if the_obj:
            inside_object = the_obj[0]
            if inside_object:
                account_number = inside_object.gl_account.split(" - ")
                the_account = self.journals_listing.get_one_account_by_id(account_number[0])
                print the_account.primary_acct_id
                if the_account.primary_acct_id == "11130": #If Cash on Bank
                    sl_name = re.split("-",inside_object.description)[1].strip()
                    #sl_name = self.getFirstSubsidiaryName()
                    #sl_name = re.sub("\(|\d+|\)","",sl_name).strip()
                    dlg = CheckNamePrintingDialog(None,sl_name)
                    #dlg.ShowWindowModal()
                    if dlg.ShowModal() == wx.ID_OK:
                        cvnumber = self.invoice_id.GetValue()
                        chkInfo = dlg.getInfo()
                        amount = inside_object.credit.replace(",","")
                        amount_value = float(amount)
                        info = {"name": chkInfo['name'],'amount':amount_value,'date':chkInfo['date'],'cvnumber':cvnumber}
                        chk = CheckPrinting('checkprinting-normal.pdf',info)
                        chk.create_pdf_normal()
                        self.parent_window.SpecialReportWindow("Checque Print Preview","checkprinting-normal.pdf")

    def EditOLVNow(self):
        self.EditTransaction()
        pass

    def CloseEntryWindow(self,evt):
        self.CloseThisPanel()

    def SaveEntry(self,evt):
        evt.Skip()
        if self.ref_id:
#            print "This is updating..."
            self.UpdateJournal()
        else:
            self._SaveNew()

    def UpdateJournal(self):
        the_value = self.totalDebitCredit.GetValue()
        the_value = the_value.replace(",","")
        if (float(the_value) != 0 ):
            dlg = wx.MessageDialog(self.parent_window, "Please check the balance of entries first.",style=wx.OK|wx.CENTRE)
            dlg.ShowModal()
            return


        postdate = self.dpc.GetValue().FormatISODate()
        explanation = self.explanation.GetValue()
        self.journals_listing.update_journal(self.ref_id,postdate,explanation,self.total_value)
        self.journals_listing.delete_journalitems(self.ref_id)
        objects = self.the_panel.dataOlv.GetObjects()

        max = len(objects)

        if self.acc_type == "journal":
            type_window = "Journal Entry"
        else:
            type_window = "Check Voucher"



        dlg2 = wx.ProgressDialog("Saving " + type_window,
                               "Saving...",
                               maximum = max,
                               parent=self.parent_window,
                               style = 0
                                | wx.PD_APP_MODAL
                               # | wx.PD_CAN_ABORT
                                #| wx.PD_CAN_SKIP
                                 | wx.PD_ELAPSED_TIME
                                #| wx.PD_ESTIMATED_TIME
                                #| wx.PD_REMAINING_TIME
                                #| wx.PD_AUTO_HIDE
                                )

        keepGoing = True
        count = 0




        for o in objects:
            o.debit = o.debit.replace(",","").strip()
            o.credit = o.credit.replace(",","").strip()
            self.journals_listing.insert_journalitem(o,self.ref_id,postdate)

            count += 1
            wx.Yield()
            if count < max:
                dlg2.Update(count,"Saving \"" + o.description + "\"")
            else:
                dlg2.Update(count,"Saving completed...")



        dlg2.Destroy()


        if self.acc_type == "journal":
            type_window = "Journal Entry"
            self.parent_window.refreshPage('JournalEntry')
        else:
            type_window = "Check Voucher"
            self.parent_window.refreshPage('CheckVoucher')

        #del busy

        #dlg = wx.MessageDialog(self.parent_window, "Finished updating...", "Updating " + type_window,style=wx.OK|wx.CENTRE)
        #dlg.ShowModal()

    def DeleteOLVNow(self):
        message = "Are you sure you want to delete this entry? "
        caption = "Delete Entry Window"
        dlg = wx.MessageDialog(self.parent_window, message, caption,style=wx.YES_NO | wx.ICON_EXCLAMATION)
        if dlg.ShowModal() == wx.ID_YES:
            the_obj = self.the_panel.dataOlv.GetSelectedObjects()[0]
            self.the_panel.dataOlv.RemoveObject(the_obj)
            self.the_panel.dataOlv.RepopulateList()
            self.totalData()



    def _SaveNew(self):
        the_value = self.totalDebitCredit.GetValue()

        the_value = the_value.replace(",","")
        if (float(the_value) != 0 ):
            dlg = wx.MessageDialog(self.parent_window, "Please check the balance of entries first.",style=wx.OK|wx.CENTRE)
            dlg.ShowModal()
            return

        if (self.invoice_id.GetValue() == "(AUTO-GENERATED)"):
            postdate = self.dpc.GetValue().FormatISODate()
            explanation = self.explanation.GetValue()
            self.totalData()

            total_value = self.total_value
            ref_value = self.journals_listing.insert_journal(postdate,explanation,total_value,self.acc_type == "checkvoucher")

            self.ref_id = ref_value['last_insert_id']
            self.invoice_id.SetValue(ref_value['ref_number'])

            objects = self.the_panel.dataOlv.GetObjects()
            for o in objects:
                o.debit = o.debit.replace(",","").strip()
                o.credit = o.credit.replace(",","").strip()
                self.journals_listing.insert_journalitem(o,self.ref_id,postdate)


        else:
            postdate = self.dpc.GetValue().FormatISODate()
            explanation = self.explanation.GetValue()
            self.totalData()
            total_value = self.total_value
            print "Not auto-generated.."
            ref_value = self.invoice_id.GetValue()
            ref_value = self.journals_listing.insert_journal_lateposting(postdate,explanation,total_value,self.acc_type == "checkvoucher",ref_value)

            self.ref_id = ref_value['last_insert_id']

            objects = self.the_panel.dataOlv.GetObjects()

            for o in objects:
                o.debit = o.debit.replace(",","").strip()
                o.credit = o.credit.replace(",","").strip()
                self.journals_listing.insert_journalitem(o,self.ref_id,postdate)

        if self.acc_type == "journal":
            type_window = "Journal Entry"
            self.parent_window.refreshPage('JournalEntry')
        else:
            type_window = "Check Voucher"
            self.parent_window.refreshPage('CheckVoucher')

        dlg = wx.MessageDialog(self.parent_window, "Finished saving...", "Saving " + type_window,style=wx.OK|wx.CENTRE)
        #dlg.SetYesNoLabels("&Quit", "&Don't quit")
        dlg.ShowModal()

    def printEntry(self,evt):
        if self.acc_type == "journal":
            if self.ref_id:
                wait = wx.BusyCursor()
                report_Entry(self.ref_id,"journal.pdf","Journal Voucher ",self.parent_window.userInfo.admin_name)
                self.parent_window.SpecialReportWindow("Journal Entry Print Preview","journal.pdf")
                del wait
        else:
            if self.ref_id:
                wait = wx.BusyCursor()
                cvEntryReport(self.ref_id,"voucher.pdf","Check Voucher",self.parent_window.userInfo.admin_name)
                self.parent_window.SpecialReportWindow("Check Voucher Print Preview","voucher.pdf")
                del wait

    def onKeyCombo(self,evt):
        self.totalData()

    def totalData(self):
        objects = self.the_panel.dataOlv.GetObjects()
        total_debit = 0.00
        total_credit = 0.00
        for o in objects:
            the_debit = o.debit.replace(",","")
            the_credit = o.credit.replace(",","")


            total_debit = round(float(the_debit),2) + total_debit
            total_credit = round(float(the_credit),2) + total_credit

        self.m_textCtrl3.SetValue("{:,.2f} ".format(total_debit))
        self.m_textCtrl4.SetValue("{:,.2f} ".format(total_credit))

        if (round(total_debit - total_credit,2 ) == 0.00):
            self.totalDebitCredit.SetValue("{:,.2f} ".format(abs(round(total_debit - total_credit,2))))
            self.totalDebitCredit.SetBackgroundColour((255,255,153))
            self.totalDebitCredit.SetForegroundColour((0,0,0))
            self.total_value = total_debit
        else:
            self.total_value = 0
           # print " result is : " + "{:,.35f} ".format(round(total_debit - total_credit,2))
            self.totalDebitCredit.SetBackgroundColour((255,0,0))
            self.totalDebitCredit.SetValue("{:,.2f} ".format(round(total_debit - total_credit,2)))
            self.totalDebitCredit.SetForegroundColour((255,255,0))



    def _pydate2wxdate(self,date):
        '''
        Convert the normal date to  the date format of wxDatePicker
        '''
        import datetime
        assert isinstance(date, (datetime.datetime, datetime.date))
        tt = date.timetuple()
        dmy = (tt[2], tt[1]-1, tt[0])
        return wx.DateTimeFromDMY(*dmy)

    def _wxdate2pydate(self,date):
         assert isinstance(date, wx.DateTime)
         if date.IsValid():
              ymd = map(int, date.FormatISODate().split('-'))
              return datetime.date(*ymd)
         else:
              return None


    def makeAccountSelection(self,olv, rowIndex, subItemIndex):
        odcb = OwnerDrawnEditor.ComboAccounts(olv)
        # OwnerDrawnComboxBoxes don't generate EVT_CHAR so look for keydown instead
        odcb.Bind(wx.EVT_KEY_DOWN, olv._HandleChar)
        return odcb

    def makeCostCenterEditor(self,olv, rowIndex, subItemIndex):
        odcb = OwnerDrawnEditor.ComboCostCenters(olv)
        # OwnerDrawnComboxBoxes don't generate EVT_CHAR so look for keydown instead
        odcb.Bind(wx.EVT_KEY_DOWN, olv._HandleChar)
        return odcb


    def newTransactionEntry(self,evt):
        is_new = True
        trans_id = None
        extra_text = ""
        the_obj = EntryWindow()

        the_total = self.totalDebitCredit.GetValue()
        #print the_total
        the_total = the_total.replace(",","")

        float_total = float(the_total)

        if (float_total > 0):
            the_obj.credit = str(abs(float_total))
        if (float_total < 0) :
            the_obj.debit = str(abs(float_total))


        if is_new:
            extra_text = " (new) "
        dlg = EditTransactionDialog(self, -1, "Edit Transaction Dialog" + extra_text,the_obj, size=(400,350),style=wx.DEFAULT_DIALOG_STYLE,isNew=True)
        resultModal =    dlg.ShowModal()
        if resultModal == wx.ID_OK:
  #          print "New Transaction adding"
            theobj = dlg.getChanges()
            the_obj.setNewValue(theobj)
            self.the_panel.dataOlv.AddObject(the_obj)

            self.totalData()


    def EditTransaction(self):
        is_new = False
        trans_id = None
        the_obj = None
        try:
            the_obj = self.the_panel.dataOlv.GetSelectedObjects()[0]
            #print the_obj.description
            trans_id = the_obj.id
        except:
            print "Error here..."
            is_new = True
            pass

        extra_text = ""
        if is_new:
            extra_text = " (new) "
        #print trans_id
        dlg = EditTransactionDialog(self, -1, "Edit Transaction Dialog" + extra_text,the_obj, size=(400,350),style=wx.DEFAULT_DIALOG_STYLE,isNew=False)
        #dlg.ShowWindowModal()
        resultModal =    dlg.ShowModal()
        #print "Modal is " + str(resultModal)
        if resultModal == wx.ID_OK:
            theobj = dlg.getChanges()
            if the_obj:
                the_obj.setNewValue(theobj)
                self.totalData()
            self.the_panel.dataOlv.RefreshObject(the_obj)
    #        print "OK is pressed...."

    def editTransactionEntry(self,evt):
        self.EditTransaction()



    def AddTransaction(self,theobj):
        the_obj = EntryWindow()
        the_obj.setNewValue(theobj)
        self.the_panel.dataOlv.AddObject(the_obj)
        self.totalData()

    def UpdateObj(self,the_obj):
        pass


    def onOLVItemSelected(self,evt):
   #     print "Nagpindot pindot pindot"
        pass

    def handleCellEditStarting(self,evt):
     #   print "Printing..."
        if evt.rowIndex == 1:
            evt.Veto()
        if evt.subItemIndex in (1, 2):
            evt.cellBounds[3] += 2 # Make the textbox a bit taller

    def handleCellEditFinishing(self,evt):
 #       print "Printing...2"
        pass

    def setEntryWindows(self, data=None):
        self.the_panel.dataOlv.SetColumns([
            ColumnDefn("GL Account", "left", 200, "gl_account",cellEditorCreator=self.makeAccountSelection,maximumWidth=250,valueSetter="SetGLAccount"),
            ColumnDefn("Sub Ledger ID", "left", 0, "sl_id"),
            ColumnDefn("Sub Ledger", "left", 100, "sl_description"),
            ColumnDefn("CostCenterID", "left", 0, "cost_center_id"),
            ColumnDefn("Cost Center", "left", 110, "cost_center",cellEditorCreator=self.makeCostCenterEditor,valueSetter="SetCostCenter"),
            ColumnDefn("Description", "left", 200, "description"),
            ColumnDefn("Debit", "right", 130, "debit"),
            ColumnDefn("Credit", "right", 130, valueGetter="credit"),
            ColumnDefn("recordid", "right", 0, "id")
        ])

        self.the_panel.dataOlv.SetObjects(self.journals)

    def OnNewView(self,event):
        pass

    def OnAddRow(self,event):
      #  print "Add Row"
        pass

    def OnDeleteRows(self,event):
        pass


    def OnDoubleClick(self, event):
        self.parent_window.AccountDetails(1)
        event.Skip()

    def return_panel(self):
        return self.the_panel

def CreateEntryWindowWindow(the_parent_window,ref_id,acct_type):
   panel = entryjournalPanel(the_parent_window,ref_id,acct_type)
   return panel.return_panel()

def GetReferenceNumber(ref_id):
    try :
        journals_listing = Journals()
        data = journals_listing.db.journal_main.filter(journals_listing.db.journal_main.id == ref_id).one()
        return data.purchase_invoice_id
    except:
        return None

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
