import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *
from EnterDatePicker import *
from Contacts import *
import PromptingComboBox

class inventoryEntryEditorPanel(wxSpecialPanel):
    def __init__(self,parent_window,date_posted,inv_info):
        self.parent_window = parent_window
        self.book = parent_window.book
        self.inv_info = inv_info

        super(inventoryEntryEditorPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("InventoryEntryEditor")
        self.Contacts = Contacts()
        self.contacts_ = self.Contacts.db.contacts

        self.setEscapeHandler(self.CloseThisPanel)

        mainSizer = wx.BoxSizer(wx.VERTICAL)

        sizerTop = wx.FlexGridSizer( 0, 2, 0, 0 )

        self.cost_centers = self.Contacts.get_costcenters()

        firstname_label = wx.StaticText(self, -1, "Date Posted:", (20,20))
        # self.invy_dateposted = EnterTextCtrl(self, -1, "", (100,20), (150,-1))

        self.invy_dateposted = EnterDatePicker(self, pos=(150,-1),
                                style = wx.DP_DROPDOWN
                                      | wx.DP_SHOWCENTURY)

        # address1 = wx.StaticText(self, -1, "Street: " )
        # self.address1 = EnterTextCtrl(self, -1, "",(600,20), (300,-1))

        # city = wx.StaticText(self, -1, "City/Town:" )
        # self.city = EnterTextCtrl(self, -1, "",(600,20), (300,-1))
        # province = wx.StaticText(self, -1, "Province:" )
        # self.province = EnterTextCtrl(self, -1, "",(600,20),(300,-1))

        sizerTop.Add(firstname_label, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.invy_dateposted, -1, wx.ALL|wx.CENTER, 5)

        entry_type_label = wx.StaticText(self, -1, "Entry Type:" )
        entry_type = ["Beginning","Ending"]
        self.entry_type = PromptingComboBox.PromptingComboBox(self,u"Beginning",entry_type,style=wx.CB_READONLY |  wx.WANTS_CHARS)
        sizerTop.Add(entry_type_label, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.entry_type, -1, wx.ALL|wx.CENTER, 5)



        self.cc_dic = {}
        for c in self.cost_centers:
            titled_case_primary_name = c.primary_name.title()
            primary_name_noblank = str(c.primary_name).lower().replace(" ","")
            ccname = wx.StaticText(self, -1, titled_case_primary_name )
            cc_text = EnterTextCtrl(self, -1, "",(600,20), (300,-1))
            self.cc_dic[primary_name_noblank] = {}
            self.cc_dic[primary_name_noblank]['entry'] = cc_text
            self.cc_dic[primary_name_noblank]['id'] = c.id
            sizerTop.Add(ccname, -1, wx.ALL|wx.CENTER, 5)
            sizerTop.Add(cc_text, -1, wx.ALL|wx.CENTER, 5)

        # sizerTop.Add(city, -1, wx.ALL|wx.CENTER, 5)
        # sizerTop.Add(self.city, -1, wx.ALL|wx.CENTER, 5)
        # sizerTop.Add(province, -1, wx.ALL|wx.CENTER, 5)
        # sizerTop.Add(self.province, -1, wx.ALL|wx.CENTER, 5)
        # sizerTop.Add(zipcode, -1, wx.ALL|wx.CENTER, 5)


        # if inv_info:
            # self.contact = self.contacts_.filter_by(id = inv_info).one()
            # self.invy_dateposted.SetValue(self.contact.short_name)

            # address_book = self.Contacts.db.address_book

            # self.address_book = address_book.filter_by(ref_id = inv_info).one()
            # if self.address_book:
                # self.address1.SetValue(self.address_book.address1)
                # self.city.SetValue(self.address_book.city_town)
                # self.province.SetValue(self.address_book.state_province)
                # # self.zipcode.SetValue(self.address_book.postal_code)


        if self.inv_info :
            updateLabel = "Update (Ctr-S)"
        else:
            updateLabel = "Save (Ctr-S)"

        self.btn_Update = wx.Button(self, -1, updateLabel)
        self.btn_Cancel = wx.Button(self, -1, "Cancel (Esc)")
        self.Bind(wx.EVT_BUTTON, self.updateInventoryInfo, self.btn_Update)
        self.Bind(wx.EVT_BUTTON, self.CloseNow, self.btn_Cancel)


        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer.Add(self.btn_Update, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btn_Cancel, 0, wx.ALL|wx.LEFT, 5)

        id_updateInventoryInfo = wx.ID_ANY
        self.Bind(wx.EVT_MENU,self.updateInventoryInfo,id=id_updateInventoryInfo)

        mainSizer.Add(sizerTop, 0, wx.TOP|wx.LEFT|wx.BOTTOM, 5)
        mainSizer.Add(btnsizer, 0, wx.LEFT|wx.BOTTOM, 5)
        self.SetSizer(mainSizer)


        self.invy_dateposted.SetFocus()
        # self.invy_dateposted.SetSelection(-1,-1)

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('S'), id_updateInventoryInfo )])
        self.SetAcceleratorTable(accel_tbl)


    def updateInventoryInfo(self,evt):
        type_window = "CC information of " + str(self.invy_dateposted.GetValue())
        print "Type is {}".format(type_window)
        print self.entry_type.GetValue()
        for c in self.cc_dic:
            print self.cc_dic[c]['id'],self.cc_dic[c]['entry'].GetValue()
        # for c in self.cc_dic:
            # print type(c)
        # if self.inv_info:
            # data = {'id':self.inv_info,'short_name':self.invy_dateposted.GetValue(),'city_town':self.city.GetValue(),'state_province':self.province.GetValue(),'address1':self.address1.GetValue()}
            # self.Contacts.updateCCInfo(data)
            # self.parent_window.refreshPage('CostCenter')
            # dlg = wx.MessageDialog(self.parent_window, "Finished updating...", "Updating " + type_window,style=wx.OK|wx.CENTRE)
            # dlg.ShowModal()
        # else:
            # data = {'id':None,'short_name':self.invy_dateposted.GetValue(),'city_town':self.city.GetValue(),'state_province':self.province.GetValue(),'address1':self.address1.GetValue()}
            # self.inv_info = self.Contacts.addCCRecord(data)
            # self.parent_window.refreshPage('CostCenter')
            # dlg = wx.MessageDialog(self.parent_window, "Finished saving...", "Saving " + type_window,style=wx.OK|wx.CENTRE)
            # self.btn_Update.SetLabel("Update (Ctr-S)")
            # dlg.ShowModal()

    def DeleteRecord(self,obj):
        self.contact_members.deleteCCRecord(obj.id)

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("InventoryEntryEditor",True)


    def CloseNow(self,evt):
        self.CloseThisPanel()


    def return_panel(self):
        return self

def CreateInventoryEntryEditor(the_parent_window,date_posted,inv_info):
   panel = inventoryEntryEditorPanel(the_parent_window,date_posted,inv_info)
   return panel

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
