import sys
import wx
from wx.lib.mixins.listctrl import CheckListCtrlMixin

from ListCtrlSample import musicdata

#----------------------------------------------------------------------

class CheckListCtrl(wx.ListCtrl, CheckListCtrlMixin):
    def __init__(self, parent):
        wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT)
        CheckListCtrlMixin.__init__(self)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)


    def OnItemActivated(self, evt):
        self.ToggleItem(evt.m_itemIndex)


    # this is called by the base class when an item is checked/unchecked
    def OnCheckItem(self, index, flag):
        data = self.GetItemData(index)
        #title = musicdata[data][1]
        if flag:
            what = "checked"
        else:
            what = "unchecked"
        #self.log.write('item "%s", at index %d was %s\n' % (title, index, what))



class roleSettingPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)
        self.SetLabel("roleSetting") 

        self.list = CheckListCtrl(self)
        sizer = wx.BoxSizer()
        sizer.Add(self.list, 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.list.InsertColumn(0, "MenuID")
        self.list.InsertColumn(1, "Menu Name")
        self.list.InsertColumn(2, "Menu Title")


        for item in parent.m_menubar1.GetMenus():
                for menuitem in item[0].GetMenuItems():
                    if menuitem.GetLabel().strip() != "":
                        #print menuitem.GetId(),menuitem.GetLabel().lower().replace(' ','_')
                        index = self.list.InsertStringItem(sys.maxint, str(menuitem.GetId()))
                        self.list.SetStringItem(index, 1, menuitem.GetLabel().lower().replace(' ','_'))
                        self.list.SetStringItem(index, 2, menuitem.GetLabel())
        #for key, data in musicdata.iteritems():
            #index = self.list.InsertStringItem(sys.maxint, data[0])
            #self.list.SetStringItem(index, 1, data[1])
            #self.list.SetStringItem(index, 2, data[2])
            #self.list.SetItemData(index, key)
      
        self.list.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.list.SetColumnWidth(1, wx.LIST_AUTOSIZE)
        self.list.SetColumnWidth(2, 100)

        self.list.CheckItem(4)
        self.list.CheckItem(7)

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnItemSelected, self.list)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.OnItemDeselected, self.list)

        
    def OnItemSelected(self, evt):
        print 'item selected: %s\n' % evt.m_itemIndex
        #self.log.write('item selected: %s\n' % evt.m_itemIndex)
        
    def OnItemDeselected(self, evt):
        print 'item deselected: %s\n' % evt.m_itemIndex
        #self.log.write('item deselected: %s\n' % evt.m_itemIndex)

    def return_panel(self):
        return self

def CreateRoleSettingWindow(the_parent_window):
   panel = roleSettingPanel(the_parent_window)
   return panel.return_panel()
   

