import wx
 
class MainWindow(wx.Frame):
    '''test frame'''
    def __init__(self,*args,**kwargs):
        '''Constructor'''
        super(MainWindow,self).__init__(*args,**kwargs)
        self.panel = wx.Panel(self)
 
        sizer = wx.BoxSizer(wx.VERTICAL)
 
        text = wx.StaticText(self.panel, wx.ID_ANY, "hallo")
        sizer.Add(text, 0, wx.ALL, 5)
 
        choice = wx.Choice(self.panel)
        choice.Append("nasowas")
        choice.Append("gibtsdas")
        sizer.Add(choice, 0, wx.ALL, 5)
 
        choice.Bind(wx.EVT_CHOICE, self.OnChoice)
        choice.Bind(wx.EVT_LEFT_DOWN, self.OnChoice)
        choice.Bind(wx.EVT_ENTER_WINDOW, self.OnChoice)
 
        self.panel.SetSizer(sizer)
 
    def OnChoice(self, event):
        print "here"
        event.Skip()
 
 
if __name__ == '__main__':
    app = wx.App()
    frame = MainWindow(None, -1, 'test window')
    frame.Show()
    app.MainLoop()
