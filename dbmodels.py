# coding: utf-8
from sqlalchemy import CHAR, Column, Date, DateTime, Enum, Float, Index, String, TIMESTAMP, Table, Text, VARBINARY, text
from sqlalchemy.dialects.mysql import ENUM, INTEGER, SMALLINT, TINYINT
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class AccountType(Base):
    __tablename__ = 'account_types'

    id = Column(INTEGER(11), primary_key=True)
    code = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    text_display = Column(String(100))
    is_asset = Column(INTEGER(11))


class AccountingPeriod(Base):
    __tablename__ = 'accounting_periods'

    period = Column(INTEGER(11), primary_key=True, server_default=text("'0'"))
    fiscal_year = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    start_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    end_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_added = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_update = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class AccountsHistory(Base):
    __tablename__ = 'accounts_history'

    id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    acct_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    amount = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    journal_id = Column(INTEGER(2), nullable=False, server_default=text("'0'"))
    purchase_invoice_id = Column(CHAR(24, u'utf8_unicode_ci'))
    so_po_ref_id = Column(INTEGER(11))
    post_date = Column(DateTime)


class AddressBook(Base):
    __tablename__ = 'address_book'
    __table_args__ = (
        Index('customer_id', 'ref_id', 'type'),
    )

    address_id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    type = Column(CHAR(2, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    primary_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    contact = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    address1 = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    address2 = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    city_town = Column(String(24, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    state_province = Column(String(24, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    postal_code = Column(String(10, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    country_code = Column(CHAR(3, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    telephone1 = Column(String(20, u'utf8_unicode_ci'), server_default=text("''"))
    telephone2 = Column(String(20, u'utf8_unicode_ci'), server_default=text("''"))
    telephone3 = Column(String(20, u'utf8_unicode_ci'), server_default=text("''"))
    telephone4 = Column(String(20, u'utf8_unicode_ci'), server_default=text("''"))
    email = Column(String(48, u'utf8_unicode_ci'), server_default=text("''"))
    website = Column(String(48, u'utf8_unicode_ci'), server_default=text("''"))
    notes = Column(Text(collation=u'utf8_unicode_ci'))


t_all_items = Table(
    'all_items', metadata,
    Column('id', INTEGER(11), server_default=text("'0'")),
    Column('purchase_invoice_id', String(24)),
    Column('post_date', Date, server_default=text("'0000-00-00'")),
    Column('project_id', String(16)),
    Column('gl_desc', String(82)),
    Column('cost_center', String(32)),
    Column('subledger', VARBINARY(46)),
    Column('description', String(255)),
    Column('debit_amount', Float(asdecimal=True), server_default=text("'0'")),
    Column('credit_amount', Float(asdecimal=True), server_default=text("'0'"))
)


class AuditLog(Base):
    __tablename__ = 'audit_log'

    id = Column(INTEGER(15), primary_key=True)
    action_date = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    user_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ip_address = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("'0.0.0.0'"))
    stats = Column(String(32, u'utf8_unicode_ci'), nullable=False)
    reference_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    action = Column(String(64, u'utf8_unicode_ci'))
    amount = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))


class ChartOfAccount(Base):
    __tablename__ = 'chart_of_accounts'

    id = Column(CHAR(15, u'utf8_unicode_ci'), primary_key=True, server_default=text("''"))
    description = Column(CHAR(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    heading_only = Column(ENUM(u'0', u'1'), nullable=False, index=True, server_default=text("'0'"))
    primary_acct_id = Column(CHAR(15, u'utf8_unicode_ci'))
    account_type = Column(TINYINT(4), nullable=False, index=True, server_default=text("'0'"))
    account_inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    weight = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class ChartOfAccountsHistory(Base):
    __tablename__ = 'chart_of_accounts_history'

    id = Column(INTEGER(11), primary_key=True)
    period = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    account_id = Column(CHAR(15, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    beginning_balance = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    debit_amount = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    credit_amount = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    budget = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    last_update = Column(Date, nullable=False, server_default=text("'0000-00-00'"))


class Configuration(Base):
    __tablename__ = 'configuration'

    configuration_key = Column(String(64, u'utf8_unicode_ci'), primary_key=True, server_default=text("''"))
    configuration_value = Column(Text(collation=u'utf8_unicode_ci'))


class Contact(Base):
    __tablename__ = 'contacts'

    id = Column(INTEGER(11), primary_key=True)
    type = Column(CHAR(1, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("'c'"))
    contacts_level = Column(CHAR(1, u'utf8_unicode_ci'), nullable=False, server_default=text("'r'"))
    short_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    contact_first = Column(String(32, u'utf8_unicode_ci'))
    contact_middle = Column(String(32, u'utf8_unicode_ci'))
    contact_last = Column(String(32, u'utf8_unicode_ci'))
    store_id = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    gl_type_account = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    gov_id_number = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    dept_rep_id = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    account_number = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    special_terms = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("'0'"))
    price_sheet = Column(String(32, u'utf8_unicode_ci'))
    tax_id = Column(INTEGER(11), nullable=False, server_default=text("'-1'"))
    attachments = Column(Text(collation=u'utf8_unicode_ci'))
    first_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_update = Column(Date)
    last_date_1 = Column(Date)
    last_date_2 = Column(Date)
    company_grouping = Column(String(32, u'utf8_unicode_ci'), server_default=text("''"))
    rdempc_id = Column(String(32, u'utf8_unicode_ci'), server_default=text("''"))


class ContactsLog(Base):
    __tablename__ = 'contacts_log'

    log_id = Column(INTEGER(11), primary_key=True)
    contact_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    entered_by = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    log_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    action = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    notes = Column(Text(collation=u'utf8_unicode_ci'))


class Currency(Base):
    __tablename__ = 'currencies'

    currencies_id = Column(INTEGER(11), primary_key=True)
    title = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    code = Column(CHAR(3, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    symbol_left = Column(String(24, u'utf8_unicode_ci'))
    symbol_right = Column(String(24, u'utf8_unicode_ci'))
    decimal_point = Column(CHAR(1, u'utf8_unicode_ci'))
    thousands_point = Column(CHAR(1, u'utf8_unicode_ci'))
    decimal_places = Column(CHAR(1, u'utf8_unicode_ci'))
    decimal_precise = Column(CHAR(1, u'utf8_unicode_ci'), nullable=False, server_default=text("'2'"))
    value = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    last_updated = Column(DateTime)


class CurrentStatu(Base):
    __tablename__ = 'current_status'

    id = Column(INTEGER(11), primary_key=True)
    next_cust_id_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'C10000'"))
    next_vend_id_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'V10000'"))
    next_po_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'5000'"))
    next_so_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'10000'"))
    next_inv_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'20000'"))
    next_check_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'100'"))
    next_deposit_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    next_cm_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'CM1000'"))
    next_vcm_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'VCM1000'"))
    next_ap_quote_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'RFQ1000'"))
    next_ar_quote_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'QU1000'"))
    next_shipment_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'1'"))
    next_crm_id_num = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))


class CvSery(Base):
    __tablename__ = 'cv_series'

    id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11))


class DataSecurity(Base):
    __tablename__ = 'data_security'

    id = Column(INTEGER(11), primary_key=True)
    module = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    ref_1 = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ref_2 = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    hint = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    enc_value = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    exp_date = Column(Date, nullable=False, server_default=text("'2049-12-31'"))


class Department(Base):
    __tablename__ = 'departments'

    id = Column(INTEGER(11), primary_key=True)
    description_short = Column(String(30, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    description = Column(String(30, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    subdepartment = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    primary_dept_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    department_type = Column(TINYINT(4), nullable=False, index=True, server_default=text("'0'"))
    department_inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))


class DepartmentsType(Base):
    __tablename__ = 'departments_types'

    id = Column(INTEGER(11), primary_key=True)
    description = Column(String(30, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))


class Inventory(Base):
    __tablename__ = 'inventory'

    id = Column(INTEGER(11), primary_key=True)
    sku = Column(String(24, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    inventory_type = Column(CHAR(2, u'utf8_unicode_ci'), nullable=False, server_default=text("'si'"))
    description_short = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    description_purchase = Column(String(255, u'utf8_unicode_ci'))
    description_sales = Column(String(255, u'utf8_unicode_ci'))
    image_with_path = Column(String(255, u'utf8_unicode_ci'))
    account_sales_income = Column(String(15, u'utf8_unicode_ci'))
    account_inventory_wage = Column(String(15, u'utf8_unicode_ci'), server_default=text("''"))
    account_cost_of_sales = Column(String(15, u'utf8_unicode_ci'))
    item_taxable = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    purch_taxable = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    item_cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    cost_method = Column(ENUM(u'a', u'f', u'l'), nullable=False, server_default=text("'f'"))
    price_sheet = Column(String(32, u'utf8_unicode_ci'))
    price_sheet_v = Column(String(32, u'utf8_unicode_ci'))
    full_price = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    full_price_with_tax = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    margin = Column(Float, nullable=False, server_default=text("'0'"))
    item_weight = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    quantity_on_hand = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    quantity_on_order = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    quantity_on_sales_order = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    quantity_on_allocation = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    minimum_stock_level = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    reorder_quantity = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    vendor_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    lead_time = Column(INTEGER(3), nullable=False, server_default=text("'1'"))
    upc_code = Column(String(13, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    serialize = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    creation_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_update = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_journal_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    attachments = Column(Text(collation=u'utf8_unicode_ci'))
    product_markup = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    product_margin = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))


class InventoryAssyList(Base):
    __tablename__ = 'inventory_assy_list'

    id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    sku = Column(String(24, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    description = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    qty = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))


class InventoryCogsOwed(Base):
    __tablename__ = 'inventory_cogs_owed'

    id = Column(INTEGER(11), primary_key=True)
    journal_main_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    store_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    sku = Column(String(24, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    qty = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    post_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))


class InventoryCogsUsage(Base):
    __tablename__ = 'inventory_cogs_usage'
    __table_args__ = (
        Index('journal_main_id', 'journal_main_id', 'inventory_history_id'),
    )

    id = Column(INTEGER(11), primary_key=True)
    journal_main_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    qty = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    inventory_history_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class InventoryEntry(Base):
    __tablename__ = 'inventory_entry'

    id = Column(INTEGER(11), primary_key=True)
    accounting_period = Column(INTEGER(11))
    cost_center_id = Column(INTEGER(11))
    entry_value = Column(Float(asdecimal=True))
    date_posted = Column(DateTime)
    date_updated = Column(DateTime)
    entry_type = Column(Enum(u'B', u'E'))


class InventoryHistory(Base):
    __tablename__ = 'inventory_history'

    id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    store_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    journal_id = Column(INTEGER(2), nullable=False, index=True, server_default=text("'6'"))
    sku = Column(String(24, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    qty = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    serialize_number = Column(String(24, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    remaining = Column(Float(asdecimal=True), nullable=False, index=True, server_default=text("'0'"))
    unit_cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    avg_cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    post_date = Column(DateTime)


class InventoryMsList(Base):
    __tablename__ = 'inventory_ms_list'

    id = Column(INTEGER(11), primary_key=True)
    sku = Column(String(24, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    attr_name_0 = Column(String(16, u'utf8_unicode_ci'))
    attr_name_1 = Column(String(16, u'utf8_unicode_ci'))
    attr_0 = Column(String(255, u'utf8_unicode_ci'))
    attr_1 = Column(String(255, u'utf8_unicode_ci'))


class InventoryPurchaseDetail(Base):
    __tablename__ = 'inventory_purchase_details'

    id = Column(INTEGER(11), primary_key=True)
    sku = Column(String(24, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    vendor_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    description_purchase = Column(String(255, u'utf8_unicode_ci'))
    purch_package_quantity = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    purch_taxable = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    item_cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    price_sheet_v = Column(String(32, u'utf8_unicode_ci'))


class InventorySpecialPrice(Base):
    __tablename__ = 'inventory_special_prices'

    id = Column(INTEGER(11), primary_key=True)
    inventory_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    price_sheet_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    price_levels = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))


class JournalItem(Base):
    __tablename__ = 'journal_item'

    id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    item_cnt = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    so_po_item_ref_id = Column(INTEGER(11), index=True)
    gl_type = Column(CHAR(3, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    reconciled = Column(INTEGER(2), nullable=False, index=True, server_default=text("'0'"))
    sku = Column(String(24, u'utf8_unicode_ci'))
    qty = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    description = Column(String(255, u'utf8_unicode_ci'))
    debit_amount = Column(Float(asdecimal=True), server_default=text("'0'"))
    credit_amount = Column(Float(asdecimal=True), server_default=text("'0'"))
    gl_account = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    taxable = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    full_price = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    serialize = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    serialize_number = Column(String(24, u'utf8_unicode_ci'))
    project_id = Column(String(16, u'utf8_unicode_ci'))
    purch_package_quantity = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    post_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    sublgr_id = Column(INTEGER(11))
    date_1 = Column(DateTime)


class JournalMain(Base):
    __tablename__ = 'journal_main'

    id = Column(INTEGER(11), primary_key=True)
    period = Column(INTEGER(2), nullable=False, index=True, server_default=text("'0'"))
    journal_id = Column(INTEGER(2), nullable=False, index=True, server_default=text("'0'"))
    post_date = Column(Date, nullable=False, index=True, server_default=text("'0000-00-00'"))
    store_id = Column(INTEGER(11), server_default=text("'0'"))
    description = Column(String(100, u'utf8_unicode_ci'))
    closed = Column(ENUM(u'0', u'1'), nullable=False, index=True, server_default=text("'0'"))
    closed_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    printed = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    freight = Column(Float(asdecimal=True), server_default=text("'0'"))
    discount = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shipper_code = Column(String(20, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    terms = Column(String(32, u'utf8_unicode_ci'), server_default=text("'0'"))
    sales_tax = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    tax_auths = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("'0'"))
    total_amount = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    currencies_code = Column(CHAR(3, u'utf8_unicode_ci'), nullable=False, server_default=text("'PHP'"))
    currencies_value = Column(Float(asdecimal=True), nullable=False, server_default=text("'1'"))
    so_po_ref_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    purchase_invoice_id = Column(String(24, u'utf8_unicode_ci'))
    purch_order_id = Column(String(24, u'utf8_unicode_ci'))
    recur_id = Column(INTEGER(11))
    admin_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    rep_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    waiting = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    gl_acct_id = Column(String(15, u'utf8_unicode_ci'))
    bill_acct_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    bill_address_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    bill_primary_name = Column(String(32, u'utf8_unicode_ci'))
    bill_contact = Column(String(32, u'utf8_unicode_ci'))
    bill_address1 = Column(String(32, u'utf8_unicode_ci'))
    bill_address2 = Column(String(32, u'utf8_unicode_ci'))
    bill_city_town = Column(String(24, u'utf8_unicode_ci'))
    bill_state_province = Column(String(24, u'utf8_unicode_ci'))
    bill_postal_code = Column(String(10, u'utf8_unicode_ci'))
    bill_country_code = Column(CHAR(3, u'utf8_unicode_ci'))
    bill_telephone1 = Column(String(20, u'utf8_unicode_ci'))
    bill_email = Column(String(48, u'utf8_unicode_ci'))
    ship_acct_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ship_address_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ship_primary_name = Column(String(32, u'utf8_unicode_ci'))
    ship_contact = Column(String(32, u'utf8_unicode_ci'))
    ship_address1 = Column(String(32, u'utf8_unicode_ci'))
    ship_address2 = Column(String(32, u'utf8_unicode_ci'))
    ship_city_town = Column(String(24, u'utf8_unicode_ci'))
    ship_state_province = Column(String(24, u'utf8_unicode_ci'))
    ship_postal_code = Column(String(24, u'utf8_unicode_ci'))
    ship_country_code = Column(CHAR(3, u'utf8_unicode_ci'))
    ship_telephone1 = Column(String(20, u'utf8_unicode_ci'))
    ship_email = Column(String(48, u'utf8_unicode_ci'))
    terminal_date = Column(Date)
    drop_ship = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    jtype = Column(INTEGER(11), server_default=text("'0'"))


class JvSery(Base):
    __tablename__ = 'jv_series'

    id = Column(INTEGER(11), primary_key=True)
    ref_id = Column(INTEGER(11))
    text_display = Column(String(50))


class Phreeform(Base):
    __tablename__ = 'phreeform'

    id = Column(INTEGER(10), primary_key=True)
    parent_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    doc_type = Column(ENUM(u'0', u'c', u's'), nullable=False, server_default=text("'s'"))
    doc_title = Column(String(64, u'utf8_unicode_ci'), server_default=text("''"))
    doc_group = Column(String(9, u'utf8_unicode_ci'))
    doc_ext = Column(String(3, u'utf8_unicode_ci'))
    security = Column(String(255, u'utf8_unicode_ci'), server_default=text("'u:0;g:0'"))
    create_date = Column(Date)
    last_update = Column(Date)


class Phreehelp(Base):
    __tablename__ = 'phreehelp'
    __table_args__ = (
        Index('doc_title', 'doc_title', 'doc_text'),
    )

    id = Column(INTEGER(10), primary_key=True)
    parent_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    doc_type = Column(ENUM(u'0', u'd'), nullable=False, server_default=text("'d'"))
    doc_lang = Column(CHAR(5, u'utf8_unicode_ci'), server_default=text("'en_us'"))
    doc_pos = Column(String(64, u'utf8_unicode_ci'))
    doc_url = Column(String(255, u'utf8_unicode_ci'))
    doc_index = Column(String(255, u'utf8_unicode_ci'))
    doc_title = Column(String(255, u'utf8_unicode_ci'))
    doc_text = Column(Text(collation=u'utf8_unicode_ci'))


class PhreeposOtherTran(Base):
    __tablename__ = 'phreepos_other_trans'

    ot_id = Column(INTEGER(11), primary_key=True)
    till_id = Column(INTEGER(11), server_default=text("'0'"))
    description = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    gl_acct_id = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    type = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("'0'"))
    use_tax = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    taxable = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class PhreeposTill(Base):
    __tablename__ = 'phreepos_tills'

    till_id = Column(INTEGER(11), primary_key=True)
    store_id = Column(INTEGER(11), server_default=text("'0'"))
    description = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    gl_acct_id = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    rounding_gl_acct_id = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    dif_gl_acct_id = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    currencies_code = Column(String(3, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    restrict_currency = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    printer_name = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    printer_starting_line = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    printer_closing_line = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    printer_open_drawer = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    balance = Column(Float(asdecimal=True), server_default=text("'0'"))
    max_discount = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    tax_id = Column(INTEGER(11), server_default=text("'-1'"))


class PriceSheet(Base):
    __tablename__ = 'price_sheets'

    id = Column(INTEGER(11), primary_key=True)
    sheet_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    type = Column(CHAR(1, u'utf8_unicode_ci'), nullable=False, server_default=text("'c'"))
    inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    revision = Column(Float, nullable=False, server_default=text("'0'"))
    effective_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    expiration_date = Column(Date)
    default_sheet = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    default_levels = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))


class ProjectsCost(Base):
    __tablename__ = 'projects_costs'

    cost_id = Column(INTEGER(8), primary_key=True)
    description_short = Column(String(16, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    description_long = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    cost_type = Column(String(3, u'utf8_unicode_ci'))
    inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))


class ProjectsPhase(Base):
    __tablename__ = 'projects_phases'

    phase_id = Column(INTEGER(8), primary_key=True)
    description_short = Column(String(16, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    description_long = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    cost_type = Column(String(3, u'utf8_unicode_ci'))
    cost_breakdown = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))


class Reconciliation(Base):
    __tablename__ = 'reconciliation'

    id = Column(INTEGER(11), primary_key=True)
    period = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    gl_account = Column(String(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    statement_balance = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    cleared_items = Column(Text(collation=u'utf8_unicode_ci'))


class ShippingLog(Base):
    __tablename__ = 'shipping_log'

    id = Column(INTEGER(11), primary_key=True)
    shipment_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ref_id = Column(String(16, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("'0'"))
    reconciled = Column(SMALLINT(4), nullable=False, server_default=text("'0'"))
    carrier = Column(String(16, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    method = Column(String(8, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    ship_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    deliver_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    actual_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    deliver_late = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    tracking_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    notes = Column(String(255, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))


class TaxAuthority(Base):
    __tablename__ = 'tax_authorities'

    tax_auth_id = Column(INTEGER(3), primary_key=True)
    type = Column(String(1, u'utf8_unicode_ci'), nullable=False, server_default=text("'c'"))
    description_short = Column(CHAR(15, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    description_long = Column(CHAR(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    account_id = Column(CHAR(15, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    vendor_id = Column(INTEGER(5), nullable=False, server_default=text("'0'"))
    tax_rate = Column(Float, nullable=False, server_default=text("'0'"))


class TaxRate(Base):
    __tablename__ = 'tax_rates'

    tax_rate_id = Column(INTEGER(3), primary_key=True)
    type = Column(String(1, u'utf8_unicode_ci'), nullable=False, server_default=text("'c'"))
    description_short = Column(String(15, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    description_long = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    rate_accounts = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    freight_taxable = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))


class User(Base):
    __tablename__ = 'users'

    admin_id = Column(INTEGER(11), primary_key=True)
    is_role = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    admin_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, index=True, server_default=text("''"))
    inactive = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    display_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    admin_email = Column(String(96, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    admin_pass = Column(String(40, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    account_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    admin_store_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    admin_prefs = Column(Text(collation=u'utf8_unicode_ci'))
    admin_security = Column(Text(collation=u'utf8_unicode_ci'))


class UsersProfile(Base):
    __tablename__ = 'users_profiles'

    id = Column(INTEGER(11), primary_key=True)
    user_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    menu_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    module_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    dashboard_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    column_id = Column(INTEGER(3), nullable=False, server_default=text("'0'"))
    row_id = Column(INTEGER(3), nullable=False, server_default=text("'0'"))
    params = Column(Text(collation=u'utf8_unicode_ci'))


t_vwaging = Table(
    'vwaging', metadata,
    Column('gl_account', String(15)),
    Column('name', String(32)),
    Column('debit', Float(asdecimal=True)),
    Column('credit', Float(asdecimal=True)),
    Column('last_postdate', Date),
    Column('age', INTEGER(7))
)


class XtraField(Base):
    __tablename__ = 'xtra_fields'

    id = Column(INTEGER(10), primary_key=True)
    module_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    tab_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    entry_type = Column(String(20, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    field_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    description = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    sort_order = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    group_by = Column(String(64, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    use_in_inventory_filter = Column(ENUM(u'0', u'1'), nullable=False, server_default=text("'0'"))
    params = Column(Text(collation=u'utf8_unicode_ci'))


class XtraTab(Base):
    __tablename__ = 'xtra_tabs'

    id = Column(INTEGER(3), primary_key=True)
    module_id = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    tab_name = Column(String(32, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    description = Column(String(80, u'utf8_unicode_ci'), nullable=False, server_default=text("''"))
    sort_order = Column(INTEGER(2), nullable=False, server_default=text("'0'"))
