'''

Created on Dec 15, 2013
@author: jojo
'''
import wx
import wx.aui
import images

from Password import passWord 
from About import * 
from childFrame import *

class PaymentRecorderMain ( wx.aui.AuiMDIParentFrame ):
    
    def __init__( self):
        self.count = 0
        wx.aui.AuiMDIParentFrame.__init__(self, None, -1, "Payroll System", size=(1024,640),style= wx.DEFAULT_FRAME_STYLE )
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        self.m_statusBar1 = self.CreateStatusBar( 1, wx.ST_SIZEGRIP, wx.ID_ANY )
        self.m_menubar1 = wx.MenuBar( 0 )
        self.m_menu1 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Record Payment", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem1 )
        
        self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Manage Account", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem2 )

        self.m_menuItem30 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Daily Report", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem30 )
        self.Bind(wx.EVT_MENU,self.doDailyReport,id=self.m_menuItem30.GetId())

        
        self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu1.AppendItem( self.m_menuItem3 )
        self.Bind( wx.EVT_MENU, self.OnExit, id = self.m_menuItem3.GetId() )

        
        self.m_menubar1.Append( self.m_menu1, u"File" ) 
        
        self.m_menu4 = wx.Menu()
        self.m_menuItem4 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem4 )
        
        self.m_menuItem5 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem5 )
        
        self.m_menuItem6 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem6 )
        
        self.m_menubar1.Append( self.m_menu4, u"Report" ) 
        
        self.m_menu2 = wx.Menu()
        self.m_menuItem7 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem7 )
        
        self.m_menuItem8 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"MyMenuItem", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem8 )
        
        self.m_menuItem9 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu2.AppendItem( self.m_menuItem9 )
        self.Bind( wx.EVT_MENU, self.AboutBox, id = self.m_menuItem9.GetId() )
        
        self.m_menubar1.Append( self.m_menu2, u"Help" ) 

        self.bg_bmp = images.thebestlogo.GetBitmap()
        self.GetClientWindow().Bind(
            wx.EVT_ERASE_BACKGROUND, self.OnEraseBackground
            )
                
        self.SetMenuBar( self.m_menubar1 )
        
        self.m_toolBar1 = self.CreateToolBar( wx.TB_HORIZONTAL, wx.ID_ANY ) 
        self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"tool", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None ) 
        
        self.SetBackgroundColour('blue')
        
        #self.Bind(wx.EVT_CLOSE, self.OnClose)        

        self.Bind(wx.EVT_CLOSE, self.OnDoClose)
        
        self.Centre( wx.BOTH )
    
    def GetMenuBar(self):
		return self.m_menubar1



    def OnDoClose(self, evt):
        # Close all ChildFrames first else Python crashes
        for m in self.GetChildren():
            if isinstance(m, wx.aui.AuiMDIClientWindow):
                for k in m.GetChildren():
                    if isinstance(k, childFrame):
                        k.Close()  
        evt.Skip()


    def OnClose(self,evt):
        dlg = wx.MessageDialog(self, 'Are you sure you want to exit?',
                               'Exit Program?',
                              wx.YES_NO | wx.ICON_INFORMATION
                               #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                               )
        if dlg.ShowModal() == wx.ID_YES :
            print "Yes is clicked"
            dlg.Destroy()
#            self.Close()
            for m in self.GetChildren():
                if isinstance(m, wx.aui.AuiMDIClientWindow):
                    for k in m.GetChildren():
                        if isinstance(k, childFrame):
                            k.Close()
            evt.Skip()
            
            exit(0)
        else:
            print "Yes is not clicked"
            dlg.Destroy()        


    def MakeMenuBar(self):
        mb = wx.MenuBar()
        menu = wx.Menu()
        item = menu.Append(-1, "New child window\tCtrl-N")
        self.Bind(wx.EVT_MENU, self.OnNewChild, item)
        item = menu.Append(-1, "Close parent")
        self.Bind(wx.EVT_MENU, self.OnExit, item)
        mb.Append(menu, "&File")
        return mb

    def OnNewChild(self, evt):
        self.count += 1
        child = ChildFrame(self, self.count)
        child.Activate()
    
    
    
    def __del__( self ):
        pass

    def OnExit(self, evt):
        self.Close()


        
    def doDailyReport(self,evt):
		self.count += 1	
		win = childFrame(self,self.count)
		win.Activate()

    def CheckPassword(self,username,password):
        print username + " " + password
        if (password == "jojo"):
            return True
        else:
            return False
        
    def OnEraseBackground(self, evt):
        dc = evt.GetDC()
        
        if not dc:
            dc = wx.ClientDC(self.GetClientWindow())
        
        # tile the background bitmap
        sz = self.GetClientSize()
        w = self.bg_bmp.GetWidth()
        h = self.bg_bmp.GetHeight()
        x = 0
        
        while x < sz.width:
            y = 0
        
            while y < sz.height:
                dc.DrawBitmap(self.bg_bmp, x, y)
                y = y + h
        
            x = x + w
            
       
    
    
    def AboutBox(self,evt):
        print "About box..."
    
if __name__ == '__main__':    
    class MyApp(wx.App):
        def OnInit(self):
            frame = PaymentRecorderMain()
            frame.Show(True)
            f = passWord(None,frame)
            f.ShowModal()            
            return True


    app = MyApp(False)
    app.MainLoop()
