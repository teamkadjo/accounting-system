import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *
from specialolv import *
from pprint import pprint
from Users import *

        
class profilePanel(wxSpecialPanel):
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book
      
        super(profilePanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("profilePanel")
        self.users = Users()


        self.setEscapeHandler(self.CloseThisPanel)   

        sizerTop = wx.FlexGridSizer( 0, 2, 0, 0 )
        

        username = wx.StaticText(self, -1, "Admin Name :" )
        self.username = EnterTextCtrl(self, -1, "",(600,20), (150,-1))     

        sizerTop.Add(username, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.username, -1, wx.ALL|wx.CENTER, 5)

        admin_email = wx.StaticText(self, -1, "Admin Email :" )
        self.admin_email = EnterTextCtrl(self, -1, "",(600,20), (150,-1))     

        sizerTop.Add(admin_email, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.admin_email, -1, wx.ALL|wx.CENTER, 5)

        display_name = wx.StaticText(self, -1, "Display Name :" )
        self.display_name = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     

        sizerTop.Add(display_name, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.display_name, -1, wx.ALL|wx.CENTER, 5)


        password1 = wx.StaticText(self, -1, "Password :" )
        self.password1 = EnterTextCtrl(self, -1, "",(600,20), (150,-1),style=wx.TE_PASSWORD)     

        sizerTop.Add(password1, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.password1, -1, wx.ALL|wx.CENTER, 5)

        password2 = wx.StaticText(self, -1, "Confirm Password :" )
        self.password2 = EnterTextCtrl(self, -1, "",(600,20), (150,-1),style=wx.TE_PASSWORD)     

        sizerTop.Add(password2, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.password2, -1, wx.ALL|wx.CENTER, 5)


        updateLabel = "Update (Ctr-S)"            
        
        self.btn_Update = wx.Button(self, -1, updateLabel)          
        self.btn_Cancel = wx.Button(self, -1, "Cancel (Esc)")          
        self.Bind(wx.EVT_BUTTON, self.updateProfile, self.btn_Update)
        self.Bind(wx.EVT_BUTTON, self.CloseNow, self.btn_Cancel)


        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer.Add(self.btn_Update, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btn_Cancel, 0, wx.ALL|wx.LEFT, 5)

        
        mainSizer = wx.BoxSizer(wx.VERTICAL)  
        mainSizer.Add(sizerTop, 0, wx.TOP|wx.LEFT|wx.BOTTOM, 5)
        mainSizer.Add(btnsizer, 0, wx.LEFT|wx.BOTTOM, 5)        

        self.SetSizer(mainSizer) 

        self.userInfo = self.parent_window.userInfo
        
        print self.userInfo.admin_name
        
        self.loadInfo()
    

            
    def loadInfo(self):
        self.display_name.SetValue(self.userInfo.display_name)
        self.admin_email.SetValue(self.userInfo.admin_email)
        self.username.SetValue(self.userInfo.admin_name)

    def isBlank(self):
        return self.password1.GetValue().strip() == "" or  self.password2.GetValue().strip() == ""
        
    
    def isPasswordMatched(self):
        return self.password1.GetValue() == self.password2.GetValue()
    
               
    def updateProfile(self,evt):
        if (self.isPasswordMatched() and not self.isBlank()):
            data = {}

            print "this is the id " + str(self.userInfo.admin_id)
            
            data['admin_id'] = self.userInfo.admin_id
            data['admin_name'] = self.username.GetValue()
            data['display_name'] = self.display_name.GetValue()
            data['admin_email'] = self.admin_email.GetValue()
            data['admin_pass'] = self.password1.GetValue()
            is_ok = self.users.updateUserInfo(data)
            if is_ok:
                dlg = wx.MessageDialog(self.parent_window, "Successfully changed profile info of " + data['admin_name'] , "Profile Updated!",style=wx.ICON_WARNING|wx.CENTRE)
                dlg.ShowModal()                 
                
        else:
            dlg = wx.MessageDialog(self.parent_window, "Your password should matched or the password you provide is empty.", "Password Mismatched!",style=wx.ICON_WARNING|wx.CENTRE)
            dlg.ShowModal()  
      

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("profilePanel",True)  
        

    def CloseNow(self,evt):
        self.CloseThisPanel()

    
    def return_panel(self):        
        return self

def CreateProfilePanel(the_parent_window):
   panel = profilePanel(the_parent_window)
   return panel
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
