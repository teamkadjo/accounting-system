import inflect
import math


def whole_converter(num):
    p = inflect.engine()
    the_word = p.number_to_words(num)
    the_word = the_word.replace(" and "," ")    
    the_word = the_word.replace(",","")    
    return the_word


def get_number_as_words(num):
    (whole, frac) = (int(num), int(str(num)[(len(str(int(num)))+1):]))
    (frac,whole1) = math.modf(float(num))
    #strNum = str(whole)
    strNum = whole

    intNum = int(strNum)
    if intNum < 0:
      print "Minus", # I didn't see this at the beginning but I'll fix it at the end
      intNum *= -1
      #strNum = strNum[1:]
      #print "Number is " + strNum

    if intNum < 1000:
        if (frac == 0):
            return whole_converter(strNum) + " and 00/100"
        else:
            frac = int(float(str(round(frac,2))) * 100)
            return whole_converter(strNum) + " and " + str(frac) + "/100"
    else:
        if (frac == 0):
            return whole_converter(strNum) + " and 00/100"
        else:
            frac = int(float(str(round(frac,2))) * 100)
            return whole_converter(strNum) + " and " + str(frac) + "/100"
          
def main():
  number = 10005.93
  #raw_input("Please enter an integer:\n>> ")
  expected='ninety-five quadrillion, five hundred and five trillion, eight hundred  ninety-six billion, six hundred and thirty-nine million, six hundred and thirty-one thousand, eight hundred and ninety-three '
  print(get_number_as_words(number))
  #print(expected)
  #assert(get_number_as_words(strNum)==expected)

if __name__ == "__main__":
  main()    

