import wx
import wx.grid as grid
import wx.lib.mixins.gridlabelrenderer as glr
from wxSpecialPanel import *
#----------------------------------------------------------------------

class InventoryGrid(grid.Grid, glr.GridWithLabelRenderersMixin):
    def __init__(self, *args, **kw):
        grid.Grid.__init__(self, *args, **kw)
        glr.GridWithLabelRenderersMixin.__init__(self)


class MyRowLabelRenderer(glr.GridLabelRenderer):
    def __init__(self, bgcolor,text=""):
        self._bgcolor = bgcolor
        self.text = text

    def Draw(self, grid, dc, rect, row):
        dc.SetBrush(wx.Brush(self._bgcolor))
        dc.SetPen(wx.TRANSPARENT_PEN)
        dc.DrawRectangleRect(rect)
        hAlign, vAlign = grid.GetRowLabelAlignment()
        #text = grid.GetRowLabelValue(row)
        text = self.text
        self.DrawBorder(grid, dc, rect)
        self.DrawText(grid, dc, rect, text, hAlign, vAlign)


class MyColLabelRenderer(glr.GridLabelRenderer):
    def __init__(self, bgcolor,text=""):
        self._bgcolor = bgcolor
        self.text = text

    def Draw(self, grid, dc, rect, col):
        dc.SetBrush(wx.Brush(self._bgcolor))
        dc.SetPen(wx.TRANSPARENT_PEN)
        dc.DrawRectangleRect(rect)
        hAlign, vAlign = grid.GetColLabelAlignment()
        #text = grid.GetColLabelValue(col)
        text = self.text
        self.DrawBorder(grid, dc, rect)
        self.DrawText(grid, dc, rect, text, hAlign, vAlign)


class MyCornerLabelRenderer(glr.GridLabelRenderer):
    def __init__(self):
        import images3
        self._bmp = images3.Smiles.getBitmap()

    def Draw(self, grid, dc, rect, rc):
        x = rect.left + (rect.width - self._bmp.GetWidth()) / 2
        y = rect.top + (rect.height - self._bmp.GetHeight()) / 2
        dc.DrawBitmap(self._bmp, x, y, True)



class GridPanel(wxSpecialPanel):
    def __init__(self, parent):


        wx.Panel.__init__(self, parent, -1)
        self.SetLabel("InventoryEntry")

        ROWS = 3
        COLS = 4

        g = InventoryGrid(self, size=(3,3))
        g.CreateGrid(ROWS, COLS)

        g.SetCornerLabelRenderer(MyCornerLabelRenderer())

        row = 0
        g.SetRowLabelRenderer(row+0, MyRowLabelRenderer('#ffe0e0',"Beginning"))
        g.SetRowLabelRenderer(row+1, MyRowLabelRenderer('#e0e0ff',"Goods Available"))
        g.SetRowLabelRenderer(row+2, MyRowLabelRenderer('#ffe0e0',"Ending Inventory"))
        #for row in range(0, ROWS, 3):
            #g.SetRowLabelRenderer(row+0, MyRowLabelRenderer('#ffe0e0'))
            #g.SetRowLabelRenderer(row+1, MyRowLabelRenderer('#e0ffe0'))
            #g.SetRowLabelRenderer(row+2, MyRowLabelRenderer('#e0e0ff'))

        col = 0
        g.SetColLabelRenderer(col+0, MyColLabelRenderer('#e0ffe0',"LENDING"))
        g.SetColLabelRenderer(col+1, MyColLabelRenderer('#e0e0ff',"TRADING"))
        g.SetColLabelRenderer(col+2, MyColLabelRenderer('#e0ffe0',"CANTEEN"))
        g.SetColLabelRenderer(col+3, MyColLabelRenderer('#e0e0ff',"BAKERY"))

        #for col in range(0, COLS, 3):
            #g.SetColLabelRenderer(col+0, MyColLabelRenderer('#e0ffe0'))
            #g.SetColLabelRenderer(col+1, MyColLabelRenderer('#e0e0ff'))
            #g.SetColLabelRenderer(col+2, MyColLabelRenderer('#ffe0e0'))

        self.Sizer = wx.BoxSizer()
        self.Sizer.Add(g, 1, wx.EXPAND)


    def return_panel(self):
            return self

def CreateInventoryEntry(the_parent_window):
    panel = GridPanel(the_parent_window)
    return panel

