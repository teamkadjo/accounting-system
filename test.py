import wx

class Test(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, -1, title='Event Test',
size=(200, 200))

        panel = wx.Panel(self)

        panel.SetFocus()

        self.Bind(wx.EVT_CHAR_HOOK, self.OnKeyUP)

    def OnKeyUP(self, event):
        print "KEY UP!"
        keyCode = event.GetKeyCode()
        if keyCode == wx.WXK_ESCAPE:
            self.Close()


class App(wx.App):
    """Application class."""

    def OnInit(self):
        self.frame = Test()
        self.frame.Show()
        self.SetTopWindow(self.frame)
        self.frame.SetFocus()
        return True

if __name__ == '__main__':
    app = App()
    app.MainLoop()
