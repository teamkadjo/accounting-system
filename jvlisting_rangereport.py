from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from podunk.prefab.formats import *
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
import time
import traceback
from datetime import datetime
from Users import *
from numberword import *
import re



class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 30
        self.bottom_margin = 36        
            
    def _draw_header(self):
        pass

    def setExplanation(self,explanation=None):
        self.explanation = explanation
        
    def getExplanation(self):
        return self.explanation 
           

class SpecialTable(Table):

    def __init__(self,starting_date,ending_date,report=None):
        super(SpecialTable,self).__init__()
        self.yoff = 850
        self.xoff = 30
        
        self.left_margin = 54
        #self.top_margin = 72
        self.top_margin = 0
        self.right_margin = 54
        self.bottom_margin = 72
        self.starting_date = starting_date
        self.ending_date = ending_date
        self.page_counter = 1
        self.report = report





    def setAccountTitle(self,account_title):
        self.account_title = account_title
        
    def setReport(self,report):
        self.report = report
        
    def create_page_header(self,canvas):
        
        config = Configuration()
        canvas.setLineWidth(.3)
        canvas.setFont('Helvetica', 9)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        canvas.drawString(30,700,"Business Name :   " + companyName.title())
        #canvas.setFont('Helvetica', 8)

        canvas.drawString(30,710,"Taxpayer           :   " + companyName)
        #canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        canvas.drawString(30,690,"Address             :   " + complete_address)   

        #canvas.setFont('Helvetica-Bold', 8)  

        #canvas.setFont('Helvetica-Bold', 10)
        canvas.drawString(30,680,"Kinds of Book    :   JOURNAL BOOK ")
        
        canvas.setFont('Helvetica', 8)
        date_object = datetime.strptime(self.starting_date, '%Y-%m-%d')
        ending_dateobject = datetime.strptime(self.ending_date,"%Y-%m-%d")
        canvas.drawString(30,670,"Date Range          :   From " + date_object.strftime("%B %d, %Y") + " to " + ending_dateobject.strftime("%B %d, %Y") )

       
                    
    def draw_amount(self,canvas,r):
        if (r.debit-r.credit) != 0:
            canvas.drawString(self.xoff + 30, self.yoff, r.gl_account_desc)
            canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(r.debit-r.credit), pivotChar='.')
            self.lineFeed()

    def draw_grandtotal(self,canvas,amount):
        canvas.setFont('Helvetica-Bold', 8) 
        canvas.drawString(self.xoff + 30, self.yoff, 'GRAND TOTAL: ')
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.setFont('Helvetica', 8) 
        self.lineFeed()

    def draw_total(self,canvas,amount):
        canvas.line(self.xoff + 150,self.yoff + 9,self.xoff + 200,self.yoff + 9)
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.line(self.xoff + 150,self.yoff-2 ,self.xoff + 200,self.yoff-2 )
        self.lineFeed()
    
    
        
        
    def lineFeed(self):
        self.yoff = self.yoff - 10      
    
    def draw_name(self,canvas,name):
        canvas.drawString(self.xoff,self.yoff,name )
        self.lineFeed()
    
    def line_number(self):
        return self.yoff
    
    def reset_linenumber(self,canvas):
        self.yoff = 900
        if self.xoff == 350 : 
            self.report._page_count = self.report._page_count + 1 
            canvas.showPage()
            self.create_page_header(canvas)   
            self.report._draw_footer()                       
            canvas.setFont('Helvetica', 8)
            self.xoff = 30
            canvas.doForm('last_page')
        else:
            self.xoff = 350


    #def _draw_footer(self,canvas):
        #super(SpecialTable,self)._draw_footer(canvas)
        
    def get_page_count(self):
        return self.page_counter
        

    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)
        self.create_page_header(canvas)



def jv_Listing_Report(pdf_name='jvlistinreport.pdf',title="Voucher Number",starting_date='2016-06-01',ending_date='2016-06-01',username='admin'):
    table = SpecialTable(starting_date,ending_date)


    col = table.add_column('PARTICULARS',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    
    col.row.style.horizontal_alignment = alignment.LEFT


    col = table.add_column('DEBIT',60)
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')    

    col = table.add_column('CREDIT',60)  
    col.row.format = format_two_decimals
    col.row.style.horizontal_alignment = alignment.ALIGNED
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')

    col = table.add_column('ACCOUNTS TITLE',200)
    col.header.box.background_color = toColor('rgb(173,216,230)')
    col.header.style.color = toColor('rgb(0,0,0)')
    col.row.style.horizontal_alignment = alignment.LEFT





    j = Journals()

    u = Users()
    uinfo = u.get_userinfo(username)

    r = j.get_journal_by_daterange(starting_date,ending_date)
    running_balance = 0

    

    #table.setAccountTitle(acc.description)
    number = ""
    for x in r:
        if number != x.number :
            table.add_row( [x.number,0.0,0.0,"" ])
            number = x.number
        #print x.post_date
        sl_name = ""
        if  x.sl_name :
            sl_name = x.sl_name
            
        table.add_row( ["         "  + x.particulars,x.debit,x.credit, x.ac_title + " - "  + sl_name ])


    table.sum_column('CREDIT')
    table.sum_column('DEBIT')
    col = table.get_footer_field('DEBIT')    
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True

    col = table.get_footer_field('CREDIT')
    col.style.horizontal_alignment = alignment.ALIGNED
    col.style.bold = True

    col = table.get_footer_field('PARTICULARS')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9
    col.value = ""

    col = table.get_footer_field('ACCOUNTS TITLE')
    col.style.horizontal_alignment = alignment.RIGHT
    col.style.bold = True
    col.style.size = 9
    col.value = ""





    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    table.setReport(report)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {}'.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    jv_Listing_Report('jvlistinreport.pdf','Journal Voucher ','2017-01-01','2017-1-31')
    os.system('start jvlistinreport.pdf')
