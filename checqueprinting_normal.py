from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
import os,sys
from numberword2 import *

from reportlab.platypus import *


class CheckPrinting:
    def __init__(self,filename='test2.pdf',info=None):       
        self.canvas = canvas.Canvas(filename, pagesize=letter)
        self.filename = filename
        self.info = info

    def create_pdf_normal(self):
        self.canvas.translate( 1 * cm,20 * cm)
       # self.canvas.rotate(90)
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Courier-Bold', 10)
        #self.canvas.setPageSize((20*cm,7.5*cm))
        name = self.info["name"].upper()
        amount_in_words = get_number_as_words(self.info["amount"]).upper()
        amount_comma = "{:,.2f}".format(self.info['amount'])
        
        self.canvas.drawString(2.5 * cm ,4.5 * cm, "**** " + name + " ****")
        self.canvas.drawString(5.5 * cm ,3.5 * cm, amount_in_words + " ONLY")
        self.canvas.drawString(14.5 * cm ,5.5 * cm, self.info["date"])
        self.canvas.drawString(16 * cm ,4.5 * cm,amount_comma)
        self.canvas.drawString(0* cm,0.5* cm,self.info['cvnumber'])
        
        PageBreak()

        self.canvas.showPage()
        
        self.canvas.translate(14 * cm,7.5 * cm)
        self.canvas.rotate(90)
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Courier-Bold', 10)
        #canvas.setPageSize((7.5*cm,20*cm))
        name = self.info["name"].upper()
        amount_in_words = get_number_as_words(self.info["amount"]).upper()
        amount_comma = "{:,.2f}".format(self.info['amount'])
        
        space = 200
        
        self.canvas.drawString(3.8 * cm ,4.5 * cm + space, "**** " + name + " ****")
        self.canvas.drawString(5.5 * cm ,3.8 * cm + space, amount_in_words + " ONLY")
        self.canvas.drawString(15.5 * cm ,5.3 * cm + space, self.info["date"])
        self.canvas.drawString(16 * cm ,4.5 * cm + space,amount_comma)
        self.canvas.drawString(4* cm,0.5* cm + space,self.info['cvnumber'])
        
        self.canvas.save()
    
    def open_pdf(self):
        os.system("start " + self.filename)


if __name__ == "__main__":
    info = {"name":'Justin Bieber','amount':14555.05,'date':'September 11, 2015','cvnumber':'CV-4722'}
    chk = CheckPrinting('test2.pdf',info)
    chk.create_pdf_normal()
    chk.open_pdf()


