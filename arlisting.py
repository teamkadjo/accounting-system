from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
import time
import traceback
from datetime import datetime
from Users import *

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 30
        self.bottom_margin = 36        
            
    def _draw_header(self):
        pass

    def setExplanation(self,explanation=None):
        self.explanation = explanation
        
    def getExplanation(self):
        return self.explanation 
           

class SpecialTable(Table):

    def __init__(self,cut_off_date,report=None):
        super(SpecialTable,self).__init__()
        self.yoff = 685
        self.xoff = 30
        
        self.left_margin = 54
        self.top_margin = 72
        self.right_margin = 54
        self.bottom_margin = 72
        self.cutoff_date = cut_off_date
        self.page_counter = 1
        self.report = report
        
    def setReport(self,report):
        self.report = report
        
    def create_page_header(self,canvas):
        
        config = Configuration()
        canvas.setLineWidth(.3)
        canvas.setFont('Helvetica', 10)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        canvas.drawString(30,750,companyName)
        canvas.setFont('Helvetica', 8)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        canvas.drawString(30,740,complete_address)   

        canvas.setFont('Helvetica-Bold', 8)  

        canvas.setFont('Helvetica-Bold', 10)
        canvas.drawString(30,725,"ACCOUNTS RECEIVABLE - LISTING")
        
        canvas.setFont('Helvetica', 8)
        date_object = datetime.strptime(self.cutoff_date, '%Y-%m-%d')
        canvas.drawString(30,710,"As of " + date_object.strftime("%B %d, %Y") )
                
        
        canvas.drawString(30,690,"MEMBERS' NAME")
        canvas.drawString(195,690,"AMOUNT")
        canvas.line(30,685,230,685)
        canvas.drawString(350,690,"MEMBERS' NAME")
        canvas.drawString( 515,690,"AMOUNT")
        canvas.line(350,685,350+200,685)        
        
    def draw_some(self, canvas, left, right, yoff, vspace):

        self.create_page_header(canvas)
        self.lineFeed()
        j = Journals()         
        mem = j.get_members()
        
        al = j.ar_list(self.cutoff_date)
        self.ar = {}
        for a in al:
            self.ar[str(a.mem_id)]  = {}
        
        for a in al:
            self.ar[str(a.mem_id)]['accounts']  = []

        for a in al:
            if ((a.gl_account != '30101') and (a.gl_account != '21007' )):
                self.ar[str(a.mem_id)]['accounts'].append(a)        

        self.total_pages = len(al)/102

        grand_total = 0
        
        for m in mem:
            try:
                if len(self.ar[str(m.ref_id)]['accounts']) > 0 :
                    #self.draw_name(canvas, '{0:04d}'.format(m.ref_id) + " " + m.primary_name)
                    self.draw_name(canvas,  m.primary_name)
                    total = 0
                    counter = 0
                    for r in self.ar[str(m.ref_id)]['accounts']:
                        self.draw_amount(canvas,r)
                        total = total + (r.debit - r.credit)
                        if (r.debit - r.credit )  != 0:
                            counter = counter + 1
                    if counter > 1 : 
                        self.draw_total(canvas,total)
                    grand_total = grand_total + total                        
                    self.lineFeed()
                           
            except:
                continue        


            if self.line_number() < 80:
                self.create_page_header(canvas)
                self.reset_linenumber(canvas)
        
        
        self.draw_grandtotal(canvas,grand_total)
        
        return 0
    
    def draw_amount(self,canvas,r):
        if (r.debit-r.credit) != 0:
            canvas.drawString(self.xoff + 30, self.yoff, r.gl_account_desc)
            canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(r.debit-r.credit), pivotChar='.')
            self.lineFeed()

    def draw_grandtotal(self,canvas,amount):
        canvas.setFont('Helvetica-Bold', 8) 
        canvas.drawString(self.xoff + 30, self.yoff, 'GRAND TOTAL: ')
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.setFont('Helvetica', 8) 
        self.lineFeed()

    def draw_total(self,canvas,amount):
        #canvas.drawString(self.xoff + 30, self.yoff, r.gl_account_desc)
        canvas.line(self.xoff + 150,self.yoff + 9,self.xoff + 200,self.yoff + 9)
        canvas.drawAlignedString(self.xoff + 190, self.yoff, "{:,.2f} ".format(amount), pivotChar='.')
        canvas.line(self.xoff + 150,self.yoff-2 ,self.xoff + 200,self.yoff-2 )
        self.lineFeed()
    
    
        
        
    def lineFeed(self):
        self.yoff = self.yoff - 10      
    
    def draw_name(self,canvas,name):
        canvas.drawString(self.xoff,self.yoff,name )
        self.lineFeed()
    
    def line_number(self):
        return self.yoff
    
    def reset_linenumber(self,canvas):
        self.yoff = 675
        if self.xoff == 350 : 
            self.report._page_count = self.report._page_count + 1 
            canvas.showPage()
            self.create_page_header(canvas)   
            self.report._draw_footer()                       
            canvas.setFont('Helvetica', 8)
            self.xoff = 30
            canvas.doForm('last_page')
        else:
            self.xoff = 350


    def _draw_footer(self,canvas):
        pass
        
    def get_page_count(self):
        return self.page_counter
        

def ar_Listing(pdf_name='test.pdf',title="Voucher Number",cutoff_date='2015-10-10',username='adr'):
    table = SpecialTable(cutoff_date)

    j = Journals()

    u = Users()
    uinfo = u.get_userinfo(username)

    veryNiceHeader = Heading(None)
    
    report = ReportSpecialHeader(pdf_name)
    table.setReport(report)
    report.title = None
    report.header.style.horizontal_alignment = alignment.CENTER
    report.author = 'Printed by {}'.format(uinfo.display_name)
    report.add(veryNiceHeader)
    report.add(table)
    report.create()

        
        
if __name__ == '__main__':
    ar_Listing('test3.pdf','Journal Voucher ','2016-4-1','admin')
    os.system('start test3.pdf')
