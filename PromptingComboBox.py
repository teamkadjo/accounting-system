'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: PromptingComboBox.py
'''
import wx
 
class PromptingComboBox(wx.ComboBox) :
    def __init__(self, parent, value, choices=[], style=0, **par):
        #wx.ComboBox.__init__(self, parent, wx.ID_ANY, value, style=style|wx.CB_DROPDOWN|wx.TE_PROCESS_ENTER|wx.TE_CAPITALIZE, choices=choices, **par)
        wx.ComboBox.__init__(self, parent, wx.ID_ANY, value, style=style|wx.CB_DROPDOWN|wx.TE_PROCESS_ENTER|wx.TE_CAPITALIZE, choices=choices, **par)
        self.choices = choices
        self.Bind(wx.EVT_TEXT, self.EvtText)
        self.Bind(wx.EVT_KEY_DOWN, self.EvtChar)        
        self.Bind(wx.EVT_COMBOBOX, self.EvtCombobox) 
        self.ignoreEvtText = False
        self.is_shift_pressed = False
        self.theValue = value
        self.process_text_now = None
     
    def EvtCombobox(self, event):
        self.ignoreEvtText = True
        self.process_text(event)
        event.Skip()
     
    def SetSpecialSelection(self):
        idx = self.FindString(self.theValue)        
        self.SetSelection(idx)   
        
    def EvtChar(self, event):
        keycode = event.GetKeyCode()
        #print keycode
        if (keycode == wx.WXK_SHIFT):
            self.is_shift_pressed = True
            return
        elif keycode == wx.WXK_TAB:            
            if event.ShiftDown():
                event.EventObject.Navigate(wx.NavigationKeyEvent.IsBackward)
            else:
                event.EventObject.Navigate()
            return
        elif keycode == wx.WXK_RETURN or keycode == wx.WXK_NUMPAD_ENTER:
            self.process_text(event=None)
            event.EventObject.Navigate()
            return
        elif keycode == 8:
            self.ignoreEvtText = True        
        event.Skip()
        
    def EvtText(self, event):
        if self.ignoreEvtText:
            self.ignoreEvtText = False
            return
        currentText = event.GetString()
        currentText = currentText.decode('utf-8', 'ignore')
        found = False
        for choice in self.choices :
            try :
                if choice.startswith(currentText):
                    self.ignoreEvtText = True
                    self.SetValue(choice)
                    self.SetInsertionPoint(len(currentText))
                    self.SetMark(len(currentText), len(choice))
                    found = True
                    break
            except:
                pass
        if not found:
            event.Skip()

    def process_text(self,event):
        if self.process_text_now:
            self.process_text_now(event)
            

class TrialPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        
        choices = ['grandmother', 'grandfather', 'cousin', 'aunt', 'uncle', 'grandson', 'granddaughter']
        for relative in ['mother', 'father', 'sister', 'brother', 'daughter', 'son']:
            choices.extend(self.derivedRelatives(relative))

        cb = PromptingComboBox(self, "default value", choices, style=wx.CB_SORT) 

    def derivedRelatives(self, relative):
        return [relative, 'step' + relative, relative + '-in-law']


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame (None, -1, 'Demo PromptingComboBox Control', size=(400, 50))
    TrialPanel(frame)
    frame.Show()
    app.MainLoop()
