
'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2018 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: EditTransaction.py
'''

import wx
import PromptingComboBox
from Journals import *
from Contacts import *
from EnterTextCtrl import *
from RadioBoxCtrl import *
from pprint import pprint
from TextCtrlAutoComplete import *
from SpecialCombobox import *
from pprint import *
from Configuration import *
import re

class EntryWindow(object):
    #----------------------------------------------------------------------
    def __init__(self, gl_account, sl_id, cost_center, description, debit,credit,rec_id):
        self.gl_account = gl_account
        self.sl_id = sl_id
        self.cost_center = cost_center
        self.description = description
        self.debit = debit
        self.amount = credit
        self.id = rec_id

    def SetCostCenter(self, value):
        if value is None or value == "":
            self.cost_center = None
        else:
            self.cost_center = value

    def SetGLAccount(self,value):
        if value is None or value == "":
            self.gl_account = None
        else:
            self.gl_account = value


class EditTransactionDialog(wx.Dialog):

    def __init__(
            self, parent, ID, title,transact_data=None, size=wx.DefaultSize, pos=wx.DefaultPosition,
            style=wx.DEFAULT_DIALOG_STYLE,isNew=False
            ):
        args = {}

        self.journals = Journals()
        transaction_data = None
        self.transactionData = None
        self.config = Configuration()

        s_ledger = ""
        c_center = "ADMIN"
        debit_string = ""
        amount_string = ""
        description = ""
        self.current_id = None
        entry_type = "Debit"
        account_id = ""


        if (transact_data ):
            #pprint(transact_data)
            if transact_data.sl_description:
                s_ledger = transact_data.sl_description
            c_center = transact_data.cost_center.upper()
            transaction_data = self.journals.get_je_transaction(transact_data.id)


            credit_test_value = str(transact_data.credit.strip()).replace(",","")
            if credit_test_value.strip() != "":
                float_value = float(credit_test_value)
            else:
                float_value = 0


            if  float_value  > 0 :
                entry_type = "Credit"
                amount_string = credit_test_value
            else:
                amount_string = str(transact_data.debit).replace(",","")
                entry_type = "Debit"

            description = transact_data.description
            descript_list = transact_data.gl_account.split(" - ")
            if len(descript_list) >=2  :
                #print transact_data.gl_account
                s = transact_data.gl_account
                descript = s[8:len(s)].strip()
                account_id = s[:5]
            else:
                descript = ""
            self.current_id = transact_data.id
        else:
            s_ledger = ""
            c_center = "ADMIN"
            transaction_data = None
            debit_string = "0.00"
            amount_string = "0.00"
            description = ""
            descript = ""


        self.parent = parent
        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent.parent_window, ID, title, pos, size, style)

        self.PostCreate(pre)
        #self.SetSize((500, 400))

        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "Transaction Editor")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "GL Account:")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        #text = wx.TextCtrl(self, -1, "", size=(80,-1))
        #text.SetHelpText("Here's some help text for field #1")
        #self.chart_of_accounts = self.journals.db.chart_of_accounts.all()
        self.chart_of_accounts = self.journals.get_chart_of_accounts()
        choices_ = [x.description for x in self.chart_of_accounts]


        args["selectCallback"] = self.selectCallback
        args["choices"] = choices_

        self.cbx = SpecialCombobox(self,choices_,self.selectCallback,descript)
        self.cbx.SetEventKillFocus(self.ChangeAccountHandler)

        box.Add(self.cbx, 1, wx.ALIGN_CENTRE|wx.ALL, 5)

        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Ledger Type :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        choices = ["Member","Supplier"]
        self.subledger_type = PromptingComboBox.PromptingComboBox(self,u"Member",choices,style=wx.CB_READONLY |  wx.WANTS_CHARS)

        box.Add(self.subledger_type, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        self.subledger_type.Bind(wx.EVT_COMBOBOX,self.ChangeLedgerSelection)
        self.subledger_type.Bind(wx.EVT_TEXT_ENTER,self.ChangeLedgerSelection)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "SubLedger :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        self._contacts = Contacts()
        self.contacts = self._contacts.get_all_members()
        self.memberchoices = [( x.name.encode('utf-8').decode('utf-8').upper() + " (" + str(x.id) + ")") for x in self.contacts]

        the_suppliers = self._contacts.get_suppliers(0,0)

        self.supplierchoices = [( x.primary_name.encode('utf-8')) for x in the_suppliers]

        #self.member = PromptingComboBox.PromptingComboBox(self,s_ledger.encode('utf-8'),self.memberchoices)
        self.member = SpecialCombobox(self,self.memberchoices,self.selectCallback2,s_ledger)
        box.Add(self.member, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Cost Center :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        self.cost_center_choices = self.journals.db.contacts.filter(self.journals.db.contacts.type == 'j').all()

        choices = [( x.short_name) for x in self.cost_center_choices ]

        self.costcenter = PromptingComboBox.PromptingComboBox(self,c_center,choices,style=wx.CB_READONLY |  wx.WANTS_CHARS)

        box.Add(self.costcenter, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Description :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        self.descript = EnterTextCtrl(self, -1, description, wx.DefaultPosition, (100,50),style=wx.TE_MULTILINE | wx.TE_PROCESS_ENTER)
        box.Add(self.descript, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "Debit :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)


        choices = ["Debit","Credit"]

        self.entry_type = PromptingComboBox.PromptingComboBox(self,entry_type,choices,style=wx.CB_READONLY |  wx.WANTS_CHARS)
        #self.entry_type = SpecialCombobox(self,choices,self.selectCallback)
        #self.debit = EnterTextCtrl(self, -1, debit_string, size=(100,-1),style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
        box.Add(self.entry_type, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)

        label1 = wx.StaticText(self, -1, "Amount :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label1, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)



        self.amount = EnterTextCtrl(self, -1, amount_string , wx.DefaultPosition,(100,-1),style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
        box.Add(self.amount, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)



        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        #btnsizer = wx.StdDialogButtonSizer()
        btnsizer = wx.BoxSizer(wx.HORIZONTAL)


        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.AddButton(btn)
        self.save_continue = wx.Button(self, label="Save and Continue", name="newView")
        self.Bind(wx.EVT_BUTTON, self.SaveAndContinue, self.save_continue)
        btnsizer.Add(self.save_continue, 0, wx.ALL|wx.LEFT, 5)
        #btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_OK)
        btn.SetHelpText("The OK button completes the dialog")
        #btn.SetDefault()
        btnsizer.Add(btn, 0, wx.ALL|wx.LEFT, 5)
        #btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)

        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")
        btnsizer.Add(btn, 0, wx.ALL|wx.LEFT, 5)
        #btnsizer.AddButton(btn)
        #btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.CENTER|wx.ALL, 5)

        ctrlA = wx.NewId()
        self.Bind(wx.EVT_MENU,self.Focuser,id=ctrlA)
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('a'), ctrlA )])
        self.SetAcceleratorTable(accel_tbl)

        self.SetSizer(sizer)

        self.CheckAccounts(account_id)

        #self.Center()
        if isNew:
            self.save_continue.Enable(True)
        else:
            self.save_continue.Enable(False)

    def CheckAccountSelection(self,evt):
        account_name = self.cbx.GetValue()
        self.selectCallback(account_name)

    def Focuser(self,evt):
        self.descript.SetSelection(-1,-1)

    def SaveAndContinue(self,evt):
        obj = self.getChanges()
        self.parent.AddTransaction(obj)
        evt.EventObject.Navigate()
        evt.EventObject.Navigate()
        evt.EventObject.Navigate()
        pass

    def doPopup(self,evt):
        #self.cbx.SetBackgroundColour((255,0,0))
        pass

    def doDismiss(self,evt):
        #print "do dismiss"
        #self.cbx.SetBackgroundColour((255,255,255))
        self.cbx.Refresh()

    def ToggleSubLedger(self,value):
        self.subledger_type.Enable(value)
        self.member.Enable(value)

    def CheckAccounts(self,account):
        accounts_with_sl_config = self.config.getConfig('SL_ACCOUNTS')
        accounts_with_sl =[]
        for ac in  accounts_with_sl_config.split(","):
            accounts_with_sl.append(ac.strip("'"))
        if account in accounts_with_sl:
            self.ToggleSubLedger(True)
        else:
            self.ToggleSubLedger(False)



    def ChangeAccountHandler(self):

        acc_name = self.cbx.GetValue()
        acc_info = self.journals.get_one_account(acc_name)
        if acc_info:
            self.CheckAccounts(acc_info.id)


    def ChangeLedgerSelection(self,event):
        ledgertype =  self.subledger_type.GetValue()
        if ledgertype == "Member":
            choices = self.memberchoices
        else:
            choices = self.supplierchoices
        self.member.SetChoices(choices)




    def selectCallback(self, values):
        """ Make sure that if the account has subledger let the user choose from the list
        """
        data = self.journals.get_one_account(values[0])
        if data:
            self.CheckAccounts(data.id)
            #print data.id + ' - ' + data.description
        #print "Select Callback called...:",  values

    def selectCallback2(self, values):
        """ Simply function that receive the row values when the
            user select an item
        """
        #print "Select Callback called...:",  values
        value = values[0]
        subledger = self._contacts.get_members(0,0,value)


    def getChanges(self):
        value = u'%s' % self.member.GetValue()

        account = self.journals.get_one_account(self.cbx.GetValue().strip())

        if (self.subledger_type.Enabled) and (value != ""):
            member_ids = re.findall("\(\d+\)",value)

            if len(member_ids) > 0 :
                member_id = re.sub("\(|\)","",member_ids[0])
                subledger = self._contacts.db.address_book.filter(self._contacts .db.address_book.ref_id == int(member_id)).one()

            else:
                subledger = None
        else:
            subledger = None
        cost_center = self._contacts.db.address_book.filter(self._contacts .db.address_book.primary_name == self.costcenter.GetValue().upper()).one()

        if account:
            glID = account.id
            account_description = account.description
        else:
            glID = None
            account_description = None

        if subledger :
            subledger_id = subledger.ref_id
            subledger_text = subledger.primary_name + " (" + str(subledger_id) + ")"
        else:
            subledger_id = None
            subledger_text = None

        if cost_center:
            costcenter_id = cost_center.ref_id
            costcenter_name = cost_center.primary_name
        else:
            costcenter_id = None
            costcenter_name = None

        description =  self.descript.GetValue()

        #debit = self.debit.GetValue()
        if self.entry_type.GetValue() == "Debit":
            debit = self.amount.GetValue()
            credit = 0
        else:
            credit = self.amount.GetValue()
            debit = 0



        self.transactionData = {'account_id':glID, 'account_description':account_description,'subledger_id':subledger_id,'subledger_description':subledger_text, 'costcenter_id':costcenter_id, 'costcenter_name':costcenter_name,'description':description, 'debit':debit,'credit':credit,'refid':self.current_id}
        return self.transactionData



class MyFrame(wx.Frame):
    #----------------------------------------------------------------------
    def __init__(self):
        #wx.Frame.__init__(self, parent=None, id=wx.ID_ANY,
                          #title="ObjectListView Demo", size=(800,600))

        #trans_id = 14

        journals = Journals()
        transaction_data = journals.get_je_transaction(14)
        dlg = EditTransactionDialog(self, -1, "Edit Transaction Dialog (Yey Test)" ,transaction_data, size=(400,350),  style=wx.DEFAULT_DIALOG_STYLE)
        #dlg.ShowWindowModal()
        if dlg.ShowModal() == wx.ID_OK:
            result = dlg.getChanges()
            #pprint (result)

class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        #frame.Show(True)
        #self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()



