'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: Journals.py
'''

import sys
from db import *
import sqlalchemy
import traceback
from Configuration import *
from datetime import date
from datetimeutil import *
from sqlalchemy import extract,and_
from Contacts import Contacts
class Journals:
    def __init__(self):
        self.db = None
        try :
            self.Journals = DatabaseMySQL.journal_main
            self.db = DatabaseMySQL
            self.cv_series = DatabaseMySQL.cv_series
            self.config = Configuration()
        except:
            traceback.print_exc()
            print "Serious connection problem occured"
            sys.exit(0)
            return None

        self.counter = 0
        return None


    def mkFirstOfMonth(self,dtDateTime):
        #what is the first day of the current month
        ddays = int(dtDateTime.strftime("%d"))-1 #days to subtract to get to the 1st
        delta = datetime.timedelta(days= ddays)  #create a delta datetime object
        return dtDateTime - delta


    def update_journal(self,ref_id_,date,explanation,amount=0):
        accounting_period = self.config.getConfig('CURRENT_ACCOUNTING_PERIOD')
        self.db.journal_main.filter(self.db.journal_main.id==ref_id_).update({'period':accounting_period,'journal_id':2,'post_date':date,'description':explanation,'total_amount':amount})
        self.db.commit()

    def insert_journal_lateposting(self,date_posted,explanation,amount=0,jtype_=0,invoice_number = 'JV-SOMETHING-SOMETHING'):
        accounting_period = self.config.getConfig('CURRENT_ACCOUNTING_PERIOD')
        self.db.journal_main.insert(period=accounting_period,journal_id=2,post_date=date_posted,description=explanation,total_amount=amount,purchase_invoice_id='',jtype=jtype_)
        self.db.commit()
        last_insert_id = self.get_last_insertid('journal_main')

        self.db.journal_main.filter(self.db.journal_main.id == last_insert_id).update({'purchase_invoice_id': invoice_number})
        self.db.commit()
        return {'last_insert_id':last_insert_id,'ref_number':invoice_number}


    def insert_journal(self,date_posted,explanation,amount=0,jtype_=0):
        accounting_period = self.config.getConfig('CURRENT_ACCOUNTING_PERIOD')
        self.db.journal_main.insert(period=accounting_period,journal_id=2,post_date=date_posted,description=explanation,total_amount=amount,purchase_invoice_id='',jtype=jtype_)
        self.db.commit()
        last_insert_id = self.get_last_insertid('journal_main')
        if jtype_ == 0:
            series_link_id = self.insert_new_journals_serial(last_insert_id)
            invoice_id = 'JV-{1}-{0:03d}'.format(series_link_id,date.today().year)
        else:
            series_link_id = self.insert_new_cv_serial(last_insert_id)
            invoice_id = 'CV-{1}-{0:03d}'.format(series_link_id,date.today().year)

        self.db.journal_main.filter(self.db.journal_main.id == last_insert_id).update({'purchase_invoice_id': invoice_id})
        self.db.commit()
        return {'last_insert_id':last_insert_id,'ref_number':invoice_id}

    def insert_journalitem(self,item,ref_id_,post_date_):
        gl_account_id = item.gl_account.split(' - ')[0]
        self.db.journal_item.insert(ref_id=ref_id_,qty=1,description=item.description,debit_amount=item.debit,credit_amount=item.credit,gl_account = gl_account_id,post_date=post_date_,sublgr_id=item.sl_id,project_id = item.cost_center_id)
        self.db.commit()

    def delete_journalitems(self,ref_id_):
        res = self.db.execute("DELETE FROM journal_item where ref_id = '%s'" % ref_id_)
        self.db.commit()

    def delete_journalmain(self,ref_id):
        res = self.db.execute("DELETE FROM journal_main where id = '%s'" % ref_id)
        self.db.commit()
        self.delete_journalitems(ref_id)


    def get_last_insertid(self,table_name):
        res = self.db.execute("SELECT AUTO_INCREMENT FROM information_schema.TABLES  WHERE TABLE_SCHEMA = 'pb' AND TABLE_NAME = '" + table_name + "'")
        one = res.fetchone()
        lastid = one[0]
        return int(lastid) - 1

    def insert_new_journals_serial(self,ref_id_):
        self.db.jv_series.insert(ref_id=ref_id_,text_display=" ")
        self.db.commit()
        return self.get_last_insertid('jv_series')

    def insert_new_cv_serial(self,ref_id_):
        self.db.cv_series.insert(ref_id=ref_id_,text_display=" ")
        self.db.commit()
        return self.get_last_insertid('cv_series')

    def insert_account(self,**data):
        try:
            self.JournalItem.insert(**data)
            self.db.commit()
            return 'Ok'
        except:
            return None

    def delete_account(self,data):
         try:
            user_account = self.JournalItem.filter_by(user_id=data).one()
            Database.delete(user_account)
            Database.commit()
            return 'Ok'
         except:
             if sqlalchemy.orm.exc.NoResultFound:
                 return 'No Result Found'
             else:
                 return None

    def update_account(self,id,data):
        try:
            self.JournalItem.filter_by(user_id=id).update(data)
            self.db.commit()
            return 'Ok'
        except:
            return None

    def get_journal_main(self,_offset=0,_limit=3):
        try :
            config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
            accounting_period = config.configuration_value

            if _limit:
                limit = " limit " + str(_offset) + ", " + str(_limit)
            else:
                limit  = " "



            rp = self.db.bind.execute("select * from journal_main m where (m.jtype = 0 or isnull(m.jtype))  and m.journal_id = 2 and period = '" + accounting_period + "' order by m.purchase_invoice_id desc " + limit)
            data = rp.fetchall()
            return data

        except:
            print(traceback.format_exc())
            return None


    def get_journal_main2(self,_offset=0,_limit=3,where = None):
        try :
            if where:
                where_value = " and (m.description like %s  or m.purchase_invoice_id like %s)"
            else:
                where_value = " "

            sql = "select * from journal_main m where (m.jtype = 0 or isnull(m.jtype))  and m.journal_id = 2 " + where_value + " order by CONVERT(digits(substr(m.purchase_invoice_id,9)),UNSIGNED INTEGER) desc,m.post_date desc limit " + str(_offset* _limit) + ", " + str(_limit)
            #print sql

            if where:
                rp = self.db.bind.execute(sql,"%{}%".format(where.strip()),"%{}".format(where.strip()))
            else:
                rp = self.db.bind.execute(sql)
            data = rp.fetchall()
            listing = []
            return data
        except:
            print(traceback.format_exc())
            return None

    def get_cv_main2(self,_offset=0,_limit=3,where = None):
        try :
            if where:
                where_value = " and (m.description like %s  or m.purchase_invoice_id like %s)"
            else:
                where_value = " "

            sql = "select * from journal_main m where (m.jtype = 1 or isnull(m.jtype))  and m.journal_id = 2 " + where_value + " order by m.id DESC,m.purchase_invoice_id desc limit " + str(_offset* _limit) + ", " + str(_limit)
            #print sql
            if where:
                rp = self.db.bind.execute(sql,"%{}%".format(where.strip()),"%{}".format(where.strip()))
            else:
                rp = self.db.bind.execute(sql)
            data = rp.fetchall()
            listing = []
            return data
        except:
            print(traceback.format_exc())
            return None

    def count_cvs(self,where=None):
            try:
                if where:
                    where_value = " and (m.description like %s  or m.purchase_invoice_id like %s)"
                else:
                    where_value = " "

                sql = "select count(*) as counter from journal_main m where (m.jtype = 1 or isnull(m.jtype))  and m.journal_id = 2 " + where_value

                #print sql
                if where:
                    rp = self.db.bind.execute(sql,"%{}%".format(where.strip()),"%{}".format(where.strip()))
                else:
                    rp = self.db.bind.execute(sql )

                value = rp.fetchone()

                return value.counter
            except:
                print "error here 219 journals.py"
                print (traceback.format_exc())
                return None

    def count_journals(self,where=None):
            try:
                if where:
                    where_value = " and (m.description like %s   or m.purchase_invoice_id like %s)"
                else:
                    where_value = " "


                sql = "select count(*) as counter from journal_main m where (m.jtype = 0 or isnull(m.jtype))  and m.journal_id = 2 " + where_value


                if where:
                    rp = self.db.bind.execute(sql,'%' + where + '%','%' + where)
                else:
                    rp = self.db.bind.execute(sql)
                value = rp.fetchone()

                return value.counter
            except:
                print "Journals.py error 240"
                print (traceback.format_exc())
                return None



    def get_cv_main(self,_offset=0,_limit=3):
        try :
            config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
            accounting_period = config.configuration_value

            if _limit:
                limit = " limit " + str(_offset) + ", " + str(_limit)
            else:
                limit  = " "

            rp = self.db.bind.execute("select * from journal_main m where (m.jtype=1)  and m.journal_id = 2  and period = '" + accounting_period + "' order by m.id desc " + limit)
            data = rp.fetchall()
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return None


    def get_je_transaction(self,id):
        try :
            rp = self.db.bind.execute("select m.id,m.ref_id,m.gl_account,m.sublgr_id,m.description,m.debit_amount,m.credit_amount,m.project_id, concat(m.gl_account,' - ' ,c.description) as gl_desc,c.description as gl_description,ct.short_name as cost_center,concat(ct2.short_name,' ') as subledger  from journal_item m left join chart_of_accounts c on m.gl_account = c.id left join contacts ct on m.project_id = ct.id left join contacts ct2 on ct2.id = m.sublgr_id where m.id = '" + str(id) + "' order by m.id limit 1")
            data = rp.fetchone()
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return None

    def get_members(self):
        try :
            rp = self.db.bind.execute("select ab.ref_id,c.short_name,ab.primary_name from contacts c left join address_book ab on ab.ref_id = c.id   where c.type = 'c'  order by ab.primary_name")
            data = rp.fetchall()
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            #print(traceback.format_exc())
            return None





    def get_chart_of_accounts(self):
        try:
            rp = self.db.bind.execute("select id,description from chart_of_accounts  order by id")
            data = rp.fetchall()
            return data
        except:
            return None

    def get_account_aginglist(self, as_of_date=None):
        try:
            if as_of_date:
                rp = self.db.bind.execute("select *  from vwaging where last_postdate <= '{}'".format(as_of_date))
            else:
                rp = self.db.bind.execute("select *  from vwaging ")
            data = rp.fetchall()
            return data
        except:
            return None

    def get_chart_of_accounts_by_page(self,_offset=1,_limit=10,searchvalue=""):
        try :
            limit = ""
            if _limit > 0:
                limit =  " limit "  + str(_offset*20) + ", " + str(_limit)

            if searchvalue != "":
                sql = "select id,description from chart_of_accounts where (description like %s or id like %s) order by id {0} ".format(limit)
                # print sql
                rp = self.db.bind.execute(sql,'%' + searchvalue + '%','%' + searchvalue)
            else:
                sql = "select id,description from chart_of_accounts  order by id {0} ".format(limit)
                rp = self.db.bind.execute(sql)

            # sql = "select id,description from chart_of_accounts  order by id {0}".format(limit)
            # #print sql
            # rp = self.db.bind.execute(sql)
            data = rp.fetchall()
            return data
        except:
            print (traceback.format_exc())

    def get_chart_of_accounts_selection(self,accounts_selection):
        try:
            rp = self.db.bind.execute("select ca.*,act.text_display as descript from chart_of_accounts ca left join account_types act on act.code = ca.account_type where ca.account_type in (" + accounts_selection + ") order by account_type,description")
            data = rp.fetchall()
            return data
        except:
            return None

    def get_summation(self,accounts_selection,cutoff_date):

        #print "select min(j.gl_account) as acc_code,min(ca.description) as descript,sum(j.debit_amount) as debit,sum(credit_amount) as credit,ca.account_type  from journal_item j left join chart_of_accounts ca on ca.id = j.gl_account where j.post_date between '2015-10-1' and '" + cutoff_date + "' and ca.account_type in (" + accounts_selection + ") group by gl_account"

        sql_query = ""
        try:
            #data = ""

            sql_query = "select min(j.gl_account) as acc_code,min(ca.description) as description,sum(j.debit_amount) as debit,sum(credit_amount) as credit,ca.account_type,act.text_display as descript from journal_item j left join chart_of_accounts ca on ca.id = j.gl_account left join account_types act on act.code = ca.account_type where j.post_date between '2016-1-1' and '" + cutoff_date + "' and ca.account_type in (" + accounts_selection + ") group by gl_account, ca.account_type, act.text_display"

            #print sql_query

            rp = self.db.bind.execute(sql_query)
            data = rp.fetchall()
            return data
        except:
            #print sql_query
            return None



    def get_one_account(self,name):
        try:
            data = self.db.chart_of_accounts.filter(self.db.chart_of_accounts.description == name).one()
            return data
        except:
            return None


    def get_contact_info(self,name):
        try:
            data = self.db.contacts.filter(self.db.contacts.short_name == name).one()
            return data
        except:
            return None

    def get_contact_info_byid(self,name):
        try:
            data = self.db.contacts.filter(self.db.contacts.id == name).one()
            return data
        except:
            return None



    def get_one_account_by_id(self,id):
        try:
            data = self.db.chart_of_accounts.filter(self.db.chart_of_accounts.id == id).one()
            return data
        except:
            return None


    def get_jelist(self,ref_id,_offset=0,_limit=3):
        try :
            word_limit = ""
            if _limit != 0:
                word_limit = "limit " + str(_offset) + ", " + str(_limit)

            rp = self.db.bind.execute("select m.id,m.ref_id,m.gl_account,m.sublgr_id,m.description,m.debit_amount,m.credit_amount,m.project_id, concat(m.gl_account,' - ' ,c.description) as gl_desc,ct.short_name as cost_center,concat(ab.primary_name,' (',m.sublgr_id,')') as subledger  from journal_item m left join chart_of_accounts c on m.gl_account = c.id left join contacts ct on m.project_id = ct.id left join contacts ct2 on ct2.id = m.sublgr_id left join address_book ab on ab.ref_id = ct2.id where m.ref_id = '" + str(ref_id) + "' order by m.id " + word_limit)

            data = rp.fetchall()
            return data
            #for a in data:
                #listing.append(a.__dict__)
            #return listing
        except:
            print(traceback.format_exc())
            return None


    def find_account(self,data):
        try:
            data = self.JournalItem.filter(self.JournalItem.user_id==data).one()
            return data
        except:
            return None
    def get_list(self):
        datas = self.JournalItem.all()
        listing = []
        for a in datas:
            listing.append(a.__dict__)

        return  listing
    def check_credential(self,uname,passw) :
        try:
            data = self.JournalItem.filter(self.JournalItem.username == uname,self.JournalItem.password==passw).one()
            return data
        except:
            return None

    def increment_count(self):
        self.counter = self.counter + 1

    def getCount(self):
        return self.counter


    def getAccountsWithSL(self):
        try:
            accounts_with_sl = self.config.getConfig('SL_ACCOUNTS')
            rp = self.db.bind.execute("select concat(id,' - ',description) as acct,id,description as descript  from chart_of_accounts where heading_only = '0' and id in (%s) " % accounts_with_sl)

            data = rp.fetchall()
            return data
        except:
            return None

    def getAgingAccounts(self):
        try:
            accounts_with_sl = self.config.getConfig('AGING_ACCOUNTS')
            rp = self.db.bind.execute("select concat(id,' - ',description) as acct,id,description as descript  from chart_of_accounts where heading_only = '0' and id in (%s) " % accounts_with_sl)

            data = rp.fetchall()
            return data
        except:
            return None

    def get_total_ledger(self,ref_id):
        try:
            rp = self.db.bind.execute('select j.gl_account,c.description as gl_account_desc,b.ref_id as mem_id,b.primary_name, sum(j.debit_amount) debit,sum(j.credit_amount) credit,sum(j.debit_amount-j.credit_amount) total from journal_item j left join chart_of_accounts c on c.id =  j.gl_account left join address_book b on b.ref_id = j.sublgr_id where j.sublgr_id = "%s" group by j.gl_account' % ref_id)
            data = rp.fetchall()
            return data
        except:
            return None

    def subledger_Entries(self,contact_id,account_id=None):
        try:
            if account_id:
                where_additional = " and j.gl_account = '%s'" % account_id
            else:
                where_additional = ""


            rp = self.db.bind.execute(("select jm.post_date,jm.purchase_invoice_id,j.gl_account as account_id, c.description as accounts,j.description,j.debit_amount,j.credit_amount from journal_item j left join journal_main jm  on jm.id = j.ref_id  left join chart_of_accounts c on j.gl_account = c.id where j.sublgr_id ='%s' " + where_additional) % contact_id + " order by jm.post_date")

            data = rp.fetchall()
            return data
        except:
            return None


    def ar_list(self,cutoff_date):
        try:
            sql = "select 1 as counter,j.gl_account,c.description as gl_account_desc,b.ref_id as mem_id,b.primary_name, sum(j.debit_amount) debit,sum(j.credit_amount) credit,sum(j.debit_amount-j.credit_amount) total from journal_item j left join chart_of_accounts c on c.id =  j.gl_account left join address_book b on b.ref_id = j.sublgr_id where not isnull(b.ref_id)   and (c.primary_acct_id in ('11700','11750','11780')) and j.post_date <= '{}' group by j.gl_account,b.ref_id, b.primary_name order by b.primary_name ".format(cutoff_date)

            print sql

            rp = self.db.bind.execute(sql)


            data = rp.fetchall()
            return data

        except:
            return None


    def ar_listing_per_acct(self,cutoff_date,account_num):
        try:
            if account_num in ['30101','21007']:
                sql = "select 1 as counter,j.gl_account,c.description as gl_account_desc,b.ref_id as mem_id,b.primary_name, sum(j.debit_amount) debit,sum(j.credit_amount) credit,sum(j.credit_amount-j.debit_amount) total from journal_item j left join chart_of_accounts c on c.id =  j.gl_account left join address_book b on b.ref_id = j.sublgr_id where not isnull(b.ref_id) and j.gl_account = '{0}'  and j.post_date <= '{1}' group by b.ref_id,j.gl_account order by primary_name".format(account_num,cutoff_date)
            else:
                sql = "select 1 as counter,j.gl_account,c.description as gl_account_desc,b.ref_id as mem_id,b.primary_name, sum(j.debit_amount) debit,sum(j.credit_amount) credit,sum(j.debit_amount-j.credit_amount) total from journal_item j left join chart_of_accounts c on c.id =  j.gl_account left join address_book b on b.ref_id = j.sublgr_id where not isnull(b.ref_id) and j.gl_account = '{0}'  and j.post_date <= '{1}' group by b.ref_id,j.gl_account order by primary_name".format(account_num,cutoff_date)

            print sql

            rp = self.db.bind.execute(sql)


            data = rp.fetchall()
            return data


        except:
            return None



    def get_ledger_listing_member(self,date1, date2,member_acctid,gl_accountid):
        try:

            rp = self.db.bind.execute("select j.gl_account,jm.id,j.post_date,jm.purchase_invoice_id,j.description,j.debit_amount,j.credit_amount from journal_item j left join journal_main jm on jm.id = j.ref_id where j.gl_account = '{0}' and  sublgr_id = {1} and j.post_date between '{2}' and '{3}' order by jm.id ".format(gl_accountid,member_acctid,date1,date2))


            data = rp.fetchall()
            return data

        except:
            traceback.print_exc()
            return None



    def get_beginning_balance_member(self,date1,member_acctid,gl_accountid):
        try:


            rp = self.db.bind.execute(" select j.gl_account,jm.id,'{2}' as post_date,'  ' as purchase_invoice_id,'BEGINNING BALANCE ' AS description,sum(j.debit_amount) debit_amount,sum(j.credit_amount) credit_amount from journal_item j left join journal_main jm on jm.id = j.ref_id where j.gl_account = '{0}' and  sublgr_id = {1} and j.post_date < '{2}'  order by jm.id ".format(gl_accountid,member_acctid,date1))


            data = rp.fetchall()
            return data

        except:
            traceback.print_exc()
            return None


    def get_ledger_listing(self,date1, date2,gl_accountid):
        try:
            sql = "select jm.id,j.post_date,jm.purchase_invoice_id,j.description,j.debit_amount,j.credit_amount,c.short_name as costctr from journal_item j left join journal_main jm on jm.id = j.ref_id left join contacts c on j.project_id = c.id  where j.gl_account = '{0}'  and j.post_date between '{1}' and '{2}' order by jm.post_date,jm.id ".format(gl_accountid,date1,date2)

            #print sql

            rp = self.db.bind.execute(sql)


            data = rp.fetchall()
            return data

        except:
            traceback.print_exc()
            return None




    def get_beginning_balance(self,date1,cost_center=None):
        try:
            thedate = datetime.datetime.strptime(date1,"%Y-%m-%d")
            first_of_month = self.mkFirstOfMonth(thedate)
            first_of_month_str = first_of_month.strftime("%Y-%m-%d")
            if not cost_center:
                sql_query = "SELECT min(gl_account) as account_id,min(ca.description) as account_name,sum(debit_amount) as debit ,sum(credit_amount) as credit,act.is_asset,ca.weight FROM journal_item left join chart_of_accounts ca on ca.id = journal_item.gl_account left join account_types act on act.code = ca.account_type  where   post_date < '{0}' group by gl_account, act.is_asset, ca.weight order by gl_account ".format(first_of_month_str)
                #print sql_query
                rp = self.db.bind.execute(sql_query)
                data = rp.fetchall()
                return data
            else:
                sql_query = "SELECT min(gl_account) as account_id,min(ca.description) as account_name,sum(debit_amount) as debit ,sum(credit_amount) as credit,act.is_asset,ca.weight FROM journal_item left join chart_of_accounts ca on ca.id = journal_item.gl_account left join account_types act on act.code = ca.account_type where   journal_item.post_date < '{0}' and journal_item.project_id = '{1}' group by gl_account, act.is_asset, ca.weight order by gl_account ".format(first_of_month_str,cost_center)
                rp = self.db.bind.execute(sql_query)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None



    def add_income_statement(self,period1,period2,account_type):
        if account_type == 30:
            ordering = "c.description desc,c.id"
        else:
            ordering = "c.description,c.id"


        sql = "select min(c.description) as descript,min(h.gl_account) as account_id,(sum(if(h.project_id = 648,h.debit_amount,0)) - sum(if(h.project_id = 648,h.credit_amount,0))) as admin,(sum(if(h.project_id = 651,h.debit_amount,0)) - sum(if(h.project_id = 651,h.credit_amount,0))) as trading,(sum(if(h.project_id = 650,h.debit_amount,0)) - sum(if(h.project_id = 650,h.credit_amount,0))) as canteen,(sum(if(h.project_id = 649,h.debit_amount,0)) - sum(if(h.project_id = 649,h.credit_amount,0))) as bakery,(sum(h.debit_amount) - sum(h.credit_amount)) as balance  from chart_of_accounts c inner join journal_item h on c.id = h.gl_account left join journal_main m on m.id = h.ref_id     where (m.post_date between '{0}' and '{1}') and c.account_type = {2} group by h.gl_account order by {3}".format(period1,period2,account_type,ordering)


        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None


    def add_income_statement2(self,cutoff_date,account_type):
        if account_type == 30:
            ordering = "c.description desc,c.id"
        else:
            ordering = "c.description,c.id"

        sql = "select min(c.description) as descript,min(h.gl_account) as account_id,(sum(if(h.project_id = 648,h.debit_amount,0)) - sum(if(h.project_id = 648,h.credit_amount,0))) as admin,(sum(if(h.project_id = 651,h.debit_amount,0)) - sum(if(h.project_id = 651,h.credit_amount,0))) as trading,(sum(if(h.project_id = 650,h.debit_amount,0)) - sum(if(h.project_id = 650,h.credit_amount,0))) as canteen,(sum(if(h.project_id = 649,h.debit_amount,0)) - sum(if(h.project_id = 649,h.credit_amount,0))) as bakery,(sum(h.debit_amount) - sum(h.credit_amount)) as balance  from chart_of_accounts c inner join journal_item h on c.id = h.gl_account left join journal_main m on m.id = h.ref_id     where m.post_date <= {0} and c.account_type = {1} group by h.gl_account order by {2}".format(cutoff_date,account_type,ordering)

        #print sql
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None

    def add_income_statement3(self,startdate,cutoff_date,account_type):
        if account_type == 30:
            ordering = "c.description desc,c.id"
        else:
            ordering = "c.description,c.id"

        sql = "select min(c.description) as descript,min(h.gl_account) as account_id,(sum(if(h.project_id = 648,h.debit_amount,0)) - sum(if(h.project_id = 648,h.credit_amount,0))) as admin,(sum(if(h.project_id = 651,h.debit_amount,0)) - sum(if(h.project_id = 651,h.credit_amount,0))) as trading,(sum(if(h.project_id = 650,h.debit_amount,0)) - sum(if(h.project_id = 650,h.credit_amount,0))) as canteen,(sum(if(h.project_id = 649,h.debit_amount,0)) - sum(if(h.project_id = 649,h.credit_amount,0))) as bakery,(sum(h.debit_amount) - sum(h.credit_amount)) as balance  from chart_of_accounts c inner join journal_item h on c.id = h.gl_account left join journal_main m on m.id = h.ref_id     where (m.post_date between '{0}' and '{1}' )and c.account_type = {2} group by h.gl_account order by {3}".format(startdate,cutoff_date,account_type,ordering)

        print sql
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None

    def get_monthly_sum(self,date1,cost_center=None):
        current_date = datetime.datetime.strptime(date1, "%Y-%m-%d")
        (first,last_ ) = get_month_day_range(current_date)
        last = current_date
        try:
            if not cost_center:
                #print "choice xxxxx"
                sql_query = "SELECT min(gl_account) as account_id,min(ca.description) as account_name,sum(debit_amount) as debit ,sum(credit_amount) as credit,act.is_asset,ca.weight FROM journal_item left join chart_of_accounts ca on ca.id = journal_item.gl_account left join account_types act on act.code = ca.account_type where   post_date between '{0}'  and '{1}' group by gl_account, act.is_asset, ca.weight order by gl_account ".format(first.strftime('%Y-%m-%d'),last.strftime('%Y-%m-%d'))
                #print sql_query
                rp = self.db.bind.execute(sql_query)
                data = rp.fetchall()
                return data
            else:
                sql_query = "SELECT min(gl_account) as account_id,min(ca.description) as account_name,sum(debit_amount) as debit ,sum(credit_amount) as credit,act.is_asset,ca.weight FROM journal_item left join chart_of_accounts ca on ca.id = journal_item.gl_account left join account_types act on act.code = ca.account_type where   post_date between '{0}'  and '{1}' and journal_item.project_id = '{2}' group by gl_account order by gl_account ".format(first.strftime('%Y-%m-%d'),last.strftime('%Y-%m-%d'),cost_center)
                #print sql_query
                rp = self.db.bind.execute(sql_query)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None

    def update_inventory_month(self,admin_entry_value,bakery_entry_value,canteen_entry_value,trading_entry_value):
            config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
            accounting_period = config.configuration_value
            sql = "DELETE FROM inventory_entry where accounting_period = '%s'" % accounting_period
            self.db.bind.execute(sql)
            self.db.commit()
            admin_record = self.db.contacts.filter(self.db.contacts.short_name == 'ADMIN').one()
            admin_id = 0
            if admin_record:
                admin_id = admin_record.id
            bakery_id = 0
            bakery_record = self.db.contacts.filter(self.db.contacts.short_name == 'BAKERY').one()
            if bakery_record:
                bakery_id = bakery_record.id

            canteen_id =0
            canteen_record = self.db.contacts.filter(self.db.contacts.short_name == 'CANTEEN').one()
            if canteen_record:
                canteen_id = canteen_record.id

            trading_id =0
            trading_record = self.db.contacts.filter(self.db.contacts.short_name == 'TRADING').one()
            if trading_record:
                trading_id = trading_record.id

            self.db.inventory_entry.insert(accounting_period=accounting_period,cost_center_id=admin_id,entry_value=admin_entry_value)
            self.db.commit()

            self.db.inventory_entry.insert(accounting_period=accounting_period,cost_center_id=bakery_id,entry_value=bakery_entry_value)
            self.db.commit()

            self.db.inventory_entry.insert(accounting_period=accounting_period,cost_center_id=canteen_id,entry_value=canteen_entry_value)
            self.db.commit()

            self.db.inventory_entry.insert(accounting_period=accounting_period,cost_center_id=trading_id,entry_value=trading_entry_value)

            self.db.commit()


    def getInventoryEntry(self):
            config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
            accounting_period = config.configuration_value
            admin_record = self.db.contacts.filter(self.db.contacts.short_name == 'ADMIN').one()
            bakery_record = self.db.contacts.filter(self.db.contacts.short_name == 'BAKERY').one()
            canteen_record = self.db.contacts.filter(self.db.contacts.short_name == 'CANTEEN').one()
            trading_record = self.db.contacts.filter(self.db.contacts.short_name == 'TRADING').one()

            try:
                admin_value = self.db.inventory_entry.filter(self.db.inventory_entry.cost_center_id == admin_record.id , self.db.inventory_entry.accounting_period == accounting_period).one()
            except:
                admin_value = None

            admin_val = 0
            if admin_value:
                admin_val = admin_value.entry_value

            try:
                bakery_value = self.db.inventory_entry.filter(self.db.inventory_entry.cost_center_id == bakery_record.id , self.db.inventory_entry.accounting_period == accounting_period).one()
            except:
                bakery_value = None

            bakery_val = 0
            if bakery_value:
                bakery_val = bakery_value.entry_value


            try:
                canteen_value = self.db.inventory_entry.filter(self.db.inventory_entry.cost_center_id == canteen_record.id , self.db.inventory_entry.accounting_period == accounting_period).one()
            except:
                canteen_value = None

            canteen_val = 0
            if canteen_value:
                canteen_val = canteen_value.entry_value

            try:
                trading_value = self.db.inventory_entry.filter(self.db.inventory_entry.cost_center_id == trading_record.id , self.db.inventory_entry.accounting_period == accounting_period).one()
            except:
                trading_value = None

            trading_val = 0
            if trading_value:
                trading_val = trading_value.entry_value

            return {'admin_value':admin_val,'bakery_value':bakery_val,'canteen_value':canteen_val,'trading_value':trading_val}

    def get_inventory_previousmonth(self):
        config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
        accounting_period = config.configuration_value
        sql = "select sum(if(cost_center_id = 648,entry_value,0)) as admin, sum(if(cost_center_id = 651,entry_value,0)) as trading,sum(if(cost_center_id = 650,entry_value,0)) as canteen,sum(if(cost_center_id = 649,entry_value,0)) as bakery from inventory_entry where accounting_period < {0} limit 1".format(accounting_period)
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data[0]
        except:
            traceback.print_exc()
            return None

    def get_inventory_current(self):
        config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
        accounting_period = config.configuration_value
        sql = "select sum(if(cost_center_id = 648,entry_value,0)) as admin, sum(if(cost_center_id = 651,entry_value,0)) as trading,sum(if(cost_center_id = 650,entry_value,0)) as canteen,sum(if(cost_center_id = 649,entry_value,0)) as bakery from inventory_entry where accounting_period = {0} limit 1".format(accounting_period)
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data[0]
        except:
            traceback.print_exc()
            return None

    def get_inventory_current2(self,cutoff_date):
        config = self.db.configuration.filter(self.db.configuration.configuration_key == 'CURRENT_ACCOUNTING_PERIOD').one()
        accounting_period = config.configuration_value
        sql = "select sum(if(cost_center_id = 648,entry_value,0)) as admin, sum(if(cost_center_id = 651,entry_value,0)) as trading,sum(if(cost_center_id = 650,entry_value,0)) as canteen,sum(if(cost_center_id = 649,entry_value,0)) as bakery from inventory_entry where accounting_period = {0} limit 1".format(accounting_period)
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data[0]
        except:
            traceback.print_exc()
            return None

    def get_journal_by_daterange(self,starting_date,ending_date):
        sql = "select jm.purchase_invoice_id as number,ji.description as particulars,ji.debit_amount as debit,ji.credit_amount as credit,ca.description as ac_title,ab.primary_name as sl_name from journal_item ji left join journal_main jm on jm.id = ji.ref_id left join chart_of_accounts ca on ca.id = ji.gl_account left join address_book ab on ji.sublgr_id = ab.ref_id   where ji.post_date between '{0}' and '{1}' and jm.jtype  = 0 order by trim(regexp_replace(jm.purchase_invoice_id,'^[a-zA-Z]+\-[0-9]+\-','')) ".format(starting_date,ending_date)
        #print sql
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None


    def get_cv_by_daterange(self,starting_date,ending_date):
        sql = "select jm.purchase_invoice_id as number,ji.description as particulars,ji.debit_amount as debit,ji.credit_amount as credit,ca.description as ac_title,ab.primary_name as sl_name from journal_item ji left join journal_main jm on jm.id = ji.ref_id left join chart_of_accounts ca on ca.id = ji.gl_account left join address_book ab on ji.sublgr_id = ab.ref_id   where ji.post_date between '{0}' and '{1}' and jm.jtype  = 1 order by ji.id,ji.post_date".format(starting_date,ending_date)
        #print sql
        try:
                rp = self.db.bind.execute(sql)
                data = rp.fetchall()
                return data
        except:
            traceback.print_exc()
            return None


    def count_accounts(self,where=None):
        sql = "select count(*) as counter from chart_of_accounts "
        try:
            if where:
                where_value = " (description like %s or id like %s)"
            else:
                where_value = " "

            if where:
                sql = "select count(*) as counter from chart_of_accounts where " + where_value
                rp = self.db.bind.execute(sql,' %' + where + '%','%' + where)
            else:
                rp = self.db.bind.execute(sql)
            data = rp.fetchone()

            if data:
                return data[0]
            else:
                return None
        except:
            traceback.print_exc()
            return None


    def get_range(self,m1,y1,m2=None,y2=None,i_type=None):
        if (m2 and y2):
            rr1 = self.db.inventory_entry.filter(extract('month',self.db.inventory_entry.date_posted) >= m1,extract('year',self.db.inventory_entry.date_posted) >= y1,extract('month',self.db.inventory_entry.date_posted) <= m2,extract('year',self.db.inventory_entry.date_posted) <= y2).all()
        else:
            if not i_type:
                rr1 = self.db.inventory_entry.filter(extract('month',self.db.inventory_entry.date_posted) == m1,extract('year',self.db.inventory_entry.date_posted) == y1).all()
            else:
                rr1 = self.db.inventory_entry.filter(extract('month',self.db.inventory_entry.date_posted) == m1,extract('year',self.db.inventory_entry.date_posted) == y1,self.db.inventory_entry.entry_type==i_type).all()
        return rr1

    def group_and_submit(self,m1,y1,m2=None,y2=None,i_type=None):
        res = self.get_range(m1,y1,m2,y2,i_type)
        grouping = {}
        if m2 and y2:
            for m in res:
                grouping[m.cost_center_id] = {}
                grouping[m.cost_center_id]['value'] = 0
                grouping[m.cost_center_id]['date'] = None
            for m in res:
                grouping[m.cost_center_id]['value'] = grouping[m.cost_center_id]['value'] + m.entry_value
                grouping[m.cost_center_id]['name'] = self.get_contact_info_byid(m.cost_center_id).short_name

        else:
            for m in res:
                grouping[m.cost_center_id] = {}
                grouping[m.cost_center_id]['value'] = 0
                grouping[m.cost_center_id]['date'] = datetime.datetime(y1,m1,1)

            for m in res:
                grouping[m.cost_center_id]['value'] = grouping[m.cost_center_id]['value'] + m.entry_value
                grouping[m.cost_center_id]['name'] = self.get_contact_info_byid(m.cost_center_id).short_name

        return grouping


    def fill_groupings(self,m1,y1,m2 = None,y2 = None,i_type=None):
        cc  = {}
        cts = Contacts()
        gs = cts.get_costcenters()
        for gg in gs:
            cc[gg.id] = {}
            cc[gg.id]['name']= gg.primary_name
            cc[gg.id]['value'] = 0
        rr = self.group_and_submit(m1,y1,m2,y2,i_type)
        for r in rr:
            cc[r]['value'] = rr[r]['value']

        return cc




if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyReportingMain.py file ***************"

'''
************** digits function for mysql here ********************

SET GLOBAL log_bin_trust_function_creators=1;
DROP FUNCTION IF EXISTS digits;
DELIMITER |
CREATE FUNCTION digits( str CHAR(32) ) RETURNS CHAR(32)
BEGIN
  DECLARE i, len SMALLINT DEFAULT 1;
  DECLARE ret CHAR(32) DEFAULT '';
  DECLARE c CHAR(1);
  IF str IS NULL
  THEN
    RETURN "";
  END IF;

  SET len = CHAR_LENGTH( str );
  REPEAT
    BEGIN
      SET c = MID( str, i, 1 );
      IF c BETWEEN '0' AND '9' THEN
        SET ret=CONCAT(ret,c);
      END IF;
      SET i = i + 1;
    END;
  UNTIL i > len END REPEAT;
  RETURN ret;
END |
DELIMITER ;
'''
