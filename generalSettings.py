import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *
from specialolv import *
from Configuration import *

        
class generalSettings(wxSpecialPanel):
    def __init__(self,parent_window):  
        self.parent_window = parent_window             
        self.book = parent_window.book
      
        super(generalSettings,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("generalSettings")
        self.parent_window = parent_window             
        self.book = parent_window.book

        self.Contacts = Contacts()
        self.Configuration = Configuration()

        self.setEscapeHandler(self.CloseThisPanel)   

        mainSizer = wx.BoxSizer(wx.VERTICAL)  

        sizerTop = wx.FlexGridSizer( 0, 2, 0, 0 )
        
        firstname_label = wx.StaticText(self, -1, "Company Name:", (20,20))
        self.company_name = EnterTextCtrl(self, -1, "", (100,20), (400,-1))
        lastname_label = wx.StaticText(self, -1, "Address 1:", (520,20))
        self.company_address1 = EnterTextCtrl(self, -1, "", (600,20), (150,-1))     
        
           
        company_address2 = wx.StaticText(self, -1, "Address 2: " )
        self.company_address2 = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     

        company_city = wx.StaticText(self, -1, "City/Town:" )
        self.company_city = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     
        company_zone = wx.StaticText(self, -1, "Province:" )
        self.company_zone = EnterTextCtrl(self, -1, "",(600,20),(300,-1))     
        company_zipcode = wx.StaticText(self, -1, "Zip Code:" )
        self.company_zipcode = EnterTextCtrl(self, -1, "",(600,20), (100,-1))     

        preparedby = wx.StaticText(self, -1, "Prepared by:" )
        self.preparedby = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     


        checkedby = wx.StaticText(self, -1, "Checked by:" )
        self.checkedby = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     


        approvedby = wx.StaticText(self, -1, "Approved by:" )
        self.approvedby = EnterTextCtrl(self, -1, "",(600,20), (300,-1))     
            
        sizerTop.Add(firstname_label, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.company_name, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(lastname_label, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.company_address1, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(company_address2, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.company_address2, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(company_city, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.company_city, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(company_zone, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.company_zone, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(company_zipcode, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.company_zipcode, -1, wx.ALL|wx.CENTER, 5)

        sizerTop.Add(preparedby, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.preparedby, -1, wx.ALL|wx.CENTER, 5)

        sizerTop.Add(checkedby, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.checkedby, -1, wx.ALL|wx.CENTER, 5)

        sizerTop.Add(approvedby, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.approvedby, -1, wx.ALL|wx.CENTER, 5)
      
        updateLabel = "Update (Ctr-S)"            
        
        self.btn_Update = wx.Button(self, -1, updateLabel)          
        self.btn_Cancel = wx.Button(self, -1, "Cancel (Esc)")          
        self.Bind(wx.EVT_BUTTON, self.updateGeneralSettings, self.btn_Update)
        self.Bind(wx.EVT_BUTTON, self.CloseNow, self.btn_Cancel)


        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer.Add(self.btn_Update, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btn_Cancel, 0, wx.ALL|wx.LEFT, 5)
        
        id_updateGeneralSettings = wx.ID_ANY
        self.Bind(wx.EVT_MENU,self.updateGeneralSettings,id=id_updateGeneralSettings)
        
        mainSizer.Add(sizerTop, 0, wx.TOP|wx.LEFT|wx.BOTTOM, 5)
        mainSizer.Add(btnsizer, 0, wx.LEFT|wx.BOTTOM, 5)
        self.SetSizer(mainSizer) 

        self.loadConfigurations()  
               
        self.company_name.SetFocus()
        self.company_name.SetSelection(-1,-1)
        

        
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('S'), id_updateGeneralSettings )])
        self.SetAcceleratorTable(accel_tbl)  
               
    def updateGeneralSettings(self,evt):  
        self.Configuration.updateConfig('COMPANY_NAME',self.company_name.GetValue())
        self.Configuration.updateConfig('COMPANY_ADDRESS1',self.company_address1.GetValue())
        self.Configuration.updateConfig('COMPANY_ADDRESS2',self.company_address2.GetValue())
        self.Configuration.updateConfig('COMPANY_CITY_TOWN',self.company_city.GetValue())
        self.Configuration.updateConfig('COMPANY_ZONE',self.company_zone.GetValue())
        self.Configuration.updateConfig('COMPANY_POSTAL_CODE',self.company_zipcode.GetValue())
        self.Configuration.updateConfig('PREPARED_BY',self.preparedby.GetValue())
        self.Configuration.updateConfig('CHECKED_BY',self.checkedby.GetValue())
        self.Configuration.updateConfig('APPROVED_BY',self.approvedby.GetValue())
        dlg = wx.MessageDialog(self.parent_window, "Finished saving...", "Saving Configuration Settings",style=wx.OK|wx.CENTRE)
        dlg.ShowModal()

    def loadConfigurations(self):
        self.company_name.SetValue(self.Configuration.getConfig('COMPANY_NAME'))
        self.company_address1.SetValue(self.Configuration.getConfig('COMPANY_ADDRESS1'))
        self.company_address2.SetValue(self.Configuration.getConfig('COMPANY_ADDRESS2'))
        self.company_city.SetValue(self.Configuration.getConfig('COMPANY_CITY_TOWN'))
        self.company_zone.SetValue(self.Configuration.getConfig('COMPANY_ZONE'))
        self.company_zipcode.SetValue(self.Configuration.getConfig('COMPANY_POSTAL_CODE'))
        self.preparedby.SetValue(self.Configuration.getConfig('PREPARED_BY'))
        self.checkedby.SetValue(self.Configuration.getConfig('CHECKED_BY'))
        self.approvedby.SetValue(self.Configuration.getConfig('APPROVED_BY'))
        

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("generalSettings",True)  
        

    def CloseNow(self,evt):
        self.CloseThisPanel()

    
    def return_panel(self):        
        return self

def CreategeneralSettingsPanel(the_parent_window):
   panel = generalSettings(the_parent_window)
   return panel
   
if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
