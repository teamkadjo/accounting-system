'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: EditTransaction.py
'''

import wx
import PromptingComboBox
from Journals import * 
from Contacts import *
from EnterTextCtrl import *

class EntryWindow(object):
    #----------------------------------------------------------------------
    def __init__(self, gl_account, sl_id, cost_center, description, debit,credit,rec_id):
        self.gl_account = gl_account
        self.sl_id = sl_id
        self.cost_center = cost_center
        self.description = description
        self.debit = debit
        self.credit = credit
        self.id = rec_id
    
    def SetCostCenter(self, value):
        if value is None or value == "":
            self.cost_center = None
        else:
            self.cost_center = value
    
    def SetGLAccount(self,value):
        if value is None or value == "":
            self.gl_account = None
        else:
            self.gl_account = value   


class EditTransactionDialog(wx.Dialog):
    def __init__(
            self, parent, ID, title,transaction_id=None, size=wx.DefaultSize, pos=wx.DefaultPosition, 
            style=wx.DEFAULT_DIALOG_STYLE,
            ):

        self.journals = Journals()
        self.transactionData = None
        transaction_data = None

        s_ledger = ""
        c_center = "ADMIN"
        debit_string = ""
        credit_string = ""
        description = ""
         
        
        if (transaction_id ):
            transaction_data = self.journals.get_je_transaction(transaction_id)


            if transaction_data.subledger:
                    s_ledger = transaction_data.subledger

            
            if transaction_data.cost_center:
                c_center = transaction_data.cost_center


            
            if transaction_data.debit_amount :
                debit_string =  str(transaction_data.debit_amount)

            
            if transaction_data.credit_amount:
                credit_string = str(transaction_data.credit_amount)
                
            if transaction_data.description:
                description = str(transaction_data.description)
        

        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        self.PostCreate(pre)
        self.SetSize((300, 300))

        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "Transaction Editor")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "GL Account:")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        #text = wx.TextCtrl(self, -1, "", size=(80,-1))
        #text.SetHelpText("Here's some help text for field #1")
        choices = [(str(x.id) + " " + x.description) for x in self.journals.db.chart_of_accounts.all()]
        descript = None
        if transaction_data:
            descript = transaction_data.gl_desc
        else:
            descript = ""
        choices = []    
        self.cbx = PromptingComboBox.PromptingComboBox(self,descript)
        
        for item in self.journals.db.chart_of_accounts.all():
            self.cbx.Append(str(item.id) + " - " + item.description, item)
        self.cbx.SetSpecialSelection()
        
        #self.Bind(wx.EVT_KEY_DOWN,self.OnKeyDown)
        box.Add(self.cbx, 1, wx.ALIGN_CENTRE|wx.ALL, 5)

        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)




        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "SubLedger :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        contacts = Contacts()
        self.contacts = contacts.get_all_members()
 

        self.member = PromptingComboBox.PromptingComboBox(self,s_ledger)
        for x in self.contacts:
            self.member.Append(x.name,x)
        self.member.SetSpecialSelection()
        
        box.Add(self.member, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Cost Center :")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        choices = [( x.short_name) for x in self.journals.db.contacts.filter(self.journals.db.contacts.type == 'j').all()]

        self.costcenter = PromptingComboBox.PromptingComboBox(self,c_center)
        for x in self.journals.db.contacts.filter(self.journals.db.contacts.type == 'j').all():
            self.costcenter.Append(x.short_name,x)
        self.costcenter.SetSpecialSelection()
            

        
        box.Add(self.costcenter, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Description :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        self.descript = EnterTextCtrl(self, -1, description, size=(100,50),style=wx.TE_MULTILINE)
        box.Add(self.descript, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        box = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(self, -1, "Debit :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)

        self.debit = EnterTextCtrl(self, -1, debit_string, size=(100,-1),style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
        box.Add(self.debit, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)

        label1 = wx.StaticText(self, -1, "Credit :")
        label.SetHelpText("This is the help text for the label")
        box.Add(label1, 0, wx.ALIGN_CENTRE|wx.LEFT, 5)
        

            
        self.credit = EnterTextCtrl(self, -1, credit_string , size=(100,-1),style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
        box.Add(self.credit, 1, wx.ALIGN_CENTRE|wx.LEFT, 5)
        
        
        
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        btnsizer = wx.StdDialogButtonSizer()
        
        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.AddButton(btn)
        
        btn = wx.Button(self, wx.ID_OK)
        btn.SetHelpText("The OK button completes the dialog")
        btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")
        btnsizer.AddButton(btn)
        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.CENTER|wx.ALL, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)

        self.Center()

    def getChanges(self):
        glID = 0
        itemObject = None
        if self.cbx.GetSelection():
            itemObject = self.cbx.GetClientData(self.cbx.GetSelection())
            glID = itemObject.id      
       
        
        subledger_id = 0

        if self.member.GetSelection() >= 0:
            subledgerObject =  self.member.GetClientData(self.member.GetSelection())
            subledger_id = subledgerObject.id

        costcenter_id = 0
        if self.costcenter.GetSelection() >= 0:
            costcenterObject =  self.costcenter.GetClientData(self.costcenter.GetSelection())
            costcenter_id = costcenterObject.id
        description = self.descript.GetValue()
        
        debit = self.debit
        credit = self.credit 
       
        self.transactionData = EntryWindow(glID, subledger_id, costcenter_id, description, debit,credit,0)
        return self.transactionData
        
class MyFrame(wx.Frame):
    #----------------------------------------------------------------------
    def __init__(self):
        #wx.Frame.__init__(self, parent=None, id=wx.ID_ANY, 
                          #title="ObjectListView Demo", size=(800,600))

        trans_id = 33
        dlg = EditTransactionDialog(None, -1, "Edit Transaction Dialog (Yey Test)" ,trans_id, size=(300, 300),
                         style=wx.DEFAULT_DIALOG_STYLE)
        #dlg.ShowWindowModal()       
        if dlg.ShowModal() == wx.ID_OK:
            transactdata = dlg.getChanges()



class MyApp(wx.App):
    def OnInit(self):
        frame = MyFrame()
        #frame.Show(True)
        #self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()



