'''+-----------------------------------------------------------------+
// |                   PyBooks Accounting System                     |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (topsoftdev.kapamilya.info/pybooks)   |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: Users.py
'''
import sys
from db import *
import sqlalchemy
import traceback
import hashlib
import random


class Users:
    def __init__(self):
        self.Contacts = DatabaseMySQL.contacts
        self.db = DatabaseMySQL
        self.counter = 0
        return None
        self.error = ''

    def error_message(self):
        return self.error

    def validate_user(self,username,passw):
        try:
            user = self.db.users.filter(self.db.users.admin_name == username).one()
            password = user.admin_pass
            splitted_encrypted = password.split(':')
            encrypted = splitted_encrypted[0]
            return  hashlib.md5(splitted_encrypted[1] + passw).hexdigest() == encrypted
        except:
            print traceback.print_exc()
            return False

    def get_userinfo(self,username):
        try:
            user = self.db.users.filter(self.db.users.admin_name == username).one()
            return user
        except:
            return None

    def updateUserInfo(self,data):
        try:
            ref_id_ = data['admin_id']
            password = self.pw_encrypt_password(data['admin_pass'])
            self.db.users.filter(self.db.users.admin_id==ref_id_).update({'admin_name':data['admin_name'],'display_name':data['display_name'],'admin_email':data['admin_email'],'admin_pass':password})
            self.db.commit()
            return True
        except:
            self.error = str(sys.exc_info()[1])
            print str(sys.exc_info()[1])
            return False

    def increment_count(self):
        self.counter = self.counter + 1


    def mt_rand (self,low = 0, high = sys.maxint):
        """Generate a better random value
        """
        return random.randint (low, high)

    def pw_encrypt_password(self,plain):
        password = ''
        for x in range(0, 9):
            password = password + str(self.mt_rand())
        salt = hashlib.md5(password).hexdigest()
        salt = salt[0:2]
        password = hashlib.md5(salt + plain).hexdigest() + ':' + salt
        return password

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyReportingMain.py file ***************"

