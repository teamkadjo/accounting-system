from podunk.project.report import Report
from podunk.widget.table import Table
from podunk.widget.heading import Heading
from podunk.prefab import alignment
from podunk.prefab.formats import format_ph_currency
from podunk.prefab.formats import format_two_decimals
from Journals import Journals
from reportlab.lib.colors import * 
import os
from Configuration import *
from numberword import *
import re
import time

class ReportSpecialHeader(Report):
    def __init__(self, pdf_file=None,receipt_info=None):
        super(ReportSpecialHeader,self).__init__(pdf_file)
        self._working_height = self._working_height - 35
        self.receipt_info = receipt_info
        
    def formatted(self,number):
        the_formatted = "({:,.2f})".format(math.fabs(number)) if number < 0 else "{:,.2f}".format(number)
        return the_formatted.strip()
        #return "{:,.2f}".format(number)
        
    def special_formatted(self,number):
        the_formatted = " {:,.2f}".format(abs(number))
        return the_formatted.strip()
        #return "{:,.2f}".format(number)
        
    def getx(self,number):
        if number >= 0:
            return 246-(len(self.formatted(number)) * 4.82)
        else:
            return 253-(len(self.formatted(number)) * 4.82)
    def _draw_header(self):
        config = Configuration()
        self.canvas.setLineWidth(.3)
        self.canvas.setFont('Helvetica', 9)
        companyName =  config.getConfig("COMPANY_NAME").upper()
        self.canvas.drawString(30,770,companyName)
        self.canvas.setFont('Helvetica', 9)

        complete_address = config.getConfig("COMPANY_ADDRESS1")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_ADDRESS2")
        complete_address = complete_address + ", " + config.getConfig("COMPANY_CITY_TOWN")
        self.canvas.drawString(40,760,complete_address)   
        
        self.canvas.setFont('Helvetica-Bold', 9)
        self.canvas.drawString(50,740,"MEMBER'S ACCOUNT INFORMATION")
        
    def _draw_footer(self):
        pass
        
    def setName(self,_name):
        self.name = _name     
    def setUsername(self,_name):
        self.printerName = _name        

class SpecialTable(Table):

    
    
    def setInvoiceNumber(self,number=None):
        self.number = number
    def setPostDate(self,date=None):
        self.post_date = date
        
    def setSLName(self,sl_name):
        self.sl_name = sl_name
    
    def setCheckValue(self,value):
        self.check_value = value
    
    def setBankName(self,value):
        self.bank_name = value
    
    def setCheckNumber(self,value):
        self.check_number = value
    
    def getInvoiceNumber(self):
        return self.number
    
    def getPostDate(self):
        return self.post_date
        
    def _draw_header(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_header(canvas,xoff,yoff)

        
    def _draw_footer(self, canvas, xoff, yoff):
        super(SpecialTable,self)._draw_footer( canvas, xoff, yoff)
        canvas.setFont('Helvetica', 9)
        config = Configuration()
        #canvas.line(24,yoff-5,586,yoff-5)
        #canvas.line(24,yoff-60,586,yoff-60)
        #canvas.line(24,yoff-20,24,yoff-60)
        #canvas.line(586,yoff-20,586,yoff-60)
        
        box_displacement = 90
        #canvas.rect(24,yoff-box_displacement,562,30,stroke=1, fill=0)        
        #canvas.line(163,yoff-5,163,yoff-box_displacement) 
        #canvas.line(24,yoff-5,24,yoff-box_displacement) 
        #canvas.line(586,yoff-5,586,yoff-box_displacement) 
        #canvas.line(302,yoff-(box_displacement - 30),302,yoff-box_displacement)
        #canvas.line(441,yoff-(box_displacement - 30),441,yoff-box_displacement)
        #canvas.line(24,yoff-(box_displacement - 30),441,yoff-box_displacement)
        
        
        #canvas.line(400,yoff-45,550,yoff-45)
        #canvas.drawString(460,yoff-55,"Payee")
       
        #preparedByText =  config.getConfig("PREPARED_BY")
        #checkedByText =  config.getConfig("CHECKED_BY")
        #approvedByText =  config.getConfig("APPROVED_BY")
       

        #if self.bank_name:
            #canvas.drawString(27,yoff-15,"DRAWEE :" + self.bank_name)
        #if self.check_number:
            #canvas.drawString(27,yoff-30,"NUMBER :" + self.check_number)
        
        #canvas.drawString(27,yoff-45,"DATE   : " + self.post_date)
        
        
        #canvas.drawString(166,yoff-15,"Received from  ADMIN of stated account amounting in PESOS ")
        
        #canvas.setFont('Helvetica-Bold', 10)
        #if (self.check_value):
            #numbervalue = get_number_as_words(self.check_value)
            #canvas.drawString(190,yoff-40,numbervalue.upper() + " ONLY")
        canvas.setFont('Helvetica', 10)
        #canvas.drawString(30,705,"Pay to : " + self.sl_name)
        
        

        
        #canvas.drawString(27,yoff-(box_displacement - 20),"Prepared By:")
        #canvas.drawString(166,yoff-(box_displacement - 20),"Checked By:")
        #canvas.drawString(305,yoff-(box_displacement - 20),"Approved By:")
        #canvas.drawString(444,yoff-(box_displacement - 20),"CV NO: " + self.getInvoiceNumber())
        ##canvas.drawString(444,yoff-50,"Date : " + self.getPostDate())

        #canvas.setFont('Helvetica-Bold', 8)
        #canvas.drawString(44,yoff-(box_displacement - 5),preparedByText)
        #canvas.drawString(183,yoff-(box_displacement - 5),checkedByText)
        #canvas.drawString(322,yoff-(box_displacement - 5),approvedByText)
        #canvas.setFont('Helvetica', 9)      


def OneFourthReport(ref_id_=50,pdf_name='test.pdf',title="Voucher Number",info=None):
    table = SpecialTable()

    j = Journals()
  
    config = Configuration()

    account_info = j.db.address_book.filter(j.db.address_book.ref_id==ref_id_).one()
    

    if account_info:
        name = account_info.primary_name

    veryNiceHeader = Heading(None)
    info = {'name':name}
    report = ReportSpecialHeader(pdf_name,info)
    report.subject = "RDEMPC Report Generated"       
    report.setName(name)
    report.title = title
    report.header.style.horizontal_alignment = alignment.CENTER
    report.add(veryNiceHeader)
    #report.add(table)
    #report.canvas.line(30,685,246,685)


    jinfo = {}
    for x in j.get_total_ledger(ref_id_):
        jinfo[x.gl_account] = x

    report.canvas.setFont('Helvetica', 9)        
    report.canvas.drawString(30,720,"Name :  ")
    report.canvas.setFont('Helvetica-Bold', 9)    
    report.canvas.drawString(62,720,  report.receipt_info["name"].upper())
    report.canvas.setFont('Helvetica', 9)  
    report.canvas.drawString(30,700,"Investment  " )

    investments = 0
    if jinfo.has_key('30101'):
        investments = jinfo['30101'].total

    report.canvas.drawString(report.getx(investments),700,report.special_formatted((investments)))
    #report.canvas.drawString(246-(len(investments) * 4.5),700,investments )
    
    savings = 0
    if jinfo.has_key('21007'):
        savings = jinfo['21007'].total
            
    report.canvas.drawString(30,690,"Savings  " )        
    #report.canvas.drawString(246-(len(savings)*4.5),690,savings )
    report.canvas.drawString(report.getx(savings),690,report.special_formatted(savings))    
    
    report.canvas.setFont('Helvetica', 9)
    #report.canvas.drawString(30,680,"Outstanding Accounts")

    #report.canvas.line(30,675,246,675)

    line = 665
    for r in jinfo:
        if (jinfo[r].gl_account != '30101' and jinfo[r].gl_account != '21007'):
            report.canvas.drawString(30,line,jinfo[r].gl_account_desc)
            total  = 0
            if jinfo[r].gl_account_desc.find("Receivable") > 0:
                total = jinfo[r].debit - jinfo[r].credit
            elif jinfo[r].gl_account_desc.find("Payable") > 0:
                total = jinfo[r].credit - jinfo[r].debit
            else:
                total = jinfo[r].debit - jinfo[r].credit
            
            report.canvas.drawString(report.getx(total),line,report.formatted(total))
            line = line - 15

    line = line + 10
    #report.canvas.line(30,line,246,line)
    #report.canvas.drawString(report.getx(19699),650,report.formatted(19699))

    line = line - 10
    #report.canvas.drawString(39,line,"Total Accounts")
    line = line - 7
    #report.canvas.line(30,line,246,line)

    line = line - 30
    report.canvas.line(30,line,246,line)
    line = line - 10
    report.canvas.drawString(39,line,"Prepared by:")    
    report.canvas.drawString(120,line,"Checked by:") 
    
    preparedby = ''       
    for u in config.getConfig("PREPARED_BY").split(' '):
        preparedby = preparedby + u[0]   

    
    checkedby = ''       
    for u in config.getConfig("CHECKED_BY").split(' '):
        checkedby = checkedby + u[0]   

    line = line - 30

    report.canvas.drawString(35,line,preparedby)    
    report.canvas.drawString(120,line,checkedby) 
    line = line - 7    
    report.canvas.line(30,line,246,line)
    line = line - 10    
    report.canvas.drawString(30,line,"For Credit Committee & Board of Directors:") 
    report.canvas.drawString(175,line+20,"Date :" + time.strftime("%m/%d/%Y"))


    line = line - 25 
    report.canvas.line(30,line,120,line)    
    report.canvas.line(160,line,250,line)  
    line = line - 25   
    report.canvas.line(30,line,120,line)    

    report.create()
    
            
if __name__ == '__main__':
    OneFourthReport(235,'onefourth.pdf','Member Account Information ')
    os.system('start onefourth.pdf')
