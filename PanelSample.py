
import wx


###########################################################################
## Class MyPanel1
###########################################################################

class TrialPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        
        choices = ['grandmother', 'grandfather', 'cousin', 'aunt', 'uncle', 'grandson', 'granddaughter']
        for relative in ['mother', 'father', 'sister', 'brother', 'daughter', 'son']:
            choices.extend(self.derivedRelatives(relative))

        cb = PromptingComboBox(self, "default value", choices, style=wx.CB_SORT) 

    def derivedRelatives(self, relative):
        return [relative, 'step' + relative, relative + '-in-law']


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame (None, -1, 'Demo PromptingComboBox Control', size=(400, 50))
    TrialPanel(frame)
    frame.Show()
    app.MainLoop()



class PanelSample ( wx.Panel ):
    
    def __init__( self,parent_book,style ):
        wx.Panel.__init__( self, parent_book, style )
        
        wx.StaticText(self, -1, caption, (20,20))
        wx.TextCtrl(parent_book, -1, "", (20,40), (150,-1))
        
        gSizer1 = wx.GridSizer( 0, 2, 0, 0 )
        
        self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        gSizer1.Add( self.m_staticText1, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        self.m_textCtrl1 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_textCtrl1, 0, wx.ALL, 5 )
        
        self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        gSizer1.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        self.m_textCtrl3 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_textCtrl3, 0, wx.ALL, 5 )
        
        self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        gSizer1.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        
        self.m_textCtrl2 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_textCtrl2, 0, wx.ALL, 5 )
        
        
        self.SetSizer( gSizer1 )
        self.Layout()
    def returnSelf(self):
        return self
    
    def __del__( self ):
        pass
    

