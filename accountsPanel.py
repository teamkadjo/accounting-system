#!/usr/bin/python
'''
// +-----------------------------------------------------------------+
// |                   PyBooks                                       |
// +-----------------------------------------------------------------+
// | Copyright(c) 2015 PyBooks (accounting.solutionsfrom.pro)        |
// +-----------------------------------------------------------------+
// | This program is free software: you can redistribute it and/or   |
// | modify it under the terms of the GNU General Public License as  |
// | published by the Free Software Foundation, either version 3 of  |
// | the License, or any later version.                              |
// |                                                                 |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of  |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   |
// | GNU General Public License for more details.                    |
// +-----------------------------------------------------------------+
//  source: accountsPanel.py
'''


import wx
import wx.dataview as dv
from ObjectListView import ObjectListView, ColumnDefn
from Contacts import *
from wxSpecialPanel import *
from EnterTextCtrl import *
from SpecialCombobox import *
from Journals import *
import PromptingComboBox

class accountsPanel(wxSpecialPanel):
    def __init__(self,parent_window,account_id=None):
        self.parent_window = parent_window
        self.book = parent_window.book
        self.account_id = account_id

        super(accountsPanel,self).__init__(self.book, style=wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)
        self.SetLabel("AccountInfo")
        self.Contacts = Contacts()
        self.chart_of_accounts = self.Contacts.db.chart_of_accounts

        self.setEscapeHandler(self.CloseThisPanel)

        mainSizer = wx.BoxSizer(wx.VERTICAL)

        sizerTop = wx.FlexGridSizer( 0, 2, 0, 0 )
        accounts_listings = ['00 - Cash','01 - Accounts Receivable','02 - Inventory','04  - Inventory','06 - Other Current Assets','08 - Fixed Assets','10 - Other Assets','20 - Accounts Payable','22 - Other Current Liabilities','24 - Long Term Liabilities','30 - Income','32 - Cost of Sales','34 - Expenses','40 - Equity - Doesnt Close','42 - Equity - Gets Closed','44 - Equity Retained Earnings']

        self.journals = Journals()
        self.chart_of_accounts1 = self.journals.get_chart_of_accounts()
        choices_ = [x.description for x in self.chart_of_accounts1]

        account_id1 = wx.StaticText(self, -1, "ID:", (20,20))
        self.account_id1 = EnterTextCtrl(self, -1, "", (200,20), (400,-1))

        account_name = wx.StaticText(self, -1, "Name:", (20,20))
        self.account_name = EnterTextCtrl(self, -1, "", (300,20), (400,-1))


        is_heading = wx.StaticText(self, -1, "is Header? :", (520,20))
        firstchoice = "No - (Non-header account)"
        self.the_choices = ["Yes - (This account is a heading and cannot accept posted values)","No - (Non-header account)"]
        self.is_heading = PromptingComboBox.PromptingComboBox(self,firstchoice,self.the_choices,style=wx.CB_READONLY |  wx.WANTS_CHARS)

        self.is_heading.process_text_now = self.process_Text_Choice


        account_type = wx.StaticText(self, -1, "Account Type:", (520,20))
        #self.account_type = EnterTextCtrl(self, -1, "", (600,20), (250,-1))
        self.account_type = SpecialCombobox(self,choices_,None,"",size=(500,-1))
        sub_account_of = wx.StaticText(self, -1, "Sub Account Of :", (520,20))
        #If this account is a sub-account, select primary account:
        self.sub_account_of = SpecialCombobox(self, accounts_listings, self.selectCallback2,accounts_listings[0],size=(600,20))

        sizerTop.Add(account_id1, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.account_id1, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(account_name, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.account_name, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(is_heading, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.is_heading, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(account_type, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.account_type, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(sub_account_of, -1, wx.ALL|wx.CENTER, 5)
        sizerTop.Add(self.sub_account_of, -1, wx.ALL|wx.CENTER, 5)


        if account_id:
            self.contact = self.chart_of_accounts.filter_by(id = account_id).one()
            self.account_name.SetValue(self.contact.description)
            self.account_id1.SetValue(self.contact.id)

        self.loadData(account_id)
        self.sub_account_of.Enabled = False

        if self.account_id :
            updateLabel = "Update (Ctr-S)"
        else:
            updateLabel = "Save (Ctr-S)"

        self.btn_Update = wx.Button(self, -1, updateLabel)
        self.btn_Cancel = wx.Button(self, -1, "Cancel (Esc)")
        self.Bind(wx.EVT_BUTTON, self.updateAccountInfo, self.btn_Update)
        self.Bind(wx.EVT_BUTTON, self.CloseNow, self.btn_Cancel)


        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer.Add(self.btn_Update, 0, wx.ALL|wx.LEFT, 5)
        btnsizer.Add(self.btn_Cancel, 0, wx.ALL|wx.LEFT, 5)

        id_updateAccountInfo = wx.ID_ANY
        self.Bind(wx.EVT_MENU,self.updateAccountInfo,id=id_updateAccountInfo)

        mainSizer.Add(sizerTop, 0, wx.TOP|wx.LEFT|wx.BOTTOM, 5)
        mainSizer.Add(btnsizer, 0, wx.LEFT|wx.BOTTOM, 5)
        self.SetSizer(mainSizer)


        self.account_id1.SetFocus()
        self.account_id1.SetSelection(-1,-1)

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('S'), id_updateAccountInfo )])
        self.SetAcceleratorTable(accel_tbl)

    def loadData(self,account_id):
        if account_id:
            self.contact = self.chart_of_accounts.filter_by(id = account_id).one()
            self.is_heading.SetValue(self.the_choices[self.contact.heading_only!='1'])
            the_value = self.is_heading.GetValue()
            if the_value.find("Yes") >= 0:
                self.sub_account_of.Enabled = True
            else:
                self.sub_account_of.Enabled = False
                the_primary_account = self.chart_of_accounts.filter(self.chart_of_accounts.id == self.contact.primary_acct_id)
                if the_primary_account.count() > 0 :
                    the_primary_account = the_primary_account.one()
                    self.account_type.SetValue(the_primary_account.description)
        else:
            self.sub_account_of.Enabled = True





    def selectCallback2(self, values):
        print "Select Callback called...: wat ap wat ap wat ap",  values


    def process_Text_Choice(self,evt):
        the_value = self.is_heading.GetValue()
        if the_value.find("Yes") >= 0:
            self.sub_account_of.Enabled = True
        else:
            self.sub_account_of.Enabled = False

    def updateAccountInfo(self,evt):
        if self.account_id:
            print "This is just updating an account"
            account_type_id = self.journals.get_one_account(self.account_type.GetValue())
            if account_type_id:
                self.journals.db.chart_of_accounts.filter_by(id=self.account_id).update({"description":self.account_name.GetValue(),'primary_acct_id':account_type_id.id})
            else:
                self.journals.db.chart_of_accounts.filter_by(id=self.account_id).update({"description":self.account_name.GetValue()})
            self.journals.db.commit()
            wx.MessageBox("Done updating account...", "Success")
            self.parent_window.refreshPage("ChartOfAccounts")
        else:
            print "This is saving new account"
            print "new id " + self.account_id1.GetValue()
            print "new account " + self.account_name.GetValue()
            print "is it heading {} " .format(self.is_heading.GetValue().find("Yes ") > -1)
            try:
                account_type_id = self.journals.get_one_account(self.account_type.GetValue())
                print "*********value {}".format(self.account_type.GetValue())

                print " Account type " + self.account_type.GetValue()
                if account_type_id is None:
                    self.journals.db.chart_of_accounts.insert(id=self.account_id1.GetValue(),description=self.account_name.GetValue(),primary_acct_id='')
                    self.journals.db.commit()
                    self.parent_window.refreshPage("ChartOfAccounts")
                    print "Blank account type id...."
                else:
                    self.journals.db.chart_of_accounts.insert(id=self.account_id1.GetValue(),description=self.account_name.GetValue(),account_type=account_type_id.id)
                    self.journals.db.commit()
                    self.parent_window.refreshPage("ChartOfAccounts")
                wx.MessageBox("Done saving account...", "Success")
           #MappedChart_of_accounts(id=u'70029',description=u'Exp., Salaries & Wages OT',heading_only=u'0',primary_acct_id=u'70000',account_type=34,account_inactive=u'0')]
            except:
                traceback.print_exc()
                self.journals.db.rollback()
                wx.MessageBox("Error in saving account name...", "Error")

    def CloseThisPanel(self):
        self.parent_window.checkIfAlreadyInTab("AccountInfo",True)


    def CloseNow(self,evt):
        self.CloseThisPanel()


    def return_panel(self):
        return self

def CreateAccountInfo(the_parent_window,account_id):
   panel = accountsPanel(the_parent_window,account_id)
   return panel

if __name__ == '__main__':
    print "************* WARNING : To be used only in conjunction with MyMainPayroll.py file ***************"
